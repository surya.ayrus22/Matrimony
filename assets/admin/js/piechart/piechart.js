var data = [{
    value: 80,
    color: "#8bd22f",
    highlight: "#00abff",
    label: "Users"
}, {
    value: 20,
    color: "#f95372",
    highlight: "#FFC870",
    label: "non Users"
}];
var successdata = [{
    value: 30,
    color: "#8bd22f",
    highlight: "#00abff",
    label: "SuccessUsers"
}, {
    value: 70,
    color: "#f95372",
    highlight: "#FFC870",
    label: "non SuccessUsers"
}];
var femaledata = [{
    value: 40,
    color: "#8bd22f",
    highlight: "#00abff",
    label: "FemaleUsers"
}, {
    value: 60,
    color: "#f95372",
    highlight: "#FFC870",
    label: "non FemaleUsers"
}];
var maledata = [{
    value: 50,
    color: "#8bd22f",
    highlight: "#00abff",
    label: "FemaleUsers"
}, {
    value: 50,
    color: "#f95372",
    highlight: "#FFC870",
    label: "non FemaleUsers"
}];
$(document).ready(function () {
    var ctx = document.getElementById("user").getContext("2d");
    var user = new Chart(ctx).Pie(data);
    $("#user ").click(function (evt) {
        var activePoints = user.getSegmentsAtEvent(evt);
        var activeLabel = activePoints[0].label;
        if (activeLabel == "Red") {
            while (user.segments.length > 0) {
                user.removeData()
            }
            for (i = 0; i < data2.length; i++) {
                user.addData(data2[i])
            }
        }

    });
   

    var successctx = document.getElementById("success").getContext("2d");
    var success = new Chart(successctx).Pie(successdata);
    $("#success ").click(function (evt) {
        var activePoints = success.getSegmentsAtEvent(evt);
        var activeLabel = activePoints[0].label;
        if (activeLabel == "Red") {
            while (success.segments.length > 0) {
                success.removeData()
            }
            for (i = 0; i < data2.length; i++) {
                success.addData(data2[i])
            }
        }

    });
    
    var famalectx = document.getElementById("famale").getContext("2d");
    var famalevar = new Chart(famalectx).Pie(femaledata);
    $("#famale ").click(function (evt) {
        var activePoints = famalevar.getSegmentsAtEvent(evt);
        var activeLabel = activePoints[0].label;
        if (activeLabel == "Red") {
            while (famalevar.segments.length > 0) {
                famalevar.removeData()
            }
            for (i = 0; i < data2.length; i++) {
                famalevar.addData(data2[i])
            }
        }

    });
    
      var malectx = document.getElementById("male").getContext("2d");
    var malevar = new Chart(malectx).Pie(maledata);
    $("#male").click(function (evt) {
        var activePoints = malevar.getSegmentsAtEvent(evt);
        var activeLabel = activePoints[0].label;
        if (activeLabel == "Red") {
            while (malevar.segments.length > 0) {
                malevar.removeData()
            }
            for (i = 0; i < data2.length; i++) {
                malevar.addData(data2[i])
            }
        }

    });
    
    });
