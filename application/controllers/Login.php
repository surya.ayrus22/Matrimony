<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class login extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper(array('url', 'language'));
        $this->load->helper('form');
        $this->load->model('User_model', '', TRUE);
        $this->load->model('Login_model', '', TRUE);
        $this->load->model('Usermanager_model', '', TRUE);
        $this->load->model('Payment_model', '', TRUE);
        $this->load->model('Mail', '', TRUE);
        $this->load->library('session');

        header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1.
        header("Pragma: no-cache"); // HTTP 1.0.
        header("Expires: 0"); // Proxies.
    }

    public function index() {
        if (isset($this->session->userdata['roleguid']) && $this->session->userdata['roleguid'] == MEMBER_ROLE_ID) {
            redirect(base_url() . 'index.php/matches/view');
        }

        $this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
        $this->output->set_header("Pragma: no-cache");

        $this->data['message'] = '';
        if ($this->input->post()) {
            $_REQUEST['username'] = strtolower($_REQUEST['username']);
            //	if($this->isValidLoginForm($_REQUEST)) {
            $formvalues = $this->input->post();
            $userdata = $this->Login_model->loginValidation($formvalues, MEMBER_ROLE_ID);

            if (!empty($userdata)) {
                /** check email count is one then redirect to crossponding user domain else invaild user */
                if (count($userdata) == 1) {/** User has one role */
                    /** checking account is activated  or not */
                    if ($userdata[0]['status'] == 1) {

                        /** Get user name  */
                        $userProfileName = ucfirst($userdata['0']['username']);

                        /** Set session value  */
                        $data['email'] = $userdata['0']['email'];
                        // 						$data['roleName']			= $userdata['0']['roleName'];
                        $data['userguid'] = $userdata['0']['userGuid'];
                        $data['gender'] = $userdata['0']['gender'];
                        $data['roleguid'] = $userdata['0']['userRoleGuid'];
                        $data['username'] = $userProfileName;

                        $img_info = $this->Usermanager_model->getProfileAndDefaultImage($userdata['0']['userGuid']);

                        $data['image'] = (!empty($img_info['image']) ? $img_info['image'] : DEFAULT_IMAGE);
                        $data['default'] = (!empty($img_info['default']) ? $img_info['default'] : DEFAULT_IMAGE);


                        $this->session->set_userdata($data);

                        /** Login activity  */
                        $loginActivity = $this->Login_model->loginActivityEntry($userdata['0']['userGuid'], 1, 'Cloud');

                        if (empty($userdata['0']['gender'])) {
                            redirect(base_url() . 'index.php/common/addgender');
                        }
                        /** Get redirection path by passing user roleguid  */
                        $loginPath = $this->Login_model->loginRedirection($userdata['0']['userRoleGuid']);

                        $offer = $this->Payment_model->getPaymentMembershipInfo($userdata['0']['userGuid']);

                        if (empty($offer)) {
                            $this->session->set_flashdata('offer', 1);
                        }
                        redirect($loginPath);
                    } else {
                        $this->data['message'] = $this->lang->line("text_please_activate_acc");
                    }
                } else {
                    $this->data['message'] = $this->lang->line("text_contact_admin");
                }
            } else {
                $this->data['message'] = $this->lang->line("text_invalid_uname_pwd");
            }
            /* }else{
              $this->data['message']	=	'Invalid Email Address';
              } */
        }
        $this->session->set_flashdata('message', $this->data['message']);
        redirect(base_url() . 'index.php/home');
    }

    public function isValidLoginForm($request) {
        /** Check email structure validation  */
        return preg_match('^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$^', $request['username']);
    }

    public function login_form() {
        $this->data['log'] = 1;
        $this->load->view('layout/header', $this->data);
        $this->load->view('login');
        $this->load->view('layout/footer');
    }

    public function fogotpassword() {
        if ($_REQUEST['email']) {
            $email = $_REQUEST['email'];
            $exists = $this->User_model->userDetails($email);

            if (!empty($exists[0]['status'])) {
                $data = array('resetpwd' => 1);
                $update = $this->User_model->updateUser($exists[0]['guid'], $data);

                $subject = $this->lang->line("mail_sub_reset_pwd");
                $body = $this->load->view('template/mail/forgetpassword.php', $exists[0], TRUE);
                $exists = $this->Mail->sendmail('', $email, $subject, $body, $attachment = '');

                $response = $this->lang->line("text_mail");
                ;
            } else {
                $response = $this->lang->line("text_email_not_exists");
            }
        } else {
            $response = $this->lang->line("text_please_enter_email");
        }
        echo $response;
        exit;
    }

    public function reset() {
        if (empty($_REQUEST['id'])) {
            redirect('home/index');
        }

        $id = $_REQUEST['id'];

        $exists = $this->User_model->userDetails('', $id);
        $this->data['message'] = $this->lang->line("text_contact_admin");
        if (!empty($exists[0]['status'])) {
            if (!empty($exists[0]['resetpwd'])) {
                if (!empty($_REQUEST['id']) && !empty($_REQUEST['pass'])) {
                    $data = array('resetpwd' => 0, 'password' => md5($_REQUEST['pass']), 'last_updated' => date(DATE_TIME_FORMAT), 'last_updated_by' => $id);
                    $update = $this->User_model->updateUser($id, $data);
                    if ($update) {
                        $this->data['user'] = $exists[0];
                        $this->data['message'] = $this->lang->line("text_success_pwd_reset");
                    }
                } else {
                    $this->data['userGuid'] = $id;
                    $this->load->view('reset', $this->data);
                    $this->load->view('layout/footer');
                }
            } else {
                $this->data['user'] = $exists[0];
                $this->data['message'] = $this->lang->line("text_already_pwd_reset");
            }
        }
        $this->load->view('home/success', $this->data);
    }

    public function activate() {
        $this->data['message'] = $this->lang->line("text_contact_admin");
        if (!empty($_REQUEST['id'])) {
            $id = $_REQUEST['id'];
            $exists = $this->User_model->userDetails('', $id);

            $this->data['activation'] = 1;
            if (!empty($exists[0])) {
                if (empty($exists[0]['status'])) {
                    $data = array('status' => '1', 'last_updated' => date(DATE_TIME_FORMAT), 'last_updated_by' => $id);
                    $update = $this->User_model->updateUser($id, $data);

                    if ($update) {
                        $this->data['user'] = $exists[0];
                        $this->data['message'] = $this->lang->line("text_account_activate");
                    }
                } else {
                    $this->data['user'] = $exists[0];
                    $this->data['message'] = $this->lang->line("text_already_account_activate");
                }
            }
        }
        $this->load->view('home/success', $this->data);
    }

}
