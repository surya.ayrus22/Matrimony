<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Admin_model', '', TRUE);
        $this->load->model('Login_model', '', TRUE);
        $this->load->model('User_model', '', TRUE);
        $this->load->model('Admin_manager', '', TRUE);
        $this->load->model('Profilemanager_model', '', TRUE);
        $this->load->model('Usermanager_model', '', TRUE);
        $this->load->model('Success_model', '', TRUE);
        $this->load->library('session');

        header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1.
        header("Pragma: no-cache"); // HTTP 1.0.
        header("Expires: 0"); // Proxies.
    }

    public function index() {  //echo print_r($this->input->post()); exit;	
        if (isset($this->session->userdata['admin_roleguid']) && !empty($this->session->userdata['admin_roleguid'])) {
            redirect(base_url() . 'index.php/admin/dashboard');
        }

        $this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
        $this->output->set_header("Pragma: no-cache");

        $this->data['message'] = '';
        if ($this->input->post()) {
            $_REQUEST['username'] = strtolower($_REQUEST['username']);
            $formvalues = $this->input->post();
            $userdata = $this->Login_model->loginValidation($formvalues, ADMIN_ROLE_ID);

            if (!empty($userdata)) {
                /** check email count is one then redirect to crossponding user domain else invaild user */
                if (count($userdata) == 1) {/** User has one role */
                    /** checking account is activated  or not */
                    if ($userdata[0]['userRoleGuid'] == ADMIN_ROLE_ID) {
                        $profile = $this->User_model->getUserImageInfo($userdata['0']['userGuid'], '', 1);

                        /** Get user name  */
                        $userProfileName = ucfirst($userdata['0']['username']);

                        /** Set session value  */
                        $data['admin_email'] = $userdata['0']['email'];
                        //$data['roleName']			= $userdata['0']['roleName'];
                        $data['admin_userguid'] = $userdata['0']['userGuid'];
                        $data['admin_roleguid'] = $userdata['0']['userRoleGuid'];
                        $data['admin_name'] = $userProfileName;
                        $data['admin_image'] = (!empty($profile[0]['image']) ? $profile[0]['image'] : '');

                        $this->session->set_userdata($data);
                        /** Login activity  */
                        $loginActivity = $this->Login_model->loginActivityEntry($userdata['0']['userGuid'], 1, 'Cloud');

                        redirect(base_url() . 'index.php/admin/dashboard');
                    } else {
                        $this->data['message'] = $this->lang->line("text_invalid_uname_pwd");
                    }
                } else {
                    $this->data['message'] = $this->lang->line("text_contact_admin");
                }
            } else {
                $this->data['message'] = $this->lang->line("text_invalid_uname_pwd");
            }
        }
        $this->load->view('admin/index', $this->data);
    }

    public function dashboard() {

        $this->data['message'] = $this->session->flashdata('message');

        if (!isset($this->session->userdata['admin_roleguid']) || empty($this->session->userdata['admin_roleguid'])) {
            redirect(base_url() . 'index.php/admin');
        }

        $recentData['month'] = 1;
        $recentData['limit'] = 5;
        $this->data['recentUpdate'] = $this->Admin_manager->getUserAndProfileDetails($recentData);
        //echo "<pre>";print_r($this->data);exit;
        $this->data['usercount'] = count($this->Profilemanager_model->getUserInformation());
        $this->data['successcount'] = count($this->Success_model->getUserSuccessInfo());
        $this->data['femalecount'] = count($this->Profilemanager_model->getUserInformation('', $gender = 'F'));
        $this->data['malecount'] = count($this->Profilemanager_model->getUserInformation('', $gender = 'M'));
        $monthcount = $this->Admin_model->getMonthWiseUserCount();

        $payment = $this->Payment_model->getPaymentMembershipInfo();
        if (!empty($payment)) {
            $userGuid = array();
            foreach ($payment as $val) {
                $userGuid[] = $val['user_id'];
            }
            $premiumUsercount = $this->Admin_model->getMonthWiseUserCount($userGuid);
            $this->data['userpayment'] = $this->Profilemanager_model->getUserInformation($userGuid, '', $limits = 5);
// 			echo '<pre>';print_r($this->data);exit;
        }
        $chartlist = '';
        $this->data['year'] = date('Y');
        
        if (!empty($monthcount[0]) && !empty($premiumUsercount[0])) {
            $premium = $premiumUsercount[0];
            $monthcount = $monthcount[0];
            foreach ($premium as $key => $value) {
                if ($key != 'YEAR') {
                    if (isset($monthcount[$key])) {
                        $free = $monthcount[$key] - $value;
                        $chartlist.= "['" . $key. "', " . $monthcount[$key] . "," . $free . "," . $value . "],";
                    }
                }
                if ($key == 'YEAR') {
                    $this->data['year'] = $value;
                }
            }
        } elseif (!empty($monthcount[0]) && empty($premiumUsercount[0])) {
            $monthcount = $monthcount[0];
            foreach ($monthcount as $key => $value) {
                if ($key != 'YEAR') {
                    $chartlist.= "['" . $key . "', " . $value . "," . $value . ",0],";
                }
                
                if ($key == 'YEAR') {
                    $this->data['year'] = $value;
                }
            }
        }

        $this->data['chart'] = $chartlist;

        $this->load->view('layout/admin/header');
        $this->load->view('admin/dashboard', $this->data);
        $this->load->view('layout/admin/footer', $this->data);
    }

    public function yeardashboard() {
        $this->load->view('layout/admin/header');
        $this->load->view('admin/yeardashboard');
        $this->load->view('layout/admin/footer');
    }

    public function userdetails() {

        if (empty($_REQUEST['type'])) {
            redirect(base_url() . 'index.php/admin/dashboard');
        }
        $data = $this->Admin_manager->getTypeBasedUserDetails($_REQUEST['type']);

        $this->load->view('layout/admin/header');
        $this->load->view('admin/profileview', $data);
        $this->load->view('layout/admin/footer');
    }

    public function successdetails() {
        if (empty($_REQUEST['type'])) {
            redirect(base_url() . 'index.php/admin/dashboard');
        }
        	$data	=	$this->Admin_manager->getTypeBasedUserDetails( $_REQUEST['type'] );
		$this->data['type']=$data['type'];
	if ( !empty( $data )) { 
			$i=0;
				foreach($data['successdata'] as $val){ 
					$this->data['success'][$i]=$val ; 
					 $this->data['success'][$i]['image']	= DEFAULT_IMAGE;
					if ( !empty( $val['photo'] )){
					if ( file_exists(dirname($_SERVER["SCRIPT_FILENAME"])."/assets/success_images/".$val['photo'])){
						$this->data['success'][$i]['image']	= $val['photo'];
					}
					}$i++;
				}
		}
        $this->load->view('layout/admin/header');
        $this->load->view('admin/success/success', $this->data);
        $this->load->view('layout/admin/footer');
    }

    public function useradd() {

        $data['message'] = $this->session->flashdata('message');

        $this->load->view('layout/admin/header');
        $this->load->view('admin/user/useradd', $data);
        $this->load->view('layout/admin/footer');
    }

    public function userRegistration() {
        if (!isset($_POST)) {
            redirect(base_url());
        }

        if (isset($_POST['step1']) && !empty($_POST['username'])) {
            $result = $this->Usermanager_model->userRegistrationStep1($_REQUEST);
            if (!empty($result['message']) && $result['message'] == 'Success') {
                $this->session->set_flashdata('message', $result['message']);
                redirect(base_url() . 'index.php/admin/userinfo?uid=' . $result['uid']);
            }
        } elseif (isset($_POST['step2']) && !empty($_POST['maritalstatus']) && !empty($_REQUEST['uid'])) {
            $result = $this->Usermanager_model->userRegistrationStep2($_REQUEST);
            if (!empty($result['message'])) {
                $this->session->set_flashdata('message', $result['message']);
                redirect(base_url() . 'index.php/admin/userphoto?uid=' . $result['uid']);
            }
        } elseif (isset($_POST['step3']) && !empty($_REQUEST['uid'])) {
            $result = $this->Usermanager_model->userRegistrationStep3($_REQUEST);
            $data = array('status'=>1);
            $userupdate = $this->User_model->updateUser($_REQUEST['uid'], $data);
            if (!empty($result['message'])) {
                $this->session->set_flashdata('message', $result['message']);
                redirect(base_url() . 'index.php/admin/UsersAddInfo');
            }
        }
    }

    public function userinfo() {

        $this->data['message'] = $this->session->flashdata('message');

        if (empty($_REQUEST['uid'])) {
            redirect('index.php/admin/dashboard');
        }

        $userExists = $this->User_model->getUserAndRoleDetails($_REQUEST['uid']);
        if (empty($userExists)) {
            redirect('index.php/admin/dashboard');
        }


        $country = array();
        $state = array();
        $city = array();
        $edu = array();
        $caste = array();
        $casteInfo = $this->Form_model->casteDetails();

        if (!empty($casteInfo)) {
            foreach ($casteInfo as $key => $val) {
                $caste[] = array('id' => $val['id'], 'text' => $val['name']);
            }
            $this->data['caste'] = json_encode($caste);
        }

        $countryInfo = $this->Form_model->countryDetails();

        if (!empty($countryInfo)) {
            foreach ($countryInfo as $key => $val) {
                $country[] = array('id' => $val['id'], 'text' => $val['name']);
            }
            $this->data['country'] = json_encode($country);
            $this->data['state'] = json_encode($state);
            $this->data['city'] = json_encode($city);
        }

        $eduInfo = $this->Form_model->educationInfo();

        if (!empty($eduInfo)) {
            foreach ($eduInfo as $k => $v) {
                $edu[] = array('id' => $v['id'], 'text' => $v['name']);
            }
            $this->data['education'] = json_encode($edu);
        }

        $this->load->view('layout/admin/header');
        $this->load->view('admin/user/userinfo', $this->data);
        $this->load->view('layout/admin/footer');
    }

    public function userphoto() {

        if (empty($_REQUEST['uid'])) {
            redirect('index.php/admin/dashboard');
        }

        $this->data['userGuid'] = $_REQUEST['uid'];


        $this->load->view('layout/admin/header');
        $this->load->view('admin/user/userphoto', $this->data);
        $this->load->view('layout/admin/footer');
    }

    public function userpartner() {


        if (empty($_REQUEST['uid'])) {
            redirect('index.php/admin/dashboard');
        }

        $userExists = $this->User_model->getUserAndRoleDetails($_REQUEST['uid']);
        if (empty($userExists)) {
            redirect('index.php/admin/dashboard');
        }

        $this->data['userGuid'] = $_REQUEST['uid'];
        $country = array();
        $state = array();
        $city = array();
        $edu = array();
        $caste = array();

        $casteInfo = $this->Form_model->casteDetails();

        if (!empty($casteInfo)) {
            foreach ($casteInfo as $key => $val) {
                $caste[] = array('id' => $val['id'], 'text' => $val['name']);
            }
            $this->data['caste'] = json_encode($caste);
        }

        $countryInfo = $this->Form_model->countryDetails();

        if (!empty($countryInfo)) {
            foreach ($countryInfo as $key => $val) {
                $country[] = array('id' => $val['id'], 'text' => $val['name']);
            }
            $this->data['country'] = json_encode($country);
            $this->data['state'] = json_encode($state);
            $this->data['city'] = json_encode($city);
        }

        $eduInfo = $this->Form_model->educationInfo();

        if (!empty($eduInfo)) {
            foreach ($eduInfo as $k => $v) {
                $edu[] = array('id' => $v['id'], 'text' => $v['name']);
            }
            $this->data['education'] = json_encode($edu);
        }

        $this->load->view('layout/admin/header');
        $this->load->view('admin/user/userpartnerinfo', $this->data);
        $this->load->view('layout/admin/footer');
    }

    public function SuccessMessage() {

        //echo $message;exit;

        $this->load->view('layout/admin/header');
        $this->load->view('admin/user/SuccessMessage');
        $this->load->view('layout/admin/footer');
    }

    public function sucessverify() {
        if (empty($_GET['mid']) || empty($_GET['type'])) {
            redirect('index.php/admin/dashboard');
        }
        $id = $_GET['mid'];
        $this->data['type'] = $_GET['type'];
        if (!empty($id)) {
            $successdata = $this->Success_model->getAllUserSuccessInfo('', $id);
            if ( !empty( $successdata )) { 
			$i=0;
				foreach($successdata as $val){ 
					$this->data['successdata'][$i]=$val ; 
					 $this->data['successdata'][$i]['image']	= DEFAULT_IMAGE;
					if ( !empty( $val['photo'] )){
					if ( file_exists(dirname($_SERVER["SCRIPT_FILENAME"])."/assets/success_images/".$val['photo'])){
						$this->data['successdata'][$i]['image']	= $val['photo'];
					}
					}$i++;
				}
			}
        }
        $this->load->view('layout/admin/header');
        $this->load->view('admin/success/successverify', $this->data);
        $this->load->view('layout/admin/footer');
    }

    public function successactive() {
        $sessionUserGuid = $this->session->userdata['admin_userguid'];
        $response = array('msg' => 'failure');
        if (empty($_REQUEST['mid'])) {
            echo json_encode($response);
            exit;
        }
        $id = $_REQUEST['mid'];
        $exists = $this->Success_model->getAllUserSuccessInfo('', $id);
        if (!empty($exists[0]['id'])) {
            if (isset($_REQUEST['active']) && (!empty($_REQUEST['active']) )) {
                if ($_REQUEST['active'] == 1) {
                    $data = array('deleted' => 0, 'last_updated' => date(DATE_TIME_FORMAT), 'last_updated_by' => $sessionUserGuid);
                    $result = $this->Success_model->updateSuccessInfo($id, $data);
                } elseif ($_REQUEST['active'] == 2) {
                    $up = array('deleted' => 1, 'last_updated' => date(DATE_TIME_FORMAT), 'last_updated_by' => $sessionUserGuid);
                    $result = $this->Success_model->updateSuccessInfo($id, $up);
                }
                $response = array('msg' => 'success');
            }
        }
        echo json_encode($response);
    }

    public function successedit() {
        if (empty($_GET['mid']) || empty($_GET['type'])) {
            redirect('index.php/admin/dashboard');
        }
        $this->data['type'] = $_GET['type'];
        $memberid = $_GET['mid'];

        $country = array();
        $state = array();
        $city = array();
        $countryInfo = $this->Form_model->countryDetails();
        if (!empty($countryInfo)) {
            foreach ($countryInfo as $key => $val) {
                $country[] = array('id' => $val['id'], 'text' => $val['name']);
            }
            $this->data['country'] = json_encode($country);
        }

        if (!empty($memberid)) {
            $successdata = $this->Success_model->getUserSuccessInfo('', '', $memberid);
            if (!empty($successdata)) {
                foreach ($successdata as $val) {
                    $this->data['successinfo'] = $val;
                    if (!empty($val['country'])) {
                        $countryDetails = $this->Form_model->countryDetails($val['country']);
                        if (!empty($countryDetails[0]['name'])) {
                            $this->data['successinfo']['country_name'] = $countryDetails[0]['name'];
                        }
                    }
                    if (!empty($val['state'])) {
                        $stateDetails = $this->Form_model->stateDetails('', $val['state']);
                        if (!empty($stateDetails[0]['name'])) {
                            $this->data['successinfo']['state_name'] = $stateDetails[0]['name'];
                        }
                    }
                    if (!empty($val['city'])) {
                        $cityDetails = $this->Form_model->cityDetails('', $val['city']);
                        if (!empty($cityDetails[0]['name'])) {
                            $this->data['successinfo']['city_name'] = $cityDetails[0]['name'];
                        }
                    }

                    $stateList = $this->Form_model->stateDetails($val['country']);
                    if (!empty($stateList[0])) {
                        foreach ($stateList as $key => $val1) {
                            $state[] = array('id' => $val1['id'], 'text' => $val1['name']);
                        }
                    }

                    $cityList = $this->Form_model->cityDetails($val['state']);
                    if (!empty($cityList[0])) {
                        foreach ($cityList as $key => $val2) {
                            $city[] = array('id' => $val2['id'], 'text' => $val2['name']);
                        }
                    }
                }
            }
        }
        $this->data['city'] = json_encode($city);
        $this->data['state'] = json_encode($state);

        $this->load->view('layout/admin/header');
        $this->load->view('admin/success/successedit', $this->data);
        $this->load->view('layout/admin/footer');
    }

    public function fileupload() {
        if (empty($_GET['uid'])) {
            echo json_encode(array('error' => 'request empty'));
            exit;
        }

        $userExists = $this->User_model->getUserAndRoleDetails($_GET['uid']);

        if (empty($userExists)) {
            echo json_encode(array('error' => 'invalid user'));
            exit;
        }

        $data = $this->Usermanager_model->fileupload($_GET['uid']);
        echo $data;
        exit;
    }

    public function removeprofileimage() {
        $userGuid = $this->session->userdata['admin_userguid'];
        $response = array('msg' => 'failure');
        $result = 0;
        if (isset($_REQUEST['id']) && isset($_REQUEST['pid']) && !empty($_REQUEST['id']) && !empty($_REQUEST['pid'])) {
            $exists = $this->User_model->getUserImageInfo($_REQUEST['pid'], '', '', $_REQUEST['id']);
            $flag = 0;
            if ($exists[0]['guid']) {
                $update = array('deleted' => 1, 'last_updated' => date(DATE_TIME_FORMAT), 'last_updated_by' => $userGuid);

                foreach ($exists as $info) {
                    $result = $this->User_model->updateImangeInfo($info['guid'], $update);
                    if (file_exists(dirname($_SERVER["SCRIPT_FILENAME"]) . "/assets/upload_images/" . $info['image'])) {
                        unlink(ASSETPATH . 'upload_images/' . $info['image']);
                    }
                    if (!empty($info['active'])) {
                        $flag = 1;
                    }
                }
            }

            if ($result) {
                $response = array('msg' => 'success');
                if ($flag) {
                    $response = array('msg' => 'Please set main profile picture');
                }
            }
        }
        echo json_encode($response);
    }

    public function activeprofileimage() {
        $userGuid = $this->session->userdata['admin_userguid'];
        $response = array('msg' => 'failure');
        $result = 0;
        if (isset($_REQUEST['id']) && isset($_REQUEST['pid']) && !empty($_REQUEST['id']) && !empty($_REQUEST['pid'])) {

            $exists = $this->User_model->getUserImageInfo($_REQUEST['pid'], '', '', $_REQUEST['id']);
            if ($exists[0]['guid']) {
                $update = array('active' => 0, 'last_updated' => date(DATE_TIME_FORMAT), 'last_updated_by' => $userGuid);
                $up = $this->User_model->updateImangeInfo('', $update, $_REQUEST['pid']);

                $update = array('active' => 1, 'last_updated' => date(DATE_TIME_FORMAT), 'last_updated_by' => $userGuid);
                $result = $this->User_model->updateImangeInfo($exists[0]['guid'], $update);
            }

            if ($result) {
                $response = array('msg' => 'success');
            }
        }
        echo json_encode($response);
        exit;
    }

    public function verifyprofileimage() {
        $userGuid = $this->session->userdata['admin_userguid'];
        $response = array('msg' => 'failure');
        $result = 0;
        if (isset($_REQUEST['id']) && isset($_REQUEST['pid']) && !empty($_REQUEST['id']) && !empty($_REQUEST['pid'])) {
            $exists = $this->User_model->getUserImageInfo($_REQUEST['pid'], '', '', $_REQUEST['id']);

            $flag = 0;
            if ($exists[0]['guid']) {
                $update = array('verify' => 1, 'last_updated' => date(DATE_TIME_FORMAT), 'last_updated_by' => $userGuid);
                $guidList = array();
                foreach ($exists as $info) {
                    $guidList[] = $info['guid'];
                }
                if (!empty($guidList)) {
                    $result = $this->User_model->updateImangeInfo($guidList, $update);
                }
            }

            if ($result) {
                $response = array('msg' => 'success');
            }
        }
        echo json_encode($response);
    }

    public function UsersAddInfo() {


        $data['message'] = $this->session->flashdata('message');

        $this->load->view('layout/admin/header');
        $this->load->view('admin/user/addUser', $data);
        $this->load->view('layout/admin/footer');
    }

    public function adminadd() {


        if (!empty($_POST)) {
            $result = $this->Usermanager_model->adminRegistration($_POST);
            if (!empty($result['message'])) {
                $this->session->set_flashdata('message', $result['message']);
                redirect(base_url() . 'index.php/admin/UsersAddInfo');
            }
        }

        $this->load->view('layout/admin/header');
        $this->load->view('admin/user/addadmin');
        $this->load->view('layout/admin/footer');
    }

    public function checkuseremailavailabilty() {

        if (isset($_REQUEST['email']) && !empty($_REQUEST['email'])) {
            $exists = $this->User_model->userDetails($_REQUEST['email']);
            $msg = 2;
            if (!empty($exists)) {
                $msg = 1;
            }
            echo $msg;
        }
    }

}
