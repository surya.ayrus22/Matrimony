<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_profile extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Admin_model', '', TRUE);
        $this->load->model('Login_model', '', TRUE);
        $this->load->model('Admin_manager', '', TRUE);
        $this->load->model('Profilemanager_model', '', TRUE);
        $this->load->model('User_model', '', TRUE);
        $this->load->model('Usermanager_model', '', TRUE);
        $this->load->library('session');
        if (!isset($this->session->userdata['admin_roleguid']) || empty($this->session->userdata['admin_roleguid'])) {
            redirect(base_url() . 'index.php/admin');
        }
    }

    public function index() {

        if (empty($_REQUEST['type'])) {
            redirect(base_url() . 'index.php/admin/dashboard');
        }
        $data = $this->Admin_manager->getTypeBasedUserDetails($_REQUEST['type']);

        $this->load->view('layout/admin/header');
        $this->load->view('admin/profile/profileapproved', $data);
        $this->load->view('layout/admin/footer');
    }

    public function viewUser() {
        if (empty($_GET['uid'])) {
            redirect(base_url() . 'index.php/admin/');
        }
        $userGuid = $_GET['uid'];
        $data = array();
        if (!empty($userGuid)) {
            $data['viewDetails'] = $this->Admin_manager->getUserAndProfileDetails('', $userGuid);
        }
        $data['userguid'] = $userGuid;
        $data['type'] = $_GET['type'];
        $this->load->view('layout/admin/header');
        $this->load->view('admin/profile/viewUser', $data);
        $this->load->view('layout/admin/footer');
    }

    public function editUser() {

        if (empty($_GET['uid'])) {
            redirect(base_url() . 'index.php/admin/');
        }
		$userGuid = $_GET['uid'];
        $completed = $this->Admin_manager->getUserAndProfileDetails('', $userGuid);
        if (empty($completed)) {
            $userInfo = $this->User_model->getUserAndRoleDetails($userGuid);
            $religionInfo = $this->User_model->getUserReligionInfo($userGuid);
            if (!empty($userInfo) && !empty($religionInfo)) {
                $userDetails = array_merge($userInfo[0], $religionInfo[0]);
                $data['editDetails'][$userGuid] = $this->Profilemanager_model->prefrenceEditData($userDetails);
            }
        } else {
            $data['editDetails'] = $completed;
        }

        $getUserImagesInfo = $this->User_model->getUserImageInfo($userGuid);

        $data['profile_images'] = (!empty($getUserImagesInfo) ? $getUserImagesInfo : '');

        $casteInfo = $this->Form_model->casteDetails();

        if (!empty($casteInfo)) {
            foreach ($casteInfo as $key => $val) {
                $caste[] = array('id' => $val['id'], 'text' => $val['name']);
            }
            $data['casteList'] = json_encode($caste);
        }

        $countryInfo = $this->Form_model->countryDetails();
        $country = array();
        if (!empty($countryInfo)) {
            foreach ($countryInfo as $key => $value) {
                $country[] = array('id' => $value['id'], 'text' => $value['name']);
            }
        }
        $data['countryList'] = json_encode($country);

        $stateInfo = $this->Admin_manager->getUserAndProfileDetails('', $userGuid);
        $city = array();
        $state = array();
        if (!empty($stateInfo)) {
            foreach ($stateInfo as $val) {
                $stateList = $this->Form_model->stateDetails($val['country']);
                if (!empty($stateList)) {


                    foreach ($stateList as $key => $value) {
                        $state[] = array('id' => $value['id'], 'text' => $value['name']);
                    }
                }

                $cityList = $this->Form_model->cityDetails($val['state']);
                if (!empty($cityList)) {


                    foreach ($cityList as $key => $value) {
                        $city[] = array('id' => $value['id'], 'text' => $value['name']);
                    }
                }
            }
        }
        $data['stateList'] = json_encode($state);
        $data['cityList'] = json_encode($city);

        $data['type'] = $_REQUEST['type'];
        $data['userGuid'] = $_REQUEST['uid'];
		$data['title'] = $this->Admin_model->getTitle( $_REQUEST['type'] ); 

        $this->load->view('layout/admin/header');
        $this->load->view('admin/profile/editUser', $data);
        $this->load->view('layout/admin/footer');
    }

    public function update() {

        //echo "<pre>";print_r($_POST);exit;

        if (empty($_POST)) {
            echo json_encode(array('msg' => $this->lang->line("request_empty")));
            exit;
        }

        $sessionUserGuid = $this->session->userdata['admin_userguid'];
        $userExits = '';
        $userGuid = '';

        $type = (!empty($_GET['type']) ? $_GET['type'] : '');
        if (!empty($_REQUEST['uid'])) {
            $userGuid = $_REQUEST['uid'];
            $userExits = $this->User_model->userDetails('', $userGuid);
        }

        if (!empty($userExits)) {
            $_REQUEST['sessionGuid'] = $sessionUserGuid;
            //echo "<pre>";print_r($_POST);exit;
            if (isset($_POST['step1']) && !empty($_POST['username'])) {  //echo "<pre>";print_r($_POST);exit;
                $result = $this->Usermanager_model->updateUserRegistrationStep1($_REQUEST);

                if (!empty($result['message'])) {
                    $response = array('msg' => $result['message']);
                }
            } elseif (isset($_POST['step2']) && !empty($_POST['maritalstatus'])) { //echo "<pre>";print_r($_POST);exit;
                $result = $this->Usermanager_model->userRegistrationStep2($_REQUEST);
                if (!empty($result['message'])) {
                    $response = array('msg' => $result['message']);
                }
            } elseif (isset($_POST['step3'])) {
                $result = $this->Usermanager_model->userRegistrationStep3($_REQUEST);
                if (!empty($result['message'])) {
                    $response = array('msg' => $result['message']);
                }
            }
        } else {
            $response = array('msg' => $this->lang->line("invalid_user_please"));
        }
        echo json_encode($response);
        exit;
    }

    public function setting() {

        if (isset($_GET['step1']) && !empty($_POST['email'])) {
            $email = $_POST['email'];
            $response = array('msg' => $this->lang->line('email_format_invalid'));
            if (preg_match('^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$^', $email)) {
                $exists = $this->User_model->userDetails($email);
                if (!empty($exists)) {
                    $response = array('msg' => $this->lang->line("email_already_exits_please_change_new_email_id"));
                } else {
                    $data = array('email' => $email);
                    $update = $this->User_model->updateUser($_POST['userGuid'], $data);
                    $response = array('msg' => $this->lang->line("success_msg"));
                }
            }

            echo json_encode($response);
            exit;
        } elseif (isset($_GET['step2'])) { //echo "<pre>";print_r($_POST);exit;
            $response = array('msg' => $this->lang->line('password_required'));
            if (!empty($_POST['new_password']) && !empty($_POST['conform_password'])) { //echo "<pre>";print_r($_POST);exit;
                //$option = array( 'email' =>  $_POST['email']);
                //echo "<pre>";print_r($option);exit;
                $userExists = $this->User_model->userDetails($_POST['email'], $_POST['userGuid']);
                //echo "<pre>";print_r($userExists);exit;
                $response = array('msg' => $this->lang->line('password_is_wrong'));
                if (!empty($userExists)) {
                    $updateData = array('password' => md5($_POST['new_password']), 'last_updated' => date(DATE_TIME_FORMAT), 'last_updated_by' => $_POST['userGuid']);
                    $update = $this->User_model->updateUser($_POST['userGuid'], $updateData);
                    $response = array('msg' => $this->lang->line("success_msg"));
                }
            }
            echo json_encode($response);
            exit;
        }
    }

    public function verifyImage() {
        $data = $this->Admin_manager->getTypeBasedUserDetails(TYPE_14);

        $this->load->view('layout/admin/header');
        $this->load->view('admin/profile/verifyimage', $data);
        $this->load->view('layout/admin/footer');
    }

    public function image() {
        if (empty($_REQUEST['uid'])) {
            redirect(base_url() . 'index.php/admin/dashboard');
        }
        $data = array();
        $userImages = $this->User_model->getUserImageInfo($_REQUEST['uid']);
        if (!empty($userImages)) {
            $i = 0;
            foreach ($userImages as $value) {
                if (empty($value['verify'])) {
                    $data['image'][$i] = $value;
                    $i++;
                }
            }
        }
        $this->load->view('layout/admin/header');
        $this->load->view('admin/profile/image', $data);
        $this->load->view('layout/admin/footer');
    }

	public function admineditprofile(){
		$sessionUserGuid = $this->session->userdata['admin_userguid'];
		if( !empty ( $sessionUserGuid ) ){
			$exitsAdmin	= $this->User_model->userDetails('',$sessionUserGuid);
			if( !empty ($exitsAdmin)){
				foreach( $exitsAdmin as $exit ){
					$data['updateAdmin'] = $this->User_model->getUserAndRoleDetails( $exit['guid'] );
				}
			} 
		}
		$this->load->view('layout/admin/header');
		$this->load->view('admin/profile/adminviewprofile.php',$data);
		$this->load->view('layout/admin/footer');
	}
	public function adminupdateprofile(){
		$sessionUserGuid = $this->session->userdata['admin_userguid'];
		
		if( empty ($_POST) ){
			redirect(base_url().'index.php/admin/dashboard');
		}
		
		if( !empty ($_REQUEST['uid']) && ( $sessionUserGuid == !empty($_POST['userguid'])) ){
			$this->db->trans_begin();
			$updateUserDetails = array ( 
									'username' 	=> (!empty($_POST['username'])?$_POST['username']:''),
									'email'		=> (!empty($_POST['email'])?$_POST['email']:''),
									'password'	=> md5(!empty($_POST['password'])?$_POST['password']:'')
								);
			$updateUser = $this->User_model->updateUser($sessionUserGuid,$updateUserDetails);				
			$updateProfileDetails = array ( 'mobile'	=> (!empty($_POST['mobile'])?$_POST['mobile']:'') );
			$updateProfile = $this->User_model->updateUserProfile($sessionUserGuid,$updateProfileDetails);							
			if( empty($updateUser || $updateProfile) ){
				$this->db->trans_rollback();
				$response = array( 'msg' => "Failed");
			} else {
				$this->db->trans_commit();
				$response = array( 'msg' => $this->lang->line("success_msg"));
			}
			echo json_encode($response);exit;;
		}
		
	}

}
