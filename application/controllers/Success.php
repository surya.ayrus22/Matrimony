<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Success extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper(array('url', 'language'));
        $this->load->helper('form');
        $this->load->model('Form_model', '', TRUE);
        $this->load->model('Success_model', '', TRUE);
        $this->load->model('Prefrence_model', '', TRUE);
        $this->load->model('Mail', '', TRUE);
        $this->load->model('Utils', '', TRUE);
        $this->load->library('session');
    }

    public function index() {
        $message = $this->session->flashdata('message');
        if (!empty($message)) {
            $this->data['message'] = $message;
        }

        $successInfo = $this->Success_model->getUserSuccessInfo();
       if ( !empty( $successInfo )) {
			$i=0;
				foreach($successInfo as $val){
					$this->data['success'][$i]=$val;
					 $this->data['success'][$i]['image']		= DEFAULT_IMAGE;
					if ( !empty( $val['photo'] )){
					if ( file_exists(dirname($_SERVER["SCRIPT_FILENAME"])."/assets/success_images/".$val['photo'])){
						$this->data['success'][$i]['image']	= $val['photo'];
					}
					}$i++;
				}
			//$this->data['success'] = $successInfo;
		}

        if (!empty($_REQUEST['tab'])) {
            $this->data['active'] = $_REQUEST['tab'];
        }

        $country = array();
        $state = array();
        $city = array();
        $countryInfo = $this->Form_model->countryDetails();

        if (!empty($countryInfo)) {
            foreach ($countryInfo as $key => $val) {
                $country[] = array('id' => $val['id'], 'text' => $val['name']);
            }
            $this->data['country'] = json_encode($country);
            $this->data['state'] = json_encode($state);
            $this->data['city'] = json_encode($city);
        }


        $this->load->view('layout/header');
        $this->load->view('success/index.php', $this->data);
        $this->load->view('layout/footer');
    }

    public function read() {
        $id = '';
        if (!empty($_REQUEST['ssid'])) {
            $id = $_REQUEST['ssid'];
        }
        $this->data = array();
        $success = array();
        $success1 = array();
        $successInfo = $this->Success_model->getUserSuccessInfo();
        if (!empty($successInfo)) {
            $i = 1;
            foreach ($successInfo as $key => $val) {
                if (!empty($val['id']) && $val['id'] === $id) {
                    $success[0] = $val;
                } else {
                    $success1[$i] = $val;
                    $i++;
                }
            }

            if (!empty($success) && !empty($success1)) {
                $this->data['success'] = array_merge($success, $success1);
            }
        }
        $this->load->view('layout/header');
        $this->load->view('success/successread.php', $this->data);
        $this->load->view('layout/footer');
    }

    public function addsuccess() {
        if (!empty($_REQUEST)) {
            $bridename = (!empty($_REQUEST['bridename']) ? $_REQUEST['bridename'] : '');
            $groomname = (!empty($_REQUEST['groomname']) ? $_REQUEST['groomname'] : '');
            $memberid = (!empty($_REQUEST['member_id']) ? $_REQUEST['member_id'] : '');
            $partnerid = (!empty($_REQUEST['partnerid']) ? $_REQUEST['partnerid'] : '');
            $email = (!empty($_REQUEST['email']) ? $_REQUEST['email'] : '');
            $address = (!empty($_REQUEST['address']) ? $_REQUEST['address'] : '');
            $country = (!empty($_REQUEST['country']) ? $_REQUEST['country'] : '');
            $state = (!empty($_REQUEST['state']) ? $_REQUEST['state'] : '');
            $city = (!empty($_REQUEST['city']) ? $_REQUEST['city'] : '');
            $mobile = (!empty($_REQUEST['mobile']) ? $_REQUEST['mobile'] : '');
            $successstory = (!empty($_REQUEST['successstory']) ? $_REQUEST['successstory'] : '');
            $marriagedate = '';
            if (!empty($_REQUEST['year']) && !empty($_REQUEST['month']) && !empty($_REQUEST['date'])) {
                $marriagedate = $_REQUEST['year'] . '-' . $_REQUEST['month'] . '-' . $_REQUEST['date'];
            }

            $created = date(DATE_TIME_FORMAT);
            $userGuid = (!empty($this->session->userdata['userguid']) ? $this->session->userdata['userguid'] : ADMIN_ROLE_ID);

            /** validate member and partner id is available or not(user table) */
            $prefrenceData['memberid'] = $memberid;
            $userExists = $this->Prefrence_model->getUserPrefrenceDetails($prefrenceData, 'M');

            $prefrenceData1['memberid'] = $partnerid;
            $userExists1 = $this->Prefrence_model->getUserPrefrenceDetails($prefrenceData1, 'F');
            /** validate member and partner id is available (user table)then go to insert */
            if (!empty($userExists) && !empty($userExists1)) {
                $exists = $this->Success_model->getAllUserSuccessInfo($memberid);
                $exists1 = $this->Success_model->getAllUserSuccessInfo('', '', $partnerid);
                /** validate member and partner id is not available (success_story table)then go to insert otherwise not inserted */
                if (empty($exists) && empty($exists1)) {
                    if (isset($_FILES['upfile']['error']) && $_FILES['upfile']['error'] == 0) {
                        $path = dirname($_SERVER["SCRIPT_FILENAME"]) . "/assets/success_images/";
                        $pathImage = dirname($_SERVER["SCRIPT_FILENAME"]) . "/assets/success_images/images/";

                        $config['upload_path'] = $pathImage;
                        $config['allowed_types'] = '*';
                        // 	        $config['max_size'] = '100';
                        // 	        $config['max_width']  = '1024';
                        // 	        $config['max_height']  = '768';
                        $config['file_name'] = 'M-' . $memberid . '_M-' . $partnerid . '_' . strtotime(date(DATE_TIME_FORMAT));
                        $this->load->library('upload', $config);

                        /** create folder and set permission */
                        $this->Utils->folderpermission($path);
                        $this->Utils->folderpermission($pathImage);

                        if (!$this->upload->do_upload('upfile')) {
                            $error = array('error' => $this->upload->display_errors());
                            $this->session->set_flashdata('message', $this->upload->display_errors());
                            redirect(base_url() . 'index.php/success');
                        } else {
                            $upload_data = $this->upload->data();
                            $filename = ImageJPEG(ImageCreateFromString(file_get_contents($upload_data['full_path'])), $path . $upload_data['file_name'], 30);
                        }
                    }

                    $insertData = array(
                        'bride_name' => $bridename,
                        'groom_name' => $groomname,
                        'groom_name' => $groomname,
                        'partner_id' => $partnerid,
                        'marriage_date' => $marriagedate,
                        'email' => $email,
                        'photo' => (!empty($upload_data['file_name']) ? $upload_data['file_name'] : ''),
                        'address' => $address,
                        'country' => $country,
                        'state' => $state,
                        'city' => $city,
                        'mobile' => $mobile,
                        'success_story' => $successstory,
                        'last_updated' => $created,
                        'last_updated_by' => $userGuid,
                        'deleted' => 1
                    );

                    if (!empty($exists[0]['id'])) {
                        $result = $this->Success_model->updateSuccessInfo($exists[0]['id'], $insertData);
                    } else {
                        $insertData['member_id'] = $memberid;
                        $insertData['created'] = $created;
                        $insertData['created_by'] = $userGuid;
                        $result = $this->Success_model->insertSuccessInfo($insertData);
                    }
                    $message = 'Failure to update please try after some time';
                    if ($result) {
                        if (file_exists($path . $upload_data['file_name'])) {
                            unlink($upload_data['full_path']);
                        }
                        $message = 'successfully added once verified it will appear';
                    }
                } else {
                    $message = "Story already exists please contact administrator";
                }
            } else {
                $message = "User doesn't exists please contact administrator";
            }

            $this->session->set_flashdata('message', $message);
            redirect(base_url() . 'index.php/success');
        }
    }

    public function validate() {
        $message = "Please enter memberid";
        if (empty($_REQUEST)) {
            echo json_encode($message);
            exit;
        }

        if (!empty($_REQUEST['memberid'])) {
            $prefrenceData['memberid'] = $_REQUEST['memberid'];
            $userExists = $this->Prefrence_model->getUserPrefrenceDetails($prefrenceData, 'M');
            $message = "User does't exists memberid - male";
            if (!empty($userExists)) {
                $exists = $this->Success_model->getAllUserSuccessInfo($_REQUEST['memberid']);
                $message = "success";
                if (!empty($exists)) {
                    $message = "Already exists memberid - male";
                }
            }
        }

        if (!empty($_REQUEST['partnerid'])) {
            $prefrenceData1['memberid'] = $_REQUEST['partnerid'];
            $userExists1 = $this->Prefrence_model->getUserPrefrenceDetails($prefrenceData1, 'F');
            $message = "User does't exists memberid - female";
            if (!empty($userExists1)) {
                $exists1 = $this->Success_model->getAllUserSuccessInfo('', '', $_REQUEST['partnerid']);
                $message = "success";
                if (!empty($exists1)) {
                    $message = "Already exists memberid - female";
                }
            }
        }

        echo json_encode($message);
        exit;
    }

    public function updatesuccess() {
        if (!empty($_REQUEST)) {
            $bridename = (!empty($_REQUEST['bridename']) ? $_REQUEST['bridename'] : '');
            $groomname = (!empty($_REQUEST['groomname']) ? $_REQUEST['groomname'] : '');
            $memberid = (!empty($_REQUEST['member_id']) ? $_REQUEST['member_id'] : '');
            $partnerid = (!empty($_REQUEST['partnerid']) ? $_REQUEST['partnerid'] : '');
            $email = (!empty($_REQUEST['email']) ? $_REQUEST['email'] : '');
            $address = (!empty($_REQUEST['address']) ? $_REQUEST['address'] : '');
            $country = (!empty($_REQUEST['country']) ? $_REQUEST['country'] : '');
            $state = (!empty($_REQUEST['state']) ? $_REQUEST['state'] : '');
            $city = (!empty($_REQUEST['city']) ? $_REQUEST['city'] : '');
            $mobile = (!empty($_REQUEST['mobile']) ? $_REQUEST['mobile'] : '');
            $successstory = (!empty($_REQUEST['successstory']) ? $_REQUEST['successstory'] : '');
            $successId = (!empty($_REQUEST['mid']) ? $_REQUEST['mid'] : '');
            $type = (!empty($_REQUEST['type']) ? $_REQUEST['type'] : TYPE_12);
            $marriagedate = '';
            if (!empty($_REQUEST['year']) && !empty($_REQUEST['month']) && !empty($_REQUEST['date'])) {
                $marriagedate = $_REQUEST['year'] . '-' . $_REQUEST['month'] . '-' . $_REQUEST['date'];
            }

            $created = date(DATE_TIME_FORMAT);
            $userGuid = (!empty($this->session->userdata['userguid']) ? $this->session->userdata['userguid'] : ADMIN_ROLE_ID);

            /** validate member and partner id is available or not(user table) */
// 			$prefrenceData['memberid'] = $memberid;
// 			$userExists = $this->Prefrence_model->getUserPrefrenceDetails($prefrenceData, 'M');
// 			$prefrenceData1['memberid'] = $partnerid;
// 			$userExists1 = $this->Prefrence_model->getUserPrefrenceDetails($prefrenceData1, 'F');

            /** validate member and partner id is available (user table)then go to insert */
// 			if ( !empty( $userExists ) && !empty( $userExists1 ) ) {
            /** validate member and partner id is available (success_story table)then already exists else update info */
// 				$exists		= $this->Success_model->getUserSuccessInfo( $memberid, $partnerid );
// 				echo '<pre>';print_r($exists);exit;
            /** validate member and partner id is not available (success_story table)then go to update otherwise not update */
            $exists = $this->Success_model->getUserSuccessInfo('', '', $successId);

            if (!empty($exists[0])) {
                if (isset($_FILES['upfile']['error']) && $_FILES['upfile']['error'] == 0) {
                    $config['upload_path'] = dirname($_SERVER["SCRIPT_FILENAME"]) . "/assets/success_images/images/";
                    $config['allowed_types'] = '*';
                    // 	        $config['max_size'] = '100';
                    // 	        $config['max_width']  = '1024';
                    // 	        $config['max_height']  = '768';
                    $config['file_name'] = 'M-' . $memberid . '_M-' . $partnerid . '_' . strtotime(date(DATE_TIME_FORMAT));
                    $this->load->library('upload', $config);

                    /** create folder and set permission */
                    $this->Utils->folderpermission($path);
                    $this->Utils->folderpermission($pathImage);

                    if (!$this->upload->do_upload('upfile')) {
                        $error = array('error' => $this->upload->display_errors());
                        $this->session->set_flashdata('message', $this->upload->display_errors());
                        redirect(base_url() . 'index.php/admin/successdetails');
                    } else {
                        $upload_data = $this->upload->data();
                        $filename = ImageJPEG(ImageCreateFromString(file_get_contents($upload_data['full_path'])), dirname($_SERVER["SCRIPT_FILENAME"]) . "/assets/success_images/" . $upload_data['file_name'], 30);
                        $image = (!empty($upload_data['file_name']) ? $upload_data['file_name'] : '');
                    }
                } else {
                    $image = (!empty($exists[0]['photo']) ? $exists[0]['photo'] : '');
                }

                $updateData = array(
                    'bride_name' => $bridename,
                    'groom_name' => $groomname,
                    'groom_name' => $groomname,
                    'partner_id' => $partnerid,
                    'marriage_date' => $marriagedate,
                    'email' => $email,
                    'photo' => $image,
                    'address' => $address,
                    'country' => $country,
                    'state' => $state,
                    'city' => $city,
                    'mobile' => $mobile,
                    'success_story' => $successstory,
                    'last_updated' => $created,
                    'last_updated_by' => $userGuid,
                );

                $result = $this->Success_model->updateSuccessInfo($successId, $updateData);
                $message = 'Failure to update please try after some time';
                if ($result) {
                    if (file_exists(dirname($_SERVER["SCRIPT_FILENAME"]) . "/assets/success_images/" . $upload_data['file_name'])) {
                        unlink($upload_data['full_path']);
                    }
                    $message = 'successfully added once verified it will appear';
                }
            } else {
                $message = "User records doesn't exists please contact administrator";
            }
            /* }else{
              $message = "User doesn't exists please contact administrator";
              } */
            //echo $this->db->last_query();exit;	
            $this->session->set_flashdata('message', $message);

            $url = base_url() . 'index.php/admin/successdetails?type=' . $type;
            if (!empty($successId)) {
                $url = base_url() . 'index.php/admin/sucessverify?mid=' . $successId . '&type=' . $type;
            }
            redirect($url);
        }
    }

}
