<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Signout extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper(array('url', 'language'));
        $this->load->model('User_model', '', TRUE);
        $this->load->model('Login_model', '', TRUE);
        $this->load->library('session');
        if (!isset($this->session->userdata['admin_userguid']) || empty($this->session->userdata['admin_userguid'])) {
            redirect(base_url() . 'index.php/admin');
        }
    }

    function index() {
        $this->session->unset_userdata('admin_email');
        $this->session->unset_userdata('admin_roleName');
        $this->session->unset_userdata('admin_userguid');
        $this->session->unset_userdata('admin_roleguid');
        $this->session->unset_userdata('admin_name');
        $this->session->unset_userdata('admin_image');
//  	$this->session->sess_destroy();
        redirect(base_url() . 'index.php/admin');
    }

}

?>