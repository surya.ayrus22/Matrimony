<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Footer extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Form_model', '', TRUE);
        $this->load->library('session');
    }

    public function feedback() {
        $message = $this->session->flashdata('message');

        if ($message) {
            $this->data['message'] = $message;
        }

        $countryInfo = $this->Form_model->countryDetails();

        if (!empty($countryInfo)) {
            foreach ($countryInfo as $key => $val) {
                $country[] = array('id' => $val['id'], 'text' => $val['name']);
            }
            $this->data['country'] = json_encode($country);
        }

        $this->load->view('layout/header');
        $this->load->view('footer/feedback', $this->data);
        $this->load->view('layout/footer');
    }

    public function privacy() {
        $this->load->view('layout/header');
        $this->load->view('footer/privacy');
        $this->load->view('layout/footer');
    }

    public function service() {
        $this->load->view('layout/header');
        $this->load->view('footer/service');
        $this->load->view('layout/footer');
    }

    public function term() {
        $this->load->view('layout/header');
        $this->load->view('footer/term');
        $this->load->view('layout/footer');
    }

}
