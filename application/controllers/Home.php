<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper(array('url', 'language'));
        $this->load->helper('form');
        $this->load->model('User_model', '', TRUE);
        $this->load->model('Usermanager_model', '', TRUE);
        $this->load->model('Form_model', '', TRUE);
        $this->load->model('Success_model', '', TRUE);
        $this->load->model('Utils', '', TRUE);
        $this->load->model('Mail', '', TRUE);
        $this->load->library('session');
        $this->lang->load("message", "english");
        if (isset($this->session->userdata['roleguid']) && $this->session->userdata['roleguid'] == MEMBER_ROLE_ID) {
            redirect(base_url() . 'index.php/matches/view/');
        }
    }

    public function index() {
        $message = $this->session->flashdata('message');
        $message1 = $this->session->flashdata('message1');

        if ($message) {
            $this->data['message'] = $message;
        }

        if ($message1) {
            $this->data['message1'] = $message1;
        }

        $casteInfo = $this->Form_model->casteDetails();
        $caste[0] = '---- select caste----';
        if (!empty($casteInfo)) {
            foreach ($casteInfo as $key => $val) {
                $caste[$val['id']] = $val['name'];
            }
            $this->data['caste'] = $caste;
        }

        $successInfo = $this->Success_model->getUserSuccessInfo();

        if ( !empty( $successInfo )) { 
                   $i=0;
				foreach($successInfo as $val){
					$this->data['success'][$i]=$val;
					 $this->data['success'][$i]['image']		= DEFAULT_IMAGE;
					if ( !empty( $val['photo'] )){
					if ( file_exists(dirname($_SERVER["SCRIPT_FILENAME"])."/assets/success_images/".$val['photo'])){
						$this->data['success'][$i]['image']	= $val['photo'];
						}
					}$i++;
				}
                 //   $this->data['success'] = $successInfo;
            }
        //echo "<pre>";print_r($this->data);exit;
        $this->data['log'] = 0;
        $this->load->view('layout/header', $this->data);
        $this->load->view('home/index', $this->data);
        $this->load->view('layout/footer');
    }

    public function register() {
        if (empty($_REQUEST['uid'])) {
            redirect(base_url() . 'index.php/home/');
        }

        $userExists = $this->User_model->getUserAndRoleDetails($_REQUEST['uid']);
        if (empty($userExists)) {
            redirect('home/index');
        }

        $this->data['userGuid'] = $_REQUEST['uid'];
        $country = array();
        $state = array();
        $city = array();
        $edu = array();
        $caste = array();
        $casteInfo = $this->Form_model->casteDetails();

        if (!empty($casteInfo)) {
            foreach ($casteInfo as $key => $val) {
                $caste[] = array('id' => $val['id'], 'text' => $val['name']);
            }
            $this->data['caste'] = json_encode($caste);
        }

        $countryInfo = $this->Form_model->countryDetails();

        if (!empty($countryInfo)) {
            foreach ($countryInfo as $key => $val) {
                $country[] = array('id' => $val['id'], 'text' => $val['name']);
            }
            $this->data['country'] = json_encode($country);
            $this->data['state'] = json_encode($state);
            $this->data['city'] = json_encode($city);
        }

        $eduInfo = $this->Form_model->educationInfo();

        if (!empty($eduInfo)) {
            foreach ($eduInfo as $k => $v) {
                $edu[] = array('id' => $v['id'], 'text' => $v['name']);
            }
            $this->data['education'] = json_encode($edu);
        }


        $this->load->view('home/register_view', $this->data);
        $this->load->view('layout/footer');
    }

    public function profile() {
        $this->data['log'] = 2;
        $this->load->view('layout/header', $this->data);
        $this->load->view('profile_edit');
        $this->load->view('layout/footer');
    }

    public function registration() {
        if (!isset($_POST)) {
            redirect(base_url());
        }

        if (isset($_POST['step1']) && !empty($_POST['username'])) {
            $result = $this->Usermanager_model->userRegistrationStep1($_REQUEST);
            if (!empty($result['message']) && !empty($result['url'])) {
                $this->session->set_flashdata('message', $result['message']);
                redirect($result['url']);
            }
        } elseif (isset($_POST['step2']) && !empty($_POST['maritalstatus']) && !empty($_REQUEST['uid'])) {
            $result = $this->Usermanager_model->userRegistrationStep2($_REQUEST);
            if (!empty($result['message']) && !empty($result['url'])) {
                $this->session->set_flashdata('message', $result['message']);
                redirect($result['url']);
            }
        } elseif (isset($_POST['step3']) && !empty($_REQUEST['uid'])) {
            $result = $this->Usermanager_model->userRegistrationStep3($_REQUEST);
            if (!empty($result['message']) && !empty($result['url'])) {
                $this->session->set_flashdata('message', $result['message']);
                redirect($result['url']);
            }
        } else {
            redirect(base_url() . 'index.php/home/index');
        }
    }

    public function checkuseremailavailabilty() {

        if (isset($_REQUEST['email']) && !empty($_REQUEST['email'])) {
            $exists = $this->User_model->userDetails($_REQUEST['email']);
            $msg = 2;
            if (!empty($exists)) {
                $msg = 1;
            }
            echo $msg;
        }
    }

    public function photo() {
        if (empty($_REQUEST['uid'])) {
            redirect(base_url() . 'index.php/home');
        }
        $this->data['userGuid'] = $_REQUEST['uid'];
        $this->load->view('home/photo_view', $this->data);
        $this->load->view('layout/footer');
    }

    public function fileupload() {
        if (empty($_GET['uid'])) {
            echo json_encode(array('error' => $this->lang->line("request_empty")));
            exit;
        }

        $userExists = $this->User_model->getUserAndRoleDetails($_GET['uid']);

        if (empty($userExists)) {
            echo json_encode(array('error' => $this->lang->line("invalid_user_please")));
            exit;
        }

        $userGuid = $_GET['uid'];
        $data = $this->Usermanager_model->fileupload($userGuid);
        echo $data;
        exit;
    }

    public function partnerinfo() {
        if (empty($_REQUEST['uid'])) {
            redirect(base_url() . 'index.php/home');
        }

        $userExists = $this->User_model->getUserAndRoleDetails($_REQUEST['uid']);
        if (empty($userExists)) {
            redirect(base_url() . 'index.php/home');
        }

        $this->data['userGuid'] = $_REQUEST['uid'];
        $country = array();
        $state = array();
        $city = array();
        $edu = array();
        $caste = array();

        $casteInfo = $this->Form_model->casteDetails();

        if (!empty($casteInfo)) {
            foreach ($casteInfo as $key => $val) {
                $caste[] = array('id' => $val['id'], 'text' => $val['name']);
            }
            $this->data['caste'] = json_encode($caste);
        }

        $countryInfo = $this->Form_model->countryDetails();

        if (!empty($countryInfo)) {
            foreach ($countryInfo as $key => $val) {
                $country[] = array('id' => $val['id'], 'text' => $val['name']);
            }
            $this->data['country'] = json_encode($country);
            $this->data['state'] = json_encode($state);
            $this->data['city'] = json_encode($city);
        }

        $eduInfo = $this->Form_model->educationInfo();

        if (!empty($eduInfo)) {
            foreach ($eduInfo as $k => $v) {
                $edu[] = array('id' => $v['id'], 'text' => $v['name']);
            }
            $this->data['education'] = json_encode($edu);
        }
        $this->load->view('home/partner_preference', $this->data);
        $this->load->view('layout/footer');
    }

    function success() {
        if (empty($_REQUEST['uid'])) {
            redirect(base_url() . 'index.php/home');
        }

        $userExists = $this->User_model->userDetails('', $_REQUEST['uid']);

        if (empty($userExists[0])) {
            redirect('home/index');
        }
        if (!empty($userExists[0]['email'])) {
            $subject = 'Verify Your Account';
            $body = $this->load->view('template/mail/account_acctivation.php', $userExists[0], TRUE);
            if (MAIL_ENABLE) {
                $exists = $this->Mail->sendmail('', $userExists[0]['email'], $subject, $body, $attachment = '');
            }
        }

        $this->data['message'] = $this->lang->line("text_contact_admin");
        $this->data['register'] = 1;
        if (!empty($exists)) {
            $message = $this->session->flashdata('message');
            if ($message) {
                $this->data['message'] = $message;
            }
        }
        $this->data['user'] = $userExists[0];
        $this->load->view('home/success', $this->data);
    }

}
