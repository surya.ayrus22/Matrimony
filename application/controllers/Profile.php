<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper(array('url', 'language'));
        $this->load->helper('form');
        $this->load->model('User_model', '', TRUE);
        $this->load->model('Prefrence_model', '', TRUE);
        $this->load->model('Profilemanager_model', '', TRUE);
        $this->load->model('Payment_model', '', TRUE);
        $this->load->model('Usermanager_model', '', TRUE);
        $this->load->model('Login_model', '', TRUE);
        $this->load->model('Form_model', '', TRUE);
        $this->load->model('Mail', '', TRUE);
        $this->load->model('Utils', '', TRUE);
        $this->load->library('session');
        header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1.
        header("Pragma: no-cache"); // HTTP 1.0.
        header("Expires: 0"); // Proxies.

        if (!isset($this->session->userdata['roleguid']) || $this->session->userdata['roleguid'] != MEMBER_ROLE_ID) {
            redirect(base_url());
        }
        if (empty($this->session->userdata['gender'])) {
            redirect(base_url() . 'index.php/common/addgender');
        }
    }

    public function index() {
        $this->data = array();
        $userGuid = $this->session->userdata['userguid'];
        $message = $this->session->flashdata('message');
        if ($message) {
            $this->data['message'] = $message;
        }

        $profile = $this->User_model->getUserinformation($userGuid);
        $city = array();
        $state = array();
        if (!empty($profile[0])) {
            $this->data['profile'] = $this->Profilemanager_model->profileEditData($profile[0]);

            $stateList = $this->Form_model->stateDetails($profile[0]['country']);
            if (!empty($stateList[0])) {
                foreach ($stateList as $key => $val) {
                    $state[] = array('id' => $val['id'], 'text' => $val['name']);
                }
             }

            $cityList = $this->Form_model->cityDetails($profile[0]['state']);
            if (!empty($cityList[0])) {
                foreach ($cityList as $key => $val) {
                    $city[] = array('id' => $val['id'], 'text' => $val['name']);
                }
            }
        }
        $this->data['stateList'] = json_encode($state);
		$this->data['cityList'] = json_encode($city);
        /** partner inforamtion */
        $prefrenceInfo = array();
        $partnerInfo = $this->User_model->getUserPartnerInfo($userGuid);
        if (!empty($partnerInfo)) {
            $prefrence = (!empty($partnerInfo[0]['prefrence']) ? json_decode($partnerInfo[0]['prefrence'], true) : '');
            if (!empty($prefrence)) {
                $this->data['prefrenceData'] = $this->Profilemanager_model->prefrenceEditData($prefrence);
            }
        }

        $casteInfo = $this->Form_model->casteDetails();
		$caste =array();
        if (!empty($casteInfo)) {
            foreach ($casteInfo as $key => $val) {
                $caste[] = array('id' => $val['id'], 'text' => $val['name']);
            }
        }
        $this->data['casteList'] = json_encode($caste);

        $countryInfo = $this->Form_model->countryDetails();
        $country = array();
        if (!empty($countryInfo)) {
            foreach ($countryInfo as $key => $val) {
                $country[] = array('id' => $val['id'], 'text' => $val['name']);
            }
        }
        $this->data['countryList'] = json_encode($country);

        $eduInfo = $this->Form_model->educationInfo();
        $edu = array();
        if (!empty($eduInfo)) {
            foreach ($eduInfo as $k => $v) {
                $edu[] = array('id' => $v['id'], 'text' => $v['name']);
            }
        }
        $this->data['education'] = json_encode($edu);

        $this->load->view('layout/header');
        $this->load->view('profile/profile_edit', $this->data);
        $this->load->view('layout/footer');
    }

    public function viewed() {
        $userGuid = $this->session->userdata['userguid'];
        $this->data['image'] = (!empty($this->session->userdata['image']) ? $this->session->userdata['image'] : 'default.png');
        $gender = GENDER_MALE;
        if (strtoupper($this->session->userdata['gender']) == GENDER_MALE) {
            $gender = GENDER_FEMALE;
        }

        $membership = $this->Payment_model->getPaymentMembershipInfo($userGuid);
        $this->data['offer'] = 2;
        if (!empty($membership) && ( $membership[0]['payment_status'] == TEXT_COMPLETED )) {
            $this->data['offer'] = 1;
        }

        $viewedInfo = $this->User_model->viwedProfileInfo('', $userGuid);
        if (!empty($viewedInfo)) {
            $userList = array();
            $viewedUserList = array();
            foreach ($viewedInfo as $key => $value) {
                $viewedUserList[] = $value['viewed_from'];
            }
            /** get block and ignore user list */
            $id = array(REQUEST_IGNORE, REQUEST_NOT_INETREST, REQUEST_BLOCK);
            /*             * get login user blocked user list  (Me blocked from others) * */
            $exists = $this->User_model->ProfileRequestInfo($userGuid, '', '', $id);
            /*             * Any one blocked from login user list (Others blocked from me) * */
            $exists1 = $this->User_model->ProfileRequestInfo('', $userGuid, '', $id);

            $IgnoreUserList = array();
            $blockList = array();
            $otherBlockList = array();
            if ($exists) {
                foreach ($exists as $user) {
                    $blockList[] = $user['request_to'];
                }
            }

            if ($exists1) {
                foreach ($exists1 as $user) {
                    $otherBlockList[] = $user['request_from'];
                }
            }

            $IgnoreUserList = array_merge($blockList, $otherBlockList);

            /** removed ignore user list and get viewed user list * */
            $userList = array_diff($viewedUserList, $IgnoreUserList);
            $getUserList = array();
            if (!empty($userList)) {
                $getUserList = $this->User_model->getUserinformation($userList, '', $gender);
            }
            if (!empty($getUserList)) {

                foreach ($getUserList as $key => $data) {
                    $this->data['viewed'][$data['userGuid']] = $this->Profilemanager_model->userRequestViewData($data['userGuid']);
                    $this->data['viewed'][$data['userGuid']]['userId'] = $data['userId'];
                    $this->data['viewed'][$data['userGuid']]['userGuid'] = $data['userGuid'];
                    $this->data['viewed'][$data['userGuid']]['username'] = $data['username'];
                    $this->data['viewed'][$data['userGuid']]['age'] = $data['age'];
                    $this->data['viewed'][$data['userGuid']]['height'] = $data['height'];
                    $this->data['viewed'][$data['userGuid']]['gender'] = $data['gender'];
                    $this->data['viewed'][$data['userGuid']]['religion'] = $data['religion'];
                    $this->data['viewed'][$data['userGuid']]['education'] = $data['education'];
                    $this->data['viewed'][$data['userGuid']]['qualification'] = $data['qualification'];
                    $this->data['viewed'][$data['userGuid']]['occupation'] = $data['occupation'];
                    $this->data['viewed'][$data['userGuid']]['log_in'] = 'Not Login';

                    $loginActivity = $this->User_model->getLoginActivityInfo($data['userGuid'], 1);
                    if (!empty($loginActivity[0]['client_date'])) {
                        $this->data['viewed'][$data['userGuid']]['log_in'] = $this->User_model->relativeTime(strtotime($loginActivity[0]['client_date']));
                    }

                    $shortList = $this->User_model->ProfileRequestInfo($userGuid, $data['userGuid'], '', REQUEST_SHORTLIST);
                    if (!empty($shortList[0]['guid'])) {
                        $this->data['viewed'][$data['userGuid']]['shortlist'] = 1;
                    }

                    $imagerequest = $this->User_model->ProfileRequestInfo($userGuid, $data['userGuid'], '', REQUEST_IMAGE);
                    if (!empty($imagerequest[0]['guid'])) {
                        $this->data['viewed'][$data['userGuid']]['imagerequest'] = 1;
                    }

                    $educationDetails = $this->Form_model->educationInfo($data['education']);
                    if (!empty($educationDetails[0]['name'])) {
                        $this->data['viewed'][$data['userGuid']]['edu_name'] = $educationDetails[0]['name'];
                    }

                    $casteDetails = $this->Form_model->casteDetails($data['caste']);
                    if (!empty($casteDetails[0]['name'])) {
                        $this->data['viewed'][$data['userGuid']]['caste_name'] = $casteDetails[0]['name'];
                    }

                    /** get active  and verified image  if not active then get default image */
                    $getUserImagesInfo = $this->Profilemanager_model->getActiveProfileImage($data['userGuid'], $active = 1, $verified = 1);
                    $this->data['viewed'][$data['userGuid']]['image'] = $getUserImagesInfo['image'];
                    $this->data['viewed'][$data['userGuid']]['image_request'] = $getUserImagesInfo['image_request'];
                }
            }
        }

        $this->load->view('layout/header');
        $this->load->view('profile/viewed_profile', $this->data);
        $this->load->view('layout/footer');
    }

    public function profileviewed() {
        $userGuid = $this->session->userdata['userguid'];
        $this->data['image'] = (!empty($this->session->userdata['image']) ? $this->session->userdata['image'] : 'default.png');
        $gender = GENDER_MALE;
        if (strtoupper($this->session->userdata['gender']) == GENDER_MALE) {
            $gender = GENDER_FEMALE;
        }
        $membership = $this->Payment_model->getPaymentMembershipInfo($userGuid);
        $this->data['offer'] = 2;
        if (!empty($membership) && ( $membership[0]['payment_status'] == TEXT_COMPLETED )) {
            $this->data['offer'] = 1;
        }
        $viewedInfo = $this->User_model->viwedProfileInfo($userGuid);
        if (!empty($viewedInfo)) {
            $userList = array();
            $viewedUserList = array();
            foreach ($viewedInfo as $key => $value) {
                $viewedUserList[] = $value['viewed_to'];
            }
            /** get block and ignore user list */
            $id = array(REQUEST_IGNORE, REQUEST_NOT_INETREST, REQUEST_BLOCK);
            /*             * get login user blocked user list  (Me blocked from others) * */
            $exists = $this->User_model->ProfileRequestInfo($userGuid, '', '', $id);
            /*             * Any one blocked from login user list (Others blocked from me) * */
            $exists1 = $this->User_model->ProfileRequestInfo('', $userGuid, '', $id);

            $IgnoreUserList = array();
            $blockList = array();
            $otherBlockList = array();
            if ($exists) {
                foreach ($exists as $user) {
                    $blockList[] = $user['request_to'];
                }
            }

            if ($exists1) {
                foreach ($exists1 as $user) {
                    $otherBlockList[] = $user['request_from'];
                }
            }

            $IgnoreUserList = array_merge($blockList, $otherBlockList);

            /** removed ignore user list and get viewed user list * */
            $userList = array_diff($viewedUserList, $IgnoreUserList);
            $getUserList = array();
            if (!empty($userList)) {
                $getUserList = $this->User_model->getUserinformation($userList, '', $gender);
            }
            if (!empty($getUserList)) {
                foreach ($getUserList as $key => $data) {
                    $this->data['viewed'][$data['userGuid']] = $this->Profilemanager_model->userRequestViewData($data['userGuid']);
                    $this->data['viewed'][$data['userGuid']]['userId'] = $data['userId'];
                    $this->data['viewed'][$data['userGuid']]['userGuid'] = $data['userGuid'];
                    $this->data['viewed'][$data['userGuid']]['username'] = $data['username'];
                    $this->data['viewed'][$data['userGuid']]['age'] = $data['age'];
                    $this->data['viewed'][$data['userGuid']]['height'] = $data['height'];
                    $this->data['viewed'][$data['userGuid']]['gender'] = $data['gender'];
                    $this->data['viewed'][$data['userGuid']]['religion'] = $data['religion'];
                    $this->data['viewed'][$data['userGuid']]['education'] = $data['education'];
                    $this->data['viewed'][$data['userGuid']]['qualification'] = $data['qualification'];
                    $this->data['viewed'][$data['userGuid']]['occupation'] = $data['occupation'];
                    $this->data['viewed'][$data['userGuid']]['log_in'] = 'Not Login';

                    $loginActivity = $this->User_model->getLoginActivityInfo($data['userGuid'], 1);
                    if (!empty($loginActivity[0]['client_date'])) {
                        $this->data['viewed'][$data['userGuid']]['log_in'] = $this->User_model->relativeTime(strtotime($loginActivity[0]['client_date']));
                    }

                    $shortList = $this->User_model->ProfileRequestInfo($userGuid, $data['userGuid'], '', REQUEST_SHORTLIST);
                    if (!empty($shortList[0]['guid'])) {
                        $this->data['viewed'][$data['userGuid']]['shortlist'] = 1;
                    }

                    $imagerequest = $this->User_model->ProfileRequestInfo($userGuid, $data['userGuid'], '', REQUEST_IMAGE);
                    if (!empty($imagerequest[0]['guid'])) {
                        $this->data['viewed'][$data['userGuid']]['imagerequest'] = 1;
                    }


                    $educationDetails = $this->Form_model->educationInfo($data['education']);
                    if (!empty($educationDetails[0]['name'])) {
                        $this->data['viewed'][$data['userGuid']]['edu_name'] = $educationDetails[0]['name'];
                    }

                    $casteDetails = $this->Form_model->casteDetails($data['caste']);
                    if (!empty($casteDetails[0]['name'])) {
                        $this->data['viewed'][$data['userGuid']]['caste_name'] = $casteDetails[0]['name'];
                    }
                    $membership = 1;
                    $offer = 0;
                    if (!empty($membership)) {
                        $this->data['viewed'][$data['userGuid']]['offer'] = 1;
                    }

                    /** get active  and verified image  if not active then get default image */
                    $getUserImagesInfo = $this->Profilemanager_model->getActiveProfileImage($data['userGuid'], $active = 1, $verified = 1);
                    $this->data['viewed'][$data['userGuid']]['image'] = $getUserImagesInfo['image'];
                    $this->data['viewed'][$data['userGuid']]['image_request'] = $getUserImagesInfo['image_request'];
                }
            }
        }

        $this->load->view('layout/header');
        $this->load->view('profile/viewed_profile', $this->data);
        $this->load->view('layout/footer');
    }

    public function sendmailinfo() {
        if (empty($_REQUEST['uid'])) {
            echo $this->lang->line('detail_not_found');
            exit;
        }
        $profileid = $_REQUEST['uid'];
        $getUserInfo = $this->User_model->getUserinformation($profileid, '', '');
        $profile = array();
        $image = '';
        if (!empty($getUserInfo[0])) {
            $profile = $this->Profilemanager_model->profileEditData($getUserInfo[0]);
            //	echo "<pre>";print_r($profileid);exit;
            /** get active  and verified image  if not active then get default image */
            $getUserImagesInfo = $this->Profilemanager_model->getActiveProfileImage($profileid, $active = 1, $verified = 1);
            $url = base_url() . 'assets/upload_images/' . $getUserImagesInfo['image'];
        }
        $payment = base_url() . 'index.php/payment/';

        $html = $this->lang->line('detail_not_found');
        if (!empty($profile)) {

            $html = '	<div class="col-md-3 colpaid">
					 <div class="proimg_border">
						<img src="' . $url . '" class="sendimage" alt=""/>
					 </div>
				  </div>
				<div class="col-md-4 colpaid">
							<div class="p_name sendtext">' . (!empty($profile['username']) ? $profile['username'] : 'Not Specified') . '</div>
							<div class="p_age sendtext"><span class="age">' . (!empty($profile['age']) ? $profile['age'] . ' Years' : 'Not Specified') . ',</span>
								<span class="fit sendtext">' . (!empty($profile['height']) ? $profile['height'] . ' Cms' : 'Not Specified') . '</span>
							</div>
							<div class="p_lang sendtext">' . (!empty($profile['mother_tongue']) ? ucwords($profile['mother_tongue']) : 'Not Specified') . '</div>
							<div class="p_caste sendtext">' . (!empty($profile['caste_name']) ? ucwords($profile['caste_name']) : 'Not Specified') . '</div>
							<div class="p_city sendtext">
								<span class="city">' . (!empty($profile['city_name']) ? ucwords($profile['city_name']) : 'Not Specified') . ' - ' . (!empty($profile['state_name']) ? $profile['state_name'] : 'Not Specified') . '</span>
							</div>
							<div class="p_qualify sendtext">
								<span class="qualify">' . (!empty($profile['edu_name']) ? $profile['edu_name'] : 'Not Specified') . ' - ' . (!empty($profile['qualification']) ? ucwords($profile['qualification']) : 'Not Specified') . ',</span><span class="deg"> ' . (!empty($profile['occupation']) ? $profile['occupation'] : 'Not Specified') . ' </span>
							</div>
							<div class="proadd">
								<a href="' . $payment . '" class="photo_view">Upgrade Now</a>
							</div>
				</div>
					<div class="col-md-5 colpaid">
						<div class=" benefit">
						<h4 class="paidmodelhead">Benefit of paid member</h4>
						<div class="paidmodelicon">
							<span class="iconcolor"><i class="fa fa-envelope"></i></span>
							<span class="paidtext">send unlimited Message</span>
						</div>					
						<div class="paidmodelicon">
							<span class="iconcolor"><i class="fa fa-phone"></i></span>
							<span class="paidtext">Call/SMS your Matches</span>
						</div>
						<div class="paidmodelicon">
							<span class="iconcolor"><i class="fa fa-user" ></i></span>
							<span class="paidtext">Get the highlight matches profile</span>
						</div>
						</div>
					</div>	
					<span class="sendfooter">
				Need help in making payment
				call this number 129897987978
				</span>';
        }
        echo $html;
        exit;
    }

    public function view() {
        $userGuid = $this->session->userdata['userguid'];
        if (empty($_GET['pid'])) {
            redirect(base_url() . 'index.php/message/');
        }
        $membership = $this->Payment_model->getPaymentMembershipInfo($userGuid);
        $this->data['offer'] = 2;
        if (!empty($membership) && ( $membership[0]['payment_status'] == TEXT_COMPLETED )) {
            $this->data['offer'] = 1;
        }

        $gender = GENDER_MALE;
        if (strtoupper($this->session->userdata['gender']) == GENDER_MALE) {
            $gender = GENDER_FEMALE;
        }

        $profileid = $_GET['pid'];
        $exits = $this->User_model->viwedProfileInfo($userGuid, $profileid);
        $created = date(DATE_TIME_FORMAT);
        $viwedData = array(
            'viewed_from' => $userGuid,
            'viewed_to' => $profileid,
            'last_updated' => $created,
            'last_updated_by' => $userGuid
        );
        if (!empty($exits[0]['guid'])) {
            $response = $this->User_model->updateUserViewedProfileInfo($exits[0]['guid'], $viwedData);
        } else {
            $viwedData['created'] = $created;
            $viwedData['created_by'] = $userGuid;
            $viwedData['guid'] = $this->Utils->getGuid();
            $response = $this->User_model->insertUserViewedProfileInfo($viwedData);
        }

        $getUserInfo = $this->User_model->getUserinformation($profileid);
        if (!empty($getUserInfo[0])) {
            foreach ($getUserInfo as $data) {
                $this->data['uid'] = $data['userGuid'];
                $this->data['profile'] = $this->Profilemanager_model->profileEditData($getUserInfo[0], $userGuid);

                /** @todo need to add move image related model */
                $getUserImagesInfo = $this->User_model->getUserImageInfo($profileid, '', '', '', $verify = 1);
                $image = array();
                $this->data['image_request'] = 1;
                if (!empty($getUserImagesInfo)) {
                    foreach ($getUserImagesInfo as $key => $val) {
                        if (!empty($val['image'])) {
                            if (file_exists(dirname($_SERVER["SCRIPT_FILENAME"]) . "/assets/upload_images/" . $val['image'])) {
                                $image[] = $val['image'];
                                $this->data['image_request'] = 0;
                            }
                        }
                    }
                } else {
                    if (!empty($data['gender'])) {
                        $profileImage = DEFAULT_IMAGE;
                        if (strtoupper($data['gender']) == GENDER_MALE) {
                            $profileImage = MALE_IMAGE;
                        } elseif (strtoupper($data['gender']) == GENDER_FEMALE) {
                            $profileImage = FEMALE_IMAGE;
                        }
                        $image[] = $profileImage;
                    }
                }

                $imagerequest = $this->User_model->ProfileRequestInfo($userGuid, $data['userGuid'], '', REQUEST_IMAGE);
                if (!empty($imagerequest[0]['guid'])) {
                    $this->data['imagerequest'] = 1;
                }
            }

            $this->data['profile_images'] = $image;
            //echo "<pre>";print_r($image);exit;
        }

        $loginActivity = $this->User_model->getLoginActivityInfo($profileid, 1);
        $this->data['profile']['log_in'] = 'Not Login';
        if (!empty($loginActivity[0]['client_date'])) {
            $this->data['profile']['log_in'] = $this->User_model->relativeTime(strtotime($loginActivity[0]['client_date']));
        }

        $partnerInfo = $this->User_model->getUserPartnerInfo($profileid);
        if (!empty($partnerInfo)) {
            $prefrence = (!empty($partnerInfo[0]['prefrence']) ? json_decode($partnerInfo[0]['prefrence'], true) : '');
            if (!empty($prefrence)) {
                $this->data['prefrenceData'] = $this->Profilemanager_model->prefrenceEditData($prefrence);
            }
        }
        $limits = 5;
        $viewedInfo = $this->User_model->viwedProfileInfo( $userGuid );
        /** get block and ignore user list */
        $id = array(REQUEST_IGNORE, REQUEST_NOT_INETREST, REQUEST_BLOCK);
        /*         * get login user blocked user list  (Me blocked from others) * */
        $exists = $this->User_model->ProfileRequestInfo($userGuid, '', '', $id);
        /*         * Any one blocked from login user list (Others blocked from me) * */
        $exists1 = $this->User_model->ProfileRequestInfo('', $userGuid, '', $id);

        $IgnoreUserList = array();
        $blockList = array();
        $otherBlockList = array();
        if ($exists) {
            foreach ($exists as $user) {
                $blockList[] = $user['request_to'];
            }
        }

        if ($exists1) {
            foreach ($exists1 as $user) {
                $otherBlockList[] = $user['request_from'];
            }
        }

        /** merge two array */
        $IgnoreUserList = array_merge($blockList, $otherBlockList);

        if (!empty($viewedInfo)) {
            $viewUserList = array();
            foreach ($viewedInfo as $key => $value) {
                $viewUserList[] = $value['viewed_to'];
            }

            /** removed ignore user list and get viewed user list * */
            $userList = array_diff($viewUserList, $IgnoreUserList);

            $getUserList = $this->User_model->getUserinformation($userList, '', $gender, $limits);
            if (!empty($getUserList)) {
                $i = 0;
                foreach ($getUserList as $key => $data) {
                    $this->data['viewed'][$i]['userId'] = $data['userId'];
                    $this->data['viewed'][$i]['userGuid'] = $data['userGuid'];
                    $this->data['viewed'][$i]['age'] = $data['age'];
                    $this->data['viewed'][$i]['height'] = $data['height'];

                    $casteDetails = $this->Form_model->casteDetails($data['caste']);
                    if (!empty($casteDetails[0]['name'])) {
                        $this->data['viewed'][$i]['caste_name'] = $casteDetails[0]['name'];
                    }

                    /** get active  and verified image  if not active then get default image */
                    $getUserImagesInfo = $this->Profilemanager_model->getActiveProfileImage($data['userGuid'], $active = 1, $verified = 1);
                    $this->data['viewed'][$i]['image'] = $getUserImagesInfo['image'];
                    $this->data['viewed'][$i]['image_request'] = $getUserImagesInfo['image_request'];
                    $i++;
                }
            }
        }

        $requestInfo = $this->User_model->ProfileRequestInfo($userGuid, '', '', REQUEST_SEND);
        if (!empty($requestInfo)) {
            $requestUserList = array();
            foreach ($requestInfo as $key => $value) {
                $requestUserList[] = $value['request_to'];
            }
            /** removed ignore user list and get viewed user list * */
            $requestUserList = array_diff($requestUserList, $IgnoreUserList);

            $getRequestUserList = $this->User_model->getUserinformation($requestUserList, '', $gender, $limits);
            if (!empty($getRequestUserList)) {
                $k = 0;
                foreach ($getRequestUserList as $key => $data1) {
                    $this->data['request'][$k]['userId'] = $data1['userId'];
                    $this->data['request'][$k]['age'] = $data1['age'];
                    $this->data['request'][$k]['height'] = $data1['height'];

                    $casteDetail = $this->Form_model->casteDetails($data1['caste']);
                    if (!empty($casteDetail[0]['name'])) {
                        $this->data['request'][$k]['caste_name'] = $casteDetail[0]['name'];
                    }

                    /** get active  and verified image  if not active then get default image */
                    $getUserImagesInfo = $this->Profilemanager_model->getActiveProfileImage($data1['userGuid'], $active = 1, $verified = 1);
                    $this->data['request'][$k]['image'] = $getUserImagesInfo['image'];
                    $this->data['request'][$k]['image_request'] = $getUserImagesInfo['image_request'];
                    $k++;
                }
            }
        }

        $partnerInfo = $this->User_model->getUserPartnerInfo($userGuid);
        $prefrenceInfo = array();
        if (!empty($partnerInfo)) {
            $prefrence = (!empty($partnerInfo[0]['prefrence']) ? json_decode($partnerInfo[0]['prefrence'], true) : '');
            if (!empty($prefrence)) {
                $added = array('maritalstatus' => $prefrence['maritalstatus'], 'caste' => $prefrence['caste']);

                $prefrenceInfo = $this->Prefrence_model->getUserPrefrenceDetails($added, $gender);
                if (!empty($prefrenceInfo)) {
                    $prefrencetUserList = array();
                    foreach ($prefrenceInfo as $key => $value) {
                        $prefrencetUserList[] = $value['userGuid'];
                    }

                    /** removed ignore user list and get viewed user list * */
                    $prefrencetUserList = array_diff($prefrencetUserList, $IgnoreUserList);

                    $getPrefrencetUserList = $this->User_model->getUserinformation($prefrencetUserList, '', $gender, $limits);
                    if (!empty($getPrefrencetUserList)) {
                        $j = 0;
                        foreach ($getPrefrencetUserList as $key => $matches) {
                            $this->data['currentmatches'][$j]['userId'] = $matches['userId'];
                            $this->data['currentmatches'][$j]['userGuid'] = $matches['userGuid'];
                            $this->data['currentmatches'][$j]['age'] = $matches['age'];
                            $this->data['currentmatches'][$j]['height'] = $matches['height'];

                            $casteDetail = $this->Form_model->casteDetails($matches['caste']);
                            if (!empty($casteDetail[0]['name'])) {
                                $this->data['currentmatches'][$j]['caste_name'] = $casteDetail[0]['name'];
                            }

                            /** get active  and verified image  if not active then get default image */
                            $getUserImagesInfo = $this->Profilemanager_model->getActiveProfileImage($matches['userGuid'], $active = 1, $verified = 1);
                            $this->data['currentmatches'][$j]['image'] = $getUserImagesInfo['image'];
                            $this->data['currentmatches'][$j]['image_request'] = $getUserImagesInfo['image_request'];
                            $j++;
                        }
                    }
                }
            }
        }
        /** get login user blocked user list  (Me blocked from others) * */
        $blockexists = $this->User_model->ProfileRequestInfo($userGuid, $profileid, '', REQUEST_IGNORE);

        $this->data['profile']['ignore'] = 0;
        if (!empty($blockexists)) {
            $this->data['profile']['ignore'] = 1;
        }

        /*         * get login user blocked user list  (Me blocked from others) * */
        $blockexists = $this->User_model->ProfileRequestInfo($userGuid, $profileid, '', REQUEST_BLOCK);

        $this->data['profile']['block'] = 0;
        if (!empty($blockexists)) {
            $this->data['profile']['block'] = 1;
        }
        //echo "<pre>";print_r($this->data);exit;
        $this->load->view('layout/header');
        $this->load->view('profile/view_profile', $this->data);
        $this->load->view('layout/footer');
    }

    public function setting() {
        $userGuid = $this->session->userdata['userguid'];
        if (isset($_GET['step1']) && !empty($_POST['email'])) {
            $email = $_POST['email'];
            $response = array('msg' => $this->lang->line('email_format_invalid'));
            if (preg_match('^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$^', $email)) {
                $exists = $this->User_model->userDetails($email);
                $response = array('msg' => $this->lang->line("success_msg"));
                if (!empty($exists)) {
                    $response = array('msg' => $this->lang->line("email_already_exits_please_change_new_email_id"));
                }
            }

            echo json_encode($response);
            exit;
        } elseif (isset($_GET['step2'])) {
            $response = array('msg' => $this->lang->line('failure_to_update_please_try_again'));
            if (!empty($_POST['email']) && !empty($_POST['password'])) {
                $email = $_POST['email'];
                $option = array('username' => $this->session->userdata['email'], 'password' => $_POST['password']);

                $userExists = $this->Login_model->loginValidation($option, MEMBER_ROLE_ID);
                if ($userExists) {
                    $updateData = array('email' => $email, 'last_updated' => date(DATE_TIME_FORMAT), 'last_updated_by' => $userGuid);
                    $update = $this->User_model->updateUser($userGuid, $updateData);
                    if ($update) {
                        $this->session->unset_userdata($data['email'] = '');
                        $data['email'] = $email;
                        $this->session->set_userdata($data);

                        $response = array('msg' => $this->lang->line('update_success_fully_use_this_email_id_to_login'));
                    }
                }
            }
            echo json_encode($response);
            exit;
        } elseif (isset($_GET['step3'])) { //echo "<pre>";print_r($_POST);print_r($this->session->userdata);exit;
            $response = array('msg' => $this->lang->line('password_required'));
            if (!empty($_POST['password']) && !empty($_POST['new_password'])) {
                $option = array('username' => $this->session->userdata['email'], 'password' => $_POST['password']);
                $userExists = $this->Login_model->loginValidation($option,MEMBER_ROLE_ID);
                $response = array('msg' => $this->lang->line('password_is_wrong'));
                if ($userExists) {
                    $updateData = array('password' => md5($_POST['new_password']), 'last_updated' => date(DATE_TIME_FORMAT), 'last_updated_by' => $userGuid);
                    $update = $this->User_model->updateUser($userGuid, $updateData);
                    $response = array('msg' => $this->lang->line("success_msg"));
                }
            }
            echo json_encode($response);
            exit;
        } elseif (isset($_GET['step4'])) {
            $response = array('msg' => $this->lang->line("please_try_again_after_some_time"));
            if (!empty($_POST['reason'])) {
                /** insert to user deactivation table */
                $msg = (!empty($_POST['message']) ? $_POST['message'] : '');
                $insertData = array('user_guid' => $userGuid, 'reason' => $_POST['reason'], 'message' => $msg, 'deleted' => 1, 'created' => date(DATE_TIME_FORMAT), 'created_by' => $userGuid);
                $insert = $this->User_model->insertUserDeactivationInfo($insertData);
                /** update user table deleted=1 */
                $updateData = array('deleted' => 1, 'last_updated' => date(DATE_TIME_FORMAT), 'last_updated_by' => $userGuid);
                $update = $this->User_model->updateUser($userGuid, $updateData);
                $response = array('msg' => $this->lang->line("failure_msg"));
                if (!empty($insert) && !empty($update)) {
                    $response = array('msg' => $this->lang->line("success_msg"));
                    $data = array('session_id' => '', 'email' => '', 'roleName' => '', 'userguid' => '', 'roleguid' => '', 'username' => '', 'imagepath' => '');
                    $this->session->unset_userdata($data);
                    $this->session->sess_destroy();
                }
            }
            echo json_encode($response);
            exit;
        }

        $this->data = $this->session->userdata;

        $this->load->view('layout/header');
        $this->load->view('profile/setting', $this->data);
        $this->load->view('layout/footer');
    }

    public function updateprofileinfo() {
        $response = array('msg' => $this->lang->line("failure_msg"));
        if (empty($_POST)) {
            echo json_encode($response);
        }
        $userGuid = $this->session->userdata['userguid'];

        $caste = (!empty($_POST['caste']) ? $_POST['caste'] : '');
        if (empty($caste)) {
            $religionExits = $this->User_model->getUserReligionInfo($userGuid);
            $caste = (!empty($religionExits[0]['caste']) ? $religionExits[0]['caste'] : '');
        }

        $exits = $this->User_model->getRecentlyUpdatedProfile($userGuid);
        $updateProfile = array('user_guid' => $userGuid, 'caste_id' => $caste, 'created_by' => $userGuid);

        if (empty($exits)) {
            $insert = $this->User_model->insertRecentlyUpdated($updateProfile);
        } else {
            $update = $this->User_model->updateRecentlyUpdated($userGuid, $updateProfile);
        }
        $created = date(DATE_TIME_FORMAT);
        if (isset($_GET['step1'])) {
            $updateProfile = array('mobile' => (!empty($_POST['mobile']) ? $_POST['mobile'] : ''),
                'martial_status' => (!empty($_POST['maritalstatus']) ? $_POST['maritalstatus'] : ''),
                'country' => (!empty($_POST['country']) ? $_POST['country'] : ''),
                'state' => (!empty($_POST['state']) ? $_POST['state'] : ''),
                'city' => (!empty($_POST['city']) ? $_POST['city'] : ''),
                'last_updated' => $created,
                'last_updated_by' => $userGuid,
            );
            $update = $this->User_model->updateUserProfile($userGuid, $updateProfile);
            if ($update) {
                $response = array('msg' => $this->lang->line("success_msg"));
            } else {
                $response = array('msg' => $this->lang->line("failure_msg"));
            }
            echo json_encode($response);
        } elseif (isset($_GET['step2'])) {
            $updateProfile1 = array('physical_status' => (!empty($_POST['physicalstatus']) ? $_POST['physicalstatus'] : ''),
                'bodytype' => (!empty($_POST['bodytype']) ? $_POST['bodytype'] : ''),
                'height' => (!empty($_POST['height']) ? $_POST['height'] : ''),
                'complexion' => (!empty($_POST['complexion']) ? $_POST['complexion'] : ''),
                'weight' => (!empty($_POST['weight']) ? $_POST['weight'] : ''),
                'last_updated' => $created,
                'last_updated_by' => $userGuid,
            );
            $update = $this->User_model->updateUserProfile($userGuid, $updateProfile1);
            if ($update) {
                $response = array('msg' => $this->lang->line("success_msg"));
            } else {
                $response = array('msg' => $this->lang->line("failure_msg"));
            }
            echo json_encode($response);
        } elseif (isset($_GET['step3'])) {
            $updateOtherInfo = array('education' => (!empty($_POST['edu']) ? $_POST['edu'] : ''),
                'qualification' => (!empty($_POST['qualification']) ? $_POST['qualification'] : ''),
                'occupation' => (!empty($_POST['occupation']) ? $_POST['occupation'] : ''),
                'employeed_in' => (!empty($_POST['emp_in']) ? $_POST['emp_in'] : ''),
                'salary' => (!empty($_POST['salary']) ? $_POST['salary'] : ''),
                'last_updated' => $created,
                'last_updated_by' => $userGuid,
            );
            $update = $this->User_model->updateOtherInfo('', $updateOtherInfo, $userGuid);
            if ($update) {
                $response = array('msg' => $this->lang->line("success_msg"));
            } else {
                $response = array('msg' => $this->lang->line("failure_msg"));
            }
            echo json_encode($response);
        } elseif (isset($_GET['step4'])) {
            $updateOtherInfo = array('food' => (!empty($_POST['food']) ? $_POST['food'] : ''),
                'drink' => (!empty($_POST['smoking']) ? $_POST['smoking'] : ''),
                'smoke' => (!empty($_POST['drinking']) ? $_POST['drinking'] : ''),
                'last_updated' => $created,
                'last_updated_by' => $userGuid,
            );
            $update = $this->User_model->updateHabbitsInfo('', $updateOtherInfo, $userGuid);
            if ($update) {
                $response = array('msg' => $this->lang->line("success_msg"));
            } else {
                $response = array('msg' => $this->lang->line("failure_msg"));
            }
            echo json_encode($response);
        } elseif (isset($_GET['step5'])) {
            $updateOtherInfo = array('caste' => (!empty($_POST['caste']) ? $_POST['caste'] : ''),
                'sub_caste' => (!empty($_POST['subcaste']) ? $_POST['subcaste'] : ''),
                'gothram' => (!empty($_POST['gothram']) ? $_POST['gothram'] : ''),
                'dhosam' => (!empty($_POST['dhosam']) ? $_POST['dhosam'] : ''),
                'dhosam_type' => (!empty($_POST['dhosam_type']) ? $_POST['dhosam_type'] : ''),
                'star' => (!empty($_POST['star']) ? $_POST['star'] : ''),
                'raasi' => (!empty($_POST['star']) ? $_POST['raasi'] : ''),
                'last_updated' => $created,
                'last_updated_by' => $userGuid,
            );
            $update = $this->User_model->updateReligionInfo('', $updateOtherInfo, $userGuid);
            if ($update) {
                $response = array('msg' => $this->lang->line("success_msg"));
            } else {
                $response = array('msg' => $this->lang->line("failure_msg"));
            }
            echo json_encode($response);
        } elseif (isset($_GET['step6'])) {
            $updateOtherInfo = array('family_status' => (!empty($_POST['fstatus']) ? $_POST['fstatus'] : ''),
                'family_type' => (!empty($_POST['ftype']) ? $_POST['ftype'] : ''),
                'family_values' => (!empty($_POST['fvalues']) ? $_POST['fvalues'] : ''),
                'last_updated' => $created,
                'last_updated_by' => $userGuid,
            );
            $update = $this->User_model->updateFamilyInfo('', $updateOtherInfo, $userGuid);
            if ($update) {
                $response = array('msg' => $this->lang->line("success_msg"));
            } else {
                $response = array('msg' => $this->lang->line("failure_msg"));
            }
            echo json_encode($response);
        }
    }

    public function updateprefrenceinfo() {
        $userGuid = $this->session->userdata['userguid'];
        $response = array('msg' => $this->lang->line("failure_msg"));
        if (isset($_GET['step1'])) {
            $_POST['uid'] = $userGuid;
            $result = $this->Usermanager_model->userRegistrationStep3($_POST);
            $response = array('msg' => $result['message']);
        }
        echo json_encode($response);
    }

    public function updateprofilerequestinfo() {
        $userGuid = $this->session->userdata['userguid'];
        $response = array('msg' => $this->lang->line("failure_msg"));
        $result = 0;

        if (isset($_REQUEST['id']) && isset($_REQUEST['pid']) && !empty($_REQUEST['id']) && !empty($_REQUEST['pid'])) {

            $result = $this->Profilemanager_model->insertOrUpdateProfileRequestInfo($userGuid, $_REQUEST['pid'], $_REQUEST['id']);
            if ($result) {
                $senderInfo = $this->Profilemanager_model->getManagedUserInformation($userGuid);
                $username = (!empty($senderInfo[$userGuid]['username']) ? ucwords($senderInfo[$userGuid]['username']) : 'unknown');
                $userId = (!empty($senderInfo[$userGuid]['userId']) ? ucwords($senderInfo[$userGuid]['userId']) : 'unknown');
                $receiverInfo = $this->User_model->getUserAndRoleDetails($_REQUEST['pid']);

                if (isset($senderInfo[$userGuid]) && !empty($senderInfo[$userGuid])) {
                    $userinfo = $senderInfo[$userGuid];
                    $partnerName = (!empty($receiverInfo[0]['username']) ? $receiverInfo[0]['username'] : 'unknown');
                    $userinfo['partnername'] = $partnerName;
                    $userinfo['partnerid'] = (!empty($receiverInfo[0]['userId']) ? $receiverInfo[0]['userId'] : 'unknown');

                    $subject = '';
                    if ($_REQUEST['id'] == REQUEST_INTEREST) {
                        $userinfo['interest'] = REQUEST_INTEREST;
                        $subject = sprintf($this->lang->line("mail_interest_sub"), ucwords($partnerName));
                    }

                    if ($_REQUEST['id'] == REQUEST_ACCEPT) {
                        $userinfo['interest'] = REQUEST_ACCEPT;
                        $subject = sprintf($this->lang->line("mail_accepted_sub"), ucwords($partnerName));
                    }

                    if ($_REQUEST['id'] == REQUEST_NOT_INETREST) {
                        $userinfo['interest'] = REQUEST_NOT_INETREST;
                        $subject = sprintf($this->lang->line("mail_not_interest_sub"), ucwords($partnerName), ucwords($username));
                    }

                    if (!empty($subject)) {
                        if (!empty($receiverInfo[0]['email'])) {
                            /** Mail send to user */
                            $body = $this->load->view('template/mail/interest.php', $userinfo, TRUE);
                            if (MAIL_ENABLE) {
                                $sendmail = $this->Mail->sendmail('', $receiverInfo[0]['email'], $subject, $body, $attachment = '');
                            }

                            /** message send to user */
                            if (MESSAGE_ENABLE) {
                                if (!empty($receiverInfo[0]['mobile'])) {
                                    $phone = $receiverInfo[0]['mobile'];
                                    $msg = rawurlencode(sprintf($this->lang->line("sms_text_sub"), $subject));
                                    $sendsms = $this->Mail->indiaSms($phone, $msg);
                                }
                            }
                        }
                    }
                }
                $response = array('msg' => $this->lang->line('success_msg'));
            }
        }
        echo json_encode($response);
    }

    public function photo_edit() {
        $userGuid = $this->session->userdata['userguid'];
        $this->data = array();
        $profile = $this->User_model->getUserinformation($userGuid);
        if (!empty($profile[0])) {
            $this->data['profile'] = $this->Profilemanager_model->profileEditData($profile[0]);
        }

        /** get active  and verified image  if not active then get default image */
        $getUserImagesInfo = $this->Profilemanager_model->getActiveProfileImage($userGuid, $active = 1);
        $this->data['image'] = $getUserImagesInfo['image'];
        $this->data['image_request'] = $getUserImagesInfo['image_request'];

        $this->load->view('layout/header');
        $this->load->view('profile/photo_edit', $this->data);
        $this->load->view('layout/footer');
    }

    public function photo_edit_change() {
        $profileid = $this->session->userdata['userguid'];
        $getUserImagesInfo = $this->User_model->getUserImageInfo($profileid);

        $this->data['profile_images'] = (!empty($getUserImagesInfo[0]) ? $getUserImagesInfo : '');
        $this->data['userguid'] = $profileid;
        $this->load->view('layout/header');
        $this->load->view('profile/photo_edit_change', $this->data);
        $this->load->view('layout/footer');
    }

    public function fileupload() {
        if (empty($_REQUEST['uid'])) {
            $userGuid = $this->session->userdata['userguid'];
        } else {
            $userGuid = $_REQUEST['uid'];
        }
        $data = array();

        /** insert and update recently update profile table start */
        $caste = (!empty($_POST['caste']) ? $_POST['caste'] : '');
        if (empty($caste)) {
            $religionExits = $this->User_model->getUserReligionInfo($userGuid);
            $caste = (!empty($religionExits[0]['caste']) ? $religionExits[0]['caste'] : '');
        }

        $exits = $this->User_model->getRecentlyUpdatedProfile($userGuid);
        $updateProfile = array('user_guid' => $userGuid, 'caste_id' => $caste, 'created_by' => $userGuid);

        if (empty($exits)) {
            $insert = $this->User_model->insertRecentlyUpdated($updateProfile);
        } else {
            $update = $this->User_model->updateRecentlyUpdated($userGuid, $updateProfile);
        }
        /** insert and update recently update profile table  end */
        /** file upload */
        $data = $this->Usermanager_model->fileupload($userGuid);
        echo $data;
        exit;
    }

    public function updatebulkrequestinfo() {
        $userGuid = $this->session->userdata['userguid'];
        $response = array('msg' => $this->lang->line("failure_msg"));
        $result = 0;
        if (isset($_REQUEST['id']) && isset($_REQUEST['shortlist']) && !empty($_REQUEST['id']) && !empty($_REQUEST['shortlist'])) {
            foreach ($_REQUEST['shortlist'] as $val) {
                /** Activity insert or update */
                $result = $this->Profilemanager_model->insertOrUpdateProfileRequestInfo($userGuid, $val, $_REQUEST['id']);
                if ($result) {
                    $senderInfo = $this->Profilemanager_model->getManagedUserInformation($userGuid);
                    $username = (!empty($senderInfo[$userGuid]['username']) ? ucwords($senderInfo[$userGuid]['username']) : 'unknown');
                    $userId = (!empty($senderInfo[$userGuid]['userId']) ? ucwords($senderInfo[$userGuid]['userId']) : 'unknown');
                    $receiverInfo = $this->User_model->getUserAndRoleDetails('', $_REQUEST['pid']);

                    if (isset($senderInfo[$userGuid]) && !empty($senderInfo[$userGuid])) {
                        $userinfo = $senderInfo[$userGuid];
                        $partnerName = (!empty($receiverInfo[0]['username']) ? $receiverInfo[0]['username'] : 'unknown');
                        $userinfo['partnername'] = $partnerName;
                        $userinfo['partnerid'] = (!empty($receiverInfo[0]['userId']) ? $receiverInfo[0]['userId'] : 'unknown');
                        if ($_REQUEST['id'] == REQUEST_INTEREST && !empty($receiverInfo[0]['email'])) {
                            /** Mail send to user */
                            $userinfo['interest'] = '4';
                            $subject = sprintf($this->lang->line("mail_interest_sub"), ucwords($partnerName));
                            $body = $this->load->view('template/mail/interest.php', $userinfo, TRUE);
                            if (MAIL_ENABLE) {
                                $exists = $this->Mail->sendmail('', $receiverInfo[0]['email'], $subject, $body, $attachment = '');
                            }
                            /** message send to user */
                            if (MESSAGE_ENABLE) {
                                if (!empty($receiverInfo[0]['mobile'])) {
                                    $phone = $receiverInfo[0]['mobile'];
                                    $msg = rawurlencode(sprintf($this->lang->line("sms_text_sub"), $subject));
                                    $sendsms = $this->Mail->indiaSms($phone, $msg);
                                }
                            }
                        }

                        /* if ( $_REQUEST['id']== REQUEST_ACCEPT ) {
                          $userinfo['interest']		= '1';
                          $subject	= sprintf($this->lang->line("mail_accepted_sub"), $username);
                          $body 		= $this->load->view('template/mail/interest.php',$userinfo,TRUE);
                          echo '<pre>';print_r($body);exit;
                          $exists		= $this->Mail->sendmail( '', $userinfo['email'], $subject, $body ,$attachment ='' );
                          } */
                    }
                }
            }
            if ($result) {
                $response = array('msg' => $this->lang->line("success_msg"));
            }
        }
        echo json_encode($response);
    }

    public function removeprofileimage() {
        $userGuid = $this->session->userdata['userguid'];
        $response = array('msg' => $this->lang->line("failure_msg"));
        $result = 0;
        if (isset($_REQUEST['id']) && isset($_REQUEST['pid']) && !empty($_REQUEST['id']) && !empty($_REQUEST['pid'])) {

            $exists = $this->User_model->getUserImageInfo($_REQUEST['pid'], '', '', $_REQUEST['id']);
            if ($exists[0]['guid']) {
                $update = array('deleted' => 1, 'last_updated' => date(DATE_TIME_FORMAT), 'last_updated_by' => $userGuid);
                $result = $this->User_model->updateImangeInfo($exists[0]['guid'], $update);
                if (file_exists(dirname($_SERVER["SCRIPT_FILENAME"]) . "/assets/upload_images/" . $_REQUEST['id'])) {
                    unlink(ASSETPATH . 'upload_images/' . $_REQUEST['id']);
                }
            }

            if ($result) {
                $response = array('msg' => $this->lang->line('success_msg'));
                if (!empty($exists[0]['active'])) {
                    $response = array('msg' => $this->lang->line("please_set_main_profile_picture"));
                }
            }
        }
        echo json_encode($response);
    }

    /**
     * Here cancle profile shortlist request info only
     */
    public function cancleprofilerequestinfo() {
        $userGuid = $this->session->userdata['userguid'];
        $response = array('msg' => $this->lang->line("failure_msg"));
        $result = 0;
        if (isset($_REQUEST['id']) && isset($_REQUEST['pid']) && !empty($_REQUEST['id']) && !empty($_REQUEST['pid'])) {
            /** get profile request details based on sessionuserguid , profileid and activity id */
            $exists = $this->User_model->ProfileRequestInfo($userGuid, $_REQUEST['pid'], '', $_REQUEST['id']);

            if (!empty($exists[0]['guid'])) {
                $update = array('deleted' => 1, 'last_updated' => date(DATE_TIME_FORMAT), 'last_updated_by' => $userGuid);
                $result = $this->User_model->updateProfileRequestInfo($exists[0]['guid'], $update);
            }

            if ($result) {
                $response = array('msg' => $this->lang->line("success_msg"));
            }
        }
        echo json_encode($response);
    }

    public function activeprofileimage() {
        $userGuid = $this->session->userdata['userguid'];
        $response = array('msg' => $this->lang->line("failure_msg"));
        $result = 0;
        if (isset($_REQUEST['id']) && isset($_REQUEST['pid']) && !empty($_REQUEST['id']) && !empty($_REQUEST['pid'])) {

            $exists = $this->User_model->getUserImageInfo($_REQUEST['pid'], '', '', $_REQUEST['id']);
            if ($exists[0]['guid']) {
                $update = array('active' => 0, 'last_updated' => date(DATE_TIME_FORMAT), 'last_updated_by' => $userGuid);
                $up = $this->User_model->updateImangeInfo('', $update, $_REQUEST['pid']);

                $update = array('active' => 1, 'last_updated' => date(DATE_TIME_FORMAT), 'last_updated_by' => $userGuid);
                $result = $this->User_model->updateImangeInfo($exists[0]['guid'], $update);
            }

            if ($result) {
                $data['image'] = '';
                $this->session->unset_userdata($data);
                $data['image'] = $_REQUEST['id'];
                $this->session->set_userdata($data);
                $response = array('msg' => $this->lang->line("success_msg"));
            }
        }
        echo json_encode($response);
        exit;
    }

    public function sendmail() {
        $userGuid = $this->session->userdata['userguid'];
        $response = array('msg' => $this->lang->line("failure_msg"));
        if (!empty($_REQUEST['userguid']) && $_REQUEST['message']) {
            $this->db->trans_begin();
            $data = array(
                'mail_from' => $userGuid,
                'mail_to' => $_REQUEST['userguid'],
                'message' => $_REQUEST['message'],
                'created' => date(DATE_TIME_FORMAT),
                'created_by' => $userGuid,
                'last_updated' => date(DATE_TIME_FORMAT),
                'last_updated_by' => $userGuid
            );

            $result = $this->Profilemanager_model->insertOrUpdateProfileRequestInfo($userGuid, $_REQUEST['userguid'], REQUEST_SEND);
            $insert = $this->Mail->insertMailInfo($data);

            if (!empty($result) && !empty($insert)) {
                $this->db->trans_commit();

                $senderInfo = $this->Profilemanager_model->getManagedUserInformation($userGuid);
                $username = (!empty($senderInfo[$userGuid]['username']) ? ucwords($senderInfo[$userGuid]['username']) : 'unknown');
                $userId = (!empty($senderInfo[$userGuid]['userId']) ? ucwords($senderInfo[$userGuid]['userId']) : 'unknown');
                $receiverInfo = $this->User_model->getUserAndRoleDetails($_REQUEST['userguid']);

                if (isset($senderInfo[$userGuid]) && !empty($senderInfo[$userGuid])) {
                    $userinfo = $senderInfo[$userGuid];
                    $partnerName = (!empty($receiverInfo[0]['username']) ? $receiverInfo[0]['username'] : 'unknown');
                    $userinfo['partnername'] = $partnerName;
                    $userinfo['partnerid'] = (!empty($receiverInfo[0]['userId']) ? $receiverInfo[0]['userId'] : 'unknown');
                    /** mail send to user */
                    $subject = sprintf($this->lang->line("mail_send_sub"), ucwords($username));
                    $body = $this->load->view('template/mail/sendmail.php', $userinfo, TRUE);
                    if (MAIL_ENABLE) {
                        $exists = $this->Mail->sendmail('', $receiverInfo[0]['email'], $subject, $body, $attachment = '');
                    }
                    /** message send to user */
                    if (MESSAGE_ENABLE) {
                        if (!empty($receiverInfo[0]['mobile'])) {
                            $phone = '9025940063'; //$receiverInfo[0]['mobile'];
                            $msg = rawurlencode(sprintf($this->lang->line("sms_text_sub"), $subject));
                            $sendsms = $this->Mail->indiaSms($phone, $msg);
                        }
                    }
                }
                $response = array('msg' => $this->lang->line("success_msg"));
            } else {
                $this->db->trans_rollback();
            }
        }
        echo json_encode($response);
        exit;
    }

}
