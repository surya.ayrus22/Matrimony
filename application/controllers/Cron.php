<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
error_reporting(0);

class Cron extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->model('User_model', '', TRUE);
        $this->load->model('Prefrence_model', '', TRUE);
        $this->load->model('Profilemanager_model', '', TRUE);
        $this->load->model('Utils', '', TRUE);
    }

    /**
     * cron run every 12 hours or 24hours once
     */
    function index() {

        /** To set cron running */
        echo 'cron running';

        /** get over all users */
        $userInfo = $this->User_model->getUserinformation();
        if (!empty($userInfo)) {
            foreach ($userInfo as $userData) {
                $userguid = $userData['userGuid'];
                /** get user prefrence details based on all users */
                $partnerInfo = $this->User_model->getUserPartnerInfo($userguid);
                if (!empty($partnerInfo) && !empty($userData['gender'])) {
                    $prefrence = (!empty($partnerInfo[0]['prefrence']) ? json_decode($partnerInfo[0]['prefrence'], true) : '');
                    if (!empty($prefrence)) {
                        $gender = 'M';
                        if (strtoupper($userData['gender']) == 'M') {
                            $gender = 'F';
                        }
                        /** get block and ignore user list */
                        $id = array(REQUEST_IGNORE, REQUEST_NOT_INETREST, REQUEST_BLOCK);
                        /*                         * get login user blocked user list  (Me blocked from others) * */
                        $exists = $this->User_model->ProfileRequestInfo($userguid, '', '', $id);
                        /*                         * Any one blocked from login user list (Others blocked from me) * */
                        $exists1 = $this->User_model->ProfileRequestInfo('', $userguid, '', $id);

                        $userList = array();
                        $blockList = array();
                        $otherBlockList = array();
                        if ($exists) {
                            foreach ($exists as $user) {
                                $blockList[] = $user['request_to'];
                            }
                        }

                        if ($exists1) {
                            foreach ($exists1 as $user) {
                                $otherBlockList[] = $user['request_from'];
                            }
                        }

                        $userList = array_merge($blockList, $otherBlockList);

                        $recentExists = $this->User_model->getRecentlyUpdatedProfile('', $caste);
                        $recentList = array();
                        if (!empty($recentExists)) {
                            foreach ($recentExists as $user1) {
                                $recentList[] = $user1['user_guid'];
                            }
                        }

                        $limits = 5;
                        $recentUserList = array();
                        $recentdata = array();
                        $similardata = array();
                        $recentUpdated = array('maritalstatus' => $prefrence['maritalstatus'], 'caste' => $prefrence['caste'], 'eduction' => $prefrence['education']);
                        /*                         * recently updated profile */
                        $recent = $this->Prefrence_model->getUserPrefrenceDetails($recentUpdated, $gender, $userList, $recentList, $limits);
                        if (!empty($recent)) {
                            foreach ($recent as $key => $data) {
                                $recentdata[] = $data['userGuid'];
                            }

                            if (!empty($recentdata)) {
                                $recentUserList['matches'] = $this->Profilemanager_model->getUserInformation($recentdata);
                            }

                            $recentCount = (!empty($recentUserList['matches']) ? count($recentUserList['matches']) : 0);
                            if ($recentCount < 5) {
                                /** similar matches */
                                $similar = array('maritalstatus' => $prefrence['maritalstatus'], 'caste' => $prefrence['caste']);
                                $simiarLimits = $limits - $recentCount;
                                $similarUserList = $this->Prefrence_model->getUserPrefrenceDetails($similar, $gender, $userList, '', $simiarLimits);
                                if (!empty($similarUserList)) {
                                    foreach ($similarUserList as $key => $val) {
                                        $similardata[] = $val['userGuid'];
                                    }
                                    if (!empty($similardata)) {
                                        $recentUserList['similar'] = $this->Profilemanager_model->getUserInformation($similardata);
                                    }
                                }
                            }
                        }
                        /* if ( !empty( $recentUserList ) && !empty( $userData['email'] )){
                          $recentUserList['username'] = $userData['username'];
                          $recentUserList['userId']	= $userData['userId'];
                          $recentUserList['title']	= $this->lang->line("mail_matches_title1");
                          $recentUserList['title1']	= $this->lang->line("mail_matches_title2");
                          $subject	= sprintf($this->lang->line("mail_matches_sub1"), $userData['username']);
                          $body 		= $this->load->view('template/mail/matches.php',$recentUserList,TRUE);
                          echo '<pre>';print_r($body);exit;
                          $sendmail	= $this->Mail->sendmail( '', $userData['email'], $subject, $body ,$attachment ='' );
                          } */


                        $similarUserInfoList = array();
                        /** similar matches */
                        $newlyAdded = array('maritalstatus' => $prefrence['maritalstatus'], 'caste' => $prefrence['caste']);

                        $similarmatches = $this->Prefrence_model->getUserPrefrenceDetails($newlyAdded, $gender, $userList, '', $limits);

                        if (!empty($similarmatches)) {
                            foreach ($similarmatches as $key => $data1) {
                                $similarGuid[] = $data1['userGuid'];
                            }
                            if (!empty($similarGuid)) {
                                $similarUserInfoList['similar'] = $this->Profilemanager_model->getUserInformation($similarGuid);
                            }
                        }
                        /*
                          if ( !empty( $similarUserInfoList ) && !empty( $userData['email'] )){
                          $similarUserInfoList['username'] = $userData['username'];
                          $similarUserInfoList['userId']	= $userData['userId'];
                          $similarUserInfoList['title1']	= $this->lang->line("mail_matches_title2");
                          $subject	= sprintf($this->lang->line("mail_matches_sub2"), $userData['username']);
                          $body 		= $this->load->view('template/mail/matches.php',$similarUserInfoList,TRUE);
                          echo '<pre>';print_r($body);exit;
                          $sendmail	= $this->Mail->sendmail( '', $userData['email'], $subject, $body ,$attachment ='' );
                          } */

                        $newmatchesUserList = array();
                        $mathesdata = array();
                        $newMatchesGuid = array();
                        /** newly added maches */
                        $newmatches = $this->Prefrence_model->getUserPrefrenceDetails($prefrence, $gender, $userList, '', $limits);

                        if (!empty($newmatches)) {
                            foreach ($newmatches as $key => $data2) {
                                $newMatchesGuid[] = $data2['userGuid'];
                            }
                            if (!empty($newMatchesGuid)) {
                                $newmatchesUserList['matches'] = $this->Profilemanager_model->getUserInformation($newMatchesGuid);
                            }
                            $matchesCount = (!empty($newmatchesUserList['matches']) ? count($newmatchesUserList['matches']) : 0);
                            if (($matchesCount < 5) && ( $matchesCount != 0 )) {
                                /** similar matches */
                                $newmatches_similar = array('maritalstatus' => $prefrence['maritalstatus'], 'caste' => $prefrence['caste']);
                                $newimits = $limits - $matchesCount;
                                $matchesUserList = $this->Prefrence_model->getUserPrefrenceDetails($newmatches_similar, $gender, $userList, '', $newimits);
                                if (!empty($matchesUserList)) {
                                    foreach ($matchesUserList as $key => $val1) {
                                        $mathesdata[] = $val1['userGuid'];
                                    }
                                    if (!empty($mathesdata)) {
                                        $newmatchesUserList['similar'] = $this->Profilemanager_model->getUserInformation($mathesdata);
                                    }
                                }
                            }
                        }
                        /* if ( !empty( $newmatchesUserList ) && !empty( $userData['email'] )){
                          $newmatchesUserList['username'] = $userData['username'];
                          $newmatchesUserList['userId']	= $userData['userId'];
                          $newmatchesUserList['title']	= $this->lang->line("mail_matches_title3");
                          $newmatchesUserList['title1']	= $this->lang->line("mail_matches_title2");
                          $subject	= sprintf($this->lang->line("mail_matches_sub4"), $userData['username']);
                          $body 		= $this->load->view('template/mail/matches.php',$newmatchesUserList,TRUE);
                          $sendmail	= $this->Mail->sendmail( '', $userData['email'], $subject, $body ,$attachment ='' );
                          } */


                        /** viewed user list */
                        $viewedUserList = array();
                        $viewedList = array();
                        $viewedInfo = $this->User_model->viwedProfileInfo('', $userguid);

                        if (!empty($viewedInfo)) {
                            $viewedList = array();
                            foreach ($viewedInfo as $key => $value) {
                                $viewedList[] = $value['viewed_from'];
                            }
                        }
                        /** removed ignore user list and get viewed user list * */
                        $userGuidList = array();
                        if (!empty($viewedList)) {
                            $userGuidList = array_diff($viewedList, $userList);
                        }
                        $getUserList = array();
                        if (!empty($userGuidList)) {
                            $viewedUserList['matches'] = $this->Profilemanager_model->getUserInformation($userGuidList, $gender, $limits);
                        }

                        /* if ( !empty( $viewedUserList ) && !empty( $userData['email'] )){
                          $viewedUserList['username']= $userData['username'];
                          $viewedUserList['userId']	= $userData['userId'];
                          $viewedUserList['title']	= $this->lang->line("mail_matches_title4");
                          $subject	= sprintf($this->lang->line("mail_matches_sub3"), $userData['username']);
                          $body 		= $this->load->view('template/mail/matches.php',$viewedUserList,TRUE);
                          $sendmail	= $this->Mail->sendmail( '', $userData['email'], $subject, $body ,$attachment ='' );
                          } */
                    }
                }
            }
        }
        die('Cron completed');
        /** To set cron executed */
    }

}

?>
