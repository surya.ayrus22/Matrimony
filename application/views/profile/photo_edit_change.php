<link href="<?php echo base_url(); ?>assets/css/photo_edit.css" media="all" rel="stylesheet" type="text/css" />
<script src="<?php echo base_url(); ?>assets/js/fileinput.js" type="text/javascript"></script>

<style>
    .file-input.file-input-ajax-new {
        display: none;
    }
</style>
<script>
    function modalshow(id) {
        $('#' + id).popModal({
            html: $('#content1'),
            placement: 'bottomRight',
            showCloseBut: true,
            onDocumentClickClose: true,
            onDocumentClickClosePrevent: '',
            overflowContent: false,
            inline: true,
            asMenu: false,
            beforeLoadingContent: 'Please, wait...',
            onOkBut: function () {
            },
            onCancelBut: function () {
            },
            onLoad: function () {
            },
            onClose: function () {
            }
        });
    }

</script>
<div style="display:none">
    <div id="content1">
        <div class="pointer texthover" >
            <span class="set_text conventxt"><?php echo $this->lang->line('main_photo'); ?></span>
        </div>
        <div class="pointer texthover" >
            <span class="set_text conventxt"><?php echo $this->lang->line('delete'); ?></span>
        </div>
    </div>
</div>

<div class="container photo_change">
    <div class="add btn" >

        <span id="upfile1" class="btn btn-warning subbtn" type="submit" style="float:right;">
            <i class="fa fa-desktop" ></i><?php echo $this->lang->line('add_photo'); ?>
        </span>
    </div>
    </br></br>
    <input id="images" type="file" name="upfile" multiple class="file" data-overwrite-initial="false" data-min-file-count="1">
    <div class="col-md-12 bor_image">
        <div class="browser_form">

            <div class="col-md-12 photo_border">
                <h2 class="registertitle"><?php echo $this->lang->line('photo_change_title'); ?></h2>
                <!--row-->
                <div class="row pho">
                    <?php
                    //echo "<pre>";print_r($profile_images); 
                    if (!empty($profile_images)) {
                        $i = 1;
                        foreach ($profile_images as $value) {
                            if (!empty($value['image'])) {
                                ?>
                                <div class="col-md-3 row  pho alg">
                                    <div class="proimg_border">
                                        <div class="page active">
                                            <a title="Click to add main photo" onclick="activeimage('<?php echo $userguid; ?>', '<?php echo $value['image']; ?>')" class="fa fa-check-circle-o fa-short" style="float:right;"></a>
                                        </div>
                                        <div class="page active" >
                                            <a title="Delete" onclick="deleteimage('<?php echo $userguid; ?>', '<?php echo $value['image']; ?>')" class="fa fa-trash fa-short" style="float:right;"></a>
                                        </div>
                                        <?php
                                        $image = '';
                                        if (file_exists(dirname($_SERVER["SCRIPT_FILENAME"]) . "/assets/upload_images/" . $value['image'])) {
                                            $image = $value['image'];
                                        }
                                        ?>
                                        <img src="<?php echo base_url() . 'assets/upload_images/' . $image ?>" class="img_1" alt=""/>
                                        <?php if ($value['active'] == 1) { ?>
                                            <div class="main subbtn"><?php echo $this->lang->line('main_photo'); ?></div>
                                        <?php } ?>
                                    </div>
                                </div>
                                <?php
                            } $i++;
                        }
                    }
                    ?>
                </div>
                <!--row end-->
            </div>
        </div>
    </div>
</div>


<input type="hidden" id="baseurl" value="<?php echo base_url(); ?>">
<input type="hidden" id="userguid" value="<?php echo $userguid; ?>">
<script>
    var baseurl = $("#baseurl").val();
    var userguid = $("#userguid").val();
    $("#images").fileinput({
        uploadUrl: baseurl + 'index.php/profile/fileupload?uid=' + userguid, // you must set a valid URL here else you will get an error
        allowedFileExtensions: ['jpg', 'png', 'jpeg'],
        overwriteInitial: false,
        maxFileSize: 1500,
        maxFilesNum: 5,
        //allowedFileTypes: ['image', 'video', 'flash'],
        slugCallback: function (filename) {
            return filename.replace('(', '_').replace(']', '_');
        }
    });

    $("#upfile1").click(function () {
        $("#images").trigger('click');
        //$(".fileinput-upload-button").hide();

    });

    function deleteimage(pid, image) {
        //alert(pid);
        //alert(image);
        if (pid == 0 || pid == '' || image == '') {
            return false;
        }
        //$('#response').show();
        $.post(baseurl + 'index.php/profile/removeprofileimage?pid=' + pid + '&id=' + image,
                function (data) {
                    alert(data.msg);
                    location.reload();
                }, "json")
                .fail(function () {
                    alert("<?php echo $this->lang->line("connection_error"); ?>");
                    location.reload();
                });
    }
    function activeimage(pid, image) {
        //alert(pid);
        //alert(image);
        if (pid == 0 || pid == '' || image == '') {
            return false;
        }
        //$('#response').show();
        $.post(baseurl + 'index.php/profile/activeprofileimage?pid=' + pid + '&id=' + image,
                function (data) {
                    alert(data.msg);
                    location.reload();
                }, "json")
                .fail(function () {
                    alert("<?php echo $this->lang->line("connection_error"); ?>");
                    location.reload();
                });
    }

    $(document).ready(function () {
        /** overall upload button hide */
        $(".fileinput-upload-button").hide();
    });
</script>


