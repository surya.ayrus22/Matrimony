

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<link href="<?php echo base_url(); ?>/assets/css/sendmail.css" rel='stylesheet' type='text/css' />
<script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/pagination/js/jquery.pajinate.js"></script>
<script>
    $(document).ready(function () {
        $('#paging_container_viewed_profile').pajinate({
            num_page_links_to_display: 3,
            items_per_page: 4
        });
    });

    $(function () {
        $(".modal-closepaid").on('click', function () {
            $('#modalpaid').hide();
        });

        $("#mail_close").on('click', function () {
            $('#modalpaid').hide();
        });
        $("#close_modal").on('click', function () {
            $('#modal').hide();
        });
        $("#close").on('click', function () {
            $('#response').hide();
        });
    });

</script>

<link href="<?php echo base_url(); ?>/assets/css/slider.css" rel='stylesheet' type='text/css' />
<!--  Modal Start -->
<div id="modal" class="modal">
    <div class="modal-dialog animated">
        <div class="modal-content">
            <div class="modal-header">
                <strong><?php echo $this->lang->line('register_profile_for_process2'); ?></strong>
            </div>
            <div class="modal-body" id="modal-body">

            </div>
            <div class="modal-footer">
                <button class="btn btn-default" type="button" id="close_modal"><?php echo $this->lang->line('close_text'); ?></button>
            </div>
        </div>
    </div>
</div>
<!--  Modal End -->

<!--sendmailpaid -->
<div id="modalpaid" class="modal">
    <div class="modal-dialog animated sample" style=" margin-top: 9%;width: 440px;">
        <div class="row modal-contentpaid">
            <div class="sendhead">
                <span style="float:left;"></span>
                <span><h3 class="subtitle" style="color: #7e7e7e;"><?php echo sprintf($this->lang->line('to_text'), $this->lang->line('profile_send_mail')); ?></h3></span>
                <span class="modal-closepaid shtclose" ><a href="#"><img src="<?php echo base_url(); ?>assets/images/m/forgot-password-close.gif"></a></span>
            </div>

            <div id="paid_mail">
                <div id='loading' class="loader" style='display: block; text-align: center; margin: 100px;'>
                    <img src="<?php echo base_url(); ?>assets/images/loader.gif"/>
                </div>
            </div>
            <div id="send_mail" style="display:none;">
                <form id="mail">
                    <div class="form-group row">
                        <div class="col-sm-10 short-text">
                            <textarea class="form-control" id="message" name="message" rows="6" maxlength="250" minlength="50" required></textarea>
                        </div>
                        <input type="hidden" name="userguid" id="uid" value="">
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-12">
                            <div class="col-sm-2 pull-right">
                                <span class="btn btn-warning" id="mail_close"><?php echo $this->lang->line('cancel_admin_register'); ?></span>
                            </div>
                            <div class="col-sm-2 pull-right">
                                <button class="btn btn-primary" type="submit"><?php echo $this->lang->line('register_profile_for_submit'); ?></button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!--sendemailendpaid --><!--Interest msg-->
<div id="response" class="modal">
    <div class="modal-dialog animated sample" style=" margin-top: 9%;width: 440px;">
        <div class="modal-contentshort">
            <div class="row">
                <div>
                    <span style="float:left;"></span>
                    <span><h3 class="subtitle"><?php echo $this->lang->line('profile_request_send'); ?></h3></span>
                    <span class="modal-closeshort shtclose" id="close" ><a href="#"><img src="<?php echo base_url(); ?>assets/images/m/forgot-password-close.gif"></a></span>
                </div>
                <div class="form-group row">
                    <div class="col-sm-10 short-text" id="msg_body" style="text-align:center;">
                        <div id='loader' style='display:block'>
                            <img src="<?php echo base_url(); ?>assets/images/loading.gif"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--sInterest msg end-->
<div class="grid_3">
    <div class="container">
        <div class="breadcrumb1">
            <ul>
                <a href="<?php echo base_url(); ?>index.php/matches/view/"><i class="fa fa-home home_1"></i></a>
                <span class="divider">&nbsp;|&nbsp;</span>
                <li class="current-page"><?php echo $this->lang->line('viewed_profile_title'); ?> (<?php echo (!empty($viewed) ? count($viewed) : 0) ?>)</li>
            </ul>
        </div>
        <div class="col-md-3 col_5">
            <img src="<?php echo base_url() . 'assets/upload_images/' . $image; ?>" class="img-responsive adj" alt=""/>
            <?php if ($offer == 2) { ?>
                <div class="pay left sendleft">
                    <div class="leftpay">
                        <span class="leftpaytxt" style="color:#fff;"><?php echo $this->lang->line("payment_text"); ?> </span>
                    </div>	
                    <div class=" leftpay pointer btnlink" >
                        <span class="btnlinkalign">
                            <a href="<?php echo base_url(); ?>index.php/payment"><?php echo $this->lang->line('payment'); ?></a></span>
                    </div>
                </div>
            <?php } ?>
        </div>
        <div class="col-md-9 profile_left">
            <div class="col_4">
                <div class="bs-example bs-example-tabs" role="tabpanel" data-example-id="togglable-tabs">
                    <ul id="myTab" class="nav nav-tabs nav-tabs1" role="tablist">
                        <li role="presentation" class="active"><a href="#home" id="home-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="true"><?php echo $this->lang->line('member_view_text'); ?></a></li>
                        <!--  <li role="presentation"><a href="#profile" role="tab" id="profile-tab" data-toggle="tab" aria-controls="profile">Privacy Settings</a></li>-->
                    </ul>
                    <div id="myTabContent" class="tab-content">
                        <div role="tabpanel" class="tab-pane fade in active" id="home" aria-labelledby="home-tab">
                            <!-- <div class="tab_box tab_box1">
                              <h1>Sed ut perspiciatis unde omnis iste natus error sit voluptatem</h1>
                              <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution</p>
                            </div>-->

                            <div id="paging_container_viewed_profile">
                                <?php if (!empty($viewed)) { ?>
                                    <div class="page_navigation pagination pagination_1"></div>
                                    <div class="clearfix"> </div>
                                    <?php foreach ($viewed as $key => $value) { ?>
                                        <div class="content">
                                            <div class="jobs-item with-thumb">
                                                <div class="thumb_top">

                                                    <div class="thumb"><a href="#" onclick="viewed('<?php echo $value['userGuid']; ?>')"><img src="<?php echo base_url() . 'assets/upload_images/' . $value['image'] ?>" class="img-responsive" alt=""/></a></div>
                                                    <div class="jobs_right">
                                                        <h6 class="title"><?php echo $value['username']; ?> (<?php echo constant('MEMBERID') . $value['userId'] ?>)</h6>
                                                        <ul class="login_details1">
                                                            <li><?php echo sprintf($this->lang->line('last_text'), $this->lang->line('text_login')); ?> : <?php echo (!empty($value['log_in']) ? $value['log_in'] : 'Not login') ?></li>
                                                        </ul>
                                                        <p class="description"><?php echo (!empty($value['age']) ? $value['age'] . ' years,' : 'not-specified') . ' ' . (!empty($value['height']) ? $value['height'] . ' Cms,' : 'not-specified') ?> | <span class="m_1"><?php echo $this->lang->line('register_profile_for_gender'); ?></span> : <?php echo (!empty($value['gender']) ? constant("GENDER_" . strtoupper($value['gender'])) : 'not-specified'); ?> | <span class="m_1"><?php echo $this->lang->line('register_profile_for_religion'); ?></span> : <?php echo (!empty($value['religion']) ? $value['religion'] : 'not-specified'); ?> | <span class="m_1"><?php echo $this->lang->line('search_education_title'); ?></span> : <?php echo (!empty($value['edu_name']) ? $value['edu_name'] : 'not-specified') . ' - ' . (!empty($value['qualification']) ? $value['qualification'] : 'not-specified') ?> | <span class="m_1"><?php echo $this->lang->line('register_profile_for_occupation'); ?></span> : <?php echo (!empty($value['occupation']) ? $value['occupation'] : 'not-specified') ?><br><a  class="m_3" href="<?php echo base_url() . 'index.php/profile/view?pid=' . $value['userGuid'] ?>"><?php echo $this->lang->line('text_view_profile'); ?></a></p>
                                                    </div>
                                                    <div class="clearfix"> </div>
                                                </div>
                                                <div class="thumb_bottom">
                                                    <?php
                                                    if (!empty($value['image_request'])) {
                                                        if (!empty($value['imagerequest'])) {
                                                            ?>
                                                            <div class="vertical  photo_view "><?php echo $this->lang->line('profile_request_send'); ?></div>
                                                        <?php } else { ?>
                                                            <div class="vertical pointer photo_view "  onclick="requestProfile('<?php echo $value['userGuid']; ?>', '<?php echo REQUEST_IMAGE; ?>')"><?php echo $this->lang->line('profile_image_request'); ?></div>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                    <div class="thumb_but"> 
                                                        <div class="vertical pointer photo_view"  onclick="sendmail('<?php echo $value['userGuid']; ?>', '<?php echo $offer; ?>')"><?php echo $this->lang->line('profile_send_mail'); ?></div>
                                                        <?php
                                                        if (!empty($value['interest'])) {
                                                            if (($value['interest'] == REQUEST_INTEREST) || ($value['interest'] == REQUEST_ARRANGED)) {
                                                                ?>
                                                                <div class="vertical photo_view"><?php echo $value['interest_name']; ?></div>
                                                            <?php } else { ?>
                                                                <div class="vertical pointer photo_view" onclick="requestProfile('<?php echo $value['userGuid']; ?>', '<?php echo $value['interest']; ?>')"><?php echo $value['interest_name']; ?></div>
                                                            <?php } ?>
                                                        <?php } else { ?>
                                                            <div class="vertical pointer photo_view" onclick="requestProfile('<?php echo $value['userGuid']; ?>', '<?php echo REQUEST_INTEREST; ?>')"><?php echo $this->lang->line('profile_send_interest'); ?></div>
                                                        <?php } if (!empty($value['shortlist'])) { ?>
                                                            <div class="vertical pointer photo_view"  onclick="requestcancel('<?php echo $value['userGuid']; ?>', '<?php echo REQUEST_SHORTLIST; ?>')"><?php echo $this->lang->line('shortlished_text'); ?></div>
                                                        <?php } else { ?>
                                                            <div class="vertical pointer photo_view"  onclick="requestProfile('<?php echo $value['userGuid']; ?>', '<?php echo REQUEST_SHORTLIST; ?>')"><?php echo sprintf($this->lang->line('profile_shortlist'), ''); ?></div>
                                                        <?php } ?>
                                                    </div>
                                                    <div class="clearfix"> </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <?php
                                } else {
                                    echo '<h4 style="text-align:center;">' . $this->lang->line("no_matches_text") . '</h4>';
                                }
                                ?>
                            </div>
                        </div>

                        <!-- <div role="tabpanel" class="tab-pane fade" id="profile" aria-labelledby="profile-tab">
                           <div class="terms">
                 <h2>Your Privacy Settings</h2>
                                  <div class="terms_top">
                                       <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English.</p>
                                       <ol class="terms_list">
                                               <li>The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum".</li>
                                               <li>Lusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati.</li>
                                               <li>Praesentium voluptatum deleniti atque corrupti quos</li>
                                               <li>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi.</li>
                                               <li>Mentum eleifend enim a feugiat distinctio lor</li>
                                       </ol>
                                       <h4>There are many variations of passages</h4>
                                       <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut non libero magna. Sed et quam lacus. Fusce condimentum eleifend enim a feugiat. Pellentesque viverra vehicula sem ut volutpat. Integer sed arcu. At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut non <a href="#">libero consectetur adipiscing</a> elit magna. Sed et quam lacus. Fusce condimentum eleifend enim a feugiat. Pellentesque viverra vehicula sem ut volutpat. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut non libero magna. Sed et quam lacus. Fusce condimentum eleifend enim a feugiat.</p>
                                       <p><strong>Iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non sit amet, consectetur adipiscing elit. Ut adipiscing elit magna. Sed et quam lacus. Fusce condimentum eleifend enim a feugiat. Pellentesque lorem ipsum dolor sit amet. Ut non libero magna. Sed et quam lacus. Fusce condimentum eleifend enim a feugiat <a href="#">consectetur adipiscing elit</a>.</strong></p>
                                 </div> 	
                  </div>
                        </div>-->
                    </div> 
                </div>
            </div>
        </div>
        <div class="clearfix"> </div>
    </div>
</div>
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script>
                                                                $(document).ready(function () {
                                                                    $("#mail").validate({
                                                                        submitHandler: function (form) {
                                                                            var dataString = $('#mail').serialize();
                                                                            if (dataString == '' || dataString == null || dataString == 0) {
                                                                                return false;
                                                                            }
                                                                            $('#send_mail').hide()
                                                                            $('#paid_mail').show();
                                                                            $.ajax({
                                                                                type: 'POST',
                                                                                url: baseurl + 'index.php/profile/sendmail',
                                                                                data: dataString,
                                                                                dataType: "json",
                                                                                success: function (data) {
                                                                                    $('#loading').hide();
                                                                                    if (data.msg == 'success') {
                                                                                        $('#paid_mail').html('<h4 style="text-align:center; color:green;"> <?php echo $this->lang->line("mail_send_successfully_text"); ?> </h4>');
                                                                                    } else {
                                                                                        $('#paid_mail').html('<h4 style="text-align:center; color:red;">' + data.msg + '</h4>');
                                                                                    }
                                                                                    setTimeout(function () {
                                                                                        $('#modalpaid').hide();
                                                                                        location.reload();
                                                                                    }, 2000);
                                                                                },
                                                                                error: function (jqXHR, textStatus, errorThrown) {
                                                                                    console.log(textStatus, errorThrown);
                                                                                    $('#paid_mail').html('<h4 style="text-align:center; color:green;"> <?php echo $this->lang->line("error_occur_contact_admin"); ?></h4>');
                                                                                    setTimeout(function () {
                                                                                        $('#modalpaid').hide();
                                                                                        location.reload();
                                                                                    }, 2000);
                                                                                }
                                                                            });
                                                                        }
                                                                    });

                                                                    $("#message").rules("add", {
                                                                        required: true,
                                                                        minlength: 50,
                                                                        maxlength: 250,
                                                                        messages: {
                                                                            required: "Please enter the text",
                                                                            minlength: jQuery.validator.format("Please, at least {0} characters are necessary"),
                                                                            maxlength: jQuery.validator.format("Please, at maximum {0} characters are allowed")
                                                                        }
                                                                    });

                                                                });

                                                                function requestProfile(pid, id) {
                                                                    if (pid == 0 || id == 0 || pid == '' || id == '') {
                                                                        return false;
                                                                    }
                                                                    $('#response').show();
                                                                    $.post(baseurl + 'index.php/profile/updateprofilerequestinfo?pid=' + pid + '&id=' + id,
                                                                            function (data) {
                                                                                if (data.msg == 'success') {
                                                                                    $('#msg_body').html('<h4 style="text-align:center; color:green;">' + data.msg + '</h4>');
                                                                                } else {
                                                                                    $('#msg_body').html('<h4 style="text-align:center; color:red;">' + data.msg + '</h4>');
                                                                                }
                                                                                setTimeout(function () {
                                                                                    $('#response').hide();
                                                                                    location.reload();
                                                                                }, 2000);
                                                                            }, "json")
                                                                            .fail(function () {
                                                                                $('#msg_body').html('<h4 style="text-align:center; color:green;"><?php echo $this->lang->line("error_occur_contact_admin"); ?></h4>');
                                                                                setTimeout(function () {
                                                                                    $('#response').hide();
                                                                                    location.reload();
                                                                                }, 2000);
                                                                            });
                                                                }

                                                                function requestcancel(pid, id) {
                                                                    if (pid == 0 || id == 0 || pid == '' || id == '') {
                                                                        return false;
                                                                    }
                                                                    var reqdata = "'" + pid + "'" + ',' + "'" + id + "'";
                                                                    var htmldata = '<span class ="col-sm-10"><?php echo $this->lang->line("are_you_sure_the _shorlisted"); ?></span>';
                                                                    htmldata1 = '<div class="buttons pull-right col-sm-3">';
                                                                    htmldata2 = '<div onclick="requestcancelinfo(' + reqdata + ')" class="vertical pointer"><?php echo $this->lang->line('yes'); ?></div>';
                                                                    htmldata3 = '</div><div class="buttons pull-right col-sm-2">';
                                                                    htmldata4 = '<div onclick="cancle()" class="vertical pointer"><?php echo $this->lang->line('no'); ?></div>';
                                                                    htmldata5 = '</div>';
                                                                    $('#response').show();
                                                                    $('#msg_body').html(htmldata + htmldata1 + htmldata2 + htmldata3 + htmldata4 + htmldata5);
                                                                }

                                                                function requestcancelinfo(pid, id) {
                                                                    if (pid == 0 || id == 0 || pid == '' || id == '') {
                                                                        return false;
                                                                    }
                                                                    $.post(baseurl + 'index.php/profile/cancleprofilerequestinfo?pid=' + pid + '&id=' + id,
                                                                            function (data) {
                                                                                if (data.msg == 'success') {
                                                                                    $('#msg_body').html('<h4 style="text-align:center; color:green;">' + data.msg + '</h4>');
                                                                                } else {
                                                                                    $('#msg_body').html('<h4 style="text-align:center; color:red;">' + data.msg + '</h4>');
                                                                                }
                                                                                setTimeout(function () {
                                                                                    $('#response').hide();
                                                                                    location.reload();
                                                                                }, 3000);
                                                                            }, "json")
                                                                            .fail(function () {
                                                                                $('#msg_body').html('<h4 style="text-align:center; color:green;"> <?php echo $this->lang->line("error_occur_contact_admin"); ?></h4>');
                                                                                setTimeout(function () {
                                                                                    $('#response').hide();
                                                                                    location.reload();
                                                                                }, 2000);
                                                                            });
                                                                }

                                                                function cancle() {
                                                                    $('#response').hide();
                                                                }

                                                                function sendmail(uid, offerid) {
                                                                    if (uid == 0 || offerid == 0 || uid == '' || offerid == '') {
                                                                        return false;
                                                                    }

                                                                    if (offerid == 2) { //need to show paid view
                                                                        $('#modalpaid').show();
                                                                        $('#loading').show();
                                                                        $.post(baseurl + 'index.php/profile/sendmailinfo?uid=' + uid,
                                                                                function (data) {
                                                                                    $('#loading').hide();
                                                                                    $('#paid_mail').show();
                                                                                    $('#paid_mail').html(data);

                                                                                }, "html")
                                                                                .fail(function () {
                                                                                    $('#paid_mail').show();
                                                                                    $('#paid_mail').html('<h4 style="text-align:center; color:green;"> <?php echo $this->lang->line("error_occur_contact_admin"); ?></h4>');
                                                                                    setTimeout(function () {
                                                                                        $('#modalpaid').hide();
                                                                                        location.reload();
                                                                                    }, 2000);
                                                                                });
                                                                    } else {//need to show send mail view
                                                                        if (offerid == 1) {
                                                                            $('#modalpaid').show();
                                                                            $('#paid_mail').hide();
                                                                            $('#send_mail').show();
                                                                            $('#uid').val(uid);


                                                                        }
                                                                    }
                                                                }

                                                                function viewed(reqData) {
                                                                    $.ajax({
                                                                        type: "POST",
                                                                        url: baseurl + 'index.php/matches/modaldata?uid=' + reqData,
                                                                        success: function (data) {
                                                                            $("#modal").show();
                                                                            $("#modal-body").html(data);
                                                                        },
                                                                        error: function (jqXHR, textStatus, errorThrown) {
                                                                            console.log(textStatus, errorThrown);
                                                                            $('#modal-body').html('<h4 style="text-align:center; color:green;"> <?php echo $this->lang->line("error_occur_contact_admin"); ?></h4>');
                                                                            setTimeout(function () {
                                                                                $('#modal').hide();
                                                                                location.reload();
                                                                            }, 2000);
                                                                        }
                                                                    });
                                                                }
</script>
