
<style>
    .drop-shadow {
        position:relative;
        width:70%;    
        padding:1em; 
        margin:2em auto 5em 10px; 
        background:#fff;
        box-shadow: 0 1px 4px rgba(0, 0, 0, 0.3), 0 0 40px rgba(0, 0, 0, 0.1);
    }

</style>  
</br>
<?php //echo print_r($profile);?>
<div class="container">
    <h2 class="registertitle"><?php echo $this->lang->line('photo_edit_title'); ?></h2>
    <div class="col-md-12">
        <div class="drop-shadow ">
            <div class="row">
                <div class="col-md-4 ">
                    <div class="proimg_border">
                        <img src="<?php echo base_url() . 'assets/upload_images/' . $image ?>" class="img_profile_edit" alt=""/>
                    </div>
                    <div class="proadd">
                        <a href="<?php echo base_url(); ?>index.php/profile/photo_edit_change"><?php echo $this->lang->line('add_text') . ' / ' . $this->lang->line('edit_photo') ?></a>
                    </div>
                </div>
                <div class="col-md-4">
                    <?php if (!empty($profile)) { ?>
                        <div class="p_name text"><?php echo (!empty($profile['username']) ? $profile['username'] : 'Not Specified'); ?></div>

                        <div class="p_age text"><span class="age"><?php echo (!empty($profile['age']) ? $profile['age'] . 'Years' : 'Not Specified'); ?>,</span>
                            <span class="fit text"><?php echo (!empty($profile['height']) ? $profile['height'] . ' cm' : 'Not Specified'); ?> </span>
                        </div>
                        <div class="p_caste text"><?php echo (!empty($profile['caste_name']) ? $profile['caste_name'] : 'Not Specified'); ?></div>
                        <div class="p_city text">
                                <span class="city"><?php echo (!empty($profile['city_name']) ? $profile['city_name'] : 'Not Specified'); ?></span><!--<span class="state"><?php echo (!empty($profile['state_name']) ? $profile['state_name'] : 'Not Specified'); ?></span>-->
                        </div>
                        <div class="p_qualify text">
                            <span class="qualify"><?php echo (!empty($profile['edu_name']) ? $profile['edu_name'] : 'Not Specified'); ?>,</span><span class="deg"><?php echo (!empty($profile['occupation']) ? $profile['occupation'] : 'Not Specified'); ?></span>
                        </div>
                        <div class="proadd">
                            <a href="<?php echo base_url(); ?>index.php/profile"><?php echo $this->lang->line('edit_profile'); ?></a>
                        </div>
                    <?php } ?>
                </div>

            </div>

        </div>
    </div>
</div>



