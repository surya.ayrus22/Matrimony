<!DOCTYPE HTML>
<html  xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title><?php echo $this->lang->line('text_matrimony') . ' | ' . $this->lang->line('admin'); ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <meta name="keywords" content="Modern Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template, 
                  Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
            <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
            <!-- Bootstrap Core CSS -->
            <link href="<?php echo base_url(); ?>assets/css/bootstrap-3.3.4/dist/css/bootstrap.min.css" rel='stylesheet' type='text/css' />
            <!-- Custom CSS -->
            <link href="<?php echo base_url(); ?>assets/admin/css/style.css" rel='stylesheet' type='text/css' />
            <!-- Graph CSS -->
            <!--<link href="<?php echo base_url(); ?>assets/admin/css/lines.css" rel='stylesheet' type='text/css' />-->
            <link href="<?php echo base_url(); ?>assets/admin/css/font-awesome.css" rel="stylesheet"> 
                <!-- jQuery -->
                <script src="<?php echo base_url(); ?>assets/admin/js/jquery.min.js"></script>
                <!----webfonts--->
                <link href='http://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900' rel='stylesheet' type='text/css'>
                    <!---//webfonts--->  
                    <!-- Nav CSS -->
                    <link href="<?php echo base_url(); ?>assets/admin/css/custom.css" rel="stylesheet">
                        <link href="<?php echo base_url(); ?>assets/admin/css/your.css" rel="stylesheet">
                            <link href="<?php echo base_url(); ?>assets/admin/css/home.css" rel="stylesheet">
                                <!-- Table -->
                                <link href="<?php echo base_url(); ?>assets/admin/css/dataTables.bootstrap.min.css" rel="stylesheet">
                                    <!--pagination-->
                                    <script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/pagination/js/jquery.pajinate.js"></script>

                                    <!-- Metis Menu Plugin JavaScript -->
                                    <script src="<?php echo base_url(); ?>assets/admin/js/metisMenu.min.js"></script>
                                    <script src="<?php echo base_url(); ?>assets/admin/js/custom.js"></script>
                                    <!-- Graph JavaScript -->
                                    <script src="<?php echo base_url(); ?>assets/admin/js/d3.v3.js"></script>
                                    <script src="<?php echo base_url(); ?>assets/admin/js/rickshaw.js"></script>

                                    <script src="<?php echo base_url(); ?>/assets/js/dobPicker.min.js"></script>
                                    <input type="hidden" id="baseurl" value="<?php echo base_url(); ?>"/>
                                    </head>
                                    <body>


                                        <div id="wrapper">
                                            <!-- Navigation -->
                                            <nav class="top1 navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
                                                <div class="navbar-header">
                                                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                                        <span class="sr-only">Toggle navigation</span>
                                                        <span class="icon-bar"></span>
                                                        <span class="icon-bar"></span>
                                                        <span class="icon-bar"></span>
                                                    </button>
                                                    <a class="navbar-brand" href="<?php echo base_url(); ?>index.php/admin/dashboard">Admin</a>
                                                </div>
                                                <!-- /.navbar-header -->
                                                <ul class="nav navbar-nav navbar-right">
                                                    <!--<li class="dropdown">
                                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-comments-o"></i><span class="badge">4</span></a>
                                                    <ul class="dropdown-menu">
                                                                    <li class="dropdown-menu-header">
                                                                            <strong>Recent User</strong>
                                                                    </li>
                                                    <?php //for($i=0;$i<3;$i++) { ?>
                                                                    <li class="avatar">
                                                                            <a href="#">
                                                                                    <img src="<?php echo base_url(); ?>assets/images/1.png" alt=""/>
                                                                                    <div>New message</div>
                                                                                    <small>1 minute ago</small>
                                                                                    <span class="label label-info">NEW</span>
                                                                            </a>
                                                                    </li>
                                                    <?php //} ?>
                                                                    <li class="dropdown-menu-footer text-center">
                                                                            <a href="#">View all messages</a>
                                                                    </li>	
                                                    </ul>
                                            </li>-->
                                                    <li class="dropdown">
                                                        <a href="#" class="dropdown-toggle avatar" data-toggle="dropdown"><i class="fa fa-cogs icon1" aria-hidden="true"></i></a>
                                                        <ul class="dropdown-menu">
                                                            <li class="dropdown-menu-header text-center">
                                                                <strong><?php echo $this->lang->line("settings"); ?></strong>
                                                            </li>
                                                            <li class="m_2"><a href="<?php echo base_url() . 'index.php/admin_profile/admineditprofile'; ?>"><i class="fa fa-wrench"></i> <?php echo $this->lang->line("settings"); ?> </a></li>
                                                            <!--li class="divider"></li-->
                                                            <li class="m_2"><a href="<?php echo base_url() . 'index.php/signout' ?>"><i class="fa fa-lock"></i> <?php echo $this->lang->line("logout"); ?></a></li>	
                                                        </ul>
                                                    </li>
                                                </ul>
                                                <!--<form class="navbar-form navbar-right">
                                      <input type="text" class="form-control1" value="Search..." onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Search...';}">
                                    </form>-->
                                                <div class="navbar-default sidebar" role="navigation">
                                                    <div class="sidebar-nav navbar-collapse">
                                                        <ul class="nav" id="side-menu">
                                                            <li>
                                                                <a href="<?php echo base_url() . 'index.php/admin/dashboard'; ?>"><i class="fa fa-dashboard fa-fw nav_icon"></i><?php echo $this->lang->line("dashboard"); ?></a>
                                                            </li>
                                                            <li>
                                                                <a href="#"><i class="fa fa-laptop nav_icon"></i><?php echo $this->lang->line("user_details"); ?><span class="fa arrow"></span></a>
                                                                <ul class="nav nav-second-level">
                                                                    <li><a href="<?php echo base_url() . 'index.php/admin/UsersAddInfo'; ?>"><?php echo $this->lang->line("useradd"); ?></a></li>
                                                                    <li><a href="<?php echo base_url() . 'index.php/admin/userdetails?type=' . constant('TYPE_1'); ?>"><?php echo $this->lang->line("users"); ?></a></li>
                                                                    <li><a href="<?php echo base_url() . 'index.php/admin/userdetails?type=' . constant('TYPE_2'); ?>"><?php echo $this->lang->line("male_user"); ?></a> </li>
                                                                    <li><a href="<?php echo base_url() . 'index.php/admin/userdetails?type=' . constant('TYPE_3'); ?>"><?php echo $this->lang->line("female_user"); ?></a> </li>
                                                                    <li><a href="<?php echo base_url() . 'index.php/admin/userdetails?type=' . constant('TYPE_4'); ?>"><?php echo $this->lang->line("premium_user"); ?></a> </li>
                                                                    <li><a href="<?php echo base_url() . 'index.php/admin/userdetails?type=' . constant('TYPE_5'); ?>"><?php echo $this->lang->line("active_user"); ?></a> </li>
                                                                    <li><a href="<?php echo base_url() . 'index.php/admin/userdetails?type=' . constant('TYPE_6'); ?>"><?php echo $this->lang->line("deactive_user"); ?></a> </li>
                                                                    <li><a href="<?php echo base_url() . 'index.php/admin/userdetails?type=' . constant('TYPE_7'); ?>"><?php echo $this->lang->line("deleted_user"); ?></a>  </li>
                                                                    <li><a href="<?php echo base_url() . 'index.php/admin/userdetails?type=' . constant('TYPE_15'); ?>"><?php echo $this->lang->line("incomplete_user"); ?></a> </li>
                                                                   
                                                                </ul>
                                                                <!-- /.nav-second-level -->
                                                            </li>
                                                            <li>
                                                                <a href="#"><i class="fa fa-indent nav_icon"></i><?php echo $this->lang->line("profile_admin"); ?><span class="fa arrow"></span></a>
                                                                <ul class="nav nav-second-level">
                                                                    <li><a href="<?php echo base_url() . 'index.php/admin_profile?type=' . constant('TYPE_11'); ?>"><?php echo $this->lang->line("profile_approved"); ?></a>  </li>
                                                                    <li><a href="<?php echo base_url() . 'index.php/admin/successdetails?type=' . constant('TYPE_12'); ?>"><?php echo $this->lang->line("success_profile"); ?></a>  </li>
                                                                    <li><a href="<?php echo base_url() . 'index.php/admin_profile/verifyImage?type=' . constant('TYPE_14'); ?>"><?php echo $this->lang->line('verify_image'); ?></a>  </li>	
                                                                </ul>
                                                                <!-- /.nav-second-level -->
                                                            </li>
                                                            <li>
                                                                <a href="#"><i class="fa fa-envelope nav_icon"></i><?php echo $this->lang->line("premium_member"); ?><span class="fa arrow"></span></a>
                                                                <ul class="nav nav-second-level">
                                                                    <li> <a href="<?php echo base_url() . 'index.php/admin/userdetails?type=' . constant('TYPE_8'); ?>"><?php echo $this->lang->line("platinum"); ?></a> </li>
                                                                    <li> <a href="<?php echo base_url() . 'index.php/admin/userdetails?type=' . constant('TYPE_9'); ?>"><?php echo $this->lang->line("premium"); ?></a></li>
                                                                    <li> <a href="<?php echo base_url() . 'index.php/admin/userdetails?type=' . constant('TYPE_10'); ?>"><?php echo $this->lang->line("gold"); ?></a></li>
                                                                </ul>
                                                                <!-- /.nav-second-level -->
                                                            </li>
                                                            <li>
                                                                <a href="#"><i class="fa fa-credit-card nav_icon"></i><?php echo $this->lang->line("header_upgrade"); ?><span class="fa arrow"></span></a>
                                                                <ul class="nav nav-second-level">
                                                                    <li><a href="<?php echo base_url() . 'index.php/adminpayment/userdetails?type=' . constant('TYPE_13'); ?>"><?php echo $this->lang->line("unpaid-users"); ?></a> </li>
                                                                    <li><a href="<?php echo base_url() . 'index.php/adminpayment/membership'; ?>"><?php echo $this->lang->line("curreny_convertion"); ?> </a> </li>
                                                                </ul>
                                                                <!-- /.nav-second-level -->
                                                            </li>
                                                            <li>
                                                                <a href="<?php echo base_url() . 'index.php/signout' ?>"><i class="fa fa-lock nav_icon"></i><?php echo $this->lang->line("logout"); ?></a>
                                                            </li>
                                                            <!-- <li>
                                                                 <a href="widgets.html"><i class="fa fa-flask nav_icon"></i>Widgets</a>
                                                             </li>
                                                              <li>
                                                                 <a href="#"><i class="fa fa-check-square-o nav_icon"></i>Forms<span class="fa arrow"></span></a>
                                                                 <ul class="nav nav-second-level">
                                                                     <li>
                                                                         <a href="forms.html">Basic Forms</a>
                                                                     </li>
                                                                     <li>
                                                                         <a href="validation.html">Validation</a>
                                                                     </li>
                                                                 </ul>
                                                             </li>
                                                             <li>
                                                                 <a href="#"><i class="fa fa-table nav_icon"></i>Tables<span class="fa arrow"></span></a>
                                                                 <ul class="nav nav-second-level">
                                                                     <li>
                                                                         <a href="basic_tables.html">Basic Tables</a>
                                                                     </li>
                                                                 </ul>
                                                                 
                                                             </li>
                                                             <li>
                                                                 <a href="#"><i class="fa fa-sitemap fa-fw nav_icon"></i>Css<span class="fa arrow"></span></a>
                                                                 <ul class="nav nav-second-level">
                                                                     <li>
                                                                         <a href="media.html">Media</a>
                                                                     </li>
                                                                     <li>
                                                                         <a href="login.html">Login</a>
                                                                     </li>
                                                                 </ul>          
                                                             </li>-->
                                                        </ul>
                                                    </div>
                                                    <!-- /.sidebar-collapse -->
                                                </div>
                                                <!-- /.navbar-static-side -->
                                            </nav>

