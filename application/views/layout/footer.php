   
<div class="footer">
    <div class="container">
        <div class="col-md-4 col_2">
            <h4><?php echo $this->lang->line("footer_step1_title1"); ?></h4>
            <p><?php echo $this->lang->line("footer_step1_title1_text"); ?></p>
        </div>
        <div class="col-md-2 col_2">
            <h4><?php echo $this->lang->line("footer_step2_title1"); ?></h4>
            <ul class="footer_links">
                <li><a href="<?php echo base_url(); ?>index.php/contact/contact_us"><?php echo $this->lang->line("footer_step2_title1_text1"); ?></a></li>
                <li><a href="<?php echo base_url(); ?>index.php/footer/feedback"><?php echo $this->lang->line("footer_step2_title1_text2"); ?></a></li>
                <li><a href="<?php echo base_url(); ?>index.php/faq/faqs"><?php echo $this->lang->line("footer_step2_title1_text3"); ?></a></li>
                <li><a href="<?php echo base_url(); ?>index.php/success/index"><?php echo $this->lang->line("footer_step2_title1_text4"); ?></a></li>
            </ul>
        </div>
        <div class="col-md-2 col_2">
            <h4><?php echo $this->lang->line("footer_step3_title1"); ?></h4>
            <ul class="footer_links">
                <li><a href="<?php echo base_url(); ?>index.php/footer/privacy"><?php echo $this->lang->line("footer_step3_title1_text1"); ?></a></li>
                <li><a href="<?php echo base_url(); ?>index.php/footer/term"><?php echo $this->lang->line("footer_step3_title1_text2"); ?></a></li>
                <li><a href="<?php echo base_url(); ?>index.php/footer/service"><?php echo $this->lang->line("footer_step3_title1_text3"); ?></a></li>
            </ul>
        </div>
        <!--<div class="col-md-2 col_2">
                <h4>Social</h4>
                <ul class="footer_social">
                          <li><a href="#"><i class="fa fa-facebook fa1"> </i></a></li>
                          <li><a href="#"><i class="fa fa-twitter fa1"> </i></a></li>
                          <li><a href="#"><i class="fa fa-google-plus fa1"> </i></a></li>
                          <li><a href="#"><i class="fa fa-youtube fa1"> </i></a></li>
                    </ul>
        </div>-->
        <div class="clearfix"> </div>
        <div class="copy printopt">
            <p class="printopt"><?php echo $this->lang->line('footer_text'); ?> <a href="http://www.kevellcorp.com" target="_blank"><?php echo $this->lang->line('kevell_text'); ?></a> </p>
        </div>
    </div>
</div>
</main>
<a href="#0" class="cd-top">Top</a>
<script src="<?php echo base_url(); ?>assets/topscroll/js/main.js"></script> <!-- Gem jQuery -->

</body>
</html>	
