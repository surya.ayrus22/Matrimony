
<style>.border1{
        border-color: #ddd;
        border-style: solid;
        border-width: 1px;
        margin-top:3%;
    }
    .regtitle1 {
        color: #c32143;
        font-size: 30px;
        padding: 10px;

    }
    .regtitle3 {
        color: #c32143;
        font-size: 20px;

    }
    .beark{
        font-size:14px;
        line-height: 1.5em;
        text-align: justify;
    }
    .overview {
        padding: 10px;
    }
    .fol{
        margin-bottom: 10px;
        margin-top: 10px;
    }
    .nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus{
        background-color:#FFA417 !important;
        color: #fff !important;
    }
    .nav.nav-tabs {
        margin-top: 17px;
        background-color:#C32143;
    }
    .nav.nav-tabs a{
        color:#fff;
    }
    .nav-tabs > li{
        width:110px !important;
    }
    .test{
        box-shadow: 0 1px 4px rgba(0, 0, 0, 0.3), 0 0 40px rgba(0, 0, 0, 0.1) inset;
    }
    .text_sample {
        margin-bottom: 10%;
        margin-left: 50px !important;
        margin-right: 50px !important;
    }
    .text_sample1 {
        margin-left: 50px !important;
        margin-right: 50px !important;

    }
    .tab_h {
        margin-left: 50px !important;
    }
    .txt{
        margin-bottom:25px !important;
    }
</style>
<div class="container">
    <div class="col-md-12 border1">
        <h2 class="regtitle1">Privacy</h2>
        <div class="overview">
            <p class="beark">The owners of this Service have created this Privacy Statement (Policy) in order to demonstrate our firm commitment to help our users better understand what information we collect about them and what may happen to that information.</p>

            <p class="beark fol">The following discloses our information gathering and dissemination practices for the Websites listed below.</p>

            <h3  class="regtitle3">Overview</h3>

            <p class="beark">As part of the normal operation of our services we collect and, in some cases, may disclose information about you. This Privacy Policy describes the information we collect about you and what may happen to that information. By accepting this Privacy Policy and our Terms and Conditions, you expressly consent to our use and disclosure of your personal information in the manner prescribed in this Privacy Policy. This Privacy Policy is incorporated into and subject to the terms of the Terms and Conditions.

                This Privacy Statement applies to all related sites. All other  worldwide sites operate under similar privacy practices as described in this Privacy Policy, and subject to the requirements of applicable local law, we strive to provide a consistent set of privacy practices throughout the  online personals sites.</p>
        </div>
        <div class="test">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#tab_a" data-toggle="tab">A Special Note About Children</a></li>
                <li class=""><a href="#tab_b" data-toggle="tab">Information We Collect</a></li>
                <li class=""><a href="#tab_c" data-toggle="tab">Our Use of Your Information</a></li>
                <li class=""><a href="#tab_d" data-toggle="tab"> Our Disclosure of Your Information</a></li>
                <li class=""><a href="#tab_5" data-toggle="tab"> Your Use of other Members Information</a></li>
                <li class=""><a href="#tab_6" data-toggle="tab"> Accessing, Reviewing & Changing Your Profile</a></li>
                <li class=""><a href="#tab_7" data-toggle="tab"> Control of Your Password</a></li>
                <li class=""><a href="#tab_8" data-toggle="tab">Other Information Collectors</a></li>
                <li class=""><a href="#tab_9" data-toggle="tab">Security</a></li>
                <li class=""><a href="#tab_10" data-toggle="tab">Notice</a></li>
            </ul>
            <div class="tab-content ">
                <!--tab-->
                <div class="tab-pane active" id="tab_a" style="margin-top: 2%; ">
                    <h3  class="regtitle3 tab_h">1.A Special Note About Children</h3>
                    <div class="beark text_sample">Children are not eligible to use our services, and we ask that minors (under the age of 18) do not submit any personal information to us. If you are a minor, you can use this service only in conjunction with your parents or guardians.</div>
                    </br>
                </div>
                <!--tabend-->
                <!--tab-->
                <div class="tab-pane" id="tab_b" style="margin-top: 2%; ">
                    <h3  class="regtitle3 tab_h">2.Information We Collect</h3>
                    <div class="beark text_sample1 txt">      
                        Our primary goal in collecting personal information is to provide you with a smooth, efficient, and customized experience. This allows us to provide services and features that most likely meet your needs and to customize our service to make your experience easier. You agree that in order to assist our members to meet each other we may feature members' profiles in our editorials and newsletters that we send out from time to time to our members. Your name, address and telephone number are confidential and will not be posted in your profile. Your profile is available for other members to view. Members' profiles include a description and photos, likes and dislikes, individual essays and other information helpful in determining matches. Your viewable profile does not include any identifying information about you, except the username you chose upon registering.
                    </div>

                    <div class="beark text_sample1 txt">
                        Please note that your personally identifiable information will be stored and processed on our computers in the United States. The laws on holding personal data in the United States may differ from the European laws but as we explain below, We will hold and transmit your information in a safe, confidential and secure environment.
                    </div>

                    <div class="beark text_sample1 txt">  
                        We automatically track certain information based upon your behavior on our Websites using cookies and other devices. We use this information for internal research on our members' demographics, interests, and behavior to better understand and serve you and our community. This information may include the URL that you just came from (whether this URL is on our site or not). Which URL you next go to (whether this URL is on our site or not), what browser you are using, and your IP address.
                    </div>

                    <div class="beark text_sample1 txt">  
                        We use data collection devices such as "cookies" on certain pages of our Websites. "Cookies" are small files placed on your hard drive that assist us in providing customized services. We also offer certain features that are only available through the use of a "cookie". Cookies can also help us provide information which is targeted to your interests. Most cookies are "session cookies", meaning that they are automatically deleted from your hard drive at the end of a session. You are always free to decline our cookies if your browser permits.
                    </div>

                    <div class="beark text_sample1 txt">  
                        We use a third party advertising company, to serve ads on our behalf across the Internet. The third party may also collect anonymous information about your visits to our Web site. This is primarily accomplished through the use of a technology device, commonly referred to as a Web beacon or an action tag, which we place on various Web pages within our Web site or in an html e-mail that allows Atlas DMT to collect anonymous information. Atlas DMT may use information about your visits to this and other Web sites in order to provide ads about goods and services of interest to you.
                    </div> </br>
                </div>
                <!--tabend-->
                <!--tab-->
                <div class="tab-pane" id="tab_c" style="margin-top: 2%;">
                    <h3  class="regtitle3 tab_h">3.Our Use of Your Information</h3>
                    <div class="beark text_sample1 txt">  
                        We use information in the files we maintain about you, and the other information we obtain from your current and past activities on the Websites to resolve disputes, troubleshoot problems and enforce our Terms and Conditions. At times, we may look across multiple members to identify problems or resolve disputes, and in particular we may examine your information to identify members multiple Member ID No. or aliases.
                    </div>
                    <div class="beark text_sample1 txt">  
                        You agree that we may use personally identifiable information about you to improve our marketing and promotional efforts, to analyze site usage, improve our content and product offerings, and customize our Site's content, layout, and services. These uses improve our site and better tailor it to meet your needs.
                    </div>
                    <div class="beark text_sample1 txt">  
                        You agree that we may use your information to contact you and deliver information to you that, in some cases, are targeted to your interests, such as targeted banner advertisements, administrative notices, product offerings, and communications relevant to your use on the Websites. By accepting this Privacy Policy, you expressly agree to receive this information. 
                    </div>
                    </br>

                </div>

                <!--tabend-->
                <!--tab-->
                <div class="tab-pane" id="tab_d" style="margin-top: 2%;">

                    <h3  class="regtitle3 tab_h">4.Our Disclosure of Your Information</h3>
                    <div class="beark text_sample1 txt">  
                        Due to the existing regulatory environment, we cannot ensure that all of your private communications and other personally identifiable information will never be disclosed in ways not otherwise described in this Privacy Policy. By way of example (without limiting and foregoing), we may be forced to disclose information to the government, law enforcement agencies or third parties. Under certain circumstances, third parties may unlawfully intercept or access transmissions or private communications, or members may abuse or misuse your information that they collect from our Websites. Therefore, although we use industry standard practices to protect your privacy, we do not promise, and you should not expect, that your personally identifiable information or private communications will always remain private.
                    </div>
                    <div class="beark text_sample1 txt">  
                        As a matter of policy, we do not sell or rent any personally identifiable information about you to any third party. However, the following describes some of the ways that your personally identifiable information may be disclosed.
                    </div>
                    <div class="beark text_sample1 txt">  

                        <span style="font-weight:bold;">Financial Information.</span> Under some circumstances we may require some additional information, such as, but not limited to credit card details. We use this financial information, including your name, address, and other information, as well as to bill you for use of our services. We use your financial information to check your qualifications, to bill you for services and/or products, to enable you to participate in discount, rebate and similar programs in which you may elect to participate. By making a purchase, or engaging in any other kind of activity or transaction that uses financial information on the Websites, you consent to our providing of your financial information to our service providers and to such third parties as we determine are necessary to support and process your activities and transactions, as well as to your credit card issuer for their purposes. These third parties may include the credit card companies and banking institutions used to process and support the transaction or activity. By purchasing, or registering or making reservations for products or services of third parties offered on the Websites, or by participating in programs offered on the Websites that are administered by third parties and that require you to submit financial information in order to use them, you also consent to our providing your financial information to those third parties. Any of these various third parties may be authorized to use your financial information in accordance with our contractual arrangements with such third parties and in accordance with their own privacy policies, over which we have no control, and you agree that we are not responsible or liable for any of their actions or omissions. Additionally, you agree that we may use and disclose all such information so submitted to such third parties in the same manner in which we are entitled to use and disclose any other information that you submit to us.
                    </div>
                    <div class="beark text_sample1 txt">  

                        <span style="font-weight:bold;">Advertisers.</span> We aggregate (gather up data across all our members' accounts) personally identifiable information and disclose such information in a non-personally identifiable manner to advertisers and other third parties for other marketing and promotional purposes. However, in these situations, we do not disclose to these entities any information that could be used to identify you personally. Certain information, such as your name, email address, password, credit card number and bank account number, are never disclosed to marketing advertisers. We may use third-party advertising companies to serve ads on our behalf. These companies may employ cookies and action tags (also known as single pixel gifs or web beacons) to measure advertising effectiveness. Any information that these third parties collect via cookies and action tags is completely anonymous.
                    </div>
                    <div class="beark text_sample1 txt">  
                        <span style="font-weight:bold;">External Service Providers.</span> There may be a number of services offered by external service providers that help you use our Websites. If you choose to use these optional services, and in the course of doing so, disclose information to the external service providers, and/or grant them permission to collect information about you, then their use of your information is governed by their private policy.
                    </div>
                    <div class="beark text_sample1 txt">  
                        <span style="font-weight:bold;">Other Corporate Entities.</span> We share much of our data, including personally identifiable information about you, with our parent and/or subsidiaries that are committed to serving your online needs and related services, throughout the world. To the extent that these entities have access to your information, they will treat it at least as protectively as they treat information they obtain from their other members. Our parent and/or subsidiaries will follow privacy practices no less protective of all members than our practices described in this document, to the extent allowed by applicable law. It is possible that We and/or its subsidiaries, or any combination of such, could merge with or be acquired by another business entity. Should such a combination occur, you should expect that we would share some or all of your information in order to continue to provide the service. You will receive notice of such event (to the extent it occurs) and we will require that the new combined entity follow the practices disclosed in this Privacy Policy.
                    </div>
                    <div class="beark text_sample1 txt">  

                        <span style="font-weight:bold;">Legal Requests. </span>we cooperates with law enforcement inquiries, as well as other third parties to enforce laws, such as: intellectual property rights, fraud and other rights. We can (and you authorize us to) disclose any information about you to law enforcement and other government officials as we, in our sole discretion, believe necessary or appropriate, in connection with an investigation of fraud, intellectual property infringements, or other activity that is illegal or may expose us or you to legal liability.
                    </div> 
                    </br>

                </div>
                <!--tabend-->
                <!--tab-->
                <div class="tab-pane" id="tab_5" style="margin-top: 2%;">


                    <h3  class="regtitle3 tab_h">5.Your Use of other Members Information</h3>
                    <div class="beark text_sample1 txt">  
                        Our services also include access to instant messaging and chat rooms. As a member you have access to the Member's ID No., and you might gain access to other contact information of the Member(s) through the regular use of the instant messaging and chat rooms. By accepting this Privacy Policy, you agree that, with respect to other Members' personally identifiable information that you obtain through instant messaging communication, the chat rooms and email, We hereby grants to you a license to use such information only for: (a) Your Company related communications that are not unsolicited commercial messages, and (b) any other purpose that such member expressly agrees to after adequate disclosure of the purpose(s). In all cases, you must give Members an opportunity to remove themselves from your database and a chance to review what information you have collected about them. In addition, under no circumstances, except as defined in this Section, can you disclose personally identifiable information about another member to any third party without our consent and the consent of such other member after adequate disclosure. Madurai Somaeswar Matrimony and our members do not tolerate spam. Therefore, without limiting the foregoing, you are not licensed to add a Madurai Somaeswar Matrimony member to your mail list (email or physical mail) without their express written consent after adequate disclosure. To report spam from other Madurai Somaeswar Matrimony, please Contact Us. 
                    </div></br>
                </div>
                <!--tabend-->
                <!--tab-->
                <div class="tab-pane" id="tab_6" style="margin-top: 2%;">


                    <h3  class="regtitle3 tab_h">6.Accessing, Reviewing and Changing Your Profile</h3>
                    <div class="beark text_sample1 txt">  
                        Following registration, you can review and change the information you submitted during registration through the Member Service Menu including: Your password and e-mail address. If you change your password and email we keep track of your old password and email. You can also change your registration information such as: name, address, city, state, zip code, country, phone number, profile, likes and dislikes, desired date profile, essays and saves search criteria.
                    </div>
                    <div class="beark text_sample1 txt">  
                        Upon your written request, we will suspend your membership, contact information, and financial information from our active databases. Such information will be suspended as soon as reasonably possible in accordance with our suspension policy and applicable law. To remove your profile so that others cannot view it, visit the Remove Your Profile section of Member Services.
                    </div>
                    <div class="beark text_sample1 txt">  
                        We will retain in our files information you have requested to remove for certain circumstances, such as to resolve disputes, troubleshoot problems and enforce our terms and conditions. Further, such prior information is never completely removed from our databases due to technical and legal constraints, including stored 'back up' systems. Therefore, you should not expect that all of your personally identifiable information will be completely removed from our databases in response to your requests. 
                    </div></br>
                </div>
                <!--tabend-->
                <!--tab-->
                <div class="tab-pane" id="tab_7" style="margin-top: 2%;">

                    <h3  class="regtitle3 tab_h">7.Control of Your Password</h3>
                    <div class="beark text_sample1 txt">  
                        You are responsible for all actions taken with your login information and password, including fees. Therefore we do not recommend that you disclose your Madurai Somaeswar Matrimony password or login information to any third parties. If you choose to share this information with third parties to provide you additional services, you are responsible for all actions taken with your login information and password and therefore should review each third party's privacy policy. If you lose control of your password, you may lose substantial control over your personally identifiable information and may be subject to legally binding actions taken on your behalf. Therefore, if your password has been compromised for any reason, you should immediately change your passwod
                    </div></br>
                </div>
                <!--tabend-->
                <!--tab-->
                <div class="tab-pane" id="tab_8" style="margin-top: 2%;">


                    <h3  class="regtitle3 tab_h">  8.Other Information Collectors</h3>
                    <div class="beark text_sample1 txt">  
                        Except as otherwise expressly included in this Privacy Policy, this document only addresses the use and disclosure of information we collect from you. To the extent that you disclose your information to other parties, whether they are on our Websites or on other sites throughout the Internet, different rules may apply to their use or disclosure of the information you disclose to them. To the extent that we use third party advertisers, they adhere to their own privacy policies. Since Madurai Somaeswar Matrimony does not control the privacy policies of the third parties, you are subject to ask questions before you disclose your personal information to others. 
                    </div></br>
                </div>
                <!--tabend-->
                <div class="tab-pane" id="tab_9" style="margin-top: 2%;">


                    <h3  class="regtitle3 tab_h"> 9.Security</h3>
                    <div class="beark text_sample1 txt">  
                        Madurai Somaeswar Matrimony uses industry standard practices to safeguard the confidentiality of your personal identifiable information, including "firewalls" and Secure Socket Layers. Madurai Somaeswar Matrimony treats data as an asset that must be protected against loss and unauthorized access. We employ many different security techniques to protect such data from unauthorized access by members inside and outside the company. However, "perfect security" does not exist on the Internet. 
                    </div></br>
                </div>
                <!--tabend-->
                <div class="tab-pane" id="tab_10" style="margin-top: 2%;">


                    <h3  class="regtitle3 tab_h">10.Notice</h3>
                    <div class="beark text_sample1 txt">  
                        We may change this Privacy Policy from time to time based on your comments and our need to accurately reflect our data collection and disclosure practices. All changes to this policy are effective after we provide you with at least (30) days notice of changes by sending email to members. We provide you with (30) days notice to allow you the opportunity to notify Madurai Somaeswar Matrimony if you do not agree to such changes.
                    </div></br>
                </div>
                <!--tabend-->
            </div><!-- tab content -->

        </div>
    </div><!-- end of container -->
</div>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="Bootstrap%203%20Navs%20Tabs%20Pills_files/jquery.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="Bootstrap%203%20Navs%20Tabs%20Pills_files/bootstrap.js"></script>


