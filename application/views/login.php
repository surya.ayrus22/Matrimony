<script src="<?php echo base_url(); ?>/assets/js/dobPicker.min.js"></script>
<style>
    .log_form{
        color: #c32143;
        font-size: 22px;
    }
</style>
<div class="grid_3">
    <div class="container">
        <div class="breadcrumb1">
            <ul>
                <a href="<?php echo base_url(); ?>index.php/home/index"><i class="fa fa-home home_1"></i></a>
                <span class="divider">&nbsp;|&nbsp;</span>
                <li class="current-page">Register</li>
            </ul>
        </div>
        <div class="services">

            <div class="col-sm-6">
                <h2 class="log_form">New Entry Register FREE!</h2>
                <form action="<?php echo base_url(); ?>index.php/home/registration" method="post">
                    <div class="form-group row">
                        <label for="profile" class="col-sm-5 form-control-label reglab">Profile for</label>
                        <div class="col-sm-7">
                            <select class="form-control" id="profile_created" name="profile_created" required >
                                <option value=""> -- Select Profile for -- </option>
                                <option value="myself">Myself</option>
                                <option value="son">Son</option>
                                <option value="daughter">Daughter</option>
                                <option value="brother">Brother</option>
                                <option value="sister">Sister</option>
                                <option value="relative">Relative</option>
                                <option value="friend">Friend</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="username" class="col-sm-5 form-control-label reglab">Name</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control" id="username" name="username" placeholder="Username" required oninvalid="this.setCustomValidity('Please enter username')" oninput="setCustomValidity('')">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="gender" class="col-sm-5 form-control-label reglab">Gender</label> 
                        <div class="col-sm-7">
                            <label class="radio-inline reglabrad">
                                <input type="radio" name="gender" id="gender" value="M" required> Male
                            </label>
                            <label class="radio-inline reglabrad">
                                <input type="radio" name="gender" id="gender" value="F" required> Female
                            </label>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="date" class="col-sm-5 form-control-label reglab">Date of birth</label>
                        <div class="col-sm-7">
                            <div class="col-md-4 dob">
                                <select id="dobday" name="date" class="form-control input-sm" required></select>
                            </div>
                            <div class="col-md-4 dob">
                                <select id="dobmonth" name="month" class="form-control input-sm" required></select>
                            </div>
                            <div class="col-md-4 dob">
                                <select id="dobyear" name="year" class="form-control input-sm" required></select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="religion" class="col-sm-5 form-control-label reglab">Religion</label>
                        <div class="col-sm-7">
                            <select class="form-control" id="religion" name="religion" required >
                                <!--<option value=""> -- Select Profile for -- </option>-->
                                <option value="hindu">Hindu</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="mother_tongue" class="col-sm-5 form-control-label reglab">Mother Tongue</label>
                        <div class="col-sm-7">
                            <select class="form-control" id="mother_tongue" name="mother_tongue" required>
                                <option value="">- Select your Mother Tongue -</option>
                                <option value="tamil">Tamil</option>
                                <option value="bengali">Bengali</option>
                                <option value="english">English</option>
                                <option value="gujarati">Gujarati</option>
                                <option value="hindi">Hindi</option>
                                <option value="kannada">Kannada</option>
                                <option value="konkani">Konkani</option>
                                <option value="malayalam">Malayalam</option>
                                <option value="marathi">Marathi</option>
                                <option value="marwari">Marwari</option>
                                <option value="oriya">Oriya</option>
                                <option value="punjabi">Punjabi</option>
                                <option value="sindhi">Sindhi</option>
                                <option value="telugu">Telugu</option>
                                <option value="urdu">Urdu
                                <option value="others">Others</option></option>
                                </optgroup>
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="email" class="col-sm-5 form-control-label reglab">Email </label>
                        <div class="col-sm-7">
                            <input type="text" style="text-transform: lowercase;" class="form-control" id="email" name="email" placeholder="Email Id" onblur="emailavailablility()" required pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" oninvalid="this.setCustomValidity('Please enter valid email format')" oninput="setCustomValidity('')">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="password" class="col-sm-5 form-control-label reglab">Password</label>
                        <div class="col-sm-7">
                            <input type="password" class="form-control" id="password" name="password" placeholder="Password" pattern=".{6,}" required oninvalid="this.setCustomValidity('Please enter password minimum 6 character')" oninput="setCustomValidity('')">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="mobile" class="col-sm-5 form-control-label reglab">Mobile Number</label>     
                        <div class="col-sm-7">
                            <input type="text" class="form-control" id="mobile" name="mobile" placeholder="Mobile Number" required oninvalid="this.setCustomValidity('Please enter mobile number')" oninput="setCustomValidity('')">

                        </div> 
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-offset-7 col-sm-5">
                            <button type="submit" class="btn btn-default subbtn" name="step1">Register Now</button>
                        </div>
                    </div>
                </form>




                <!--  <ul class="sharing">
                              <li><a href="#" class="facebook" title="Facebook"><i class="fa fa-boxed fa-fw fa-facebook"></i> Share on Facebook</a></li>
                              <li><a href="#" class="twitter" title="Twitter"><i class="fa fa-boxed fa-fw fa-twitter"></i> Tweet</a></li>
                              <li><a href="#" class="google" title="Google"><i class="fa fa-boxed fa-fw fa-google-plus"></i> Share on Google+</a></li>
                              <li><a href="#" class="linkedin" title="Linkedin"><i class="fa fa-boxed fa-fw fa-linkedin"></i> Share on LinkedIn</a></li>
                              <li><a href="#" class="mail" title="Email"><i class="fa fa-boxed fa-fw fa-envelope-o"></i> E-mail</a></li>
                      </ul>-->
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            $.dobPicker({
                daySelector: '#dobday', /* Required */
                monthSelector: '#dobmonth', /* Required */
                yearSelector: '#dobyear', /* Required */
                dayDefault: 'DD', /* Optional */
                monthDefault: 'MM', /* Optional */
                yearDefault: 'YY', /* Optional */
                minimumAge: 18, /* Optional */
                maximumAge: 40 /* Optional */
            });
        });
    </script>
</div>

