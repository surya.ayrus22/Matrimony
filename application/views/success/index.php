<link href="<?php echo base_url(); ?>/assets/css/sucessstory.css" rel='stylesheet' type='text/css' />
<script src="<?php echo base_url(); ?>/assets/js/dobPicker.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/pagination/js/jquery.pajinate.js"></script>
<style>

</style>
<script>
    $('#close').click(function () {
        $('#response').hide();
    });

<?php if (!empty($active)) { ?>
        $(document).ready(function () {
            $("#tab_b").addClass('active');
            $("#tab_a").removeClass('active');
            $("#tab2").addClass('active');
            $("#tab1").removeClass('active');
        });
<?php } ?>

    $(document).ready(function () {
        $(".js-example-basic-single").select2();

        $("#country").select2({
            data: <?php echo $country; ?>
        })

        $("#state").select2({
            data: <?php echo $state; ?>
        })
        $("#city").select2({
            data: <?php echo $city; ?>
        })
    });
</script>
<div class="container">
    <div class="col-md-12 ">
        <ul class="nav nav-tabs" style=" margin-top: 25px;">
            <li class="active" id="tab1"><a href="#tab_a" data-toggle="tab">  <?php echo $this->lang->line('success_story_title'); ?></a></li>
            <li class="" id="tab2"><a href="#tab_b" data-toggle="tab"><?php echo $this->lang->line('post_title'); ?></a></li>

        </ul>
        <!--Interest msg-->
        <?php if (!empty($message)) { ?>

            <script>
                $(function () {
                    $('#response').show();
                    setTimeout(function () {
                        $('#response').hide();
                    }, 3000);
                });
            </script>

            <div id="response" class="modal">
                <div class="modal-dialog animated sample" style=" margin-top: 9%;width: 440px;">
                    <div class="modal-contentshort">
                        <div class="row">
                            <div>
                                <span style="float:left;"></span>
                                <span><h3 class="subtitle" id="title"> <?php echo $this->lang->line('success_story_title'); ?></h3></span>
                                <span class="modal-closeshort shtclose" id="close" ><a href="#"><img src="<?php echo base_url(); ?>assets/images/m/forgot-password-close.gif"></a></span>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-10" id="msg_body" style="margin-top: 50px; margin-left: 35px;">
                                    <h4 style="color:blue;"><?php echo $message; ?></h4>'
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
        <!--sInterest msg end-->
        <div class="tab-content ">

            <div class="tab-pane active" id="tab_a"><!--tab a strat-->
                <div class="col-md-9 ">
                    <div  id="paging_container3"><!--page content3-->
                        <?php if (!empty($success)) { ?>
                            <div class="alt_page_navigation pull-right sucesscontent" ></div>
                            <div class="clearfix"></div>
                            <div class="row sucesscontent">
                                <?php foreach ($success as $value) { ?>
                                    <div class=" alt_content " >  <!--alt content-->                        
                                        <div class="col-md-4">
                                            <div class="border1"> 
                                                <img class="imagesucess"src="<?php echo base_url() . 'assets/success_images/' . $value['image']; ?>" >
                                            </div>
                                            <div class="nameid">
                                                <a href="<?php echo base_url() . 'index.php/success/read?ssid=' . $value['id']; ?>"><span class="sucname"><?php echo (!empty($value['member_id']) ? constant('MEMBERID') . $value['member_id'] : 'Not Specified'); ?></span> | <span class="sucid"><?php echo $value['marriage_date'] ?></span></a></br>
                                                <span class="sucname"><?php echo (!empty($value['bride_name']) ? $value['bride_name'] : 'Not Specified') . ' & ' . (!empty($value['groom_name']) ? $value['groom_name'] : 'Not Specified'); ?></span>
                                            </div>	
                                            <a href="<?php echo base_url() . 'index.php/success/read?ssid=' . $value['id']; ?>"><span class="photo_view pull-right"><?php echo $this->lang->line('read_text'); ?></span></a>
                                        </div>
                                    </div>	<!--alt content end--> 
                                <?php } ?>
                            </div><!--row end-->
                        <?php } else { ?>
                            <div class="row sucesscontent">
                                <h4 style="text-align:center;"><?php echo $this->lang->line('success_not_found'); ?></h4>'; 
                            </div><!--row end-->
                        <?php } ?>
                    </div><!--page content3 end-->
                </div><!--col-md 9-->
                <div class="col-md-3 sucesscontent">
                    <div class="sucessstory">
                        <div class="sucessstycont"><?php echo $this->lang->line('post_your_text'); ?> </br><?php echo $this->lang->line('success_story_title'); ?></div>
                        <div class="postsucess" onclick="sucess();"> <a href="#"><span class="post-btn2" style="background: #FFA417;"><?php echo $this->lang->line('post_title'); ?></span></a></div>
                    </div> 
                </div>		 
            </div><!--tab a-->
            <div class="tab-pane " id="tab_b" style="  margin-top: 7%;">
                <div class="col-md-9 ">
                    <form action="<?php echo base_url(); ?>index.php/success/addsuccess" method="POST" id="successpost" enctype="multipart/form-data">    
                        <div class="form-group row">
                            <label  class="col-sm-4 form-control-label"><?php echo $this->lang->line('groom_name'); ?> (<?php echo constant('GENDER_M'); ?>) <label class="red">*</label> </label>  
                            <div class="col-sm-7">
                                <input type="text" class="form-control" name="groomname" placeholder="Groom Name (Male)" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label  class="col-sm-4 form-control-label"><?php echo $this->lang->line('bride_name'); ?> (<?php echo constant('GENDER_F'); ?>) <label class="red">*</label> </label>  
                            <div class="col-sm-7">
                                <input type="text" class="form-control" name="bridename" placeholder="Bride Name (Female)" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 form-control-label"><?php echo $this->lang->line('memeber_id'); ?> (<?php echo constant('GENDER_M'); ?>) <label class="red">*</label> </label>  
                            <div class="col-sm-7">
                                <input type="text" id="member_id"  class="form-control" name="member_id"  onchange="memberValidation(this.value, 'memberid')" placeholder="Member Id (Male)" required>
                                <p><?php echo $this->lang->line('ex_text') . ':' . $this->lang->line('member_msg_text'); ?></p>
                                <label for="member_id" class="error" id="err_member"></label>
                            </div>
                            <div class="col-sm-1" style="display: none;" id="loader_memberid">
                                <span><img src="<?php echo base_url(); ?>assets/images/ajax-loader.gif"></span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="occupation" class="col-sm-4 form-control-label"> <?php echo $this->lang->line('memeber_id'); ?> (<?php echo constant('GENDER_F'); ?>) <label class="red">*</label> </label>  
                            <div class="col-sm-7">
                                <input type="text" class="form-control" name="partnerid" id="partnerid" onchange="memberValidation(this.value, 'partnerid')" placeholder="Member Id (Female)" required>
                                <p><?php echo $this->lang->line('ex_text') . ':' . $this->lang->line('partner_msg_text'); ?></p>
                                <label for="partnerid" class="error" id="err_partner"></label>
                            </div>
                            <div class="col-sm-1" style="display: none;" id="loader_partnerid">
                                <span><img src="<?php echo base_url(); ?>assets/images/ajax-loader.gif"></span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 form-control-label reglab"> <?php echo $this->lang->line('register_profile_for_email'); ?>  <label class="red">*</label> </label>
                            <div class="col-sm-7">
                                <input type="text" style="text-transform: lowercase;" class="form-control" name="email" placeholder="Email Id" required pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" oninvalid="this.setCustomValidity('Please enter valid email format')" oninput="setCustomValidity('')" >
                            </div>
                        </div>

                        <div class="form-group row">
                            <label  class="col-sm-4 form-control-label reglab"> <?php echo $this->lang->line('marriage_date'); ?> <label class="red">*</label> </label>
                            <div class="col-sm-7">
                                <div class="col-md-4 dob">
                                    <select id="dobday" name="date" class="form-control input-sm" required></select>
                                </div>
                                <div class="col-md-4 dob">
                                    <select id="dobmonth" name="month" class="form-control input-sm" required></select>
                                </div>
                                <div class="col-md-4 dob">
                                    <select id="dobyear" name="year" class="form-control input-sm" required></select>
                                </div>	
                            </div>
                        </div>
                        <!--<div class="form-group row">
                          <label  class="col-sm-4 form-control-label reglab">Upload Photo   </label>
                                <div class="col-sm-8">
                                        <input type="file" name="upfile" id="images" onclick="upload()" value="">
                                </div>
                        </div>-->

                        <div class="form-group row">
                            <label  class="col-sm-4 form-control-label reglab"><?php echo $this->lang->line('upload_text'); ?> <label class="red">*</label></label>
                            <div class="col-sm-4">
                                <span style="float:left;"><img alt="" src="#" id="blah"></span>
                            </div>
                            <div class="col-sm-4" id="image">
                                <input type="file" name="upfile" id="photo" onchange="readURL(this);" accept="image/*" required/>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label  class="col-sm-4 form-control-label reglab"><?php echo $this->lang->line('address_text'); ?>  </label>
                            <div class="col-sm-7">
                                <textarea class="form-control"  name="address" rows="3" ></textarea>
                            </div>
                        </div>
                        <div class="form-group row" >
                            <label for="country" class="col-sm-4 form-control-label "><?php echo $this->lang->line('register_profile_for_country'); ?> <label class="red">*</label>   </label>  
                            <div class="col-sm-7">
                                <select class="form-control  js-example-data-array" id="country" name="country" onchange="stateInfo(this.value)" required>
                                    <option value=""> -- Select Country -- </option>
                                </select>
                                <label for="country" class="error"></label> 
                            </div>
                        </div>

                        <div class="form-group row" >
                            <label for="state" class="col-sm-4 form-control-label "><?php echo $this->lang->line('register_profile_for_state'); ?>  </label>  
                            <div class="col-sm-7">
                                <select class="form-control js-example-data-array"  name="state" id="state" onchange="cityInfo(this.value)">
                                    <option value=""> -- Select State -- </option>
                                </select>        
                            </div>
                        </div>

                        <div class="form-group row" >
                            <label for="city" class="col-sm-4 form-control-label "><?php echo $this->lang->line('register_profile_for_city'); ?> </label>  
                            <div class="col-sm-7">
                                <select class="form-control js-example-data-array"  id ="city" name="city">
                                    <option value=""> -- Select City -- </option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 form-control-label reglab"><?php echo $this->lang->line('register_profile_for_mobile'); ?>  <label class="red">*</label> </label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control"maxlength="10" id="mobile" name="mobile" placeholder="Mobile Number" required >
                            </div>
                        </div>
                        <div class="form-group row">
                            <label  class="col-sm-4 form-control-label reglab"><?php echo $this->lang->line('success_story_title'); ?> <label class="red">*</label>  </label>
                            <div class="col-sm-7">
                                <textarea class="form-control"  name="successstory" rows="3" required></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-offset-4 col-sm-8">
                                <button style="float:right;"type="submit" id="sbtn" class="btn btn-primary subbtn"><?php echo $this->lang->line('register_profile_for_submit'); ?></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>



        </div><!-- tab content -->

    </div><!-- col-md-9 end -->



</div><!-- end of container -->
<script src="<?php echo base_url() ?>assets/js/jquery-validate/jquery.validate.min.js"></script>
<script>
                                    $(document).ready(function () {
                                        $("#successpost").validate({
                                            rules: {
                                                phone: {
                                                    matches: "/\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/",
                                                    maxlength: 6,
                                                    maxlength: 15
                                                },
                                                member_id: {
                                                    number: true,
                                                    maxlength: 10
                                                },
                                                partnerid: {
                                                    number: true,
                                                    maxlength: 10
                                                },
                                            }
                                        });
                                        pagination();
                                        $.dobPicker({
                                            daySelector: '#dobday', /* Required */
                                            monthSelector: '#dobmonth', /* Required */
                                            yearSelector: '#dobyear', /* Required */
                                            dayDefault: 'DD', /* Optional */
                                            monthDefault: 'MM', /* Optional */
                                            yearDefault: 'YY', /* Optional */
                                            minimumAge: -1, /* Optional */
                                            maximumAge: 6 /* Optional */
                                        });

                                        //called when key is pressed in textbox
                                        $("#member_id").keypress(function (e) {
                                            //if the letter is not digit then display error and don't type anything
                                            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                                                //display error message
                                                $("#err_member").html("Numbers Only").show().fadeOut("slow");
                                                return false;
                                            }
                                        });
                                        //called when key is pressed in textbox
                                        $("#partnerid").keypress(function (e) {
                                            //if the letter is not digit then display error and don't type anything
                                            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                                                //display error message
                                                $("#err_partner").html("Numbers Only").show().fadeOut("slow");
                                                return false;
                                            }
                                        });
                                    });
							  $("#mobile").keypress(function (e) {
										//if the letter is not digit then display error and don't type anything
										if (e.which != 8 && e.which != 0 && ((e.which < 48) || (e.which > 57))) {
											//display error message
											// $("#mobile").html('<label for="mobile" class="error">Numbers Only</label>').show().fadeOut("slow");
											return false;
										}
									});

                                    /* $('#sbtn').click(function(){
                                     var partnerid = $('#partnerid').val();
                                     var member_id = $('#member_id').val();
                                     if( partnerid == member_id){
                                     alert('Member and partner id should not same');
                                     return false;
                                     }
                                     }); */
                                    function pagination() {

                                        $('#paging_container3').pajinate({
                                            items_per_page: 3,
                                            item_container_id: '.alt_content',
                                            nav_panel_id: '.alt_page_navigation',
                                            nav_label_first: 'Prev',
                                            nav_label_last: 'Next'

                                        });
                                    }
                                    function sucess() {
                                        //alert(123);
                                        $("#tab_b").addClass('active');
                                        $("#tab_a").removeClass('active');
                                        $("#tab2").addClass('active');
                                        $("#tab1").removeClass('active');

                                    }
                                    function memberValidation(id, name) {
                                        if ((id == '' || id == null) && (name == '' || name == null)) {
                                            return false
                                        }
                                        var baseurl = $('#baseurl').val();
                                        $('#loader_' + name).show();
                                        $.ajax({
                                            type: "post",
                                            dataType: "json",
                                            url: baseurl + 'index.php/success/validate?' + name + '=' + id,
                                            success: function (request) {
                                                if (request == 'success') {
                                                    $('#loader_' + name).hide();
                                                    //do next
                                                } else {
                                                    $('#loader_' + name).hide();
                                                    alert(request);
                                                    $('#member_id').val('');
                                                    $('#partnerid').val('');
                                                }
                                            },
                                            error: function (data) {
                                                alert(data);
                                            },
                                        });

                                        return false;
                                    }


                                    function stateInfo(reqData) {
                                        if (reqData == 0) {
                                            return false
                                        }
                                        $("#city").html('');
                                        $("#state").html('');
                                        var baseurl = $('#baseurl').val();//alert(baseurl);
                                        $.ajax({
                                            type: "post",
                                            dataType: "json",
                                            url: baseurl + 'index.php/common/statelist/' + reqData,
                                            success: function (request) {
                                                if (request.status == 0) {
                                                    $("#text-state").html('<input type="text" class="form-control" name="state_text" id="state" placeholder=" Enthe the state">');
                                                    $("#text-city").html('<input type="text" class="form-control" name="city_text" id="city" placeholder=" Enthe the city">');
                                                } else {
                                                    $("#state").select2({
                                                        data: request
                                                    });
                                                }
                                            },
                                            error: function (data) {

                                            },
                                        });

                                        return false;
                                    }

                                    function cityInfo(reqData) {
                                        if (reqData == 0) {
                                            return false
                                        }
                                        $("#city").html('');
                                        var baseurl = $('#baseurl').val();
                                        $.ajax({
                                            type: "post",
                                            dataType: "json",
                                            url: baseurl + 'index.php/common/citylist/' + reqData,
                                            success: function (request) {
                                                if (request.status == 0) {
                                                    $("#text-city").html('<input type="text" class="form-control" name="city_text" id="city" placeholder=" Enthe the city">');
                                                } else {
                                                    $("#city").select2({
                                                        data: request
                                                    });
                                                }
                                            },
                                            error: function (data) {

                                            },
                                        });

                                        return false;
                                    }


                                    function readURL(input) {
                                        var ext = $('#photo').val().split('.').pop().toLowerCase();
                                        if ($.inArray(ext, ['gif', 'png', 'jpg', 'jpeg']) == -1) {
                                            $('#photo').val('');
                                            $('#blah').attr('src', '');
// 		$("#photo" ).attr( "current-error", "Invalid File Format.Allows Only Image File!" );
// 		$('#photo').removeClass('valid').addClass('error');
// 		$("#image").removeClass('has-success').addClass('has-error');
// 		$('#photo').html('<p class="msg error-msg">Invalid File Format.Allows Only Image File</p>');
// 		$('.msg .error-msg').show();
                                            alert('Invalid File Format.Allows Only Image File ');
                                            return false;
                                        }
                                        if (input.files && input.files[0]) {
                                            var reader = new FileReader();
                                            reader.onload = function (e) {
                                                $('#blah')
                                                        .attr('src', e.target.result)
                                                        .width(100)
                                                        .height(100);
                                            }
                                            reader.readAsDataURL(input.files[0]);
                                        }
                                    }
</script>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="<?php echo base_url() . 'assets/css/select2/select2.min.css' ?>">
<!-- Latest compiled and minified JavaScript -->
<script src="<?php echo base_url() . 'assets/js/select2/select2.min.js' ?>"></script>
