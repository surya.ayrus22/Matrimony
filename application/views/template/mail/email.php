<div><div class="adM"> </div><table cellspacing="0" cellpadding="0" border="0" style="color:#333;background:#fff;padding:0;margin:0;width:100%;font:15px 'Helvetica Neue',Arial,Helvetica"> <tbody><tr width="100%"> <td valign="top" align="left" style="background:#f0f0f0;font:15px 'Helvetica Neue',Arial,Helvetica"> <table style="border:none;padding:0 18px;margin:50px auto;width:500px"> <tbody>

                            <tr height="57" width="100%"> <td valign="top" align="left" style="border-top-left-radius:4px;border-top-right-radius:4px;background:#0079bf;padding:12px 18px;text-align:center">

                                    <img width="122" height="37" style="font-weight:bold;font-size:18px;color:#fff;vertical-align:top" title="Matrimony" src="http://www.kevellcorp.com/wp-content/uploads/2015/04/ver1-e1428127669963.png"> </td> </tr>

                            <tr width="100%"> <td valign="top" align="left" style="border-bottom-left-radius:4px;border-bottom-right-radius:4px;background:#fff;padding:18px"> <h1 style="font-size:20px;margin:0;color:#333"> Hello <?php echo $username; ?>, </h1>

                                    <p style="font:15px/1.25em 'Helvetica Neue',Arial,Helvetica;color:#333"> We heard you need a <span class="il">password</span> reset. Click the link below and you'll be redirected to a secure site from which you can set a new <span class="il">password</span>. </p>

                                    <p style="font:15px/1.25em 'Helvetica Neue',Arial,Helvetica;margin-bottom:0;text-align:center;color:#333">

                                        <a style="border-radius:3px;background:#3aa54c;color:#fff;display:block;font-weight:700;font-size:16px;line-height:1.25em;margin:24px auto 24px;padding:10px 18px;text-decoration:none;width:180px;text-align:center" href="<?php echo base_url() . 'index.php/login/reset?id=' . $guid ?>"> Reset Password </a> </p>

                                    <p style="font:15px/1.25em 'Helvetica Neue',Arial,Helvetica;color:#939393;margin-bottom:0"> If you didn't try to reset your <span class="il">password</span>, <a data-saferedirecturl="">click here</a> and we'll <span class="il">forget</span> this ever happened. </p>

                                </td> </tr>

                        </tbody> </table> </td> </tr></tbody> </table> </div>
