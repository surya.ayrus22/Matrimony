<script src="<?php echo base_url(); ?>/assets/js/dobPicker.min.js"></script>
<link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
<script>
    $(document).ready(function () {
        $.dobPicker({
            daySelector: '#dobday', /* Required */
            monthSelector: '#dobmonth', /* Required */
            yearSelector: '#dobyear', /* Required */
            dayDefault: 'DD', /* Optional */
            monthDefault: 'MM', /* Optional */
            yearDefault: 'YY', /* Optional */
            minimumAge: 18, /* Optional */
            maximumAge: 40 /* Optional */
        });
    });
</script>
<script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/jquery.onImagesLoad.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/jquery.responsiveSlides.js"></script>
<script language="javascript">

    $(function () {

        var p = $('#content').responsiveSlides({
            height: 650, // slides conteiner height
            background: '#fff', // background color and color of overlayer to fadeout on init
            autoStart: true, // boolean autostart
            startDelay: 0, // start whit delay
            effectInterval: 5000, // time to swap photo
            effectTransition: 1000, // time effect
            pagination: [
                {
                    active: false, // activate pagination
                    inner: true, // pagination inside or aouside slides conteiner
                    position: 'B_L', /* 
                     pagination align:
                     T_L = top left
                     T_C = top center
                     T_R = top right
                     
                     B_L = bottom left
                     B_C = bottom center
                     B_R = bottom right
                     */
                    margin: 10, // pagination margin
                    dotStyle: '', // dot pagination class style
                    dotStyleHover: '', // dot pagination class hover style
                    dotStyleDisable: ''		// dot pagination class disable style
                }
            ]
        });

    });
</script>
<script src="<?php echo base_url(); ?>assets/js/register.js"></script>
<input type="hidden" id="baseurl" value="<?php echo base_url() ?>"/>
<div class="banner " >
    <div id="" class="mobbg">
        <div style="position: absolute; z-index: 1;">
            <div class="container">
                <div class="banner_info">
                    <div class="pull-left register">
                        <h2 class="regtitle"><?php echo $this->lang->line("register_profile_for_title1"); ?></h2>
                        <form action="<?php echo base_url(); ?>index.php/home/registration" method="post">
                            <div class="form-group row">
                                <label for="profile" class="col-sm-5 form-control-label reglab"><?php echo $this->lang->line("register_profile_for_profile1"); ?></label>
                                <div class="col-sm-7">
                                    <select class="form-control" id="profile_created" name="profile_created" required >
                                        <option value=""> -- Select Profile for -- </option>
                                        <option value="myself">Myself</option>
                                        <option value="son">My son</option>
                                        <option value="daughter">My daughter</option>
                                        <option value="brother">My brother</option>
                                        <option value="sister">My sister</option>
                                        <option value="relative">My relative</option>
                                        <option value="friend">My friend</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="username" id="label_name" class="col-sm-5 form-control-label reglab"><?php echo $this->lang->line("register_profile_for_name"); ?></label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="username" name="username" placeholder="Username" required oninvalid="this.setCustomValidity('Please enter username')" oninput="setCustomValidity('')">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="gender" class="col-sm-5 form-control-label reglab"><?php echo $this->lang->line("register_profile_for_gender"); ?></label> 
                                <div class="col-sm-7">
                                    <label class="radio-inline reglabrad">
                                        <input type="radio" name="gender" id="male" value="M" required> <?php echo $this->lang->line('register_profile_for_male'); ?>
                                    </label>
                                    <label class="radio-inline reglabrad">
                                        <input type="radio" name="gender" id="female" value="F" required> <?php echo $this->lang->line('register_profile_for_female'); ?>
                                    </label>
                                    <input type="hidden" name="gender_hidden" id="gender_hidden" value="0" >
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="date" class="col-sm-5 form-control-label reglab"><?php echo $this->lang->line("register_profile_for_dob"); ?></label>
                                <div class="col-sm-7">
                                    <div class="col-md-4 dob">
                                        <select id="dobday" name="date" class="form-control input-sm" required></select>
                                    </div>
                                    <div class="col-md-4 dob">
                                        <select id="dobmonth" name="month" class="form-control input-sm" required></select>
                                    </div>
                                    <div class="col-md-4 dob">
                                        <select id="dobyear" name="year" class="form-control input-sm" required></select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="religion" class="col-sm-5 form-control-label reglab"><?php echo $this->lang->line("register_profile_for_religion"); ?></label>
                                <div class="col-sm-7">
                                    <select class="form-control" id="religion" name="religion" required >
                                        <!--<option value=""> -- Select Profile for -- </option>-->
                                        <option value="hindu">Hindu</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="mother_tongue" class="col-sm-5 form-control-label reglab"><?php echo $this->lang->line("register_profile_for_tongue"); ?></label>
                                <div class="col-sm-7">
                                    <select class="form-control" id="mother_tongue" name="mother_tongue" required>
                                        <option value="">- Select your Mother Tongue -</option>
                                        <option value="tamil">Tamil</option>
                                        <option value="bengali">Bengali</option>
                                        <option value="english">English</option>
                                        <option value="gujarati">Gujarati</option>
                                        <option value="hindi">Hindi</option>
                                        <option value="kannada">Kannada</option>
                                        <option value="konkani">Konkani</option>
                                        <option value="malayalam">Malayalam</option>
                                        <option value="marathi">Marathi</option>
                                        <option value="marwari">Marwari</option>
                                        <option value="oriya">Oriya</option>
                                        <option value="punjabi">Punjabi</option>
                                        <option value="sindhi">Sindhi</option>
                                        <option value="telugu">Telugu</option>
                                        <option value="urdu">Urdu
                                        <option value="others">Others</option></option>
                                        </optgroup>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="email" class="col-sm-5 form-control-label reglab"><?php echo $this->lang->line("register_profile_for_email"); ?> </label>
                                <div class="col-sm-7">
                                    <input type="text" style="text-transform: lowercase;" class="form-control" id="email" name="email" value="" placeholder="Email Id" onblur="emailavailablility()" required pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" oninvalid="this.setCustomValidity('Please enter valid email format')" oninput="setCustomValidity('')">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="password" class="col-sm-5 form-control-label reglab"><?php echo $this->lang->line("register_profile_for_password"); ?></label>
                                <div class="col-sm-7">
                                    <input type="password" class="form-control" id="password" name="password" placeholder="Password" pattern=".{6,}" required oninvalid="this.setCustomValidity('Please enter password minimum 6 character')" oninput="setCustomValidity('')">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="mobile" class="col-sm-5 form-control-label reglab"><?php echo $this->lang->line("register_profile_for_mobile"); ?></label>     
                                <div>
                                    <div class="col-sm-1">
                                        <label class="form-control-label reglab">+91 </label>
                                    </div>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" title="Enter numbers only" maxlength="10" id="mobile" name="mobile"  placeholder="Mobile Number" required oninvalid="this.setCustomValidity('Please enter mobile number')" oninput="setCustomValidity('')">
                                    </div>
                                </div>  
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-offset-7 col-sm-5">
                                    <button type="submit" class="btn btn-default subbtn" name="step1"><?php echo $this->lang->line("register_profile_for_reg_now"); ?></button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
                    <!--img src="<?php echo base_url(); ?>/assets/images/backnew1.jpeg" width="100%">
                    <img src="<?php echo base_url(); ?>/assets/images/bg3.jpg" width="100%">
                    <!--img src="<?php echo base_url(); ?>/assets/2.jpg">
                    <img src="<?php echo base_url(); ?>/assets/3.jpg"-->
        </div>
    </div>
    <div class="profile_search">
        <div class="container wrap_1">
            <h1 class="titlesome"><?php echo $this->lang->line("home_page_step2_title"); ?></h1>
            <form action="<?php echo base_url(); ?>index.php/login/login_form" method="post">
                <div class="search_top">
                    <div class="col-md-2 " style="margin-top: 3px;">
                        <label class="radio-inline ">
                            <input type="radio" name="gender" value="M" style="margin-top:3px;"> <?php echo $this->lang->line('home_page_groom_text'); ?>
                        </label>
                        <label class="radio-inline ">
                            <input type="radio" name="gender" value="F" style="margin-top:3px;"> <?php echo $this->lang->line('home_page_bride_text'); ?> 
                        </label>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group row">
                            <label for="age" class="form-control-label"><?php echo $this->lang->line("search_profile_for_age"); ?> :</label>
                            <input class="transparent" placeholder="18" style="width: 30%; border:1px solid #E0E0E0;height: 29px;" type="text" value="">
                            &nbsp;-&nbsp;
                            <input class="transparent" placeholder="26" style="width: 30%; border:1px solid #E0E0E0; height: 29px;" type="text" value="">

                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <select class="form-control" id="" style="height:30px;">
                                <option value="">Hindu</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <?php echo form_dropdown('caste', (!empty($caste) ? $caste : ''), '', 'class="form-control" style="height:30px;"'); ?>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="submit inline-block">
                            <input id="submit-btn" class="hvr-wobble-vertical" type="submit" value="<?php echo $this->lang->line('home_page_find_matches'); ?>">
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div> 
</div> 

<script type="text/javascript">

    $(document).ready(function () {
        $(".divs div").each(function (e) {
            if (e != 0)
                $(this).hide();
        });

        $("#next").click(function () {
            if ($(".divs div:visible").next().length != 0)
                $(".divs div:visible").next().show().prev().hide();
            else {
                $(".divs div:visible").hide();
                $(".divs div:first").show();
            }
            return false;
        });

        $("#prev").click(function () {
            if ($(".divs div:visible").prev().length != 0)
                $(".divs div:visible").prev().show().next().hide();
            else {
                $(".divs div:visible").hide();
                $(".divs div:last").show();
            }
            return false;
        });

        $("#mobile").keypress(function (e) {
            //if the letter is not digit then display error and don't type anything
            if (e.which != 8 && e.which != 0 && ((e.which < 48) || (e.which > 57))) {
                //display error message
                // $("#mobile").html('<label for="mobile" class="error">Numbers Only</label>').show().fadeOut("slow");
                return false;
            }
        });

        $("#profile_created").change(function () {
            var create = $(this).val();
            if (create == "son" || create == "brother") {
                $("#male").prop("checked", true);
                $("#male").prop("disabled", true);
                $("#female").prop("disabled", true);
                $("#gender_hidden").val("M");
                if (create == "son") {
                    $("#label_name").html("Son's Name");
                } else {
                    $("#label_name").html("Groom's Name");
                }
            }

            if (create == "daughter" || create == "sister") {
                $("#female").prop("checked", true);
                $("#male").prop("disabled", true);
                $("#female").prop("disabled", true)
                $("#gender_hidden").val("F");
                if (create == "daughter") {
                    $("#label_name").html("Daughter's Name");
                } else {
                    $("#label_name").html("Bride's Name");
                }
            }

            if (create == "myself" || create == "friend" || create == "relative") {
                $("#male").prop("disabled", false);
                $("#female").prop("disabled", false);
                $("#male").prop("checked", false);
                $("#female").prop("checked", false);
                $("#label_name").html("Name");
                $("#gender_hidden").val("");
            }

            return false;
        });

    });


</script>

<div class="backimg">
    <div class="row contbg"> 
        <div class="col-md-6">
            <img src="<?php echo base_url(); ?>assets/images/m/bg.jpg" width="100%" height="400" >  
        </div>
        <div class="col-md-6 contactus">
            <div class="col-md-offset-1 col-md-7 col-md-offset-4 cont"><?php echo $this->lang->line('home_page_service'); ?></div>
            <div class="col-md-offset-1 col-md-7"><a href="<?php echo base_url(); ?>index.php/contact/contact_us"><span style="background: #FFA417;" class="innerblock-btn2"><?php echo $this->lang->line('home_page_contact'); ?></span></a></div>
        </div>
        <!--</div>-->
    </div>
    <div class="bg">
        <div class="container"> 
            <h3><?php echo $this->lang->line("home_page_step3_title"); ?></h3>
            <div class="heart-divider">
                <span class="grey-line"></span>
                <i class="fa fa-heart pink-heart"></i>
                <i class="fa fa-heart grey-heart"></i>
                <span class="grey-line"></span>
            </div>
            <?php if (!empty($success)) { ?>
                <div class="col-sm-12">	
                    <div class="divs">
                        <?php foreach ($success as $value) { ?>
                            <div class="cls1 ">
                                <p class="col-sm-6 bg_left">
                                    <img src="<?php echo base_url() . 'assets/success_images/' . $value['image']; ?>" width="100%" height="300" style="padding: 2em 1em;">              
                                </p>
                                <span class="col-sm-6 bg-rightmes">
                                    <h1> <?php echo $this->lang->line('mr_text') . '.' . (!empty($value['groom_name']) ? $value['groom_name'] : 'Not Specified'); ?>  &  <?php echo $this->lang->line('mrs_text') . '.' . (!empty($value['bride_name']) ? $value['bride_name'] : 'Not Specified'); ?></h1>
                                    <p style="font-size:22px;margin-top:25px;"><?php echo $value['success_story']; ?></p>
                                </span>
                            </div>

                        <?php } ?>
                    </div>
                </div>
                <div class="text-right">
                    <a href="#" id="prev" style="color:floralwhite;"><i class="fa fa-caret-square-o-left fa-2x" aria-hidden="true"></i></a>
                    <a href="#" id="next" style="color:floralwhite;"><i class="fa fa-caret-square-o-right fa-2x" aria-hidden="true"></i></a>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
<!--div class="map">
   <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d3150859.767904157!2d-96.62081048651531!3d39.536794757966845!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sin!4v1408111832978"> </iframe>
</div-->


