<link href="<?php echo base_url(); ?>assets/css/bootstrap-3.1.1.min.css" rel='stylesheet' type='text/css' />
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
<style>
    .tab{ margin-left:4px;}
    .reg_border{
        border: 1px solid #ddd;
        border-radius: 4px;
        box-shadow: 0 6px 12px rgba(0, 0, 0, 0.176);
        padding-bottom: 30px;
    }
    .someone_container {
        margin: 15% auto 0;
        width: 100%;
    }
    .sucess{
        font-size: 25px;
        margin-top: 2%;
        color:#58B035;
        margin-bottom: 2%;
    }
    .det_pro {
        height: 35px;
    }

    @media (max-width:480px){

        .reg_border{

            height: 270px;
        }
        .sucess{
            font-size:20px;
        }
        .reg_border1 {
            margin-left: 0px;
        }
    }
    .refer{
        font-size: 14px;

    }

    .do_this_later {
        float: right;
    }
    .reg_border1 {
        margin-left: 53px;
    }
    .center{
        margin-left:25%;
    }
</style>
<div class="container someone_container">
    <div class="row reg_border">
        </br>
        <div class="do_this_later">
            <a href="<?php echo base_url(); ?>" class="light_blue"><?php echo $this->lang->line("success_page_text_skip"); ?> </a><a href="#"><span class="gray_right_arrow"></span></a>
        </div>

        <div class="reg_border1">
            <div class="center">
                <div class="sucess"><?php echo (!empty($message) ? $message : ''); ?></div>
                <?php if (!empty($user)) { ?>
                    <p class="name"><?php echo sprintf($this->lang->line("success_page_hi"), (!empty($user['username']) ? ucwords($user['username']) : '')); ?>,</p>
                    <p class="name tab"><?php echo sprintf($this->lang->line("success_page_text_step1"), (!empty($user['email']) ? $user['email'] : '')); ?></p>
                    <?php if (!empty($register) && !empty($user)) { ?>
                        <p class="refer tab">
                            <?php echo $this->lang->line("success_page_text_step2"); ?> 
                        </p>
                        <?php
                    }
                }
                ?>
                <a href="<?php echo base_url(); ?>"  name="step1">Login </a>
            </div>
        </div>
    </div>
</div>



