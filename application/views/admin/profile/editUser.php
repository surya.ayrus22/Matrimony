<?php //foreach($editDetails as $state ) {echo $state['stateList'];}    ?>
<link href="<?php echo base_url(); ?>assets/css/photo_edit.css" media="all" rel="stylesheet" type="text/css" />
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/select2/select2.min.css">

<!-- Latest compiled and minified JavaScript -->
<script src="<?php echo base_url(); ?>assets/js/select2/select2.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/fileinput.js" type="text/javascript"></script>
<link href="<?php echo base_url(); ?>assets/css/fileinput.css" media="all" rel="stylesheet" type="text/css" />
<script src="<?php echo base_url(); ?>assets/admin/js/register.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.validate.min.js"></script>
<link rel="stylesheet" href="<?php echo base_url() . 'assets/css/jquery-ui.css'; ?>">
<script src="<?php echo base_url() . 'assets/js/jquery-ui.js'; ?>"></script>
<script>
    $(document).ready(function () {
        $(".js-example-basic-single").select2();
        $(".js-example-data-array-selected").select2();
        $("#caste").select2({
            data: <?php echo (!empty($casteList) ? $casteList : NULL) ?>
        });

        $("#country").select2({
            data: <?php echo (!empty($countryList) ? $countryList : NULL) ?>
        });

        $("#state").select2({
            data: <?php echo (!empty($stateList) ? $stateList : NULL); ?>
        });
        $("#city").select2({
            data: <?php echo (!empty($cityList) ? $cityList : NULL); ?>
        });

        $("#pre_caste").select2({
            data: <?php echo (!empty($casteList) ? $casteList : NULL) ?>
        });

        $("#pre_country").select2({
            data: <?php echo (!empty($countryList) ? $countryList : NULL) ?>
        });

        $("#pre_state").select2({
            data: <?php echo (!empty($stateList) ? $stateList : NULL) ?>
        });

        $("#pre_city").select2({
            data: <?php echo (!empty($cityList) ? $cityList : NULL) ?>
        });



        $.dobPicker({
            daySelector: '#dobday', /* Required */
            monthSelector: '#dobmonth', /* Required */
            yearSelector: '#dobyear', /* Required */
            dayDefault: 'DD', /* Optional */
            monthDefault: 'MM', /* Optional */
            yearDefault: 'YY', /* Optional */
            minimumAge: 0, /* Optional */
            maximumAge: 50 /* Optional */
        });



    });

    $(function () {
        $("#marialstatus").change(function () {
            var val = $(this).val();
            if (val == "1") {
                $("#childs").hide();
            }
            else if (val == "2") {
                $("#childs").show();
                $("#childs").css("style", "block");
            }
            else if (val == "3") {
                $("#childs").show();
                $("#childs").css("style", "block");
            }
            else if (val == "4") {
                $("#childs").show();
                $("#childs").css("style", "block");
            }
        });
    });

    $(function () {
        $("#datepicker").datepicker({dateFormat: 'y-mm-dd'});
    });

</script>
<style>
    .file-input-new{
        display:none;	
    }
    .file-input.file-input-ajax-new {
        display: none;
    }
    .col-md-4.dob {
        padding-left: 0;
        padding-right: 0;
    }
    .tab-content {
        display: block;
    }
</style>

<div id="page-wrapper">
    <div class="graphs bgimage">
        <content-top>
            <div class="content-top clearfix">
                <h1 class="al-title"><?php echo $this->lang->line('edit_text'), ' ', $this->lang->line('profile_admin'); ?></h1>
                <ul class="breadcrumb al-breadcrumb">
                    <li><a href="<?php echo base_url() . 'index.php/admin/dashboard'; ?>"><?php echo $this->lang->line('dashboard'); ?> </a></li>
                    <?php if ($type == constant('TYPE_1') || $type == constant('TYPE_2') || $type == constant('TYPE_3') || $type == constant('TYPE_4') || $type == constant('TYPE_5') || $type == constant('TYPE_6') || $type == constant('TYPE_7') || $type == constant('TYPE_13') || $type == constant('TYPE_15') ) { ?>		
                        <li>
                            <a href="<?php echo base_url() . 'index.php/admin/userdetails?type=' . $type; ?>"> <?php echo $title; ?>  </a>
                        </li>  			

                    <?php } elseif ($type == constant('TYPE_8') || $type == constant('TYPE_9') || $type == constant('TYPE_10') ) { ?>
                        <li>
                            <a href="<?php echo base_url() . 'index.php/adminuser/premiummember?type=' . $type; ?>"><?php echo $title; ?> </a>
                        </li>                
                    <?php } elseif ($type == constant('TYPE_11') ) { ?>
                        <li>
                            <a href="<?php echo base_url() . 'index.php/admin_profile?type=' . $type; ?>"><?php echo $title; ?> </a>
                        </li> 
                    <?php } ?>	
                    <?php if($type != constant('TYPE_15') ){ ?>
                    <li>
                        <a href="<?php echo base_url() . 'index.php/admin_profile/viewUser?uid=' . $userGuid . '&type=' . $type; ?>"><?php echo $this->lang->line('view_user_profile_text'); ?></a>
                    </li> 
                    <?php } ?>
                    <li class=""><?php echo $this->lang->line('edit_text'), ' ', $this->lang->line('profile_admin'); ?></li>
                </ul>
            </div>
        </content-top>
        <div class="col-md-12 contentinner" >
            <?php 
            if (!empty($editDetails[$userGuid])  ) {  
                //foreach ($editDetails as $profile) {
                $profile = (!empty($editDetails[$userGuid])?$editDetails[$userGuid]:'');
					if( !empty ( $editDetails[$userGuid] ) == (!empty($profile['userGuid']) ? $profile['userGuid'] : '')){
                    /** profile */
                    
                    $userGuid = (!empty($profile['userGuid']) ? $profile['userGuid'] : '');
                    $username = (!empty($profile['username']) ? $profile['username'] : '');
                    $profile_created = (!empty($profile['profile_created']) ? $profile['profile_created'] : '');
                    $userid = (!empty($profile['userId']) ? $profile['userId'] : '');
                    $password = (!empty($profile['password']) ? $profile['password'] : '');
                    $email = (!empty($profile['email']) ? $profile['email'] : '');
                    $mobile = (!empty($profile['mobile']) ? $profile['mobile'] : '');
                    $martialstatus = (!empty($profile['martial_status']) ? $profile['martial_status'] : '');
                    $casteid = (!empty($profile['caste']) ? $profile['caste'] : '');
                    $subCaste = (!empty($profile['sub_caste']) ? $profile['sub_caste'] : '');
                    $gothram = (!empty($profile['gothram']) ? $profile['gothram'] : '');
                    $dob = (!empty($profile['dob']) ? $profile['dob'] : '');
                    $childs = (!empty($profile['childs']) ? $profile['childs'] : '');
                    $gender = (!empty($profile['gender']) ? $profile['gender'] : '');
                    $phystatus = (!empty($profile['physical_status']) ? $profile['physical_status'] : '');
                    $bodytype = (!empty($profile['bodytype']) ? $profile['bodytype'] : '');
                    $complexion = (!empty($profile['complexion']) ? $profile['complexion'] : '');
                    $height = (!empty($profile['height']) ? $profile['height'] : '');
                    $weight = (!empty($profile['weight']) ? $profile['weight'] : '');
                    $country = (!empty($profile['country']) ? $profile['country'] : '');
                    $state = (!empty($profile['state']) ? $profile['state'] : '');
                    $city = (!empty($profile['city']) ? $profile['city'] : '');
                    $country_name = (!empty($profile['country_name']) ? $profile['country_name'] : '');
                    $state_name = (!empty($profile['state_name']) ? $profile['state_name'] : '');
                    $city_name = (!empty($profile['city_name']) ? $profile['city_name'] : '');
                    $education = (!empty($profile['education']) ? $profile['education'] : '');
                    $occupation = (!empty($profile['occupation']) ? $profile['occupation'] : '');
                    $qualification = (!empty($profile['qualification']) ? $profile['qualification'] : '');
                    $employeed = (!empty($profile['employeed_in']) ? $profile['employeed_in'] : '');
                    $salary = (!empty($profile['salary']) ? $profile['salary'] : '');
                    $salary_view = (!empty($profile['salary_view']) ? $profile['salary_view'] : '');
                    $religion = (!empty($profile['religion']) ? $profile['religion'] : '');
                    $tongue = (!empty($profile['mother_tongue']) ? $profile['mother_tongue'] : '');
                    $dhosam = (!empty($profile['dhosam']) ? $profile['dhosam'] : '');
                    $gothram = (!empty($profile['gothram']) ? $profile['gothram'] : '');
                    $dhosamtype = (!empty($profile['dhosam_type']) ? $profile['dhosam_type'] : '');
                    $star = (!empty($profile['star']) ? $profile['star'] : '');
                    $raasi = (!empty($profile['raasi']) ? $profile['raasi'] : '');
                    $food = (!empty($profile['food']) ? $profile['food'] : '');
                    $drink = (!empty($profile['drink']) ? $profile['drink'] : '');
                    $smoke = (!empty($profile['smoke']) ? $profile['smoke'] : '');
                    $ftype = (!empty($profile['family_type']) ? $profile['family_type'] : '');
                    $fvalues = (!empty($profile['family_values']) ? $profile['family_values'] : '');
                    $fstatus = (!empty($profile['family_status']) ? $profile['family_status'] : '');
                    $castename = (!empty($profile['caste_name']) ? $profile['caste_name'] : '');
                    $eduname = (!empty($profile['edu_name']) ? $profile['edu_name'] : '');
                    $summary = (!empty($profile['summary']) ? $profile['summary'] : '');
                    ?> 
                    <div class="col-md-6 text-left"><h3><?php echo constant('MEMBERID') . $userid . " / " . $username; ?></h3>
                        <?php
                        if (!empty($message)) {
                            echo $message;
                        }
                        ?>
                    </div>
                    <div class="col-md-6 text-right">
                            <!--div class="form-group"><input type="submit" value="<?php echo $this->lang->line('save_text'); ?>" class="btn btn-primary">
                            <button class="btn btn-danger"><?php //echo $this->lang->line('delete');    ?></button>	</div-->				
                    </div>

                    <hr>
                    <div class="col-md-12">
                        <h4 class="col-md-3 text-left"><?php echo $this->lang->line('profile_image'); ?></h4>
                        <div class="col-md-12">
                            <span id="upfile1" class="btn btn-success subbtn" type="submit" style="float:right;">
                                <i class="fa fa-desktop" ></i><?php echo $this->lang->line('add_photo'); ?>
                            </span>

                            <input id="images" type="file" name="upfile" multiple class="file" data-overwrite-initial="false" data-min-file-count="1">

                        </div>
                    </div>								

                    <div class="col-md-12 text-left">

                        <table class="table">
                            <tbody>
                                <tr class="">
                                    <td class="">
                                        <?php
                                        //echo "<pre>";print_r($profile_images); 
                                        if (!empty($profile_images)) {
                                            $i = 1;
                                            foreach ($profile_images as $val) { //echo "<pre>";print_r($val);exit;
                                                if (!empty($val['image'])) {
                                                    ?>
                                                    <div class="col-md-3 ">
                                                        <div class="proimg_border">
                                                            <div class="page active">
                                                                <span class="pull-right pointer"><a  title="Click to add main photo" onclick="activeimage('<?php echo $val['user_guid']; ?>', '<?php echo $val['image']; ?>')" class="fa fa-check-circle-o fa-short" style=" padding: 10px;" ></a></span>
                                                                <span class="pull-right pointer"><a  title="Delete" onclick="deleteimage('<?php echo $val['user_guid']; ?>', '<?php echo $val['image']; ?>')" class="fa fa-trash-o " style=" padding: 10px;"  ></a></span>
                                                            </div>
                                                            <?php
                                                            $image = '';
                                                            if (file_exists(dirname($_SERVER["SCRIPT_FILENAME"]) . "/assets/upload_images/" . $val['image'])) {
                                                                $image = $val['image'];
                                                            }
                                                            ?>
                                                            <img src="<?php echo base_url() . 'assets/upload_images/' . $image ?>" class="img_1" alt="" width="200" height="200" />
                                                            <?php if ($val['active'] == 1) { ?>
                                                                <div class="main subbtn"><?php echo $this->lang->line('main_photo'); ?></div>
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                    <?php
                                                } //else { echo "No Image"; }
                                                $i++;
                                            }
                                        } else {
                                            echo $this->lang->line('no_data');
                                        }
                                        ?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <hr>
                    <h4> <?php echo $this->lang->line('register_profile_for_email') . ' & ' . $this->lang->line('register_profile_for_password'); ?> </h4>
                    <div class="col-md-12 text-left">
                        <div id="div7" style="display:block; ">
                            <div class="edit_btn">
                                <a id="" href="javascript:SwapDivsWithClick('div7','div8')" style="float: right;" class="btn btn-primary ">
                                    <i class="fa fa-pencil"></i><?php echo $this->lang->line('edit_text'); ?></a>
                            </div>
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <td><label class=" form-control-label"><?php echo $this->lang->line('register_profile_for_email'); ?></label></td>
                                        <td>:</td>		
                                        <td><?php echo (!empty($email) ? $email : 'Not Specified'); ?></td>
                                    </tr>

                                </tbody>	
                            </table>
                        </div>
                        <!--form-->
                        <div id="div8" style="display:none; ">
                            <div class="edit_btn"><a id="" href="javascript:SwapDivsWithClick('div7','div8')" style="float: right;" class="btn btn-primary ">
                                    <i class="fa fa-times"></i><?php echo $this->lang->line('close_text'); ?></a></div>
                            <div class="col-md-12" style="margin-top:20px;">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a data-toggle="tab" href="#emailsetting">Email</a></li>
                                    <li><a data-toggle="tab" href="#passwordsetting">Password</a></li>
                                </ul>

                                <div class="tab-content">
                                    <div id="emailsetting" class="tab-pane fade in active">
                                        <form id="email-form">
                                            <div class="form-group row">
                                                <label class="col-sm-3 form-control-label"><?php echo $this->lang->line('register_profile_for_email'); ?></label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control"  name="email" id="email" placeholder="Email" required pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" value="<?php echo $email; ?>" >
                                                    <label class="error" id="exits"></label>
                                                </div>
                                                <input type="hidden" value="<?php echo $userGuid; ?>" name="userGuid">
                                            </div>

                                            <div class="form-group row ">
                                                <div class="col-sm-offset-3 col-sm-9 pull-right">
                                                    <button type="submit" class="btn btn-primary" ><?php echo $this->lang->line("save_text"); ?></button>
                                                </div>
                                            </div>	
                                        </form>
                                    </div>
                                    <div id="passwordsetting" class="tab-pane fade">
                                        <form id="passchange-form">
                                            <!--div class="form-group row">
                                                  <label for="inputEmail3" class="col-sm-3 form-control-label"><?php echo sprintf($this->lang->line('enter_current_password'), ''); ?></label>
                                                  <div class="col-sm-9">
                                                    <input class="form-control" id="pass_change"  name="password" id="inputEmail3"  type="text"   required	Placeholder ="Current Password">
                                                    
                                                  </div>
                                            </div-->
                                            <div class="form-group row">
                                                <label for="" class="col-sm-3 form-control-label"><?php echo $this->lang->line('enter_new_password'); ?></label>
                                                <div class="col-sm-9">
                                                    <input class="form-control" id="new" name="new_password" type="text"   required 	Placeholder ="New Password"	>
                                                    <input type="hidden" name="email" value="<?php echo $email; ?>">
                                                    <input type="hidden" value="<?php echo $userGuid; ?>" name="userGuid">
                                                </div>

                                            </div>
                                            <div class="form-group row">
                                                <label for="" class="col-sm-3 form-control-label"><?php echo $this->lang->line('conform_password'); ?></label>
                                                <div class="col-sm-9">
                                                    <input class="form-control" id="confirm" name="conform_password" type="text"   required 	Placeholder ="Confirm Password"	>
                                                </div>
                                            </div>	
                                            <div class="form-group row">
                                                <div class="col-sm-offset-3 col-sm-9 pull-right">
                                                    <button type="submit" class="btn btn-primary" name="step2"><?php echo $this->lang->line('save_text'); ?></button>
                                                </div>
                                            </div>
                                        </form>				
                                    </div>

                                </div>

                            </div>	


                        </div>
                    </div>		
                    <hr>

                    <h4><?php echo $this->lang->line('basic'); ?></h4>									

                    <div class="col-md-12 text-left">
                        <div id="div1" style="display:block; ">
                            <div class="edit_btn">
                                <a id="" href="javascript:SwapDivsWithClick('div1','div2')" style="float: right;" class="btn btn-primary ">
                                    <i class="fa fa-pencil"></i><?php echo $this->lang->line('edit_text'); ?></a>
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-6">
                                    <table class="table">
                                        <tr>
                                            <td><label class=" form-control-label"><?php echo sprintf($this->lang->line('search_profile_create'), $this->lang->line('for_text')); ?></label></td>
                                            <td>:</td>		
                                            <td><?php echo (!empty($profile_created) ? $profile_created : 'Not Specified'); ?></td>
                                        </tr>
                                        <tr>
                                            <td><label class=" form-control-label"><?php echo $this->lang->line('register_profile_for_name'); ?></label></td>
                                            <td>:</td>		
                                            <td><?php echo (!empty($username) ? $username : 'Not Specified'); ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><label class=" form-control-label"><?php echo sprintf($this->lang->line('mobile_admin_register'), ''); ?></label></td>
                                            <td>:</td>		
                                            <td><?php echo (!empty($mobile) ? $mobile : 'Not Specified'); ?></td>
                                        </tr>

                                        <tr >
                                            <td ><label class=" form-control-label"><?php echo $this->lang->line('register_profile_for_gender'); ?></label></td>
                                            <td> :</td>
                                            <td ><?php echo (!empty($gender) ? constant('GENDER_' . $gender) : 'Not Specified'); ?></td>
                                        </tr>
                                    </table>	
                                </div>
                                <div class="col-md-6">
                                    <table class="table">
                                        <tr>
                                            <td><label class=" form-control-label"><?php echo $this->lang->line('register_profile_for_dob'); ?> </label></td>	
                                            <td>:</td>
                                            <td ><?php echo (!empty($dob) ? $dob : 'Not Specified'); ?></td>
                                        </tr>
                                        <tr>
                                            <td><label class=" form-control-label"><?php echo $this->lang->line('register_profile_for_religion'); ?> </label></td>
                                            <td>:</td>
                                            <td ><?php echo (!empty($religion) ? $religion : 'Not Specified'); ?></td>
                                        </tr>
                                        <tr>
                                            <td ><label class=" form-control-label"><?php echo $this->lang->line('register_profile_for_tongue'); ?></label></td>
                                            <td>  :</td>
                                            <td ><?php echo (!empty($tongue) ? $tongue : 'Not Specified'); ?></td>
                                        </tr>					 
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!--form-->
                        <div id="div2" style="display:none; ">
                            <div class="edit_btn"><a id="" href="javascript:SwapDivsWithClick('div1','div2')" style="float: right;" class="btn btn-primary ">
                                    <i class="fa fa-times"></i><?php echo $this->lang->line('close_text'); ?></a></div>
                            <form class="form-horizontal" id="pesonal" method="post">	
                                <div class="col-md-12">
                                    <div class="col-md-6">						
                                        <table class="table">
                                            <tr>
                                                <td> <label for="profile_created" class=" form-control-label"><?php echo sprintf($this->lang->line('search_profile_create'), $this->lang->line('for_text')); ?><span class="red">*</span></label></td>
                                                <td>:</td>		
                                                <td>        
                                                    <select class="form-control js-example-basic-single" id="profilecreated" name="profile_created" required >
                                                        <option value=""> -- Select Profile for -- </option>
                                                        <option value="myself">Myself</option>
                                                        <option value="son">My son</option>
                                                        <option value="daughter">My daughter</option>
                                                        <option value="brother">My brother</option>
                                                        <option value="sister">My sister</option>
                                                        <option value="relative">My relative</option>
                                                        <option value="friend">My friend</option>
                                                    </select>
                                                    <label for="profile_created" class="error"></label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td> <label class=" form-control-label "><?php echo $this->lang->line('register_profile_for_name'); ?><span class="red">*</span></label></td>
                                                <td>:</td>		
                                                <td><input class="form-control" value="<?php echo $username; ?>" name="username" id ="username" type="text" required></td>
                                            </tr>
                                            <tr>
                                                <td> <label class=" form-control-label"><?php echo sprintf($this->lang->line('mobile_admin_register'), ''); ?><span class="red">*</span></label></td>
                                                <td>:</td>		
                                                <td><input class="form-control" value="<?php echo $mobile; ?>" name="mobile" id ="mobile" type="text" required></td>
                                            </tr>
                                            <tr >
                                                <td ><label class=" form-control-label" for="gender"><?php echo $this->lang->line('register_profile_for_gender'); ?></label><span class="red">*</span></td>
                                                <td> :</td>
                                                <td>
                                                    <?php
                                                    $gender_m = $gender_f = '';
                                                    if ($gender == 'M') {
                                                        $gender_m = 'checked';
                                                    } elseif ($gender == 'F') {
                                                        $gender_f = 'checked';
                                                    }
                                                    ?>
                                                    <div class="col-md-3">
                                                        <label class="radio-inline reglabrad">
                                                            <input type="radio" name="gender" <?php echo $gender_m; ?> id="gender" value="M" required> <?php echo $this->lang->line('register_profile_for_male'); ?>
                                                            <input type="hidden" name="gender_hidden" id="gender_hidden" value="0" >
                                                        </label>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <label class="radio-inline reglabrad">
                                                            <input type="radio" name="gender" <?php echo $gender_f; ?> id="gender" value="F" required> <?php echo $this->lang->line('register_profile_for_female'); ?>
                                                        </label>
                                                    </div>
                                                    <label for="gender" class="error"></label>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="col-md-6">
                                        <table class="table">
                                            <tr>
                                                <td><label class=" form-control-label"><?php echo $this->lang->line('register_profile_for_dob'); ?></label> </td>	
                                                <td>:</td>
                                                <td class="">										
                                                    <?php
                                                    $date = strtotime($dob);
                                                    $year = date('Y', $date);
                                                    $month = date('M', $date);
                                                    $monthval = date('m', $date);
                                                    $day = date('d', $date);
// 											 echo '<pre>';print_r($year);exit;
                                                    ?>									
                                                    <div class="col-md-4 dob" for="date">
                                                        <select id="dobday" name="date" class="form-control input-sm js-example-basic-single"  selected="selected" required>
                                                            <option value="<?php echo $day ?>"><?php echo $day ?></option>
                                                        </select>
                                                        <label for="dobday" class="error"></label>
                                                    </div>

                                                    <div class="col-md-4 dob" for="month">
                                                        <select id="dobmonth" name="month" class="form-control input-sm js-example-basic-single" required>
                                                            <option value="<?php echo $monthval ?>"><?php echo $month ?></option>
                                                        </select>
                                                        <label for="dobmonth" class="error"></label>
                                                    </div>

                                                    <div class="col-md-4 dob" for="year">
                                                        <select id="dobyear" name="year" class="form-control input-sm js-example-basic-single" required>
                                                            <option value="<?php echo $year ?>"><?php echo $year ?></option>
                                                        </select>
                                                        <label for="dobyear" class="error"></label>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><label class=" form-control-label" for="religion"><?php echo $this->lang->line('register_profile_for_religion'); ?> </label><span class="red">*</span></td>
                                                <td>:</td>
                                                <td >									
                                                    <select class="form-control js-example-basic-single" id="religion" name="religion"  >
                                                        <option <?php if ($religion == "hindu")  ?> value="hindu">Hindu</option>
                                                    </select>
                                                    <label class="error" for="religion"></label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><label class=" form-control-label" for="mother_tongue"><?php echo $this->lang->line('register_profile_for_tongue'); ?> </label><span class="red">*</span></td>
                                                <td>:</td>
                                                <td>
                                                    <select class="form-control js-example-basic-single" id="mother_tongue" name="mother_tongue" required >
                                                        <option value="">- Select your Mother Tongue -</option>
                                                        <option value="tamil">Tamil</option>
                                                        <option value="bengali">Bengali</option>
                                                        <option value="english">English</option>
                                                        <option value="gujarati">Gujarati</option>
                                                        <option value="hindi">Hindi</option>
                                                        <option value="kannada">Kannada</option>
                                                        <option value="konkani">Konkani</option>
                                                        <option value="malayalam">Malayalam</option>
                                                        <option value="marathi">Marathi</option>
                                                        <option value="marwari">Marwari</option>
                                                        <option value="oriya">Oriya</option>
                                                        <option value="punjabi">Punjabi</option>
                                                        <option value="sindhi">Sindhi</option>
                                                        <option value="telugu">Telugu</option>
                                                        <option value="urdu">Urdu
                                                        <option value="others">Others</option></option>
                                                    </select>
                                                    <label class="error" for="mother_tongue"></label>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="form-group pull-right">
                                        <button class="btn btn-default" type="button" ><?php echo $this->lang->line('cancel_admin_register'); ?></button>
                                        <button class="btn btn-success" type="submit" id="pesonal_submit" name="step1"><?php echo $this->lang->line('save_text'); ?></button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <!--form-->
                    </div>
                    <hr>
                    <h4><?php echo $this->lang->line('register_profile_for_process2'); ?></h4>
                    <div class="col-md-12 text-left">
                        <div id="div3" style="display:block; ">							
                            <div class="edit_btn">
                                <a id="" href="javascript:SwapDivsWithClick('div3','div4')" style="float: right;" class="btn btn-primary ">
                                    <i class="fa fa-pencil"></i><?php echo $this->lang->line('edit_text'); ?></a>
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-6">
                                    <table class="table">
                                        <tr class="">
                                            <td><label class=" form-control-label"><?php echo $this->lang->line('register_profile_for_marriagestatus'); ?></label></td>
                                            <td>:</td>		
                                            <td><?php echo (!empty($martialstatus) ? constant("MARITAL_STATUS_" . $martialstatus) : 'Not Specified'); ?>
                                            </td>
                                        </tr>
                                        <tr class="">
                                            <td><label class="form-control-label"><?php echo $this->lang->line('register_profile_for_caste'); ?></label></td>
                                            <td>:</td>		
                                            <td><?php echo (!empty($castename) ? $castename : 'Not Specified'); ?>
                                            </td>
                                        </tr>
                                        <tr class="">
                                            <td class=""><label class="form-control-label"><?php echo $this->lang->line('register_profile_for_subcaste'); ?></label></td>
                                            <td>:</td>
                                            <td class=""><?php echo (!empty($subCaste) ? $subCaste : 'Not Specified'); ?></td>
                                        </tr>
                                        <tr class="">
                                            <td class=""><label class="form-control-label"><?php echo $this->lang->line('register_profile_for_gothram'); ?></label></td>
                                            <td>:</td>
                                            <td class=""><?php echo (!empty($gothram) ? $gothram : 'Not Specified'); ?></td>
                                        </tr>
                                        <tr>
                                            <td><label class=" form-control-label"><?php echo $this->lang->line('register_profile_for_country'); ?></label> </td>
                                            <td>:</td>		
                                            <td><?php echo (!empty($country_name) ? $country_name : 'Not Specified'); ?></td>
                                        </tr>
                                        <tr>
                                            <td><label class=" form-control-label"><?php echo $this->lang->line('register_profile_for_state'); ?></label></td>
                                            <td>:</td>		
                                            <td><?php echo (!empty($state_name) ? $state_name : 'Not Specified'); ?></td>
                                        </tr>
                                        <tr>
                                            <td><label class=" form-control-label"><?php echo $this->lang->line('register_profile_for_city'); ?></label></td>
                                            <td>:</td>		
                                            <td><?php echo (!empty($city_name) ? $city_name : 'Not Specified'); ?></td>
                                        </tr>
                                        <tr class="">
                                            <td class=""><label class=" form-control-label"><?php echo $this->lang->line('register_profile_for_height'); ?></label></td>
                                            <td> :</td>
                                            <td class=""><?php echo (!empty($height) ? $height . ' Cms' : 'Not Specified'); ?></td>
                                        </tr>
                                        <tr class="">
                                            <td class=""><label class=" form-control-label"><?php echo $this->lang->line('register_profile_for_weight'); ?> </label></td>
                                            <td> :</td>
                                            <td class=""><?php echo (!empty($weight) ? $weight . ' kg' : 'Not Specified'); ?> </td>
                                        </tr>
                                        <tr class="">
                                            <td class=""><label class=" form-control-label"><?php echo $this->lang->line('register_profile_for_bodytype'); ?></label></td> 
                                            <td> :</td>
                                            <td class=""><?php echo (!empty($bodytype) ? constant('BODY_TYPE_' . $bodytype) : 'Not Specified'); ?></td>
                                        </tr>
                                        <tr class="">
                                            <td class=""><label class=" form-control-label"><?php echo $this->lang->line('register_profile_for_complexion'); ?></label></td>
                                            <td>  :</td>
                                            <td class=""><?php echo (!empty($complexion) ? constant('COMPLEXION_TYPE_' . $complexion) : 'Not Specified'); ?></td>
                                        </tr>
                                        <tr class="">
                                            <td class=""><label class=" form-control-label"><?php echo $this->lang->line('register_profile_for_physicalstatus'); ?></label></td>
                                            <td> :</td>
                                            <td class=" "><?php echo (!empty($phystatus) ? constant('PHYSICAL_STATUS_' . $phystatus) : 'Not Specified'); ?></span></td>
                                        </tr>
                                        <tr class="">
                                            <td class=""><label class=" form-control-label"><?php echo $this->lang->line('search_education_title'); ?></label></td>
                                            <td>   :</td>
                                            <td class=""><?php echo (!empty($eduname) ? $eduname : 'Not Specified'); ?></td>
                                        </tr>
                                        <tr class="">
                                            <td class=""><label class=" form-control-label"><?php echo $this->lang->line('register_profile_for_educationqualification'); ?></label></td>
                                            <td>   :</td>
                                            <td class=""><?php echo (!empty($qualification) ? $qualification : 'Not Specified'); ?></td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="col-md-6">
                                    <table class="table">
                                        <tr class="">
                                            <td class=""><label class=" form-control-label"><?php echo $this->lang->line('register_profile_for_occupation'); ?></label></td>
                                            <td> :</td>
                                            <td class=" "><?php echo (!empty($occupation) ? $occupation : 'Not Specified'); ?></td>
                                        </tr>
                                        <tr class="">
                                            <td class=""><label class=" form-control-label"><?php echo $this->lang->line('register_profile_for_employeed'); ?></label></td>
                                            <td> :</td>
                                            <td class=" "><?php echo (!empty($employeed) ? constant('EMPLOYEED_IN_' . $employeed) : 'Not Specified'); ?></td>
                                        </tr>
                                        <tr class="">
                                            <td class=""><label class=" form-control-label"><?php echo $this->lang->line('register_profile_for_salary'); ?></label></td>
                                            <td> :</td>
                                            <td class=" "><?php echo (!empty($salary_view) ? $salary_view : 'Not Specified'); ?></span></td>
                                        </tr>
                                        <tr class="">
                                            <td class=""><label class=" form-control-label"><?php echo $this->lang->line('search_eatting_habit'); ?></label></td>
                                            <td>  :</td>
                                            <td class=" "><?php echo (!empty($food) ? constant('FOOD_TYPE_' . $food) : 'Not Specified'); ?></td>
                                        </tr>
                                        <tr class="">
                                            <td class=""><label class=" form-control-label"><?php echo $this->lang->line('register_profile_for_smoking'); ?></label></td>
                                            <td> :</td>
                                            <td class=" "><?php echo (!empty($smoke) ? constant('SMOKE_' . $smoke) : 'Not Specified'); ?></td>
                                        </tr>
                                        <tr class="">
                                            <td class=""><label class=" form-control-label"><?php echo $this->lang->line('register_profile_for_drinking'); ?></label></td>
                                            <td>  :</td>
                                            <td class=" "><?php echo (!empty($drink) ? constant('DRINK_' . $drink) : 'Not Specified'); ?></td>
                                        </tr>
                                        <tr class="">
                                            <td class=""><label class=" form-control-label"><?php echo $this->lang->line('register_profile_for_dhosam'); ?></label></td>
                                            <td> :</td>
                                            <td class=" "><span><?php echo (!empty($dhosam) ? constant('DHOSAM_TYPE_' . $dhosam) : 'Not Specified'); ?></span></td>
                                        </tr>
                                        <tr class="">
                                            <td class=""><label class=" form-control-label"><?php echo $this->lang->line('register_profile_for_star'); ?></label></td>
                                            <td>  :</td>
                                            <td class=" "><span><?php echo (!empty($star) ? $star : 'Not Specified'); ?></span></td>
                                        </tr>
                                        <tr class="">
                                            <td class=""><label class=" form-control-label"><?php echo $this->lang->line('register_profile_for_raasi'); ?></label></td>
                                            <td>  :</td>
                                            <td class=""><?php echo (!empty($raasi) ? $raasi : 'Not Specified'); ?></td>
                                        </tr>
                                        <tr class="">
                                            <td class=""><label class=" form-control-label"><?php echo $this->lang->line('register_profile_for_familystatus'); ?></label></td>
                                            <td>  :</td>
                                            <td class=""><?php echo (!empty($fstatus) ? constant('FAMILY_STATUS_' . $fstatus) : 'Not Specified'); ?></td>
                                        </tr>
                                        <tr class="">
                                            <td class=""><label class=" form-control-label"><?php echo $this->lang->line('register_profile_for_familytype'); ?></label></td>
                                            <td>  :</td>
                                            <td class=""><?php echo (!empty($ftype) ? constant('FAMILY_TYPE_' . $ftype) : 'Not Specified'); ?></td>
                                        </tr>
                                        <tr class="">
                                            <td class=""><label class=" form-control-label"><?php echo $this->lang->line('register_profile_for_familyvalue'); ?></label></td>
                                            <td>  :</td>
                                            <td class=""><?php echo (!empty($fvalues) ? constant('FAMILY_VALUES_' . $fvalues) : 'Not Specified'); ?></td>
                                        </tr>
                                        <tr class="">
                                            <td class=""><label class=" form-control-label"><?php echo $this->lang->line('register_profile_for_something_title1'); ?></label></td>
                                            <td>  :</td>
                                            <td class=""><?php echo (!empty($summary) ? $summary : 'Not Specified'); ?></td>
                                        </tr>

                                    </table>
                                </div>
                            </div>
                        </div>

                        <div id="div4" style="display:none; ">
                            <div class="edit_btn">
                                <a id="" href="javascript:SwapDivsWithClick('div3','div4')" style="float: right;" class="btn btn-primary ">
                                    <i class="fa fa-times"></i><?php echo $this->lang->line('close_text'); ?></a>
                            </div>
                            <form class="form-horizontal" id="physical_form" method="post">	
                                <div class="col-md-12">
                                    <div class="col-md-6">
                                        <table class="table">
                                            <tr>
                                                <td> <label for="maritalstatus" class="form-control-label"><?php echo $this->lang->line('register_profile_for_marriagestatus'); ?> <span class="red">*</span></label></td>
                                                <td>:</td>
                                                <td>
                                                    <select class="form-control js-example-data-array-selected" id="maritalstatus" name="maritalstatus" required >
                                                        <option value="" > -- Select  -- </option>
                                                        <option value="1" > <?php echo $this->lang->line('register_profile_for_marriagestatus_1'); ?> </option>
                                                        <option value="2"  > <?php echo $this->lang->line('register_profile_for_marriagestatus_2'); ?> </option>
                                                        <option value="3"  > <?php echo $this->lang->line('register_profile_for_marriagestatus_3'); ?> </option>
                                                        <option value="4"  > <?php echo $this->lang->line('register_profile_for_marriagestatus_4'); ?> </option>
                                                    </select>  

                                                    <label class="error" for="maritalstatus"></label>
                                                </td>
                                            </tr>
                                            <tr class="">
                                                <td class=""><label for="caste" class="form-control-label"><?php echo $this->lang->line('register_profile_for_caste'); ?></label>  <label class="import">*</label></td>
                                                <td>:</td>
                                                <td class="">
                                                    <select class="form-control js-example-data-array-selected" id="caste" name="caste" required >
                                                        <option value="" > -- Select Caste -- </option>
                                                    </select> 	
                                                    <label class="error" for="caste"></label>								  
                                                </td>
                                            </tr>
                                            <tr class="">
                                                <td class=""><label for="subcaste" class="form-control-label"><?php echo $this->lang->line('register_profile_for_subcaste'); ?></label></td>
                                                <td>:</td>
                                                <td class=""><input type="textbox" name="subcaste" value="<?php echo $subCaste; ?>" class="form-control" ></td>
                                            </tr>
                                            <tr class="">
                                                <td class=""><label for="gothram" class="form-control-label"><?php echo $this->lang->line('register_profile_for_gothram'); ?></label></td>
                                                <td>:</td>
                                                <td class=""><input type="textbox" name="gothram" value="<?php echo $gothram; ?>" class="form-control" ></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <label for="country" class="form-control-label"><?php echo $this->lang->line('register_profile_for_country'); ?>  <span class="red">*</span></label> 
                                                </td>
                                                <td>:</td>
                                                <td>				
                                                    <select class="form-control js-example-data-array-selected" id="country" name="country" onchange="stateInfo(this.value)" required>
                                                        <option value=""> -- Select Country -- </option>
                                                        <option value="<?php echo $country ?>" selected="selected"> <?php echo $country_name; ?> </option>
                                                    </select>
                                                    <label class="error" for="country"></label> 
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <label for="state" class=" form-control-label"><?php echo $this->lang->line('register_profile_for_state'); ?> <span class="red">*</span></label>  
                                                </td>
                                                <td>:</td>
                                                <td><div id="text-state">			
                                                        <select class="form-control js-example-data-array-selected"  name="state" id="state" onchange="cityInfo(this.value)" required>
                                                            <option value=""> -- Select State -- </option>
                                                            <option value="<?php echo $state ?>" selected="selected"> <?php echo $state_name; ?> </option>
                                                        </select>
                                                        <label class="error" for="state"></label>  
                                                    </div>	  
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <label for="city" class=" form-control-label"><?php echo $this->lang->line('register_profile_for_city'); ?> <span class="red">*</span></label> 
                                                </td>
                                                <td>:</td>
                                                <td><div id="text-city">					
                                                        <select class="form-control js-example-data-array-selected"  id ="city" name="city" required>
                                                            <option value=""> -- Select City -- </option>
                                                            <option value="<?php echo $city ?>" selected="selected"> <?php echo $city_name; ?> </option>
                                                        </select>
                                                        <label class="error" for="city"></label>    
                                                </td></div>
                                            </tr>
                                            <tr class="">
                                                <td class=""><label for="height" class=" form-control-label"><?php echo $this->lang->line('register_profile_for_height'); ?> </label><label class="import">*</label></td>
                                                <td>:</td>
                                                <td class="">
                                                                                 
                                                    <select class="form-control js-example-basic-single" name="height" id="height" required>
                                                        <option value=""> -- <?php echo $this->lang->line("register_profile_for_height"); ?> -- </option>
                                                        <option value="145" label="4' 9&quot; (145 cm)">4' 9" (145 cm)</option>
                                                        <option value="147" label="4' 10&quot; (147 cm)">4' 10" (147 cm)</option>
                                                        <option value="150" label="4' 11&quot; (150 cm)">4' 11" (150 cm)</option>
                                                        <option value="152" label="5'  0&quot; (152 cm)">5'  0" (152 cm)</option>
                                                        <option value="155" label="5' 1&quot; (155 cm)">5' 1" (155 cm)</option>
                                                        <option value="157" label="5' 2&quot; (157 cm)">5' 2" (157 cm)</option>
                                                        <option value="160" label="5' 3&quot; (160 cm)">5' 3" (160 cm)</option>
                                                        <option value="162" label="5' 4&quot; (162 cm)">5' 4" (162 cm)</option>
                                                        <option value="165" label="5' 5&quot; (165 cm)">5' 5" (165 cm)</option>
                                                        <option value="167" label="5' 6&quot; (167 cm)">5' 6" (167 cm)</option>
                                                        <option value="170" label="5' 7&quot; (170 cm)">5' 7" (170 cm)</option>
                                                        <option value="173" label="5' 8&quot; (173 cm)">5' 8" (173 cm)</option>
                                                        <option value="175" label="5' 9&quot; (175 cm)">5' 9" (175 cm)</option>
                                                        <option value="178" label="5' 10&quot; (178 cm)">5' 10" (178 cm)</option>
                                                        <option value="180" label="5' 11&quot; (180 cm)">5' 11" (180 cm)</option>
                                                        <option value="183" label="6' 0&quot; (183 cm)">6' 0" (183 cm)</option>
                                                        <option value="185" label="6' 1&quot; (185 cm)">6' 1" (185 cm)</option>
                                                        <option value="188" label="6' 2&quot; (188 cm)">6' 2" (188 cm)</option>
                                                        <option value="190" label="6' 3&quot; (190 cm)">6' 3" (190 cm)</option>
                                                        <option value="193" label="6' 4&quot; (193 cm)">6' 4" (193 cm)</option>
                                                        <option value="21">Above the Height </option>
                                                    </select>
                                                    <label class="error" for="height"></label>    
                                                </td>
                                            </tr>	
                                            <tr class="">
                                                <td class=""><label for="weight" class=" form-control-label"><?php echo $this->lang->line('register_profile_for_weight'); ?></label> </td>
                                                <td>:</td>
                                                <td class="">
                                                  
                                                    <select class="form-control js-example-basic-single" name="weight" id="weight">
                                                        <option value=""> -- <?php echo $this->lang->line("register_profile_for_weight"); ?> -- </option>
                                                        <option value="41">41 kg</option>
                                                        <option value="42">42 kg</option>
                                                        <option value="43">43 kg</option>
                                                        <option value="44">44 kg</option>
                                                        <option value="45">45 kg</option>
                                                        <option value="46">46 kg</option>
                                                        <option value="47">47 kg</option>
                                                        <option value="48">48 kg</option>
                                                        <option value="49">49 kg</option>
                                                        <option value="50">50 kg</option>
                                                        <option value="51">51 kg</option>
                                                        <option value="52">52 kg</option>
                                                        <option value="53">53 kg</option>
                                                        <option value="54">54 kg</option>
                                                        <option value="55">55 kg</option>
                                                        <option value="56">56 kg</option>
                                                        <option value="57">57 kg</option>
                                                        <option value="58">58 kg</option>
                                                        <option value="59">59 kg</option>
                                                        <option value="60">60 kg</option>
                                                        <option value="61">61 kg</option>
                                                        <option value="62">62 kg</option>
                                                        <option value="63">63 kg</option>
                                                        <option value="64">64 kg</option>
                                                        <option value="65">65 kg</option>
                                                        <option value="66">66 kg</option>
                                                        <option value="67">67 kg</option>
                                                        <option value="68">68 kg</option>
                                                        <option value="69">69 kg</option>
                                                        <option value="70">70 kg</option>
                                                        <option value="71">71 kg</option>
                                                        <option value="72">72 kg</option>
                                                        <option value="73">73 kg</option>
                                                        <option value="74">74 kg</option>
                                                        <option value="75">75 kg</option>
                                                        <option value="76">76 kg</option>
                                                        <option value="77">77 kg</option>
                                                        <option value="78">78 kg</option>
                                                        <option value="79">79 kg</option>
                                                        <option value="80">80 kg</option>
                                                        <option value="81">81 kg</option>
                                                        <option value="82">82 kg</option>
                                                        <option value="83">83 kg</option>
                                                        <option value="84">84 kg</option>
                                                        <option value="85">85 kg</option>
                                                        <option value="86">86 kg</option>
                                                        <option value="88">88 kg</option>
                                                        <option value="89">89 kg</option>
                                                        <option value="90">90 kg</option>
                                                        <option value="91">91 kg</option>
                                                        <option value="92">92 kg</option>
                                                        <option value="93">93 kg</option>
                                                        <option value="94">94 kg</option>
                                                        <option value="95">95 kg</option>
                                                        <option value="96">96 kg</option>
                                                        <option value="97">97 kg</option>
                                                        <option value="98">98 kg</option>
                                                        <option value="99">99 kg</option>
                                                        <option value="100">100 kg</option>
                                                        <option value="101">Above the weight </option>
                                                    </select>  
                                                </td>
                                            </tr>								
                                            <tr class="">
                                                <td class=""><?php echo $this->lang->line('register_profile_for_bodytype'); ?></td>
                                                <td>:</td>
                                                <td class="">
                                                    <select name="bodytype" class="form-control  js-example-basic-single" id="bodytype">
                                                        <option value="0">-- Select Option --</option>
                                                        <option  value="1">Slim</option>
                                                        <option  value="2">Average</option>
                                                        <option  value="3">Athletic</option>
                                                        <option  value="4">Heavy</option>
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr class="">
                                                <td class=""><label for="bodytype" class=" form-control-label"><?php echo $this->lang->line('register_profile_for_complexion'); ?></label> </td>
                                                <td>:</td>
                                                <td class="">
                                                    <select class="form-control js-example-basic-single" id="complexion" name="complexion" >
                                                        <option value="">- Select  -</option>
                                                        <option value="1">Very Fair</option>
                                                        <option value="2">Fair</option>
                                                        <option value="3">Wheatish</option>
                                                        <option value="4">Dark</option>
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr class="">
                                                <td class=""><label for="physicalstatus" class=" form-control-label"><?php echo $this->lang->line('register_profile_for_physicalstatus'); ?> </label><label class="import">*</label></td>
                                                <td>:</td>
                                                <td class=" ">
                                                    <select class="form-control js-example-basic-single" name="physicalstatus"id="physicalstatus"  required >
                                                        <option value=""> -- Select Status -- </option>
                                                        <option value="1">Normal</option>
                                                        <option value="2">Physically challenged</option>
                                                    </select>
                                                    <label class="error" for="physicalstatus"></label> 
                                                </td>
                                            </tr>	

                                            <tr class="">
                                                <td class=""> <label for="education" class="form-control-label"><?php echo $this->lang->line('register_profile_for_highesteducation'); ?> </label><label class="import">*</label></td>
                                                <td>:</td>
                                                <td class="">

                                                    <select class="form-control js-example-basic-single" name="education" id="education" required >
                                                        <option value="">- Select your education -</option>		
                                                        <option value="5" label="High school">High school</option>
                                                        <option value="4" label="Diploma">Diploma</option>
                                                        <option value="3" label="Under Graduate">Under Graduate</option>
                                                        <option value="2" label="Post Graduate">Post Graduate</option>
                                                        <option value="1" label="PhD / Post Doctoral">PhD / Post Doctoral</option>
                                                        <option value="6" label="No answer">No answer</option>
                                                    </select>
                                                    <label class="error" for="education"></label> 
                                                </td>
                                            </tr>
                                            <tr class="">
                                                <td class=""> <label for="qualification" class=" form-control-label"><?php echo $this->lang->line('register_profile_for_educationqualification'); ?>  </label>
                                                    <label class="import">*</label></td>
                                                <td>:</td>
                                                <td class=""><input type="textbox" name="qualification" value="<?php echo $qualification; ?>" class="form-control" required >
                                                    <label class="error" for="qualification"></label> 
                                                </td>									 
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="col-md-6">
                                        <table class="table">
                                            <tr class="">
                                                <td class=""><label for="occupation" class=" form-control-label"><?php echo $this->lang->line('register_profile_for_occupation'); ?></label><label class="import">*</label></td>
                                                <td>:</td>
                                                <td class=""><input type="textbox" name="occupation" value="<?php echo $occupation; ?>" class="form-control" required >
                                                    <label class="error" for="occupation"></label> 
                                                </td>
                                            </tr>
                                            <tr class="">
                                                <td class=""><label for="emp_in" class=" form-control-label"> <?php echo $this->lang->line('register_profile_for_employeed'); ?></label><label class="import">*</label></td>
                                                <td>:</td>
                                                <td class="">
                                                    <select class="form-control js-example-basic-single" name="emp_in" id="employeed" required >
                                                        <option value=""> -- Select -- </option>
                                                        <option value="1">Government </option>
                                                        <option value="2">Self Employeed </option>
                                                        <option value="3">Businees</option>
                                                        <option value="4">Private </option>
                                                    </select>
                                                    <label class="error" for="emp_in"></label>
                                                </td>
                                            </tr>
                                            <tr class="">
                                                <td class=""> <label for="salary" class=" form-control-label"> <?php echo $this->lang->line('register_profile_for_salary'); ?></label></td>
                                                <td>:</td>
                                                <td class="">
                                                    <select class="form-control js-example-basic-single" name="salary" id="salary">
                                                        <option value=""> -- select salary -- </option>
                                                        <option value="1">100000 or less</option>
                                                        <option value="2">200000 to 300000</option>
                                                        <option value="3">300000 to 400000</option>
                                                        <option value="4">400000 to 500000</option>
                                                        <option value="5">500000 to 600000</option>
                                                        <option value="6">600000 to 700000</option>
                                                        <option value="7">700000 to 800000</option>
                                                        <option value="8">800000 to 900000</option>
                                                        <option value="9">900000 Above</option>
                                                        <option value="10">any</option>
                                                    </select>  
                                                </td>
                                            </tr>
                                            <tr class="">
                                                <td class=""><label for="food" class=" form-control-label"><?php echo $this->lang->line('register_profile_for_food'); ?></label> </td>
                                                <td>:</td>
                                                <td class=" ">
                                                    <select class="form-control js-example-basic-single" name="food" id="food">
                                                        <option value=""> -- Select -- </option>
                                                        <option value="1">Vegetarian</option>
                                                        <option value="2">Non-Vegetarian	</option>
                                                        <option value="3">Eggetarian</option>
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr class="">
                                                <td class=""><label for="smoking" class=" form-control-label"><?php echo $this->lang->line('register_profile_for_smoking'); ?> </label></td>
                                                <td>:</td>
                                                <td class=" ">	
                                                    <select class="form-control js-example-basic-single" name="smoking" id="smoke" >
                                                        <option value=""> -- Select  -- </option>
                                                        <option value="1">Yes</option>
                                                        <option value="2">No</option>
                                                        <option value="3">Occasionally </option>
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr class="">
                                                <td class=""><label for="drinking" class=" form-control-label"><?php echo $this->lang->line('register_profile_for_drinking'); ?> </label></td>
                                                <td>:</td>
                                                <td class=" ">
                                                    <select class="form-control js-example-basic-single" name="drinking" id="drink" >
                                                        <option value=""> -- Select Status -- </option>
                                                        <option value="1">Yes</option>
                                                        <option value="2">No</option>
                                                        <option value="3">Drinks Socially </option>
                                                    </select>
                                                </td>
                                            </tr>						


                                            <tr class="_1">
                                                <td class=""><label for="dhosam" class=" form-control-label"><?php echo $this->lang->line('register_profile_for_dhosam'); ?></label></td>
                                                <td>:</td>
                                                <td class="">	
                                                    <select class="form-control js-example-basic-single" id="dhosam" name="dhosam"  >
                                                        <option value=""><?php echo $this->lang->line("register_profile_for_dhosam"); ?></option>
                                                        <option value="1">Yes</option>
                                                        <option value="2">No</option>
                                                        <option value="3">Don't know</option>
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr class="">
                                                <td class=""><label for="star" class=" form-control-label"><?php echo $this->lang->line('register_profile_for_star'); ?></label></td>
                                                <td>:</td>
                                                <td class="">                                                
                                                    <select class="form-control js-example-basic-single" name="star" id="star">
                                                        <option value=""> -- <?php echo $this->lang->line("register_profile_for_star"); ?> -- </option>
                                                        <option value="ashwini">Ashwini</option>
                                                        <option value="bharani">Bharani</option>
                                                        <option value="karthigai">Karthigai</option>
                                                        <option value="rohini">Rohini</option>
                                                        <option value="mirigasirisham">Mirigasirisham</option>
                                                        <option value="thiruvathirai">Thiruvathirai</option>
                                                        <option value="punarpoosam">Punarpoosam</option>
                                                        <option value="poosam">Poosam</option>
                                                        <option value="ayilyam">Ayilyam</option>
                                                        <option value="makam">Makam</option>
                                                        <option value="pooram">Pooram</option>
                                                        <option value="uthiram">Uthiram</option>
                                                        <option value="hastham">Hastham</option>
                                                        <option value="chithirai">Chithirai</option>
                                                        <option value="swathi">Swathi</option>
                                                        <option value="visakam">Visakam</option>
                                                        <option value="anusham">Anusham</option>
                                                        <option value="kettai">Kettai</option>
                                                        <option value="moolam">Moolam</option>
                                                        <option value="pooradam">Pooradam</option>
                                                        <option value="uthradam">Uthradam</option>
                                                        <option value="thiruvonam">Thiruvonam</option>
                                                        <option value="avittam">Avittam</option>
                                                        <option value="sadhayam">Sadhayam</option>
                                                        <option value="puratathi">Puratathi</option>
                                                        <option value="uthirattathi">Uthirattathi</option>
                                                        <option value="revathi">Revathi</option>										
                                                    </select>  
                                                </td>
                                            </tr>

                                            <tr class="">
                                                <td class=""><label for="raasi" class=" form-control-label"> <?php echo $this->lang->line('register_profile_for_raasi'); ?></label></td>
                                                <td>:</td>
                                                <td class="">
                                                    <select class="form-control js-example-basic-single"  name="raasi" id="raasi">
                                                        <option value="">-- select one -- </option>
                                                        <option value="mesham" label="Mesham">Mesham</option>
                                                        <option value="rishabam" label="Rishabam">Rishabam</option>
                                                        <option value="mithunam" label="Mithunam">Mithunam</option>
                                                        <option value="katagam" label="Katagam">Katagam</option>
                                                        <option value="simham" label="Simham">Simham</option>
                                                        <option value="kanni" label="Kanni">Kanni</option>
                                                        <option value="thulam" label="Thulam">Thulam</option>
                                                        <option value="vrichigam" label="Vrichigam">Vrichigam</option>
                                                        <option value="dhanush" label="Dhanush">Dhanush</option>
                                                        <option value="magaram" label="Magaram">Magaram</option>
                                                        <option value="kumbham" label="Kumbham">Kumbham</option>
                                                        <option value="meenam" label="Meenam">Meenam</option>	
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr class="">
                                                <td class=""><label for="fstatus" class=" form-control-label"><?php echo $this->lang->line('register_profile_for_familystatus'); ?></label><label class="import">*</label></td>
                                                <td>:</td>
                                                <td class="">
                                                    <select class="form-control js-example-basic-single" name="fstatus" id="fstatus" required>
                                                        <option value=""> -- select -- </option>
                                                        <option value="1">Middle class</option>
                                                        <option value="2">Upper middle class</option>
                                                        <option value="3">Rich</option>
                                                        <option value="4">Affluent</option>
                                                    </select>
                                                    <label class="error" for="fstatus"></label> 
                                                </td>
                                            </tr>
                                            <tr class="">
                                                <td class=""><label for="ftype" class=" form-control-label"><?php echo $this->lang->line('register_profile_for_familytype'); ?></label><label class="import">*</label></td>
                                                <td>:</td>
                                                <td class="">
                                                    <select class="form-control js-example-basic-single" name="ftype" id="ftype" required >
                                                        <option value=""> -- Select -- </option>
                                                        <option value="1">Joint</option>
                                                        <option value="2">Nuclear</option>
                                                    </select>
                                                    <label class="error" for="ftype"></label> 
                                                </td>
                                            </tr>
                                            <tr class="">
                                                <td class=""><label for="fvalues" class=" form-control-label"><?php echo $this->lang->line('register_profile_for_familyvalue'); ?></label><label class="import">*</label></td>
                                                <td>:</td>
                                                <td class=" ">
                                                    <select class="form-control js-example-basic-single" name="fvalues" id="fvalues" required >
                                                        <option value=""> -- select -- </option>
                                                        <option value="1">Orthodox</option>
                                                        <option value="2">Traditional</option>
                                                        <option value="3">Moderate</option>
                                                        <option value="4">Liberal</option>
                                                    </select>
                                                    <label class="error" for="fvalues"></label> 
                                                </td>
                                            </tr>
                                            <tr class="opened">
                                                <td><label for="summary" class=" form-control-label"><?php echo $this->lang->line('register_profile_for_something_title1'); ?></label></td>
                                                <td>:</td>
                                                <td><textarea class="form-control" id="exampleTextarea" name="summary" rows="3" ><?php echo $summary; ?></textarea></td>
                                            </tr>
                                            <tr>
                                                <td colspan="3" class="text-right">
                                                    <button class="btn btn-default" type="button" ><?php echo $this->lang->line('cancel_admin_register'); ?></button>
                                                    <button class="btn btn-success" type="submit" id="" name="step2"><?php echo $this->lang->line('save_text'); ?></button>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </form>

                        </div>	
                    </div>

                    <hr>
                    <?php
                    $prefrenceData = !empty($profile['prefrenceData']) ? $profile['prefrenceData'] : '';
                    //foreach($profile['partnerInfo'] as $prefrenceData){ 
                    /** partner prefrence */
                    $ageFrom = (!empty($prefrenceData['age_from']) ? $prefrenceData['age_from'] : '');
                    $ageTo = (!empty($prefrenceData['age_to']) ? $prefrenceData['age_to'] : '');
                    $pre_maritalstatus = (!empty($prefrenceData['maritalstatus']) ? $prefrenceData['maritalstatus'] : '');
                    $pre_height = (!empty($prefrenceData['height']) ? $prefrenceData['height'] : '');
                    $pre_physicalstatus = (!empty($prefrenceData['physicalstatus']) ? $prefrenceData['physicalstatus'] : '');
                    $pre_food = (!empty($prefrenceData['food']) ? $prefrenceData['food'] : '');
                    $pre_smoke = (!empty($prefrenceData['smoking']) ? $prefrenceData['smoking'] : '');
                    $pre_drink = (!empty($prefrenceData['drinking']) ? $prefrenceData['drinking'] : '');
                    $pre_caste = (!empty($prefrenceData['caste']) ? $prefrenceData['caste'] : '');
                    $pre_dhosam = (!empty($prefrenceData['dhosam']) ? $prefrenceData['dhosam'] : '');
                    $pre_dhosam_type = (!empty($prefrenceData['dhosam_type']) ? $prefrenceData['dhosam_type'] : '');
                    $pre_star = (!empty($prefrenceData['star']) ? $prefrenceData['star'] : '');
                    $pre_country = (!empty($prefrenceData['country']) ? $prefrenceData['country'] : '');
                    $pre_state = (!empty($prefrenceData['state']) ? $prefrenceData['state'] : '');
                    $pre_city = (!empty($prefrenceData['city']) ? $prefrenceData['city'] : '');
                    $pre_education = (!empty($prefrenceData['education']) ? $prefrenceData['education'] : '');
                    $pre_qualification = (!empty($prefrenceData['qualification']) ? $prefrenceData['qualification'] : '');
                    $pre_occupation = (!empty($prefrenceData['occupation']) ? $prefrenceData['occupation'] : '');
                    $pre_salary = (!empty($prefrenceData['salary']) ? $prefrenceData['salary'] : '');
                    $pre_employeed = (!empty($prefrenceData['employeed_in']) ? $prefrenceData['employeed_in'] : '');
                    $pre_salaryView = (!empty($prefrenceData['salary_view']) ? $prefrenceData['salary_view'] : '');
                    $pre_castename = (!empty($prefrenceData['caste_name']) ? $prefrenceData['caste_name'] : '');
                    $pre_eduname = (!empty($prefrenceData['edu_name']) ? $prefrenceData['edu_name'] : '');
                    $pre_country_name = (!empty($prefrenceData['country_name']) ? $prefrenceData['country_name'] : '');
                    $pre_state_name = (!empty($prefrenceData['state_name']) ? $prefrenceData['state_name'] : '');
                    $pre_city_name = (!empty($prefrenceData['city_name']) ? $prefrenceData['city_name'] : '');
                    $pre_castename = (!empty($prefrenceData['caste_name']) ? $prefrenceData['caste_name'] : '');
                    ?>
                    <h4><?php echo $this->lang->line('register_profile_for_process4'); ?></h4>
                    <div class="col-md-12 text-left">
                        <div id="div5" style="display:block; ">
                            <div class="edit_btn">
                                <a id="" href="javascript:SwapDivsWithClick('div5','div6')" style="float: right;" class="btn btn-primary ">
                                    <i class="fa fa-pencil"></i><?php echo $this->lang->line('edit_text'); ?></a>
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-6">
                                    <table class="table">
                                        <tr class="opened">
                                            <td class="day_label"><label  class=" form-control-label"><?php echo $this->lang->line('search_profile_for_age'); ?></label> </td>  
                                            <td> :</td>
                                            <td class="day_value"><?php echo (!empty($ageFrom) ? $ageFrom : 'Not Specified') . ' - ' . (!empty($ageTo) ? $ageTo : 'Not Specified'); ?></td>
                                        </tr>
                                        <tr class="opened">
                                            <td class="day_label"><label  class=" form-control-label"><?php echo $this->lang->line('register_profile_for_marriagestatus'); ?></label></td>
                                            <td>:</td>
                                            <td class="day_value"><?php echo (!empty($pre_maritalstatus) ? constant('MARITAL_STATUS_' . $pre_maritalstatus) : 'Not Specified'); ?></td>
                                        </tr>
                                        <tr class="opened">
                                            <td class="day_label"><label class=" form-control-label"><?php echo $this->lang->line('register_profile_for_height'); ?></label></td>
                                            <td> :</td>
                                            <td class="day_value closed"><?php echo (!empty($pre_height) ? $pre_height . ' Cms' : 'Not Specified'); ?> </td>
                                        </tr>
                                        <tr class="opened">
                                            <td class="day_label"><label class=" form-control-label"><?php echo $this->lang->line('register_profile_for_physicalstatus'); ?></label> </td>
                                            <td> :</td>
                                            <td class="day_value"><?php echo (!empty($pre_physicalstatus) ? constant('PHYSICAL_STATUS_' . $pre_physicalstatus) : 'Not Specified'); ?></td>
                                        </tr>
                                        <tr class="opened">
                                            <td class="day_label"><label class=" form-control-label"><?php echo $this->lang->line('search_eatting_habit'); ?></label> </td>
                                            <td> :</td>
                                            <td class="day_value closed"><?php echo (!empty($pre_food) ? constant('FOOD_TYPE_' . $pre_food) : 'Not Specified'); ?></td>
                                        </tr>
                                        <tr class="opened">
                                            <td class="day_label"><label class=" form-control-label"><?php echo $this->lang->line('register_profile_for_smoking'); ?></label> </td>
                                            <td> :</td>
                                            <td class="day_value closed"><?php echo (!empty($pre_smoke) ? constant('SMOKE_' . $pre_smoke) : 'Not Specified'); ?></td>
                                        </tr>
                                        <tr class="opened">
                                            <td class="day_label"><label class=" form-control-label"><?php echo $this->lang->line('register_profile_for_drinking'); ?> </label></td>
                                            <td> :</td>
                                            <td class="day_value closed"><?php echo (!empty($pre_drink) ? constant('DRINK_' . $pre_drink) : 'Not Specified'); ?></td>
                                        </tr>
                                        <tr class="opened">
                                            <td class="day_label"><label class=" form-control-label"><?php echo $this->lang->line('register_profile_for_caste'); ?></label> </td>
                                            <td> :</td>
                                            <td class="day_value closed"><?php echo (!empty($pre_castename) ? $pre_castename : 'Not Specified'); ?></td>
                                        </tr>
                                        <tr class="opened">
                                            <td class="day_label"><label class=" form-control-label"><?php echo $this->lang->line('register_profile_for_star'); ?> </label></td>
                                            <td> :</td>
                                            <td class="day_value closed"><?php echo (!empty($pre_star) ? $pre_star : 'Not Specified'); ?></td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="col-md-6">
                                    <table class="table">
                                        <tr class="opened">
                                            <td class="day_label"><label class=" form-control-label"><?php echo $this->lang->line('register_profile_for_country'); ?></label></td>
                                            <td> :</td>
                                            <td class="day_value closed"><?php echo (!empty($pre_country_name) ? $pre_country_name : 'Not Specified'); ?></td>
                                        </tr>
                                        <tr class="opened">
                                            <td class="day_label"><label  class=" form-control-label"><?php echo $this->lang->line('register_profile_for_state'); ?></label> </td>
                                            <td> :</td>
                                            <td class="day_value closed"><?php echo (!empty($pre_state_name) ? $pre_state_name : 'Not Specified'); ?></td>
                                        </tr>
                                        <tr class="opened">
                                            <td class="day_label"><label class=" form-control-label"><?php echo $this->lang->line('register_profile_for_city'); ?></label> </td>
                                            <td> :</td>
                                            <td class="day_value closed"><?php echo (!empty($pre_city_name) ? $pre_city_name : 'Not Specified'); ?></td>
                                        </tr>
                                        <tr class="opened">
                                            <td class="day_label"><label class=" form-control-label"><?php echo $this->lang->line('search_education_title'); ?></label> </td>
                                            <td> :</td>
                                            <td class="day_value closed"><?php echo (!empty($pre_eduname) ? $pre_eduname : 'Not Specified'); ?></td>
                                        </tr>
                                        <tr class="opened">
                                            <td class="day_label"><label class=" form-control-label"><?php echo $this->lang->line('register_profile_for_educationqualification'); ?> </label></td>
                                            <td> :</td>
                                            <td class="day_value closed"><?php echo (!empty($pre_qualification) ? $pre_qualification : 'Not Specified'); ?></td>
                                        </tr>
                                        <tr class="opened">
                                            <td class="day_label"><label  class=" form-control-label"><?php echo $this->lang->line('register_profile_for_occupation'); ?> </label></td>
                                            <td> :</td>
                                            <td class="day_value closed"><?php echo (!empty($pre_occupation) ? $pre_occupation : 'Not Specified'); ?></td>
                                        </tr>

                                        <tr class="opened">
                                            <td class="day_label"><label class=" form-control-label"><?php echo $this->lang->line('register_profile_for_employeed'); ?></label> </td>
                                            <td> :</td>
                                            <td class="day_value closed"><?php echo (!empty($pre_employeed) ? constant('EMPLOYEED_IN_' . $pre_employeed) : 'Not Specified'); ?></td>
                                        </tr>

                                        <tr class="opened">
                                            <td class="day_label"><label class=" form-control-label"><?php echo $this->lang->line('salary'); ?></label> </td>
                                            <td> :</td>
                                            <td class="day_value closed"><?php echo (!empty($pre_salaryView) ? $pre_salaryView : 'Not Specified'); ?></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>


                        </div>
                    </div>	
                    <!--form-->
                    <div id="div6" style="display:none; ">
                        <a id="" href="javascript:SwapDivsWithClick('div5','div6')" style="float: right;" class="btn btn-primary ">
                            <i class="fa fa-pencil"></i><?php echo $this->lang->line('close_text'); ?></a>
                        <form class="form-horizontal" role="form" id="preference_form">
                            <div class="col-md-12">
                                <div class="col-md-6">
                                    <table class="table">
                                        <tr class="">
                                            <td class=""> <label class="form-control-label" for="age_from">  <?php echo $this->lang->line('search_profile_for_age'); ?></label><label class="import">*</label></td>
                                            <td>:</td>
                                            <td class="">
                                                <label class="col-sm-2 form-control-label" for="age_from"><?php echo $this->lang->line('from_text'); ?></label>
                                                <div class="col-md-3" style="padding-right: 0px !important;">
                                                    <select class="form-control js-example-basic-single" id="pre_age_from" name="age_from" required>
                                                        <option value="">From</option>
                                                        <option value="21">21</option>
                                                        <option value="22">22</option>
                                                        <option value="23">23</option>
                                                        <option value="24">24</option>
                                                        <option value="25">25</option>
                                                        <option value="26">26</option>
                                                        <option value="27">27</option>
                                                        <option value="28">28</option>
                                                        <option value="29">29</option>
                                                        <option value="30">30</option>
                                                        <option value="31">31</option>
                                                        <option value="32">32</option>
                                                        <option value="33">33</option>
                                                        <option value="34">34</option>
                                                        <option value="35">35</option>
                                                        <option value="36">36</option>
                                                        <option value="37">37</option>
                                                        <option value="38">38</option>
                                                        <option value="39">39</option>
                                                        <option value="40">40</option>
                                                        <option value="41">41</option>
                                                        <option value="42">42</option>
                                                        <option value="43">43</option>
                                                        <option value="44">44</option>
                                                        <option value="45">45</option>
                                                        <option value="46">46</option>
                                                        <option value="47">47</option>
                                                        <option value="48">48</option>
                                                        <option value="49">49</option>
                                                        <option value="50">50</option>
                                                        <option value="51">51</option>
                                                        <option value="52">52</option>
                                                        <option value="53">53</option>
                                                        <option value="54">54</option>
                                                        <option value="55">55</option>
                                                        <option value="56">56</option>
                                                        <option value="57">57</option>
                                                        <option value="58">58</option>
                                                        <option value="59">59</option>
                                                        <option value="60">60</option>
                                                        <option value="61">61</option>
                                                        <option value="62">62</option>
                                                        <option value="63">63</option>
                                                        <option value="64">64</option>
                                                        <option value="65">65</option>
                                                        <option value="66">66</option>
                                                        <option value="67">67</option>
                                                        <option value="68">68</option>
                                                        <option value="69">69</option>
                                                        <option value="70">70</option>
                                                    </select>
                                                    <label class="error" for="age_from"></label>
                                                </div>
                                                <label class="col-sm-2 form-control-label" for="age_to"><?php echo $this->lang->line('to_text'); ?></label>
                                                <div class="col-md-3"  style="padding-left: 0px !important;">
													<select class="form-control js-example-basic-single" id="pre_age_to" name="age_to" required>
                                                        <option value="">To</option>
                                                        <option value="21">21</option>
                                                        <option value="22">22</option>
                                                        <option value="23">23</option>
                                                        <option value="24">24</option>
                                                        <option value="25">25</option>
                                                        <option value="26">26</option>
                                                        <option value="27">27</option>
                                                        <option value="28">28</option>
                                                        <option value="29">29</option>
                                                        <option value="30">30</option>
                                                        <option value="31">31</option>
                                                        <option value="32">32</option>
                                                        <option value="33">33</option>
                                                        <option value="34">34</option>
                                                        <option value="35">35</option>
                                                        <option value="36">36</option>
                                                        <option value="37">37</option>
                                                        <option value="38">38</option>
                                                        <option value="39">39</option>
                                                        <option value="40">40</option>
                                                        <option value="41">41</option>
                                                        <option value="42">42</option>
                                                        <option value="43">43</option>
                                                        <option value="44">44</option>
                                                        <option value="45">45</option>
                                                        <option value="46">46</option>
                                                        <option value="47">47</option>
                                                        <option value="48">48</option>
                                                        <option value="49">49</option>
                                                        <option value="50">50</option>
                                                        <option value="51">51</option>
                                                        <option value="52">52</option>
                                                        <option value="53">53</option>
                                                        <option value="54">54</option>
                                                        <option value="55">55</option>
                                                        <option value="56">56</option>
                                                        <option value="57">57</option>
                                                        <option value="58">58</option>
                                                        <option value="59">59</option>
                                                        <option value="60">60</option>
                                                        <option value="61">61</option>
                                                        <option value="62">62</option>
                                                        <option value="63">63</option>
                                                        <option value="64">64</option>
                                                        <option value="65">65</option>
                                                        <option value="66">66</option>
                                                        <option value="67">67</option>
                                                        <option value="68">68</option>
                                                        <option value="69">69</option>
                                                        <option value="70">70</option>
                                                    </select> 
                                                    <label class="error" for="age_to"></label> 
                                                </div>
                                            </td>
                                        </tr>
                                        <tr class="">
                                            <td class=""><label class="form-control-label" for="maritalstatus"> <?php echo $this->lang->line('register_profile_for_marriagestatus'); ?></label><label class="import">*</label></td>
                                            <td>:</td>
                                            <td class="">
                                                <select name="maritalstatus" class="form-control js-example-basic-single" required id="pre_maritalstatus">
                                                    <option value="">-- Select Option --</option>
                                                    <option value="1">Never Married</option>
                                                    <option value="2">Widowed</option>
                                                    <option value="3">Divorced</option>
                                                    <option value="4">Awaiting divorce</option>
                                                </select>
                                                <label class="error" for="maritalstatus"></label>
                                            </td>
                                        </tr>

                                        <tr class="">
                                            <td class=""><label class="form-control-label" for="height"> <?php echo $this->lang->line('register_profile_for_height'); ?></label></td>
                                            <td>:</td>
                                            <td class="">
                                                <select class="form-control js-example-basic-single" name="height"  id="pre_height">
                                                    <option value=""> -- <?php echo $this->lang->line("register_profile_for_height"); ?> -- </option>
                                                    <option value="145" label="4' 9&quot; (145 cm)">4' 9" (145 cm)</option>
                                                    <option value="147" label="4' 10&quot; (147 cm)">4' 10" (147 cm)</option>
                                                    <option value="150" label="4' 11&quot; (150 cm)">4' 11" (150 cm)</option>
                                                    <option value="152" label="5'  0&quot; (152 cm)">5'  0" (152 cm)</option>
                                                    <option value="155" label="5' 1&quot; (155 cm)">5' 1" (155 cm)</option>
                                                    <option value="157" label="5' 2&quot; (157 cm)">5' 2" (157 cm)</option>
                                                    <option value="160" label="5' 3&quot; (160 cm)">5' 3" (160 cm)</option>
                                                    <option value="162" label="5' 4&quot; (162 cm)">5' 4" (162 cm)</option>
                                                    <option value="165" label="5' 5&quot; (165 cm)">5' 5" (165 cm)</option>
                                                    <option value="167" label="5' 6&quot; (167 cm)">5' 6" (167 cm)</option>
                                                    <option value="170" label="5' 7&quot; (170 cm)">5' 7" (170 cm)</option>
                                                    <option value="173" label="5' 8&quot; (173 cm)">5' 8" (173 cm)</option>
                                                    <option value="175" label="5' 9&quot; (175 cm)">5' 9" (175 cm)</option>
                                                    <option value="178" label="5' 10&quot; (178 cm)">5' 10" (178 cm)</option>
                                                    <option value="180" label="5' 11&quot; (180 cm)">5' 11" (180 cm)</option>
                                                    <option value="183" label="6' 0&quot; (183 cm)">6' 0" (183 cm)</option>
                                                    <option value="185" label="6' 1&quot; (185 cm)">6' 1" (185 cm)</option>
                                                    <option value="188" label="6' 2&quot; (188 cm)">6' 2" (188 cm)</option>
                                                    <option value="190" label="6' 3&quot; (190 cm)">6' 3" (190 cm)</option>
                                                    <option value="193" label="6' 4&quot; (193 cm)">6' 4" (193 cm)</option>
                                                    <option value="21">Above the Height </option>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr class="">
                                            <td class=""><label class="form-control-label" for="physicalstatus">  <?php echo $this->lang->line('register_profile_for_physicalstatus'); ?></label><label class="import">*</label></td>
                                            <td>:</td>
                                            <td class=" ">	
                                                <select class="form-control js-example-basic-single" name="physicalstatus" id="pre_physicalstatus"  required>
                                                    <option value=""> -- Select Status -- </option>
                                                    <option value="1">Normal</option>
                                                    <option value="2">Physically challenged</option>
                                                </select>
                                                <label class="error" for="physicalstatus"> </label>
                                            </td>
                                        </tr>
                                        <tr class="">
                                            <td class=""><label class="form-control-label" for="food"> <?php echo $this->lang->line('register_profile_for_food'); ?></label></td>
                                            <td>:</td>
                                            <td class=" ">
                                                <select class="form-control js-example-basic-single" name="food" id="pre_food">
                                                    <option value=""> -- Select -- </option>
                                                    <option value="1">Vegetarian</option>
                                                    <option value="2">Non-Vegetarian	</option>
                                                    <option value="3">Eggetarian</option>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr class="">
                                            <td class=""><label class="form-control-label" for="smoking"> <?php echo $this->lang->line('register_profile_for_smoking'); ?></label> </td>
                                            <td>:</td>
                                            <td class=" ">			
                                                <select class="form-control js-example-basic-single" name="smoking" id="pre_smoke" >
                                                    <option value=""> -- Select  -- </option>
                                                    <option value="1">Yes</option>
                                                    <option value="2">No</option>
                                                    <option value="3">Occasionally </option>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr class="">
                                            <td class=""><label class="form-control-label" for="drinking"> <?php echo $this->lang->line('register_profile_for_drinking'); ?></label></td>
                                            <td>:</td>
                                            <td class=" ">
                                                <select class="form-control js-example-basic-single" name="drinking" id="pre_drink" >
                                                    <option value=""> -- Select Status -- </option>
                                                    <option value="1">Yes</option>
                                                    <option value="2">No</option>
                                                    <option value="3">Drinks Socially </option>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr class="">
                                            <td class=""><label class="form-control-label" for="caste"> <?php echo $this->lang->line('register_profile_for_caste'); ?></label></td>
                                            <td>:</td>
                                            <td class="">
                                                <select class="form-control js-example-data-array-selected" id="pre_caste" name="caste">
                                                    <option value=""> -- Select Caste -- </option>
                                                </select> 
                                            </td>
                                        </tr>
                                        <tr class="">
                                            <td class=""><label class="form-control-label" for="star"> <?php echo $this->lang->line('register_profile_for_star'); ?></label></td>
                                            <td>:</td>
                                            <td class="">
                                                <select class="form-control js-example-basic-single" name="star" id="pre_star">
                                                    <option value=""> -- <?php echo $this->lang->line("register_profile_for_star"); ?> -- </option>
                                                    <option value="ashwini">Ashwini</option>
                                                    <option value="bharani">Bharani</option>
                                                    <option value="karthigai">Karthigai</option>
                                                    <option value="rohini">Rohini</option>
                                                    <option value="mirigasirisham">Mirigasirisham</option>
                                                    <option value="thiruvathirai">Thiruvathirai</option>
                                                    <option value="punarpoosam">Punarpoosam</option>
                                                    <option value="poosam">Poosam</option>
                                                    <option value="ayilyam">Ayilyam</option>
                                                    <option value="makam">Makam</option>
                                                    <option value="pooram">Pooram</option>
                                                    <option value="uthiram">Uthiram</option>
                                                    <option value="hastham">Hastham</option>
                                                    <option value="chithirai">Chithirai</option>
                                                    <option value="swathi">Swathi</option>
                                                    <option value="visakam">Visakam</option>
                                                    <option value="anusham">Anusham</option>
                                                    <option value="kettai">Kettai</option>
                                                    <option value="moolam">Moolam</option>
                                                    <option value="pooradam">Pooradam</option>
                                                    <option value="uthradam">Uthradam</option>
                                                    <option value="thiruvonam">Thiruvonam</option>
                                                    <option value="avittam">Avittam</option>
                                                    <option value="sadhayam">Sadhayam</option>
                                                    <option value="puratathi">Puratathi</option>
                                                    <option value="uthirattathi">Uthirattathi</option>
                                                    <option value="revathi">Revathi</option>										
                                                </select> 
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="col-md-6">
                                    <table class="table">
                                        <tr class="">
                                            <td class=""><label class="form-control-label" for="country"> <?php echo $this->lang->line('register_profile_for_country'); ?>  </label></td>
                                            <td>:</td>
                                            <td class=" ">
                                                <select class="form-control js-example-data-array-selected" id="pre_country" name="country" onchange="stateInfo1(this.value)">
                                                    <option value=""> -- Select Country -- </option>
                                                    <?php if ($pre_country == 0) { ?>
                                                        <option value="0" selected="selected"> Any </option>
                                                    <?php } else { ?>
                                                        <option value="<?php echo $pre_country ?>" selected="selected"> <?php echo $pre_country_name; ?> </option>
                                                    <?php } ?>	           	 </select>
                                            </td>
                                        </tr>
                                        <tr class="">
                                            <td class=""><label class="form-control-label" for="state">  <?php echo $this->lang->line('register_profile_for_state'); ?> </label></td>
                                            <td>:</td>
                                            <td class=" ">    
                                                <div id="text-state">  
                                                    <select class="form-control js-example-data-array-selected" name="state" id="pre_state" onchange="cityInfo1(this.value)">
                                                        <option value=""> -- Select State -- </option>
                                                        <?php if ($pre_state == 0) { ?>
                                                            <option value="0" selected="selected"> Any </option>
                                                        <?php } else { ?>
                                                            <option value="<?php echo $pre_state ?>" selected="selected"> <?php echo $pre_state_name; ?> </option>
                                                        <?php } ?>	
                                                    </select>
                                                </div>
                                            </td>																	
                                        </tr>
                                        <tr class="">
                                            <td class=""><label class="form-control-label" for="city"> <?php echo $this->lang->line('register_profile_for_city'); ?> </label></td>
                                            <td>:</td>
                                            <td class=" ">   
                                                <div id="text-city">   
                                                    <select class="form-control js-example-data-array-selected"  id ="pre_city" name="city">
                                                        <option value=""> -- Select City -- </option>
                                                        <?php if ($pre_city == 0) { ?>
                                                            <option value="0" selected="selected"> Any </option>
                                                        <?php } else { ?>
                                                            <option value="<?php echo $pre_city ?>" selected="selected"> <?php echo $pre_city_name; ?> </option>
                                                        <?php } ?>	
                                                    </select>
                                                </div>
                                            </td>																	
                                        </tr>
                                        <tr class="">
                                            <td class=""> <label class="form-control-label" for="education"><?php echo $this->lang->line('register_profile_for_highesteducation'); ?> </label><label class="import">*</label>  </td>
                                            <td>:</td>
                                            <td class="">
                                                <select class="form-control js-example-basic-single" name="education" id ="pre_education" required>
                                                    <option value="">- Select your education -</option>							
                                                    <option value="1" label="High school">High school</option>
                                                    <option value="2" label="Diploma">Diploma</option>
                                                    <option value="3" label="Under Graduate">Under Graduate</option>
                                                    <option value="4" label="Post Graduate">Post Graduate</option>
                                                    <option value="5" label="PhD / Post Doctoral">PhD / Post Doctoral</option>
                                                    <option value="6" label="No answer">No answer</option>						
                                                </select>
                                                <label class="error" for="education"></label>
                                            </td>
                                        </tr>
                                        <tr class="">
                                            <td class=""> <label class="form-control-label" for="qualification"> <?php echo $this->lang->line('register_profile_for_educationqualification'); ?> </label></td>
                                            <td>:</td>
                                            <td class=""><input type="textbox" name="qualification" value="<?php echo $pre_qualification; ?>" class="form-control" ></td>
                                        </tr>
                                        <tr class="">
                                            <td class=""><label class="form-control-label" for="occupation"> <?php echo $this->lang->line('register_profile_for_occupation'); ?></label></td>
                                            <td>:</td>
                                            <td class=""><input type="textbox" name="occupation" value="<?php echo $pre_occupation; ?>" class="form-control" ></td>
                                        </tr>
                                        <tr class="">
                                            <td class=""><label class="form-control-label" for="emp_in"> <?php echo $this->lang->line('register_profile_for_employeed'); ?></label></td>
                                            <td>:</td>
                                            <td class="">
                                                <select class="form-control js-example-basic-single" name="emp_in" id="pre_employeed">
                                                    <option value=""> -- Select -- </option>
                                                    <option value="1">Government </option>
                                                    <option value="2">Self Employeed </option>
                                                    <option value="3">Businees</option>
                                                    <option value="4">Private </option>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr class="">
                                            <td class=""><label class="form-control-label" for="salary">  <?php echo $this->lang->line('annual_income'); ?></label></td>
                                            <td>:</td>
                                            <td class="">
                                                <select class="form-control js-example-basic-single" name="salary" id="pre_salary">
                                                    <option value=""> -- select salary -- </option>
                                                    <option value="1">100000 or less</option>
                                                    <option value="2">200000 to 300000</option>
                                                    <option value="3">300000 to 400000</option>
                                                    <option value="4">400000 to 500000</option>
                                                    <option value="5">500000 to 600000</option>
                                                    <option value="6">600000 to 700000</option>
                                                    <option value="7">700000 to 800000</option>
                                                    <option value="8">800000 to 900000</option>
                                                    <option value="9">900000 Above</option>
                                                    <option value="10">any</option>
                                                </select>  
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3" class="text-right">
                                                <button class="btn btn-default" type="button" ><?php echo $this->lang->line('cancel_admin_register'); ?></button>
                                                <button class="btn btn-success" type="submit" id="" name="step3"><?php echo $this->lang->line('save_text'); ?></button>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </form>
                    </div>
                    <?php //}   ?>
                    <hr>
                    <div class="col-md-12 text-left">

                    </div>				
                    <?php
                } //else { echo $this->lang->line('no_data');
            } else {
                echo $this->lang->line('no_data');
            }
            ?>
        </div>

        <input type="hidden" id="baseurl" value="<?php echo base_url(); ?>"/>
        <input type="hidden" id="userguid" value="<?php echo $userGuid; ?>">
        <input type="hidden" id="type" value="<?php echo $type; ?>">


        <script>

            var baseurl = $("#baseurl").val();
            var userguid = $("#userguid").val();
            $("#images").fileinput({
                uploadUrl: baseurl + 'index.php/admin/fileupload?uid=' + userguid, // you must set a valid URL here else you will get an error
                allowedFileExtensions: ['jpg', 'png', 'jpeg'],
                overwriteInitial: false,
                maxFileSize: 1500,
                maxFilesNum: 5,
                //allowedFileTypes: ['image', 'video', 'flash'],
                slugCallback: function (filename) {
                    return filename.replace('(', '_').replace(']', '_');
                }
            });

            $("#upfile1").click(function () {
                $("#images").trigger('click');
                //$(".fileinput-upload-button").hide();

            });


            var baseurl = $('#baseurl').val();

            function stateInfo(reqData) {

                if (reqData == 0) {
                    $("#city").html('');
                    $("#state").html('');
                    return false
                }
                $("#city").html('');
                $("#state").html('');
                $.ajax({
                    type: "post",
                    dataType: "json",
                    url: baseurl + 'index.php/common/statelist/' + reqData,
                    success: function (request) {
                        if (request.status == 0) {
                            $("#text-state").html('<input type="text" class="form-control" name="state_text" id="text-state" placeholder=" Enthe the state">');
                            $("#text-city").html('<input type="text" class="form-control" name="city_text" id="text-city" placeholder=" Enthe the city">');
                        } else {
                            $("#state").select2({
                                data: request
                            });
                        }
                    },
                    error: function (data) {

                    },
                });

                return false;
            }

            function cityInfo(reqData) {
                if (reqData == 0) {
                    return false
                }
                $("#city").html('');
                $.ajax({
                    type: "post",
                    dataType: "json",
                    url: baseurl + 'index.php/common/citylist/' + reqData,
                    success: function (request) {
                        if (request.status == 0) {
                            $("#text-city").html('<input type="text" class="form-control" name="city_text" id="text-city" placeholder=" Enthe the city">');
                        } else {
                            $("#city").select2({
                                data: request
                            });
                        }
                    },
                    error: function (data) {

                    },
                });

                return false;
            }

            function stateInfo1(reqData) {
                if (reqData == 0) {
                    $("#pre_city").html('');
                    $("#pre_state").html('');
                    $("#pre_city").select2({
                        data: [{"id": "0", "text": "Any"}]
                    });
                    $("#pre_state").select2({
                        data: [{"id": "0", "text": "Any"}]
                    });

                    return false
                }
                $("#pre_state").html('');
                $.ajax({
                    type: "post",
                    dataType: "json",
                    url: baseurl + 'index.php/common/statelist/' + reqData,
                    success: function (request) {
                        if (request.status == 0) {
                            $("#text-state").html('<input type="text" class="form-control" name="state_text" id="text-state" placeholder=" Enthe the state">');
                            $("#text-city").html('<input type="text" class="form-control" name="city_text" id="text-city" placeholder=" Enthe the city">');
                        } else {
                            $("#pre_state").select2({
                                data: request
                            });
                        }
                    },
                    error: function (data) {

                    },
                });

                return false;
            }

            function cityInfo1(reqData) {
                if (reqData == 0) {
                    $("#pre_city").html('');
                    $("#pre_city").select2({
                        data: [{"id": "0", "text": "Any"}]
                    });
                    return false
                }
                $("#pre_city").html('');
                $.ajax({
                    type: "post",
                    dataType: "json",
                    url: baseurl + 'index.php/common/citylist/' + reqData,
                    success: function (request) {
                        if (request.status == 0) {
                            $("#text-city").html('<input type="text" class="form-control" name="city_text" id="text-city" placeholder=" Enthe the city">');
                        } else {
                            $("#pre_city").select2({
                                data: request
                            });
                        }
                    },
                    error: function (data) {

                    },
                });

                return false;
            }

            function deleteimage(pid, image) {
                //alert(pid);
                //alert(image);
                if (pid == 0 || pid == '' || image == '') {
                    return false;
                }
                //$('#response').show();
                $.post(baseurl + 'index.php/admin/removeprofileimage?pid=' + pid + '&id=' + image,
                        function (data) {
                            alert(data.msg);
                            location.reload();
                        }, "json");
            }
            function activeimage(pid, image) {
                //alert(pid);
                //alert(image);
                if (pid == 0 || pid == '' || image == '') {
                    return false;
                }
                //$('#response').show();
                $.post(baseurl + 'index.php/admin/activeprofileimage?pid=' + pid + '&id=' + image,
                        function (data) {
                            alert(data.msg);
                            location.reload();
                        }, "json");
            }

            $("#upfile1").click(function () {
                $("#images").trigger('click');

            });
        </script>
        <script type="text/javascript">
            function SwapDivsWithClick(div1, div2)
            {
                d1 = document.getElementById(div1);
                d2 = document.getElementById(div2);
                if (d2.style.display == "none")
                {
                    d1.style.display = "none";
                    d2.style.display = "block";
                }
                else
                {
                    d1.style.display = "block";
                    d2.style.display = "none";
                }
            }

            function SwapDivsWithClick(div3, div4)
            {
                d3 = document.getElementById(div3);
                d4 = document.getElementById(div4);
                if (d4.style.display == "none")
                {
                    d3.style.display = "none";
                    d4.style.display = "block";
                }
                else
                {
                    d3.style.display = "block";
                    d4.style.display = "none";
                }
            }

            function SwapDivsWithClick(div5, div6)
            {
                d5 = document.getElementById(div5);
                d6 = document.getElementById(div6);
                if (d6.style.display == "none")
                {
                    d5.style.display = "none";
                    d6.style.display = "block";
                }
                else
                {
                    d5.style.display = "block";
                    d6.style.display = "none";
                }
            }

            function SwapDivsWithClick(div7, div8)
            {
                d7 = document.getElementById(div7);
                d8 = document.getElementById(div8);
                if (d8.style.display == "none")
                {
                    d7.style.display = "none";
                    d8.style.display = "block";
                }
                else
                {
                    d7.style.display = "block";
                    d8.style.display = "none";
                }
            }
            function SwapDivsWithClick(div9, div10)
            {
                d9 = document.getElementById(div9);
                d10 = document.getElementById(div10);
                if (d10.style.display == "none")
                {
                    d9.style.display = "none";
                    d10.style.display = "block";
                }
                else
                {
                    d9.style.display = "block";
                    d10.style.display = "none";
                }
            }

        </script>
        <script>

            $(document).ready(function () {

                var baseurl = $('#baseurl').val();
                var userguid = $("#userguid").val();
                var type = $('#type').val();
                
                /* Profile */
                $("#profilecreated").val( "<?php echo (!empty($profile_created) ? $profile_created : 0); ?>" ).trigger("change");
                $("#mother_tongue").val( "<?php echo (!empty($tongue)?$tongue:0);?>" ).trigger("change");
                $("#height").val(<?php echo (!empty($height) ? $height : 0); ?>).trigger("change");
				$("#maritalstatus").val(<?php echo (!empty($martialstatus)?$martialstatus:0); ?>).trigger("change");
				$("#weight").val(<?php echo (!empty($weight)?$weight:0);?>).trigger("change");
				$("#bodytype").val(<?php echo (!empty($bodytype)?$bodytype:0);?>).trigger("change");
				$("#complexion").val(<?php echo (!empty($complexion)?$complexion:0);?>).trigger("change");
				$("#physicalstatus").val(<?php echo (!empty($phystatus)?$phystatus:0);?>).trigger("change");
				$("#education").val(<?php echo (!empty($education)?$education:0);?>).trigger("change");
				$("#employeed").val(<?php echo (!empty($employeed)?$employeed:0);?>).trigger("change");
				$("#salary").val(<?php echo (!empty($salary)?$salary:0);?>).trigger("change");
				$("#food").val(<?php echo (!empty($food)?$food:0);?>).trigger("change");
				$("#smoke").val(<?php echo (!empty($smoke)?$smoke:0);?>).trigger("change");
				$("#drink").val(<?php echo (!empty($drink)?$drink:0);?>).trigger("change");
				$("#dhosam").val(<?php echo (!empty($dhosam)?$dhosam:0)?>).trigger("change");
				$("#star").val("<?php echo (!empty($star)?$star:0);?>").trigger("change");
				$("#raasi").val("<?php echo (!empty($raasi) ? $raasi : 0); ?>").trigger("change");
				$("#fstatus").val(<?php echo (!empty($fstatus) ? $fstatus : 0); ?>).trigger("change");
				$("#fvalues").val(<?php echo (!empty($fvalues) ? $fvalues : 0) ?>).trigger("change");
				$("#ftype").val(<?php echo (!empty($ftype) ? $ftype : 0) ?>).trigger("change");
				
				/* Partner */
				$("#pre_age_from").val(<?php echo (!empty($ageFrom) ? $ageFrom : 0); ?>).trigger("change");
				$("#pre_age_to").val(<?php echo (!empty($ageTo) ? $ageTo : 0); ?>).trigger("change");
				$("#pre_maritalstatus").val(<?php echo (!empty($pre_maritalstatus) ? $pre_maritalstatus : 0); ?>).trigger("change");
				$("#pre_height").val(<?php echo (!empty($pre_height) ? $pre_height : 0); ?>).trigger("change");
				$("#pre_physicalstatus").val(<?php echo (!empty($pre_physicalstatus) ? $pre_physicalstatus : 0); ?>).trigger("change");
				$("#pre_food").val(<?php echo (!empty($pre_food) ? $pre_food : 0); ?>).trigger("change");
				$("#pre_smoke").val(<?php echo (!empty($pre_smoke) ? $pre_smoke : 0); ?>).trigger("change");
				$("#pre_drink").val(<?php echo (!empty($pre_drink) ? $pre_drink : 0); ?>).trigger("change");	
				$("#pre_caste").val(<?php echo (!empty($pre_caste) ? $pre_caste : 0); ?>).trigger("change");
				$("#pre_star").val("<?php echo (!empty($pre_star) ? $pre_star : 0); ?>").trigger("change");				
				$("#pre_education").val(<?php echo (!empty($pre_education) ? $pre_education : 0); ?>).trigger("change");
				$("#pre_employeed").val(<?php echo (!empty($pre_employeed) ? $pre_employeed : 0); ?>).trigger("change");
				$("#pre_salary").val(<?php echo (!empty($pre_salary) ? $pre_salary : 0); ?>).trigger("change");

                $("#email-form").validate({
                    //perform an AJAX post to ajax.php
                    submitHandler: function () {
                        $("#exits").hide();
                        $.post(baseurl + 'index.php/admin_profile/setting?step1=1',
                                $('#email-form').serialize(),
                                function (data) {
                                    if (data.msg == 'success') {
                                        $("#email-form").hide();
                                        $("#pass-form").show();
                                        alert(data.msg);
                                        location.reload();
                                        //$("#email-text").html('<?php echo $this->lang->line('pls_provide_your_password_for_security'); ?>');
                                    } else {
                                        $("#exits").show();
                                        $('#exits').html(data.msg);
                                        alert(data.msg);
                                        location.reload();
                                    }
                                }, "json")
                                .fail(function () {
                                    alert("<?php echo $this->lang->line('connection_error'); ?>");
                                    location.reload();
                                });
                    }
                });


                $("#pass-form").validate({
                    //perform an AJAX post to ajax.php
                    submitHandler: function () {
                        var myObject = new Object();
                        myObject.email = $('#email').val();
                        myObject.password = $('#password').val();
                        var reqData = JSON.stringify(myObject);
                        $.post(baseurl + 'index.php/admin_profile/setting?step2=1',
                                myObject,
                                function (data) {
                                    $("#email-form").hide();
                                    $("#pass-form").hide();
                                    $("#email-text").html(data.msg);
                                }, "json")
                                .fail(function () {
                                    alert("<?php echo $this->lang->line('connection_error'); ?>");
                                    location.reload();
                                });
                    }
                });



                $('#pesonal').validate({
                    rules: {
                        mobile: {
                            required: true,
                            number: true
                        }
                    },
                    //perform an AJAX post to ajax.php
                    submitHandler: function () {
                        $.post(baseurl + 'index.php/admin_profile/update?uid=' + userguid + '&type=' + type + '&step1=1',
                                $('#pesonal').serialize(),
                                function (data) {
                                    alert(data.msg);
                                    location.reload();
                                }, "json")
                                .fail(function () {
                                    alert("<?php echo $this->lang->line("connection_error"); ?>")
                                    location.reload();
                                });
                    }
                });


                $('#physical_form').validate({
                    //perform an AJAX post to ajax.php
                    submitHandler: function () {
                        $.post(baseurl + 'index.php/admin_profile/update?uid=' + userguid + '&type=' + type + '&step2=1',
                                $('#physical_form').serialize(),
                                function (data) {
                                    alert(data.msg);
                                    location.reload();
                                }, "json")
                                .fail(function () {
                                    alert("<?php echo $this->lang->line("connection_error"); ?>");
                                    location.reload();
                                });
                    }
                });

                $('#preference_form').validate({
                    //perform an AJAX post to ajax.php
                    submitHandler: function () {
                        $.post(baseurl + 'index.php/admin_profile/update?uid=' + userguid + '&type=' + type + '&step3=1',
                                $('#preference_form').serialize(),
                                function (data) {
                                    alert(data.msg);
                                    location.reload();
                                }, "json")
                                .fail(function () {
                                    alert("<?php echo $this->lang->line("connection_error"); ?>");
                                    location.reload();
                                });
                    }
                });

                $("#passchange-form").validate({
                    rules: {
                        new_password: {
                            minlength: 6
                        },
                        conform_password: {
                            equalTo: "#new"
                        }
                    },
                    //perform an AJAX post to ajax.php
                    submitHandler: function () {
                        $.post(baseurl + 'index.php/admin_profile/setting?step2=1',
                                $('#passchange-form').serialize(),
                                function (data) {
                                    if (data.msg == 'success') {
                                        alert('successfully updated');
                                        $('#pass_change').val('');
                                        $('#new').val('');
                                        $('#confirm').val('');
                                        location.reload();
                                    } else {
                                        $("#pass_error label.error").show();
                                        $('#pass_change').removeClass('valid');
                                        $('#pass_change').addClass('error');
                                        $("#pass_error label.error").html(data.msg);
                                        $('#pass_change').val('');
                                        location.reload();
                                    }
                                }, "json")
                                .fail(function () {
                                    alert("<?php echo $this->lang->line('connection_error'); ?>");
                                    location.reload();
                                });
                    }
                });
                $("#caste").val(<?php echo (!empty($casteid) ? $casteid : ''); ?>).trigger("change");



            });
        </script>

