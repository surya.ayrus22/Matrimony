<!--<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jspdf/0.9.0rc1/jspdf.js"></script>-->
<script type='text/javascript' src="<?php echo base_url(); ?>assets/admin/js/pdf/jspdf.debug.js"></script>					
<div id="page-wrapper">
    <div class="graphs bgimage">
        <div id="mydiv"><!--print start-->	
            <content-top>
                <div class="content-top clearfix">
                    <h1 class="al-title"><?php echo $this->lang->line('user_profile_title'); ?></h1>
                    <ul class="breadcrumb al-breadcrumb printopt" id="pdfhidden">
                        <li><a href="<?php echo base_url() . 'index.php/admin/dashboard'; ?>"><?php echo $this->lang->line('dashboard'); ?> </a></li>
                        <?php
                        if ($type == constant('TYPE_1') ) {
                            ?>
                            <li><a href="<?php echo base_url() . 'index.php/admin/userdetails?type=' . $type; ?>"><?php echo $this->lang->line('users'), ' ', $this->lang->line('detail_text'); ?> </a></li> 
                        <?php } elseif ($type == constant('TYPE_2') ) { ?>

                            <li><a href="<?php echo base_url() . 'index.php/admin/userdetails?type=' . $type; ?>"><?php echo constant('GENDER_M'), ' ', $this->lang->line('users'); ?> </a></li> 
                        <?php } elseif ($type == constant('TYPE_3') ) { ?>

                            <li><a href="<?php echo base_url() . 'index.php/admin/userdetails?type=' . $type; ?>"><?php echo constant('GENDER_F'), ' ', $this->lang->line('users'); ?> </a></li> 
                        <?php } elseif ($type == constant('TYPE_4')) { ?>

                            <li><a href="<?php echo base_url() . 'index.php/admin/userdetails?type=' . $type; ?>"><?php echo $this->lang->line('premium'), ' ', $this->lang->line('users'); ?></a></li> 
                        <?php } elseif ($type == constant('TYPE_5')) { ?>

                            <li><a href="<?php echo base_url() . 'index.php/admin/userdetails?type=' . $type; ?>"><?php echo $this->lang->line('active'), ' ', $this->lang->line('users'); ?> </a></li> 
                        <?php } elseif ($type == constant('TYPE_6')) { ?>

                            <li><a href="<?php echo base_url() . 'index.php/admin/userdetails?type=' . $type; ?>"><?php echo $this->lang->line('deactive'), ' ', $this->lang->line('users'); ?> </a></li> 
                        <?php } elseif ($type == constant('TYPE_7')) { ?>

                            <li><a href="<?php echo base_url() . 'index.php/admin/userdetails?type=' . $type; ?>"><?php echo $this->lang->line('delete'), ' ', $this->lang->line('users'); ?> </a></li> 
                        <?php } elseif ($type == constant('TYPE_8')) { ?>

                            <li><a href="<?php echo base_url() . 'index.php/adminuser/premiummember?type=' . $type; ?>"><?php echo $this->lang->line('platinum'); ?> </a></li>
                        <?php } elseif ($type == constant('TYPE_9')) { ?>    

                            <li><a href="<?php echo base_url() . 'index.php/adminuser/premiummember?type=' . $type; ?>"><?php echo $this->lang->line('diamond'); ?> </a></li>
                        <?php } elseif ($type == constant('TYPE_10')) { ?>

                            <li><a href="<?php echo base_url() . 'index.php/adminuser/premiummember?type=' . $type; ?>"><?php echo $this->lang->line('gold'); ?> </a></li>
                        <?php } elseif ($type == constant('TYPE_13')) { ?>

                            <li><a href="<?php echo base_url() . 'index.php/adminpayment/userdetails?type=' . $type; ?>"><?php echo $this->lang->line('unpaid-users'); ?> </a></li>
                        <?php } elseif ($type == constant('TYPE_11')) { ?>
							
                            <li><a href="<?php echo base_url() . 'index.php/admin_profile?type=' . $type; ?>"><?php echo $this->lang->line('profile_approved'); ?> </a></li>
                        <?php } else { ?>

                        <?php } ?>
                        <li class=""><?php echo $this->lang->line('user_profile_title'); ?></li>
                    </ul>
                </div>
            </content-top>

            <div class="col-md-12 contentinner" >
                <div class="form-group printopt text-right" id="pdfhidden">
                    <a href="<?php echo base_url() . 'index.php/admin_profile/editUser?uid=' . $userguid . '&type=' . $type; ?>">
                        <div class="btn btn-primary"><?php echo $this->lang->line('edit_text'); ?></div>
                    </a>
                    <div class="btn btn-danger" onclick="deleteuser('<?php echo $userguid ?>', '<?php echo $deleteid = 1; ?>')"><?php echo $this->lang->line('delete'); ?></div>
                    <button class="btn btn-success printopt" onclick="PrintElem('#mydiv')" >
                        <span class="faicon"><i class="fa fa-print"></i></span><?php echo $this->lang->line('print'); ?>
                    </button>
                </div>
                <hr>
                <?php
                if (!empty($viewDetails)) {
                    foreach ($viewDetails as $value) { //echo "<pre>"; print_r($value); echo "</pre>";	
                        ?>
                        <div class="col-md-3">
                            <img src="<?php echo base_url() . 'assets/upload_images/' . $value['image']; ?>" width="200" height="200">
                        </div>
                        <div class="col-md-6">
                            <table class="table">
                                <tbody>
                                    <tr class="opened">
                                        <td class="day_label"><?php echo $this->lang->line('username') . ' - ' . $this->lang->line('profile_id') ?> :</td>
                                        <td class="day_value"><?php echo (!empty($value['username']) ? $value['username'] : 'Not Specified') . ' - ' . constant('MEMBERID'), (!empty($value['userId']) ? $value['userId'] : 'Not Specified'); ?></td>
                                    </tr>
                                    <tr class="opened">
                                        <td class="day_label"><?php echo $this->lang->line('search_profile_for_age') . ' / ' . $this->lang->line('register_profile_for_height'); ?> :</td>
                                        <td class="day_value"><?php echo (!empty($value['age']) ? $value['age'] : 'Not Specified') . ' / ' . (!empty($value['height']) ? $value['height'] . ' Cms' : 'Not Specified'); ?></td>
                                    </tr>
                                    <tr class="opened">
                                        <td class="day_label"><?php echo $this->lang->line('register_profile_for_religion') . ' / ' . $this->lang->line('register_profile_for_marriagestatus'); ?>:</td>
                                        <td class="day_value"><?php echo (!empty($value['religion']) ? $value['religion'] : 'Not Specified') . ' / ' . (!empty($value['martial_status']) ? constant("MARITAL_STATUS_" . $value['martial_status']) : 'Not Specified'); ?></td>
                                    </tr>
                                    <tr class="opened">  
                                        <td class="day_label"><?php echo sprintf($this->lang->line('search_profile_create'), $this->lang->line('by_text')) . ' / ' . $this->lang->line('location_text') . ' / ' . sprintf($this->lang->line('last_text'), $this->lang->line('text_login')); ?>:</td>
                                        <td class="day_value"><?php echo (!empty($value['profile_created']) ? $value['profile_created'] : 'Not Specified') . ' / ' . (!empty($value['city_name']) ? $value['city_name'] : 'Not Specified') . ' / ' . (!empty($value['log_in']) ? $value['log_in'] : 'Not Specified'); ?></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>


                        <div class="col-md-3 printopt" id="pdfhidden">
                        <!--	<div class="form-group"><a href="<?php echo base_url() . 'index.php/admin_profile/editUser?uid=' . $value['userGuid'] . '&type=' . $type; ?>"><div class="btn btn-primary"><?php echo $this->lang->line('edit_text'); ?></div></a></div>
                                <div class="form-group"><div class="btn btn-danger" onclick="deleteuser('<?php echo $value['userGuid']; ?>', '<?php echo $deleteid = 1; ?>')"><?php echo $this->lang->line('delete'); ?></div></div>
                                <div class="form-group"><button class="btn btn-success" onclick="deactive('<?php echo $value['userGuid']; ?>', '<?php echo $status = 0; ?>')"><?php echo $this->lang->line('deactivate'); ?></button></div>-->


                        </div>

                        <div class="clearfix"></div>
                        <hr>

                        <div id="tabs-container">
                            <ul class="tabs-menu printopt" id="pdfhidden">
                                <li class="current printopt"><a href="#tab-1"><?php echo $this->lang->line('about'); ?></a></li>
                                <li class="printopt"><a href="#tab-2"><?php echo $this->lang->line('register_profile_for_process2'); ?></a></li>
                                <li class="printopt"><a href="#tab-3"><?php echo $this->lang->line('register_profile_for_process4'); ?></a></li>
                            </ul>
                            <div class="tab">
                                <div id="tab-1" class="tab-content">
                                    <h3><?php echo $this->lang->line('about'); ?></h3>
                                    <p><?php echo (!empty($value['summary']) ? $value['summary'] : 'Not Specified'); ?></p>
                                </div>
                                <div id="tab-2" class="tab-content">
                                    <h3><?php echo $this->lang->line('basic'); ?></h3>
                                    <div class="col-md-6 text-left">
                                        <table class="table">
                                            <tbody>
                                                <tr class="opened">
                                                    <td class="day_label"><?php echo $this->lang->line('register_profile_for_name'); ?> :</td>
                                                    <td class="day_value"><?php echo (!empty($value['username']) ? $value['username'] : 'Not Specified'); ?></td>
                                                </tr>
                                                <tr class="opened">
                                                    <td class="day_label"><?php echo $this->lang->line('register_profile_for_email'); ?> :</td>
                                                    <td class="day_value"><?php echo (!empty($value['email']) ? $value['email'] : 'Not Specified'); ?></td>
                                                </tr>
                                                <tr class="opened">
                                                    <td class="day_label"><?php echo $this->lang->line('register_profile_for_gender'); ?> :</td>
                                                    <td class="day_value"><?php echo (!empty($value['gender']) ? constant("GENDER_" . strtoupper($value['gender'])) : 'Not Specified'); ?></td>
                                                </tr>
                                                <tr class="opened">
                                                    <td class="day_label"><?php echo $this->lang->line('register_profile_for_mobile'); ?> :</td>
                                                    <td class="day_value"><?php echo (!empty($value['mobile']) ? $value['mobile'] : 'Not Specified'); ?></td>
                                                </tr>
                                                <tr class="opened">
                                                    <td class="day_label"><?php echo $this->lang->line('register_profile_for_marriagestatus'); ?>  :</td>
                                                    <td class="day_value"><?php echo (!empty($value['martial_status']) ? constant("MARITAL_STATUS_" . $value['martial_status']) : 'Not Specified'); ?></td>
                                                </tr>
                                                <?php if ($value['martial_status'] != 1) { ?> 
                                                    <tr class="opened">
                                                        <td class="day_label"><?php echo $this->lang->line('no_of_children_admin_register'); ?>:</td>
                                                        <td class="day_value"><?php echo (!empty($value['childs']) ? $value['childs'] : 'Not Specified'); ?></td>
                                                    </tr>
                                                <?php } ?>
                                                <tr class="opened">
                                                    <td class="day_label"><?php echo $this->lang->line('register_profile_for_bodytype'); ?> :</td>
                                                    <td class="day_value"><?php echo (!empty($value['bodytype']) ? constant("BODY_TYPE_" . $value['bodytype']) : 'Not Specified'); ?></td>
                                                </tr>
                                                <tr class="opened">
                                                    <td class="day_label"><?php echo $this->lang->line('register_profile_for_height'); ?> :</td>
                                                    <td class="day_value"><?php echo (!empty($value['height']) ? $value['height'] . ' cm' : 'Not Specified'); ?></td>
                                                </tr>
                                                <tr class="opened">
                                                    <td class="day_label"> <?php echo $this->lang->line('register_profile_for_physicalstatus'); ?> :</td>
                                                    <td class="day_value closed"><span><?php echo (!empty($value['physical_status']) ? constant("PHYSICAL_STATUS_" . $value['physical_status']) : 'Not Specified'); ?></span></td>
                                                </tr>
                                                <tr class="opened">
                                                    <td class="day_label"><?php echo sprintf($this->lang->line('search_profile_create'), $this->lang->line('by_text')); ?> :</td>
                                                    <td class="day_value closed"><span><?php echo (!empty($value['profile_created']) ? $value['profile_created'] : 'Not Specified'); ?></span></td>
                                                </tr>
                                                <tr class="opened">
                                                    <td class="day_label"><?php echo $this->lang->line('register_profile_for_drinking'); ?> :</td>
                                                    <td class="day_value closed"><span><?php echo (!empty($value['drink']) ? constant("DRINK_" . $value['drink']) : 'Not Specified'); ?></span></td>
                                                </tr>

                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="col-md-6 text-left">
                                        <table class="table">
                                            <tbody>
                                                <tr class="opened">
                                                    <td class="day_label"><?php echo $this->lang->line('search_profile_for_age'); ?> :</td>
                                                    <td class="day_value"><?php echo (!empty($value['age']) ? $value['age'] . ' Yrs' : 'Not Specified'); ?></td>
                                                </tr>
                                                <tr class="opened">
                                                    <td class="day_label"><?php echo $this->lang->line('register_profile_for_tongue'); ?>  :</td>
                                                    <td class="day_value"><?php echo (!empty($value['mother_tongue']) ? $value['mother_tongue'] : 'Not Specified'); ?></td>
                                                </tr>
                                                <tr class="opened">
                                                    <td class="day_label"><?php echo $this->lang->line('register_profile_for_complexion'); ?>  :</td>
                                                    <td class="day_value"><?php echo (!empty($value['complexion']) ? constant("COMPLEXION_TYPE_" . $value['complexion']) : 'Not Specified'); ?></td>
                                                </tr>
                                                <tr class="opened">
                                                    <td class="day_label"><?php echo $this->lang->line('register_profile_for_weight'); ?>  :</td>
                                                    <td class="day_value"><?php echo (!empty($value['weight']) ? $value['weight'] . ' kg' : 'Not Specified'); ?> </td>
                                                </tr>
                                                <tr class="closed">
                                                    <td class="day_label"><?php echo $this->lang->line('search_eatting_habit'); ?> :</td>
                                                    <td class="day_value closed"><span><?php echo (!empty($value['food']) ? constant("FOOD_TYPE_" . $value['food']) : 'Not Specified'); ?></span></td>
                                                </tr>
                                                <tr class="closed">
                                                    <td class="day_label"><?php echo $this->lang->line('register_profile_for_smoking'); ?> :</td>
                                                    <td class="day_value closed"><span><?php echo (!empty($value['smoke']) ? constant("SMOKE_" . $value['smoke']) : 'Not Specified'); ?></span></td>
                                                </tr>

                                                <tr class="closed">
                                                    <td class="day_label"><?php echo $this->lang->line('register_profile_for_country'); ?> :</td>
                                                    <td class="day_value closed"><span><?php echo (!empty($value['country_name']) ? $value['country_name'] : 'Not Specified'); ?></span></td>
                                                </tr>
                                                <tr class="opened">
                                                    <td class="day_label"><?php echo $this->lang->line('register_profile_for_state'); ?> :</td>
                                                    <td class="day_value closed"><span><?php echo (!empty($value['state_name']) ? $value['state_name'] : 'Not Specified'); ?></span></td>
                                                </tr>
                                                <tr class="closed">
                                                    <td class="day_label"><?php echo $this->lang->line('register_profile_for_city'); ?> :</td>
                                                    <td class="day_value closed"><span><?php echo (!empty($value['city_name']) ? $value['city_name'] : 'Not Specified'); ?></span></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <hr>
                                    <h3><?php echo $this->lang->line('reg_social_title'); ?></h3>
                                    <div class="col-md-6 text-left">
                                        <table class="table">
                                            <tbody>
                                                <tr class="opened">
                                                    <td class="day_label"><?php echo $this->lang->line('register_profile_for_dob'); ?> :</td>
                                                    <td class="day_value"><?php echo (!empty($value['dob']) ? date("d-m-Y", strtotime($value['dob'])) : 'Not Specified'); ?></td>
                                                </tr>
                                                <tr class="opened">
                                                    <td class="day_label"><?php echo $this->lang->line('register_profile_for_caste'); ?> :</td>
                                                    <td class="day_value"><?php echo (!empty($value['caste_name']) ? $value['caste_name'] : 'Not Specified'); ?></td>
                                                </tr>
                                                <tr class="opened_1">
                                                    <td class="day_label"><?php echo $this->lang->line('register_profile_for_religion'); ?> :</td>
                                                    <td class="day_value"><?php echo (!empty($value['religion']) ? $value['religion'] : 'Not Specified'); ?></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="col-md-6 text-left">
                                        <table class="table">
                                            <tbody>
                                                <tr class="opened">
                                                    <td class="day_label"><?php echo $this->lang->line('register_profile_for_subcaste'); ?> :</td>
                                                    <td class="day_value"><?php echo (!empty($value['sub_caste']) ? $value['sub_caste'] : 'Not Specified'); ?></td>
                                                </tr>
                                                <tr class="opened">
                                                    <td class="day_label"><?php echo $this->lang->line('register_profile_for_raasi'); ?> :</td>
                                                    <td class="day_value"><?php echo (!empty($value['raasi']) ? $value['raasi'] : 'Not Specified'); ?></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <hr>
                                    <div class="col-md-7 text-left">
                                        <h3><?php echo $this->lang->line('edu_career_title'); ?></h3>
                                        <table class="table">
                                            <tbody>
                                                <tr class="opened">
                                                    <td class="day_label"><?php echo $this->lang->line('search_education_title'); ?>   :</td>
                                                    <td class="day_value"><span><?php echo (!empty($value['edu_name']) ? $value['edu_name'] : 'Not Specified') . ' - ' . (!empty($value['qualification']) ? ucfirst($value['qualification']) : 'Not Specified'); ?></td>
                                                </tr>
                                                <tr class="opened">
                                                    <td class="day_label"><?php echo $this->lang->line('register_profile_for_occupation'); ?> :</td>
                                                    <td class="day_value closed"><span><?php echo (!empty($value['occupation']) ? $value['occupation'] : 'Not Specified'); ?></span></td>
                                                </tr>
                                                <tr class="opened">
                                                    <td class="day_label"><?php echo $this->lang->line('register_profile_for_salary'); ?> :</td>
                                                    <td class="day_value closed"><span><?php echo (!empty($value['salary_view']) ? $value['salary_view'] : 'Not Specified'); ?></span></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>

                                </div>
                                <div id="tab-3" class="tab-content">
                                    <div class="col-md-12 text-left">
                                        <h3><?php echo $this->lang->line('register_profile_for_process4'); ?></h3>
                                        <table class="table">
                                            <tbody>
                                                <?php
                                                $prefrenceData = (!empty($value['prefrenceData']) ? $value['prefrenceData'] : '');
                                                if (!empty($prefrenceData)) {
                                                    //echo "<pre>";print_r($prefrenceData);exit;
                                                    //foreach($value['prefrenceData'] as $prefrenceData){ 
                                                    ?>
                                                    <tr class="opened">
                                                        <td class="day_label"><?php echo $this->lang->line('search_profile_for_age'); ?>   :</td>
                                                        <td class="day_value"><?php echo (!empty($prefrenceData['age_from']) ? $prefrenceData['age_from'] : 'Not Specified') . '-' . (!empty($prefrenceData['age_to']) ? $prefrenceData['age_to'] : 'Not Specified'); ?></td>
                                                    </tr>
                                                    <tr class="opened">
                                                        <td class="day_label"><?php echo $this->lang->line('register_profile_for_marriagestatus'); ?>:</td>
                                                        <td class="day_value"><?php echo (!empty($prefrenceData['maritalstatus']) ? constant("MARITAL_STATUS_" . $prefrenceData['maritalstatus']) : 'Not Specified'); ?></td>
                                                    </tr>
                                                    <tr class="opened">
                                                        <td class="day_label"><?php echo $this->lang->line('register_profile_for_physicalstatus'); ?> :</td>
                                                        <td class="day_value"><?php echo (!empty($prefrenceData['physicalstatus']) ? constant("PHYSICAL_STATUS_" . $prefrenceData['physicalstatus']) : 'Not Specified'); ?></td>
                                                    </tr>
                                                    <tr class="opened">
                                                        <td class="day_label"><?php echo $this->lang->line('register_profile_for_height'); ?>:</td>
                                                        <td class="day_value closed"><span><?php echo (!empty($prefrenceData['height']) ? $prefrenceData['height'] . ' cm' : 'Not Specified'); ?> </span></td>
                                                    </tr>
                                                    <tr class="opened">
                                                        <td class="day_label"><?php echo $this->lang->line('search_eatting_habit'); ?> :</td>
                                                        <td class="day_value closed"><span><?php echo (!empty($prefrenceData['food']) ? constant("FOOD_TYPE_" . $prefrenceData['food']) : 'Not Specified'); ?></span></td>
                                                    </tr>
                                                    <tr class="opened">
                                                        <td class="day_label"><?php echo $this->lang->line('register_profile_for_smoking'); ?> :</td>
                                                        <td class="day_value closed"><span><?php echo (!empty($prefrenceData['smoking']) ? constant("SMOKE_" . $prefrenceData['smoking']) : 'Not Specified'); ?></span></td>
                                                    </tr>
                                                    <tr class="opened">
                                                        <td class="day_label"><?php echo $this->lang->line('register_profile_for_drinking'); ?> :</td>
                                                        <td class="day_value closed"><span><?php echo (!empty($prefrenceData['drinking']) ? constant("DRINK_" . $prefrenceData['drinking']) : 'Not Specified'); ?></span></td>
                                                    </tr>
                                                    <tr class="opened">
                                                        <td class="day_label"><?php echo $this->lang->line('dhosam'); ?> :</td>
                                                        <td class="day_value closed"><span><?php echo (!empty($prefrenceData['dhosam']) ? constant("DHOSAM_TYPE_" . $prefrenceData['dhosam']) : 'Not Specified'); ?></span></td>
                                                    </tr>
                                                    <!--tr class="opened">
                                                            <td class="day_label">Religion :</td>
                                                            <td class="day_value closed"><span><?php echo (!empty($prefrenceData['religion']) ? $prefrenceData['religion'] : 'Not Specified'); ?></span></td>
                                                    </tr-->
                                                    <tr class="opened">
                                                        <td class="day_label"><?php echo $this->lang->line('register_profile_for_caste'); ?> :</td>
                                                        <td class="day_value closed"><span><?php echo (!empty($prefrenceData['caste_name']) ? $prefrenceData['caste_name'] : 'Not Specified'); ?></span></td>
                                                    </tr>
                                                    <!--tr class="opened">
                                                            <td class="day_label">Mother Tongue :</td>
                                                            <td class="day_value closed"><span><?php echo (!empty($prefrenceData['mother_tongue']) ? $prefrenceData['mother_tongue'] : 'Not Specified'); ?></span></td>
                                                    </tr-->
                                                    <tr class="opened">
                                                        <td class="day_label"><?php echo $this->lang->line('search_education_title'); ?> :</td>
                                                        <td class="day_value closed"><span><?php echo (!empty($prefrenceData['edu_name']) ? $prefrenceData['edu_name'] : 'Not Specified') . ' - ' . (!empty($prefrenceData['qualification']) ? ucfirst($prefrenceData['qualification']) : 'Not Specified'); ?></span></td>
                                                    </tr>
                                                    <tr class="opened">
                                                        <td class="day_label"><?php echo $this->lang->line('register_profile_for_occupation'); ?> :</td>
                                                        <td class="day_value closed"><span><?php echo (!empty($prefrenceData['occupation']) ? $prefrenceData['occupation'] : 'Not Specified'); ?></span></td>
                                                    </tr>
                                                    <tr class="opened">
                                                        <td class="day_label"><?php echo $this->lang->line('salary'); ?> :</td>
                                                        <td class="day_value closed"><span><?php echo (!empty($prefrenceData['salary_view']) ? $prefrenceData['salary_view'] : 'Not Specified'); ?></span></td>
                                                    </tr>
                                                    <tr class="opened">
                                                        <td class="day_label"><?php echo $this->lang->line('register_profile_for_country'); ?>:</td>
                                                        <td class="day_value closed"><span><?php echo (!empty($prefrenceData['country_name']) ? $prefrenceData['country_name'] : 'Not Specified'); ?></span></td>
                                                    </tr>
                                                    <tr class="opened">
                                                        <td class="day_label"><?php echo $this->lang->line('register_profile_for_state'); ?> :</td>
                                                        <td class="day_value closed"><span><?php echo (!empty($prefrenceData['state_name']) ? $prefrenceData['state_name'] : 'Not Specified'); ?></span></td>
                                                    </tr>
                                                    <?php
                                                } else {
                                                    echo $this->lang->line('no_inform_text');
                                                } //}  
                                                ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                </div><!--print end-->
                <?php
            }
        } else {
            echo $this->lang->line('no_data');
        }
        ?>

        <script>
            var baseurl = $("#baseurl").val();
            $(document).ready(function () {
                $(".tabs-menu a").click(function (event) {
                    event.preventDefault();
                    $(this).parent().addClass("current");
                    $(this).parent().siblings().removeClass("current");
                    var tab = $(this).attr("href");
                    $(".tab-content").not(tab).css("display", "none");
                    $(tab).fadeIn();
                });
                /*pdf*/
                var doc = new jsPDF();
                var specialElementHandlers = {
                    '#pdfhidden': function (element, renderer) {
                        return true;
                    }
                };
                $('#cmd').click(function () {
                    doc.fromHTML($('#mydiv').html(), 15, 15, {
                        'width': 170,
                        'elementHandlers': specialElementHandlers
                    });
                    doc.save('sample-file.pdf');
                    location.reload();
                });
                /*end*/

            });



            function PrintElem(elem)
            {
                Popup($(elem).html());
            }

            function Popup(data)
            {

                var mywindow = window.open('', 'my div', 'height=400,width=600');
                mywindow.document.write('<html><head><title></title>');
                mywindow.document.write('<link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/main.css" type="text/css" />');
                mywindow.document.write('</head><body >');
                mywindow.document.write(data);
                mywindow.document.write('</body></html>');

                mywindow.document.close(); // necessary for IE >= 10
                mywindow.focus(); // necessary for IE >= 10

                mywindow.print();
                mywindow.close();

                return true;
            }
            function deactive(userGuid, status) {
                if (userGuid == 0 || userGuid == '' || status == '') {
                    return false;
                }
                $.post(baseurl + 'index.php/adminuser/activeDeactiveuser?userGuid=' + userGuid + '&status=' + status,
                        function (data) {
                            alert(data.msg);
                            location.reload();
                        }, "json");
            }
            function deleteuser(userGuid, deleteid) {
                if (userGuid == 0 || userGuid == '' || deleteid == '') {
                    return false;
                }
                
                $.post(baseurl + 'index.php/adminuser/profiledeleteuser?userGuid=' + userGuid + '&delete=' + deleteid,
                        function (data) {
							if (confirm('Are you sure you want to save this thing into the database?')) {
                            location.reload(); }
						 else {
						// Do nothing!
}
                        }, "json");
            }
            
            

        </script>	
		

