<div id="page-wrapper">
    <div class="graphs bgimage">
        <content-top>
            <div class="content-top clearfix">
                <h1 class="al-title"><?php echo $this->lang->line('profile_approved'); ?> </h1>
                <ul class="breadcrumb al-breadcrumb">
                    <li><a href="<?php echo base_url() . 'index.php/admin/dashboard'; ?>"><?php echo $this->lang->line('dashboard'); ?></a></li>
                    <li class=""><?php echo $title; ?> </li>
                </ul>
            </div>
        </content-top>
        <?php $type = $_GET['type']; ?>
        <div class="col-md-12 contentinner" >
            <div class="form-group text-left">
                <button type="button" id="userguid" disabled="true" value="" onclick="active('<?php echo $status = 1; ?>')" class="btn btn-danger activity" ><?php echo $this->lang->line('active'); ?></button>
                <button type="button" id="userguid" disabled="true" value="" onclick="deactive('<?php echo $status = 0; ?>')" class="btn btn-primary reactive" ><?php echo $this->lang->line('deactive'); ?></button>
                <button type="button" id="userguid" disabled="true"  value="" onclick="deleteuser('<?php echo $deleteid = 1; ?>')"class="btn btn-success delete" ><?php echo $this->lang->line('delete'); ?></button>
            </div>
            <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th style="text-align:center;"><input value="0" id="selectall" type="checkbox"></th>
                        <th><?php echo $this->lang->line('profile_id'); ?></th>
                        <th><?php echo $this->lang->line('register_profile_for_name'); ?></th>
                        <th class="mob"><?php echo $this->lang->line('register_profile_for_gender'); ?></th>
                        <th class="mob"><?php echo $this->lang->line('register_profile_for_email'); ?></th>
                        <th class="mob"><?php echo $this->lang->line('register_profile_for_mobile'); ?></th>
                        <th><?php echo $this->lang->line('status_text'); ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if (!empty($userinfo)) {
                        foreach ($userinfo as $value) {
                            ?>
                            <tr>
                                <td><input  id="status" value="" onclick="requestdata('<?php echo $value['userGuid'];?>','<?php echo $value['status']; ?>')" class="checkbox user" type="checkbox"  ></td>
                                <td ><?php echo (!empty($value['userId']) ? constant('MEMBERID') . $value['userId'] : 'Not Specified'); ?></td>
                                <td><a href="<?php echo base_url() . 'index.php/admin_profile/viewUser?uid=' . $value['userGuid'] . '&type=' . $type; ?>"><?php echo (!empty($value['username']) ? $value['username'] : 'Not Specified'); ?></a></td>
                                <td class="mob"><?php echo (!empty($value['gender']) ? constant("GENDER_" . strtoupper($value['gender'])) : 'Not Specified'); ?></td>
                                <td class="mob"><?php echo (!empty($value['email']) ? $value['email'] : 'Not Specified'); ?></td>
                                <td class="mob"><?php echo (!empty($value['mobile']) ? $value['mobile'] : 'Not Specified'); ?></td>
                                <td>
                                    <?php
                                    $status = $value['status'];
                                    if ($status == 1) {
                                        echo 'Active';
                                    } else {
                                        echo "Deactive";
                                    }
                                    ?>	
                                </td>
                            </tr>
                            <?php
                        }
                    } else {
                        echo $this->lang->line('no_data');
                    }
                    ?>
                </tbody>
            </table>	
        </div>	

</div>	
        <script>
            var baseurl = $("#baseurl").val();
            $(document).ready(function () {

                $('#example').DataTable();

                $("#selectall").change(function () {
                    $(".checkbox").prop('checked', $(this).prop("checked"));
                    $('.delete').prop('disabled', $('input.checkbox:checked').length == 0);
                    $('.activity').prop('disabled', $('input.checkbox:checked').length == 0);
                    $('.reactive').prop('disabled', $('input.checkbox:checked').length == 0);
                });

                $(function () {
                    $('.checkbox').click(function () {
						var status=$('#status').val();
						if(status==1){
						$('.reactive').prop('disabled', $('input.checkbox:checked').length == 0);
						}else{
						$('.activity').prop('disabled', $('input.checkbox:checked').length == 0);
						}
                        $('.delete').prop('disabled', $('input.checkbox:checked').length == 0);   
                    });
                });

            });
            function requestdata(userGuid,status) {
                $("#userguid").val(userGuid);
                $("#status").val(status);
            }
            function active(status) {
                var userguid = $('#userguid').val();
                if (status == '') {
                    return false;
                }
                $.post(baseurl + 'index.php/adminuser/activeDeactiveuser?userGuid=' + userguid + '&status=' + status,
                        function (data) {
                            alert(data.msg);
                            location.reload();
                        }, "json");
            }
            function deactive(status) {
                var userguid = $('#userguid').val();
                if (status == '') {
                    return false;
                }
                $.post(baseurl + 'index.php/adminuser/activeDeactiveuser?userGuid=' + userguid + '&status=' + status,
                        function (data) {
                            alert(data.msg);
                            location.reload();
                        }, "json");
            }
            function deleteuser(deleteid) {
                var userguid = $('#userguid').val();
                if (deleteid == '') {
                    return false;
                }
                $.post(baseurl + 'index.php/adminuser/profiledeleteuser?userGuid=' + userguid + '&delete=' + deleteid,
                        function (data) {
                            alert(data.msg);
                            location.reload();
                        }, "json");
            }

        </script>		
		<style>

		</style>
