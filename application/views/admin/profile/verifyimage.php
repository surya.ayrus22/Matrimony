<div id="page-wrapper">
    <div class="graphs bgimage">
        <content-top>
            <div class="content-top clearfix">
                <h1 class="al-title"><?php echo $title; ?> </h1>
                <ul class="breadcrumb al-breadcrumb">
                    <li><a href="<?php echo base_url() . 'index.php/admin/dashboard'; ?>"><?php echo $this->lang->line('dashboard'); ?></a></li>
                    <li class=""><?php echo $title; ?> </li>
                </ul>
            </div>
        </content-top>
        <div class="col-md-12 contentinner" >

            <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                            <!--th style="text-align:center;"><input value="0" id="selectall" type="checkbox"></th-->
                        <th><?php echo $this->lang->line('profile_id'); ?></th>
                        <th><?php echo $this->lang->line('register_profile_for_name'); ?></th>
                        <th><?php echo $this->lang->line('register_profile_for_gender'); ?></th>
                        <th class="mob"><?php echo $this->lang->line('register_profile_for_email'); ?></th>
                        <th class="mob"><?php echo $this->lang->line('register_profile_for_mobile'); ?></th>
                        <th><?php echo $this->lang->line('status_text'); ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if (!empty($user)) {
                        foreach ($user as $value) {
                            ?>
                            <tr>
                                    <!--td><input value="<?php echo (!empty($value['userId']) ? $value['userId'] : 'Not Specified'); ?>" onclick="requestdata('<?php echo $value['userGuid']; ?>')" class="checkbox" type="checkbox"  ></td-->
                                <td >
                                    <?php echo (!empty($value['userId']) ? constant('MEMBERID') . $value['userId'] : 'Not Specified'); ?>
                                </td>
                                <td>
                                    <a href="<?php echo base_url() . 'index.php/admin_profile/image?uid=' . $value['userGuid'] . '&type=' . $type; ?>"><?php echo (!empty($value['username']) ? $value['username'] : 'Not Specified'); ?></a>
                                </td>
                                <td>
                                    <?php echo (!empty($value['gender']) ? constant("GENDER_" . strtoupper($value['gender'])) : 'Not Specified'); ?>
                                </td>
                                <td class="mob">
                                    <?php echo (!empty($value['email']) ? $value['email'] : 'Not Specified'); ?>
                                </td>
                                <td class="mob">
                                    <?php echo (!empty($value['mobile']) ? $value['mobile'] : 'Not Specified'); ?>
                                </td>
                                <td>
                                    <?php
                                    $status = $value['status'];
                                    if ($status == 1) {
                                        echo 'Active';
                                    } else {
                                        echo "Deactive";
                                    }
                                    ?>	
                                </td>
                            </tr>
                            <?php
                        }
                    } else {
                        echo $this->lang->line('no_data');
                    }
                    ?>
                </tbody>
            </table>
        </div>
        </div>	
        <script>
            var baseurl = $("#baseurl").val();
            $(document).ready(function () {

                $('#example').DataTable();
            });
        </script>

