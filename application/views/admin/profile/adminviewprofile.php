<script src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
<script src="<?php echo base_url().'assets/admin/js/register.js';?>"></script>

<script>
	
</script>
<div id="page-wrapper">
	<div class="graphs bgimage">
		<content-top>
            <div class="content-top clearfix">
              <h1 class="al-title">Admin Profile</h1>
              <ul class="breadcrumb al-breadcrumb">
                <li><a href="<?php echo base_url().'index.php/admin/dashboard';?>">Home</a></li>
                <li class="">Admin Profile</li>
              </ul>
            </div>
        </content-top>
		<div class="col-md-12 contentinner" >
			<h3>Settings</h3>
			<div class="col-md-offset-2 col-md-8 col-md-offset-2">	
				<?php //echo "<pre>"; print_r($updateAdmin);exit; 
				foreach( $updateAdmin as $value ){
				?>
				<form id="setting" method="post">
				  <div class="form-group row">
					<label for="username" class="col-sm-3 form-control-label">Change Username</label>
					<div class="col-sm-9">
					  <input type="text" class="form-control" id="username" name="username" placeholder="Username" value="<?php echo $value['username']; ?>" required >
					  <label for="username" class="error"></label>
					</div>
				  </div>
				  <div class="form-group row">
					<label for="mobile" class="col-sm-3 form-control-label">Change Mobile</label>
					<div class="col-sm-9">
					  <input type="text" class="form-control" id="mobile" name="mobile" placeholder="Mobile" value="<?php echo $value['mobile']; ?>" required >
					  <label class="error" for="mobile"></label>
					</div>
				  </div>
				  <div class="form-group row">
					<label for="email" class="col-sm-3 form-control-label" >Change Email</label>
					<div class="col-sm-9">
					  <input type="text" style="text-transform: lowercase;" class="form-control" id="email" name="email" value="<?php echo $value['email']; ?>" placeholder="Email Id" onblur="emailavailablility()" required pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" oninvalid="this.setCustomValidity('Please enter valid email format')" oninput="setCustomValidity('')">
					  <label for="email" class="error"></label>
					</div>					
				  </div>
				  <!--div class="form-group row">
					<label for="currentpassword" class="col-sm-3 form-control-label">Current Password</label>
					<div class="col-sm-9">
					  <input type="text" class="form-control" id="currentpassword" name="currentpassword" placeholder="Current Password" value="" required >
					  <label for="currentpassword" class="error"></label>
					</div>
				  </div-->
				  <div class="form-group row">
					<label for="password" class="col-sm-3 form-control-label">New Password</label>
					<div class="col-sm-9">
					  <input type="text" class="form-control" id="password" name="password" placeholder="New Password" value="" required>
					  <label for="password" class="error"></label>
					</div>
				  </div>
				  <div class="form-group row">
					<label for="conform-password" class="col-sm-3 form-control-label">Confirm Password</label>
					<div class="col-sm-9">
					  <input type="text" class="form-control" id="conform_password" name="conform_password" placeholder="Confirm Password" value="" required >
					  <label class="" for="conform-password"></label>
					</div>
				  </div>
					<input type="hidden" value="<?php echo $value['userGuid'];?>" id="userguid" name="userguid" id="userguid">

				  <div class="form-group row">
					<div class="col-sm-offset-3 col-sm-9">
					  <button type="submit" class="btn btn-primary" name="save">Save</button>
					</div>
				  </div>
				</form>
				
				<?php } ?>
			</div>	

			<div class="clearfix"></div>
			<input type="hidden" value="<?php echo base_url();?>" id="baseurl">
		
		</div>
		
<script>
$(document).ready(function() {
	
	var baseurl = $("#baseurl").val();
	var userguid = $("#userguid").val();
	
    $(".tabs-menu a").click(function(event) {
        event.preventDefault();
        $(this).parent().addClass("current");
        $(this).parent().siblings().removeClass("current");
        var tab = $(this).attr("href");
        $(".tab-content").not(tab).css("display", "none");
        $(tab).fadeIn();
    });
    
    $("#setting").validate({ 
		rules: {
			password: { minlength:6 },
			conform_password:{ equalTo: "#password" }
		},
		submitHandler: function() {
			$.post(baseurl+'index.php/admin_profile/adminupdateprofile?uid='+userguid,
			$("#setting").serialize(),
			function(data){
				if(data.msg == 'Success'){
					alert(data.msg);
					location.reload();
				} else {
					alert(data.msg);
					location.reload();
				}
			}, "json" )
			.fail(function() {
		    	  alert("<?php echo $this->lang->line('connection_error');?>");
		    	  location.reload();
		    });
		}
		
	});
	
  
      $("#mobile").keypress(function (e) {
	     //if the letter is not digit then display error and don't type anything
	     if (e.which != 8 && e.which != 0 && ((e.which < 48 )|| (e.which > 57))) {
	        //display error message
	       // $("#mobile").html('<label for="mobile" class="error">Numbers Only</label>').show().fadeOut("slow");
	               return false;
	    }
	  });
	
});
</script>	

		
