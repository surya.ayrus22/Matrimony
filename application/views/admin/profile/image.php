<div id="page-wrapper">
    <div class="graphs bgimage">
        <content-top>
            <div class="content-top clearfix">
                <h1 class="al-title">Images </h1>
                <ul class="breadcrumb al-breadcrumb">
                    <li><a href="<?php echo base_url() . 'index.php/admin/dashboard'; ?>"><?php echo $this->lang->line('dashboard'); ?></a></li>
                    <li><a href="<?php echo base_url() . 'index.php/admin_profile/verifyImage?type=' . constant('TYPE_14'); ?>"><?php echo $this->lang->line('verify_image'); ?></a> </li>
                    <li class="">Images </li>
                </ul>
            </div>
        </content-top>
        <div class="col-md-12 contentinner" >
            <h3> Verify Images </h3>
            <hr>
            <?php if (!empty($image)) { ?>
                <div class="">
                    <input value="0" id="selectall" type="checkbox">
                    <div  class="btn btn-success verfiy" onclick="verifyimagebulk()">Verify</div>
                    <div  class="btn btn-success delete" onclick="deleteimagebulk()">Delete</div>
                </div>
                <form id="form" method="post">
                    <?php foreach ($image as $val) { ?>
                        <div class="col-md-3 ">
                            <div class="imageactive_border">
                                <div class="page active">
                                    <span class="pull-left"><input class="checkbox" type="checkbox" name="id[]" value="<?php echo $val['image']; ?>"></span>
                                    <span class="pull-right pointer">	<a class="fa fa-check-circle-o iconsz1"  title="Click to add main photo" onclick="activeimage('<?php echo $val['user_guid']; ?>', '<?php echo $val['image']; ?>')"></a></span>
                                    <span class="pull-right pointer">	<a  class="fa fa-trash-o iconsz1" title="Delete" onclick="deleteimage('<?php echo $val['user_guid']; ?>', '<?php echo $val['image']; ?>')"></a></span>
                                </div>
                                <img alt="" class="imgactive" src="<?php echo base_url() . '/assets/upload_images/' . $val['image']; ?>">
                                <?php if ($val['active'] == 1) { ?>
                                    <div class="main subbtn"><?php echo $this->lang->line('main_photo'); ?></div>
                                <?php } ?>
                            </div>
                        </div>
                        <input type="hidden" name="pid" value="<?php echo $val['user_guid']; ?>">
                    <?php } ?>
                </form>
                <?php
            } else {
                echo $this->lang->line('no_data');
            }
            ?>		
        </div>

        <script>
            var baseurl = $('#baseurl').val();
            $(document).ready(function () {
                $("#selectall").change(function () {
                    $(".checkbox").prop('checked', $(this).prop("checked"));
                });

            });

            function deleteimage(pid, image) {
                if (pid == 0 || pid == '' || image == '') {
                    return false;
                }
                $.post(baseurl + 'index.php/admin/removeprofileimage?pid=' + pid + '&id=' + image,
                        function (data) {
                            alert(data.msg);
                            location.reload();
                        }, "json");
            }
            function activeimage(pid, image) {
                if (pid == 0 || pid == '' || image == '') {
                    return false;
                }

                $.post(baseurl + 'index.php/admin/activeprofileimage?pid=' + pid + '&id=' + image,
                        function (data) {
                            alert(data.msg);
                            location.reload();
                        }, "json");
            }

            function verifyimagebulk( ) {
                var dataString = $('#form').serialize();
                if (dataString == '' || dataString == null || dataString == 0) {
                    alert('Please select options');
                    return false;
                }

                $.ajax({
                    type: 'POST',
                    url: baseurl + 'index.php/admin/verifyprofileimage',
                    data: dataString,
                    dataType: "json",
                    success: function (data) {
                        alert(data.msg);
                        location.reload();
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        alert('<?php echo $this->lang->line("error_occur_contact_admin"); ?>');
                        location.reload();
                    }
                });
                return false;
            }
            function deleteimagebulk( ) {
                var dataString = $('#form').serialize();
                if (dataString == '' || dataString == null || dataString == 0) {
                    alert('Please select options');
                    return false;
                }

                $.ajax({
                    type: 'POST',
                    url: baseurl + 'index.php/admin/removeprofileimage',
                    data: dataString,
                    dataType: "json",
                    success: function (data) {
                        alert(data.msg);
                        location.reload();
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        alert('<?php echo $this->lang->line("error_occur_contact_admin"); ?>');
                        location.reload();
                    }
                });
                return false;
            }
        </script>
