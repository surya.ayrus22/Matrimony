<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/css/select2.min.css">
<!-- Latest compiled and minified JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/js/select2.min.js"></script>
<script>
    $(document).ready(function () {
        $(".js-example-basic-single").select2();

        $("#country").select2({
            data: <?php echo (!empty($country) ? $country : null); ?>
        })

        $("#state").select2({
            data: <?php echo (!empty($state) ? $state : null); ?>
        })
        $("#city").select2({
            data: <?php echo (!empty($city) ? $city : null); ?>
        })
    });
</script>
<div id="page-wrapper">
    <div class="graphs bgimage" style="min-height:600px;">
        <content-top>
            <div class="content-top clearfix">
                <h1 class="al-title"><?php echo $this->lang->line('success_story_title') . ' ' . $this->lang->line('edit_text'); ?></h1>
                <?php $mid = $_GET['mid']; ?>
                <ul class="breadcrumb al-breadcrumb">
                    <li><a href="<?php echo base_url() . 'index.php/admin/dashboard'; ?>"><?php echo $this->lang->line('dashboard'); ?></a></li>
                    <li ><a href="<?php echo base_url() . 'index.php/admin/successdetails?type=' . $type; ?>"><?php echo $this->lang->line('success_story_title'); ?></a></li>
                    <li ><a href="<?php echo base_url() . 'index.php/admin/sucessverify?mid=' . $mid . '&type=' . $type; ?>"><?php echo $this->lang->line('profile_verfication_text'); ?></a></li>
                    <li class=""><?php echo $this->lang->line('success_story_title') . ' ' . $this->lang->line('edit_text'); ?></li>
                </ul>
            </div>
        </content-top>	     
        <div class="col-md-9 box-content">
            <!--col-md-4-->
            <?php if (!empty($successinfo)) { ?>
                <form action="<?php echo base_url() . 'index.php/success/updatesuccess?mid=' . $mid . '&type=' . $type; ?>"  method="POST" id="successpost" enctype="multipart/form-data">	
                    <div class="col-md-6 text-left margintop"><h3></h3></div>
                    <div class="col-md-10 text-left">	
                        <table class="table">
                            <tbody>
                                <tr class="opened">
                                    <td class="day_label"><?php echo $this->lang->line('bride_name'); ?> (<?php echo constant('GENDER_F'); ?>) :</td>
                                    <td class="day_value"><input type="text" class="form-control" name="groomname" placeholder="Groom Name (Male)"   readonly  value="<?php echo (!empty($successinfo['bride_name']) ? $successinfo['bride_name'] : 'Not Specified'); ?>"></td>
                                </tr>
                                <tr class="opened">
                                    <td class="day_label"><?php echo $this->lang->line('groom_name'); ?> (<?php echo constant('GENDER_M'); ?>)   :</td>
                                    <td class="day_value"><input type="text" class="form-control" name="bridename" placeholder="Bride Name (Female)"  readonly value="<?php echo (!empty($successinfo['groom_name']) ? $successinfo['groom_name'] : 'Not Specified'); ?>"></td>
                                </tr>
                                <tr class="opened">
                                    <td class="day_label"> <?php echo $this->lang->line('your_text') . ' ' . $this->lang->line('memeber_id'); ?> :</td>
                                    <td class="day_value">
                                        <input type="text" id="member_id" class="form-control" name="member_id"  onchange="memberValidation(this.value, 'memberid')" placeholder="Member Id (Male)"  readonly value="<?php echo (!empty($successinfo['member_id']) ? $successinfo['member_id'] : 'Not Specified'); ?>"><p><?php echo $this->lang->line('ex_text') . ':' . $this->lang->line('member_msg_text'); ?></p>
                                        <label for="member_id" class="error" id="err_member"></label>
                                    </td>
                                </tr>
                                <tr class="opened">
                                    <td class="day_label"><?php echo $this->lang->line('your_text') . ' ' . $this->lang->line('partner_id'); ?> :</td>
                                    <td class="day_value">
                                        <input type="text" class="form-control" name="partnerid" id="partnerid" onchange="memberValidation(this.value, 'partnerid')" placeholder="Member Id (Female)"  readonly value="<?php echo (!empty($successinfo['partner_id']) ? $successinfo['partner_id'] : 'Not Specified'); ?>"><p><?php echo $this->lang->line('ex_text') . ':' . $this->lang->line('partner_msg_text'); ?></p>							 
                                        <label for="partnerid" class="error" id="err_partner"></label>
                                    </td>
                                </tr>
                                <tr class="opened">
                                    <td class="day_label"><?php echo $this->lang->line('register_profile_for_email'); ?> :</td>
                                    <td class="day_value"><input type="email" style="text-transform: lowercase;" name="email" class="form-control" placeholder="Email" value="<?php echo (!empty($successinfo['email']) ? $successinfo['email'] : 'Not Specified'); ?>" required pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" oninvalid="this.setCustomValidity('Please enter valid email format')" oninput="setCustomValidity('')"></td>
                                </tr>

                                <tr class="opened">
                                    <td class="day_label"><?php echo $this->lang->line('marriage_date'); ?> :</td>
                                    <td class="day_value">	
                                        <?php
                                        $date = strtotime($successinfo['marriage_date']);
                                        $year = date('Y', $date);
                                        $month = date('M', $date);
                                        $monthval = date('m', $date);
                                        $day = date('d', $date);
// 											 echo '<pre>';print_r($year);exit;
                                        ?>									
                                        <div class="col-md-4 dob">
                                            <select id="dobday" name="date" class="form-control input-sm"  selected="selected" required>
                                                <option value="<?php echo $day ?>"><?php echo $day ?></option>
                                            </select>
                                        </div>
                                        <div class="col-md-4 dob">
                                            <select id="dobmonth" name="month" class="form-control input-sm" required>
                                                <option value="<?php echo $monthval ?>"><?php echo $month ?></option>
                                            </select>
                                        </div>
                                        <div class="col-md-4 dob">
                                            <select id="dobyear" name="year" class="form-control input-sm" required>
                                                <option value="<?php echo $year ?>"><?php echo $year ?></option>
                                            </select>
                                        </div>
                                    </td>
                                </tr>
                                <tr class="opened">
                                    <td class="day_label"><?php echo $this->lang->line('upload_text'); ?> :</td>
                                    <td class="day_value">
                                        <div class="col-sm-4">
                                            <span style="float:left;"><img width="50" height="50" alt="" src="<?php echo base_url() . 'assets/success_images/' . $successinfo['photo']; ?>" id="blah"></span>
                                        </div>
                                        <div class="col-sm-4" id="image">
                                            <input type="file" name="upfile" id="photo" onchange="readURL(this);" accept="image/*" >
                                        </div>
                                    </td>
                                </tr>

                                <tr class="opened">
                                    <td class="day_label"><?php echo $this->lang->line('address_text'); ?> :</td>
                                    <td class="day_value"><textarea rows="3" name="address" class="form-control"><?php echo $successinfo['address']; ?></textarea></td>
                                </tr>
                                <tr class="closed">
                                    <td class="day_label"><?php echo $this->lang->line('register_profile_for_country'); ?> :</td>
                                    <td class="day_value closed">
                                        <select class="js-example-data-array form-control" id="country" name="country" onchange="stateInfo(this.value)" required>
                                            <option value=""> -- Select Country -- </option>
                                            <option value="<?php echo (!empty($successinfo['country']) ? $successinfo['country'] : ''); ?>" selected="selected"> <?php echo (!empty($successinfo['country_name']) ? $successinfo['country_name'] : ''); ?> </option>
                                        </select>
                                    </td>
                                </tr>
                                <tr class="opened">
                                    <td class="day_label"><?php echo $this->lang->line('register_profile_for_state'); ?>  :</td>
                                    <td class="day_value closed">      
                                        <select class="form-control js-example-data-array"  name="state" id="state" onchange="cityInfo(this.value)">
                                            <option value=""> -- Select State -- </option>
                                            <option value="<?php echo (!empty($successinfo['state']) ? $successinfo['state'] : ''); ?>" selected="selected"> <?php echo (!empty($successinfo['state_name']) ? $successinfo['state_name'] : ''); ?> </option>
                                        </select>
                                    </td>
                                </tr>
                                <tr class="closed">
                                    <td class="day_label"><?php echo $this->lang->line('register_profile_for_city'); ?> :</td>
                                    <td class="day_value closed">   
                                        <select class="form-control js-example-data-array"  id ="city" name="city">
                                            <option value=""> -- Select City -- </option>
                                            <option value="<?php echo (!empty($successinfo['city']) ? $successinfo['city'] : ''); ?>" selected="selected"> <?php echo (!empty($successinfo['city_name']) ? $successinfo['city_name'] : ''); ?> </option>
                                        </select>
                                    </td>
                                </tr>

                                <tr class="opened">
                                    <td class="day_label"><?php echo $this->lang->line('register_profile_for_mobile'); ?> :</td>
                                    <td class="day_value"><input type="number"  name="mobile" class="form-control" placeholder="Mobile Number" value="<?php echo (!empty($successinfo['mobile']) ? $successinfo['mobile'] : 'Not Specified'); ?>" required></td>
                                </tr>
                                <tr class="opened">
                                    <td class="day_label"><?php echo $this->lang->line('success_story_title'); ?> :</td>
                                    <td class="day_value"><textarea rows="3" name="successstory" class="form-control" required><?php echo (!empty($successinfo['success_story']) ? $successinfo['success_story'] : 'Not Specified'); ?></textarea></td>
                                </tr>
                            </tbody>
                        </table>
                        <div class=" text-right">
                            <button style="float:right;"type="submit"  class="btn btn-primary"><?php echo $this->lang->line('register_profile_for_submit'); ?></button>
                        </div>		
                    </div>					
                </form>
                <?php
            } else {
                echo 'please verified and update or details not fount';
            }
            ?>
        </div><!--row end-->			  
        <div class="clearfix"> </div>
    </div>

    <script src="<?php echo base_url() ?>assets/js/jquery-validate/jquery.validate.min.js"></script>
    <script>
                                            $(document).ready(function () {
                                                $("#successpost").validate({
                                                    rules: {
                                                        phone: {
                                                            matches: "/\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/",
                                                            maxlength: 6,
                                                            maxlength: 15
                                                        },
                                                        member_id: {
                                                            number: true,
                                                            maxlength: 10
                                                        },
                                                        partnerid: {
                                                            number: true,
                                                            maxlength: 10
                                                        },
                                                    }
                                                });
                                                $.dobPicker({
                                                    daySelector: '#dobday', /* Required */
                                                    monthSelector: '#dobmonth', /* Required */
                                                    yearSelector: '#dobyear', /* Required */
                                                    dayDefault: 'DD', /* Optional */
                                                    monthDefault: 'MM', /* Optional */
                                                    yearDefault: 'YY', /* Optional */
                                                    minimumAge: -1, /* Optional */
                                                    maximumAge: 6 /* Optional */
                                                });
                                                //called when key is pressed in textbox
                                                $("#member_id").keypress(function (e) {
                                                    //alert('123');
                                                    //if the letter is not digit then display error and don't type anything
                                                    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                                                        //display error message
                                                        $("#err_member").html("Numbers Only").show().fadeOut("slow");
                                                        return false;
                                                    }
                                                });
                                                //called when key is pressed in textbox
                                                $("#partnerid").keypress(function (e) {
                                                    //if the letter is not digit then display error and don't type anything
                                                    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                                                        //display error message
                                                        $("#err_partner").html("Numbers Only").show().fadeOut("slow");
                                                        return false;
                                                    }
                                                });
                                            });
                                            function memberValidation(id, name) {
                                                if ((id == '' || id == null) && (name == '' || name == null)) {
                                                    return false
                                                }
                                                var baseurl = $('#baseurl').val();
                                                $('#loader_' + name).show();
                                                $.ajax({
                                                    type: "post",
                                                    dataType: "json",
                                                    url: baseurl + 'index.php/success/validate?' + name + '=' + id,
                                                    success: function (request) {
                                                        if (request == 'success') {
                                                            $('#loader_' + name).hide();
                                                            //do next
                                                        } else {
                                                            $('#loader_' + name).hide();
                                                            alert(request);
                                                            $('#member_id').val('');
                                                            $('#partnerid').val('');
                                                        }
                                                    },
                                                    error: function (data) {
                                                        alert(data);
                                                    },
                                                });

                                                return false;
                                            }

                                            function stateInfo(reqData) {
                                                if (reqData == 0) {
                                                    return false
                                                }
                                                $("#city").html('');
                                                $("#state").html('');
                                                var baseurl = $('#baseurl').val();//alert(baseurl);
                                                $.ajax({
                                                    type: "post",
                                                    dataType: "json",
                                                    url: baseurl + 'index.php/common/statelist/' + reqData,
                                                    success: function (request) {
                                                        if (request.status == 0) {
                                                            $("#text-state").html('<input type="text" class="form-control" name="state_text" id="state" placeholder=" Enthe the state">');
                                                            $("#text-city").html('<input type="text" class="form-control" name="city_text" id="city" placeholder=" Enthe the city">');
                                                        } else {
                                                            $("#state").select2({
                                                                data: request
                                                            });
                                                        }
                                                    },
                                                    error: function (data) {

                                                    },
                                                });

                                                return false;
                                            }

                                            function cityInfo(reqData) {
                                                if (reqData == 0) {
                                                    return false
                                                }
                                                $("#city").html('');
                                                var baseurl = $('#baseurl').val();
                                                $.ajax({
                                                    type: "post",
                                                    dataType: "json",
                                                    url: baseurl + 'index.php/common/citylist/' + reqData,
                                                    success: function (request) {
                                                        if (request.status == 0) {
                                                            $("#text-city").html('<input type="text" class="form-control" name="city_text" id="city" placeholder=" Enthe the city">');
                                                        } else {
                                                            $("#city").select2({
                                                                data: request
                                                            });
                                                        }
                                                    },
                                                    error: function (data) {

                                                    },
                                                });

                                                return false;
                                            }

                                            function readURL(input) {
                                                var ext = $('#photo').val().split('.').pop().toLowerCase();
                                                if ($.inArray(ext, ['gif', 'png', 'jpg', 'jpeg']) == -1) {
                                                    $('#photo').val('');
                                                    $('#blah').attr('src', '');
// 		$("#photo" ).attr( "current-error", "Invalid File Format.Allows Only Image File!" );
// 		$('#photo').removeClass('valid').addClass('error');
// 		$("#image").removeClass('has-success').addClass('has-error');
// 		$('#photo').html('<p class="msg error-msg">Invalid File Format.Allows Only Image File</p>');
// 		$('.msg .error-msg').show();
                                                    alert('Invalid File Format.Allows Only Image File ');
                                                    return false;
                                                }
                                                if (input.files && input.files[0]) {
                                                    var reader = new FileReader();
                                                    reader.onload = function (e) {
                                                        $('#blah')
                                                                .attr('src', e.target.result)
                                                                .width(100)
                                                                .height(100);
                                                    }
                                                    reader.readAsDataURL(input.files[0]);
                                                }
                                            }
    </script>
