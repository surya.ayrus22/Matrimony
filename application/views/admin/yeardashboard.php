<!--bar chart-->
<script src="<?php echo base_url(); ?>assets/admin/js/barchart/canvasjs.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/js/barchart/loader.js"></script>
<!--pie chart-->
<script type="text/javascript" src="<?php echo base_url(); ?>/assets/admin/js/piechart/Chart.js"></script>	

<script type="text/javascript">
    /*bar chart */

    google.charts.load('current', {'packages': ['bar']});
    google.charts.setOnLoadCallback(drawChart);
    function drawChart() {
        var data = google.visualization.arrayToDataTable([
            ['month', 'user', 'free', 'Payment'],
            ['2015', 1170, 460, 250],
            ['2016', 660, 1120, 300],
            ['2017', 1030, 540, 350]
        ]);

        var options = {
        };

        var chart = new google.charts.Bar(document.getElementById('columnchart'));

        chart.draw(data, options);
    }
    /*pie chart */
    var yeardata = [{
            value: 200,
            color: "#F7464A",
            highlight: "#00abff",
            label: "Red"
        }, {
            value: 200,
            color: "#FDB45C",
            highlight: "#8bd22f",
            label: "Yellow"
        }];
    var yearsuccess = [{
            value: 20,
            color: "#F7464A",
            highlight: "#00abff",
            label: "Red"
        }, {
            value: 90,
            color: "#FDB45C",
            highlight: "#8bd22f",
            label: "Yellow"
        }];
    var yearmale = [{
            value: 50,
            color: "#F7464A",
            highlight: "#00abff",
            label: "Red"
        }, {
            value: 10,
            color: "#FDB45C",
            highlight: "#8bd22f",
            label: "Yellow"
        }];
    var yearfemale = [{
            value: 90,
            color: "#F7464A",
            highlight: "#00abff",
            label: "Red"
        }, {
            value: 90,
            color: "#FDB45C",
            highlight: "#8bd22f",
            label: "Yellow"
        }];
    $(document).ready(function () {
        var ctx = document.getElementById("myChart").getContext("2d");
        var myChart = new Chart(ctx).Pie(yeardata);
        $("#myChart ").click(
                function (evt) {
                    var activePoints = myChart.getSegmentsAtEvent(evt);
                    var activeLabel = activePoints[0].label;

                    if (activeLabel == "Red") {
                        while (myChart.segments.length > 0) {
                            myChart.removeData()
                        }
                        for (i = 0; i < data2.length; i++) {
                            myChart.addData(data2[i])
                        }
                    }
                });

        var ctxsucess = document.getElementById("yearsuccess").getContext("2d");
        var ysucess = new Chart(ctxsucess).Pie(yearsuccess);
        $("#yearsuccess ").click(
                function (evt) {
                    var activePoints = ysucess.getSegmentsAtEvent(evt);
                    var activeLabel = activePoints[0].label;

                    if (activeLabel == "Red") {
                        while (ysucess.segments.length > 0) {
                            ysucess.removeData()
                        }
                        for (i = 0; i < data2.length; i++) {
                            ysucess.addData(data2[i])
                        }
                    }
                });
        var ctxmale = document.getElementById("yearmale").getContext("2d");
        var ymale = new Chart(ctxmale).Pie(yearmale);
        $("#yearmale ").click(
                function (evt) {
                    var activePoints = ymale.getSegmentsAtEvent(evt);
                    var activeLabel = activePoints[0].label;

                    if (activeLabel == "Red") {
                        while (ymale.segments.length > 0) {
                            ymale.removeData()
                        }
                        for (i = 0; i < data2.length; i++) {
                            ymale.addData(data2[i])
                        }
                    }

                });
        var ctxfemale = document.getElementById("yearfemale").getContext("2d");
        var yfemale = new Chart(ctxfemale).Pie(yearfemale);
        $("#yearfemale ").click(
                function (evt) {
                    var activePoints = yfemale.getSegmentsAtEvent(evt);
                    var activeLabel = activePoints[0].label;

                    if (activeLabel == "Red") {
                        while (yfemale.segments.length > 0) {
                            yfemale.removeData()
                        }
                        for (i = 0; i < data2.length; i++) {
                            yfemale.addData(data2[i])
                        }
                    }

                });
    });
</script>


<script>


</script>

<div id="page-wrapper">
    <div class="graphs bgimage">
        <content-top>
            <div class="content-top clearfix">
                <h1 class="al-title">Dashboard</h1>
                <ul class="breadcrumb al-breadcrumb">
                    <li><a href="">Home</a></li>
                    <li class="">Dashboard</li>
                </ul>
            </div>
        </content-top> 
        <div id="tabs-container">
            <ul class="tabs-menu printopt" id="pdfhidden">
                <li class="printopt"><a href="<?php echo base_url(); ?>index.php/admin/dashboard">Month</a></li>
                <li  class="current printopt"><a href="<?php echo base_url(); ?>index.php/admin/yeardashboard">year</a></li>
            </ul>
            <div class="tab">
                <div class="col_3">
                    <div class="col-md-6 widget widget1">
                        <div class="r3_counter_box">

                            <canvas id="myChart" class="pie-title-center pull-left" width="400" height="400"></canvas>
                            <a href="<?php echo base_url() . 'index.php/admin/userdetails?type=1'; ?>">
                                <div class="stats">
                                    <div class="pull-left"><h5><strong>80</strong></h5>
                                        <span>Users</span></div>
                                    <span class="pull-right"><i class="pull-left fa fa-user icon-rounded"></i></span>
                                </div>
                        </div>
                        </a>
                    </div>
                    <div class="col-md-6 widget widget1">
                        <div class="r3_counter_box">
                            <canvas id="yearsuccess" class="pie-title-center pull-left" width="400" height="400"></canvas>
                            <a href="<?php echo base_url(); ?>index.php/admin/successdetails">
                                <div class="stats">
                                    <div class="pull-left"><h5><strong>90</strong></h5>
                                        <span>Success</span></div>
                                    <span class="pull-right"><i class="pull-left fa fa-users user1 icon-rounded"></i></span>
                                </div>
                        </div>
                        </a>
                    </div>
                    <div class="col-md-6 widget widget1">
                        <div class="r3_counter_box">
                            <canvas id="yearmale" class="pie-title-center pull-left" width="400" height="400"></canvas>
                            <a href="<?php echo base_url() . 'index.php/admin/userdetails?type=2'; ?>">
                                <div class="stats">
                                    <div class="pull-left"><h5><strong>90</strong></h5>
                                        <span>Male</span></div>
                                    <span class="pull-right"><i class="pull-left fa fa-male icon-rounded"></i></span>
                                </div>
                        </div>
                        </a>
                    </div>
                    <div class="col-md-6 widget">
                        <div class="r3_counter_box">
                            <canvas id="yearfemale" class="pie-title-center pull-left" width="400" height="400"></canvas>
                            <a href="<?php echo base_url() . 'index.php/admin/userdetails?type=3'; ?>">
                                <div class="stats  ">
                                    <div class="pull-left"><h5><strong>90</strong></h5>
                                        <span>Female</span></div>
                                    <span class="pull-right"><i class="pull-left fa fa-female dollar1 icon-rounded"></i></span>
                                </div>
                        </div>
                        </a>
                    </div>
                    <div class="clearfix"> </div>
                </div>

                <div class="span_11">
                    <div class="col-md-6 col_4">
                        <div class="map_container r3_counter_box">
                            <div id="columnchart" class="barchart" ></div>
                        </div> 
                    </div>
                    <div class="col-md-6 col_5">
                        <div class="chart_container r3_counter_box">
                            <div id="pie" class="example" style="width: 100px;"></div>		  
                        </div>
                    </div>
                    <div class="clearfix"> </div>
                </div>

                <div class="content_bottom">
                    <div class="col-md-6 col_5">
                        <div class=" r3_counter_box">
                            <ba-card bacardclass="large-card with-scroll feed-panel" title="view profile"><div bacardblur="" zoom-in="" class="animated fadeIn card  large-card with-scroll feed-panel">
                                    <div class="card-header clearfix">  <h3 class="card-title">Recent Update</h3> </div>
                                    <div class="card-body">
                                        <feed>
                                            <?php
                                            //echo "<pre>";print_r($userinfo);
                                            //foreach($userinfo as $value) { 
                                            ?>
                                            <div class="feed-messages-container">
                                                <div class="feed-message">
                                                    <div class="message-icon">
                                                        <img class="photo-icon" src="<?php echo base_url() . 'assets/upload_images/1.png'; ?>">
                                                    </div>
                                                    <div class="text-block text-message">
                                                        <div class="message-header textcolor">
                                                            <span class="author">sample</span>
                                                            <span>(M1)</span>
                                                        </div>
                                                        <div class="message-content line-clamp line-clamp-2 textcolor">
                                                            <span class="">Gender:malee</span><span class="textleft">|</span>
                                                            <span class="textleft">Profile_created:Son</span>
                                                        </div>

                                                        <div class="message-time textcolor">
                                                            <div class="post-time">12.09.18</div>
                                                            <div class="ago-time">
                                                                <a href="#">view Profile</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>			
                                            </div>
                                            <?php //}   ?>
                                        </feed>
                                    </div>
                                </div>
                            </ba-card>
                        </div>
                    </div>
                    <div class="col-md-6 col_5">
                        <div class=" r3_counter_box">
                            <ba-card bacardclass="large-card with-scroll feed-panel" title="view profile"><div bacardblur="" zoom-in="" class="animated fadeIn card  large-card with-scroll feed-panel">
                                    <div class="card-header clearfix">  <h3 class="card-title">Recent Premium User</h3> </div>
                                    <div class="card-body">
                                        <feed>
                                            <?php //foreach($userpayment as $val) {    ?><?php //echo "<pre>";print_r($val);  ?>
                                            <div class="feed-messages-container">
                                                <div class="feed-message">
                                                    <div class="message-icon">
                                                        <img class="photo-icon" src="<?php echo base_url() . 'assets/upload_images/1.jpeg'; ?>">
                                                    </div>
                                                    <div class="text-block text-message">
                                                        <div class="message-header textcolor">
                                                            <span class="author">dddd<?php // echo $val['username'];    ?>(m1<?php //echo $val['userId'];    ?>)</span>
                                                        </div>
                                                        <div class="message-content line-clamp line-clamp-2 textcolor">
                                                            <span class="">Email:sdcd@hg.com<?php // echo $val['email'];    ?></span><span class="textleft"></span>
                                                            <!--<span class="textleft">Month:<?php // echo $val['month'];    ?></span>-->
                                                        </div>
                                                        <div class="message-time textcolor">
                                                            <div class="post-time">Gender:dd<?php // echo $val['gender'];    ?></div>
                                                            <div class="ago-time">
                                                                <a class="textcolor" href="#">view profile</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>			
                                            </div>
                                            <?php // }  ?>
                                        </feed>
                                    </div>
                                </div>
                            </ba-card>
                        </div>
                    </div>
                    <div class="clearfix"> </div>
                </div>
            </div><!--tab -->
        </div><!--tab cont-->

        <script>
            $(document).ready(function () {


            });

        </script>
        <style>
            .copy {
                background: transparent none repeat scroll 0 0 !important;
            }
            .tab {
                background-color: transparent !important;
            }
        </style>
