<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
    <head>
        <title>Admin | Login</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="keywords" content="Modern Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template, 
              Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
        <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
        <!-- Bootstrap Core CSS -->
        <link href="<?php echo base_url() . 'assets/admin/css/bootstrap.min.css'; ?>" rel='stylesheet' type='text/css' />
        <!-- Custom CSS -->
        <link href="<?php echo base_url() . 'assets/admin/css/style.css'; ?>" rel='stylesheet' type='text/css' />
        <link href="<?php echo base_url() . 'assets/admin/css/your.css'; ?>" rel='stylesheet' type='text/css' />

        <link href="<?php echo base_url() . 'assets/admin/css/font-awesome.css'; ?>" rel="stylesheet"> 
        <!-- jQuery -->
        <script src="<?php echo base_url() . 'assets/admin/js/jquery.min.js'; ?>"></script>
        <script src="<?php echo base_url() . 'assets/admin/js/jquery.form-validator.js'; ?>"></script>

        <!----webfonts--->
        <link href='http://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900' rel='stylesheet' type='text/css'>
        <!---//webfonts--->  
        <!-- Bootstrap Core JavaScript -->
        <script src="<?php echo base_url() . 'assets/admin/js/bootstrap.min.js'; ?>"></script>
        <style>
            html, body {
                /* overflow-x: hidden !important;  */
            }
            html {
                background: rgba(0, 0, 0, 0) url("../../assets/admin/images/bg.jpg") no-repeat scroll 0 0 / 100% 100% !important;
                /*overflow-x: hidden !important; */
                min-height: 0 !important;
            }
            body#login {
                min-height: 0 !important;
                height:100% important;
            }

            .login {
                margin: 1em 0 0 !important;
            }
            ul.new {
                margin: 0 0 0 !important;
            }
            .text {
				color: #ffffff !important;
			}
        </style>
    </head>
    <body id="login">
        <div class="login-logo">
            <a href="index.html"><img src="images/logo.png" alt=""/></a>
        </div>
        <h2 class="form-heading"><?php echo $this->lang->line('login'); ?> </h2>
        <h5 class="text-center colormesg"><?php echo $message; ?></h5>
        <div class="app-cam">
            <form action="<?php echo base_url() . 'index.php/admin'; ?>" method="post">
                <div class="form-group">
                    <input type="text" name="username" class="text" value="" placeholder="<?php echo $this->lang->line('register_profile_for_email'); ?>" data-validation="required"  data-validation-pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" data-validation-error-msg="<?php echo $this->lang->line('text_please_enter_email'); ?>" class="colormesg">			
                </div>
                <div class="form-group">
                    <input type="password"  name="password" value="" placeholder="<?php echo $this->lang->line('register_profile_for_password'); ?>" data-validation="required" data-validation-error-msg="<?php echo $this->lang->line('text_please_enter_pwd'); ?>" class="colormesg text">
                </div>

                <div class="submit"><input type="submit" value="<?php echo $this->lang->line('login'); ?>"></div>
                <div class="login-social-link">
                </div>
                <ul class="new">
                        <!--li class="new_left"><p><a href="#">Forgot Password ?</a></p></li-->
                        <!--li class="new_right"><p class="sign">New here ?<a href=""> Sign Up</a></p></li-->
                    <div class="clearfix"></div>
                </ul>
            </form>
            <div class="copy_layout login">
                <p><?php echo $this->lang->line('copyright_admin'); ?> <a href="http://www.kevellcorp.com/" target="<?php echo $this->lang->line('kevellcorp'); ?>"><?php echo $this->lang->line('kevellcorp'); ?></a> </p>
            </div>
        </div>
        <script>
            // Setup form validation
            $.validate({
                onError: function () {
                    $(":input.error:first").focus();
                    return false;
                },
            });
        </script>
    </body>
</html>
