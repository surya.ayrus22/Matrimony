<link src="<?php echo base_url();?>assets/admin/css/dataTables.bootstrap.min.css" rel="stylesheet" />
<script src="<?php echo base_url();?>assets/admin/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url();?>assets/admin/js/dataTables.bootstrap.min.js"></script> 
<script>
    $(document).ready(function () {
		
		$('#example').DataTable( {
			"lengthMenu": [[12, 24, 48, -1], [12, 24, 48, "All"]]
		});
		
        $("#selectall").change(function () {
            $(".case").prop('checked', $(this).prop("checked"));
        });
    });

</script>
<style>
.dataTables_length, .dataTables_filter, .dataTables_info, .dataTables_empty {
    color:#fff !important;
}

</style>
<div id="page-wrapper">
    <div class="graphs bgimage">
        <content-top>
            <div class="content-top clearfix">
                <h1 class="al-title"> <?php echo (!empty($title) ? $title : ''); ?> </h1>              

                <ul class="breadcrumb al-breadcrumb">
                    <li><a href="<?php echo base_url(); ?>index.php/admin/dashboard"><?php echo $this->lang->line('dashboard'); ?></a></li>
                    <li class=""><?php echo (!empty($title) ? $title : ''); ?></li>
                </ul>
            </div>
        </content-top> 
        <?php if (!empty($user)) { ?>
        <table id="example" class="" cellspacing="0" width="100%">
			<thead>
				<th></th>
			</thead>
			<tbody> 
			<?php   foreach ($user as $value) { //echo "<pre>";print_r($value); exit;?>
			<tr class="col-md-4">
				<td class="col-md-4">
					<!--col-md-4-->
                    <div class=" inbox_right space">      	
						<div class="mailbox-content profilecontent">
							<div class="thumb profile_img">
								<a  href="#"> <img alt="" class="img-responsive" src="<?php echo base_url() . 'assets/upload_images/' . $value['image'] ?>"></a>
                            </div>
                            <div class="innercontent" title="<?php echo $value['username'] . ' ( ' . $value['userId'] . ' )'; ?>">
                            <!--<span><input value=""  class="case" type="checkbox" name=""></span>-->
								<span class="email-title "><?php echo $value['username']; ?>(<?php echo constant('MEMBERID') . $value['userId']; ?>)</span>
                            </div>
                            <table class="half">
								<tbody>
									<tr class="opened_1">
										<td class="short"><?php echo $this->lang->line('search_profile_for_age'); ?>  </td>
                                        <td class="short">:</td>
                                        <td class="short"><?php echo (!empty($value['age']) ? $value['age'] : 'Not Specified'); ?> Yrs </td>
                                    </tr>
                                    <tr class="opened">
										<td class="short"><?php echo $this->lang->line('register_profile_for_gender'); ?> </td>
                                        <td class="short">:</td>
                                        <td class="short"><?php echo (!empty($value['gender']) ? constant("GENDER_" . strtoupper($value['gender'])) : 'Not Specified'); ?></td>
                                    </tr>
                                    <tr class="opened">
										<td class="short"><?php echo $this->lang->line('register_profile_for_email'); ?> </td>
                                        <td class="short">:</td>
                                        <td class="short "><div class="profilemail" title="<?php echo (!empty($value['email']) ? $value['email'] : 'Not Specified'); ?>"> <?php echo (!empty($value['email']) ? $value['email'] : 'Not Specified'); ?></div></td>
                                    </tr>   
                                </tbody>
                            </table>
                            <div class=" viewbtn">
                            <?php if ($type == constant('TYPE_5')) { ?>
								<div class="btn btn-danger text-left" onclick="deactive('<?php echo $value['userGuid']; ?>', '	<?php echo $status = 0; ?>')"><?php echo $this->lang->line('deactive'); ?></div>
                                <?php } elseif ($type == constant('TYPE_6') ){?>
									<div class="btn btn-primary text-left"  onclick="active('<?php echo $value['userGuid']; ?>', '<?php echo $status = 1; ?>')"><?php echo $this->lang->line('active'); ?></div>
									 <?php } elseif ($type == constant('TYPE_13')) { ?>	
									 	<a href="<?php echo base_url() . 'index.php/adminpayment?uid=' . $value['userGuid']; ?>">
											<div class="btn btn-primary"><?php echo $this->lang->line('payment'); ?></div>	
										</a>					 
                                    <?php } 
                                    if ($type != constant('TYPE_7')) {
                                    if ($type != constant('TYPE_15')) {  ?>
										<a href="<?php echo base_url() . 'index.php/admin_profile/viewUser?uid=' . $value['userGuid'] . '&type=' . $type; ?>">
											<div class="btn btn-success"> <?php echo $this->lang->line('view_profile'); ?></div>
										</a>
                                        <?php } else { ?>
                                        <a href="<?php echo base_url() . 'index.php/admin_profile/editUser?uid=' . $value['userGuid'] . '&type=' . $type; ?>">
											<div class="btn btn-success"> <?php echo $this->lang->line('edit_profile'); ?></div>
										</a>
                                    <?php  } } ?>
                                 </div>
                              </div>
                           </div>    
					</td>    
				</tr>
			<?php }  ?>     				
 		  </tbody> 
         </table> 
		<?php  } else {
            echo '<h4 class="textcolor" style="text-align:center;">' . $this->lang->line('details_not_found') . '</h4>';
        } ?>	
        <div class="clearfix"> </div>
        <input type="hidden" id="baseurl" value="<?php echo base_url(); ?>"/>

    </div>
    <script>
        var baseurl = $("#baseurl").val();

        function active(userGuid, status) {
            if (userGuid == 0 || userGuid == '' || status == '') {
                return false;
            }
            $.post(baseurl + 'index.php/adminuser/activeDeactiveuser?userGuid=' + userGuid + '&status=' + status,
                    function (data) {
                        alert(data.msg);
                        location.reload();
                    }, "json");
        }

        function deactive(userGuid, status) {
            if (userGuid == 0 || userGuid == '' || status == '') {
                return false;
            }
            $.post(baseurl + 'index.php/adminuser/activeDeactiveuser?userGuid=' + userGuid + '&status=' + status,
                    function (data) {
                        alert(data.msg);
                        location.reload();
                    }, "json");
        }



    </script>
