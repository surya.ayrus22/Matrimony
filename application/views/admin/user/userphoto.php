<link href="<?php echo base_url(); ?>/assets/admin/css/process/style.css" rel='stylesheet' type='text/css' />
<link href="<?php echo base_url(); ?>assets/css/fileinput.css" media="all" rel="stylesheet" type="text/css" />
<script src="<?php echo base_url(); ?>assets/js/fileinput.js" type="text/javascript"></script>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/progress-wizard.min.css" rel='stylesheet' type='text/css' />

<style></style>
<div id="page-wrapper">
    <div class="graphs bgimage" style="min-height:600px;">
        <content-top>
            <div class="content-top clearfix">
                <h1 class="al-title"><?php echo $this->lang->line('add_photo'); ?></h1>
                <ul class="breadcrumb al-breadcrumb">
                    <li><a href="<?php echo base_url() . 'index.php/admin/dashboard'; ?>"><?php echo $this->lang->line('home'); ?></a></li>
                    <li><a href="<?php echo base_url() . 'index.php/admin/UsersAddInfo'; ?>"><?php echo sprintf($this->lang->line('add'), $this->lang->line('users')); ?> </a></li>
                    <li class=""><?php echo $this->lang->line('add_photo'); ?></li>
                </ul>
            </div>
        </content-top>
        <!--div class="checkout-wrap">
            <ul class="checkout-bar ">
                <li class="active"><?php echo $this->lang->line("register_profile_for_process1"); ?>   </li> 
                <li class="active"><?php echo $this->lang->line("register_profile_for_process2"); ?></li>       
                <li class=""><?php echo $this->lang->line("register_profile_for_process3"); ?></li>       
                <li class=""><?php echo $this->lang->line("register_profile_for_process4"); ?></li>         
                <li class=""><?php echo $this->lang->line("register_profile_for_process5"); ?></li>                  
            </ul>
        </div-->

		<ul class="progress-indicator">
            <li class="completed">
                <span class="bubble"></span>
                <?php echo $this->lang->line("register_profile_for_process1"); ?> <br><small>(complete)</small>
            </li>
            <li class="completed">
                <span class="bubble"></span>
                <?php echo $this->lang->line("register_profile_for_process2"); ?> <br><small>(complete)</small>
            </li>
            <li class="active">
                <span class="bubble"></span>
                <?php echo $this->lang->line("register_profile_for_process3"); ?> <br><small>(active)</small>
            </li>
            <li>
                <span class="bubble"></span>
                <?php echo $this->lang->line("register_profile_for_process4"); ?>
            </li>
            <li>
                <span class="bubble"></span>
                <?php echo $this->lang->line("register_profile_for_process5"); ?>
            </li>
        </ul>
        <div class="col-md-12 box-content">
            <div class="col-md-6 text-left margintop">	<h4><?php echo $this->lang->line('add_photo'); ?></h4>	</div>						
            <div class="clearfix"> </div>

            <!--col-md-4-->
            <form action="<?php echo base_url() . 'index.php/admin/userRegistration?uid=' . $_GET['uid']; ?>" method="post">	
                <div class="photo">
                    <form enctype="multipart/form-data">
                        <div class="form-group">
                            <input id="images" type="file" name="upfile" multiple class="file" data-overwrite-initial="false" data-min-file-count="1">
                        </div>
                    </form>
                </div>				
            </form>
            <div class=" text-right">
                <a href="<?php echo base_url() . 'index.php/admin/userpartner?uid=' . $userGuid; ?>"><div class="btn btn-primary">Next</div></a>
            </div>

        </div><!--row end-->
        <div class="clearfix"> </div>
    </div>
    <input type="hidden" id="baseurl" value="<?php echo base_url(); ?>">
    <input type="hidden" id="userguid" value="<?php echo $userGuid; ?>">
    <script>
        var baseurl = $("#baseurl").val();
        var userguid = $("#userguid").val();
        $("#images").fileinput({
            uploadUrl: baseurl + 'index.php/admin/fileupload?uid=' + userguid, // you must set a valid URL here else you will get an error
            allowedFileExtensions: ['jpg', 'png', 'jpeg'],
            overwriteInitial: false,
            maxFileSize: 1000,
            maxFilesNum: 5,
            //allowedFileTypes: ['image', 'video', 'flash'],
            slugCallback: function (filename) {
                return filename.replace('(', '_').replace(']', '_');
            }
        });

        $(document).ready(function () {
            /** overall upload button hide */
            $(".fileinput-upload-button").hide();
        });
    </script>
