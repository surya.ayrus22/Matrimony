<!--search-->
<script src="<?php echo base_url(); ?>assets/admin/js/search/animated-search-filter.js"></script>
<script>
    $(document).ready(function () {
        $('#paging_container').pajinate({
            num_page_links_to_display: 3,
            items_per_page: 4
        });
        $("#selectall").change(function () {
            $(".case").prop('checked', $(this).prop("checked"));
        });
    });
</script>
<div id="page-wrapper">
    <div class="graphs bgimage" style="min-height:600px;">
        <content-top>
            <div class="content-top clearfix">
                <?php
                $type = $_GET['type'];
                if ($type == 8) {
                    ?>
                    <h1 class="al-title"><?php echo $this->lang->line('platinum_member_text'); ?></h1>
                <?php } elseif ($type == 9) { ?>
                    <h1 class="al-title"><?php echo $this->lang->line('diamond_member_text'); ?></h1>
                <?php } elseif ($type == 10) { ?>
                    <h1 class="al-title"><?php echo $this->lang->line('gold_member_text'); ?></h1>
                <?php } ?>
                <ul class="breadcrumb al-breadcrumb">
                    <li><a href="<?php echo base_url(); ?>index.php/admin/dashboard">Dashboard</a></li>
                    <li class=""><?php echo (!empty($title) ? $title : ''); ?></li>

                </ul>
            </div>
        </content-top> 
        <?php if (!empty($payinfo)) { ?>
            <div id="paging_container">
                <div class="page_navigation pagination pagination_profile"></div>
                <!--<div class="">
                                <input value="0" id="selectall" type="checkbox">
                                <div class="btn btn-warning">active</div>
                        </div>-->
                <div style=" float: left;"class="floatleft">	
                    <input placeholder=Search class="animated-search-filter form-control input-search ">
                </div>
                <div class="clearfix"> </div>
                <div class="row ">
                    <?php foreach ($payinfo as $value) { ?>
                        <div class=animated-search-filter><!--search-->
                            <div class="content">
                                <!--col-md-4-->
                                <div class="col-md-4 inbox_right space">      	
                                    <div class="mailbox-content profilecontent">
                                        <div class="thumb profile_img">
                                            <a  href="#"> <img alt="" class="img-responsive" src="<?php echo base_url() . 'assets/upload_images/' . $value['image'] ?>"></a>
                                        </div>
                                        <div class="innercontent" title="<?php echo $value['username'] . ' ( ' . $value['userId'] . ' )'; ?>">
                                                <!--<span><input value=""  class="case" type="checkbox" name=""></span>-->
                                            <span class="email-title "><?php echo $value['username']; ?>(<?php echo constant('MEMBERID') . $value['userId']; ?>)</span>
                                        </div>

                                        <table class="half">
                                            <tbody>
                                                <tr class="opened_1">
                                                    <td class="short"><?php echo $this->lang->line('search_profile_for_age'); ?>  </td>
                                                    <td class="short">:</td>
                                                    <td class="short"><?php echo (!empty($value['age']) ? $value['age'] : 'Not Specified'); ?> Yrs </td>
                                                </tr>

                                                <tr class="opened">
                                                    <td class="short"><?php echo $this->lang->line('register_profile_for_gender'); ?> </td>
                                                    <td class="short">:</td>
                                                    <td class="short"><?php echo (!empty($value['gender']) ? constant("GENDER_" . strtoupper($value['gender'])) : 'Not Specified'); ?></td>
                                                </tr>
                                                <tr class="opened">
                                                    <td class="short"><?php echo $this->lang->line('register_profile_for_email'); ?> </td>
                                                    <td class="short">:</td>
                                                    <td class="short "><div class="profilemail" title="<?php echo (!empty($value['email']) ? $value['email'] : 'Not Specified'); ?>"> <?php echo (!empty($value['email']) ? $value['email'] : 'Not Specified'); ?></div></td>
                                                </tr>   
                                            </tbody>
                                        </table>
                                        <div class=" viewbtn">
                                            <?php if ($type == constant('TYPE_5')) { ?>
                                                <div class="btn btn-danger text-left" onclick="deactive('<?php echo $value['userGuid']; ?>', '<?php echo $status = 0; ?>')"><?php echo $this->lang->line('deactive'); ?></div>
                                            <?php } elseif ($type == constant('TYPE_6')) { ?>
                                                <div class="btn btn-primary text-left"  onclick="active('<?php echo $value['userGuid']; ?>', '<?php echo $status = 1; ?>')"><?php echo $this->lang->line('active'); ?></div>
                                            <?php } elseif ($type == constant('TYPE_13')) { ?>	
                                                <a href="<?php echo base_url() . 'index.php/adminpayment?uid=' . $value['userGuid']; ?>"><div class="btn btn-primary"><?php echo $this->lang->line('payment'); ?></div>	</a>					 
                                            <?php } ?>
                                            <?php if ($type != constant('TYPE_15')) { ?>
                                                <a href="<?php echo base_url() . 'index.php/admin_profile/viewUser?uid=' . $value['userGuid'] . '&type=' . $type; ?>"><div class="btn btn-success"> <?php echo $this->lang->line('view_profile'); ?></div></a>
                                            <?php } else { ?>
                                                <a href="<?php echo base_url() . 'index.php/admin_profile/editUser?uid=' . $value['userGuid'] . '&type=' . $type; ?>"><div class="btn btn-success"> <?php echo $this->lang->line('edit_profile'); ?></div></a>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div><!--content-->
                        </div><!--search-->
                    <?php } ?>	
                </div><!--row end-->
            </div><!--paging_container_short-->
            <?php
        } else {
            echo '<h4 class="textcolor" style="text-align:center;">' . $this->lang->line('no_search_text') . '</h4>';
        }
        ?>
        <div class="clearfix"> </div>
    </div>
