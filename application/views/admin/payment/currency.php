
<div id="page-wrapper">
    <div class="graphs bgimage" style="min-height:600px;">
        <content-top>
            <div class="content-top clearfix">
                <h1 class="al-title"><?php echo $this->lang->line('curreny_convertion'); ?></h1>
                <ul class="breadcrumb al-breadcrumb">
                    <li><a href="<?php echo base_url() . 'index.php/admin/dashboard'; ?>"><?php echo $this->lang->line('dashboard'); ?></a></li>
                    <li class=""><?php echo $this->lang->line('curreny_convertion'); ?></li>
                </ul>
            </div>
        </content-top>

        <div class="col-md-offset-2 col-md-8 col-md-offset-2">		
            <div class="">


                <?php if (!empty($message)) { ?>
                    <script>
                        $(function () {
                            $('#success_msg').show();
                            setTimeout(function () {
                                $('#success_msg').hide();
                            }, 3000);
                        });
                    </script>
                    <div class="error" id="success_msg" style="color:blue; text-align: center; display:none; padding-buttom:10px;"><?php echo $message; ?></div>
                <?php } ?>
                <ul style="padding-left:0px !important;">
                    <li class="selfservice_tab_active_new"><?php echo $this->lang->line('curreny_convertion'); ?></li>
                </ul>
                <div class=" cart_shadow">
                    <div class="row">
                        <div class="frmalg">
                            <form id="currency" action="" method="post">
                                <div class="form-group col-md-offset-1 col-md-10 col-md-offset-1">
                                    <label  class="col-md-4 form-control-label reglab"><?php echo $this->lang->line('membership_text'); ?> </label>
                                    <div class="col-md-8">
                                        <select class="form-control" name="member" id="member" required >
                                            <option value="">-- Select Membership -- </option>
                                            <option value="1">Platinum </option>
                                            <option value="2">Diamond </option>
                                            <option value="3">Gold</option>
                                        </select>
                                        <label class="error" for="member" style="display: none;"></label>
                                    </div>
                                </div>
                                <div class="form-group col-md-offset-1 col-md-10 col-md-offset-1">
                                    <label  class="col-md-4 form-control-label reglab"><?php echo $this->lang->line('default_amount'); ?> </label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control"  name="default"  id="default"  required >
                                        <label class="error" for="default" style="display: none;"></label>
                                    </div>
                                </div>										
                                <div class="form-group col-md-offset-1 col-md-10 col-md-offset-1">
                                    <label  class="col-md-4 form-control-label reglab"><?php echo $this->lang->line('month'); ?> </label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control"  name="month"  id="month" placeholder="Enter the month " required >
                                        <label class="error" for="month" style="display: none;"></label>
                                    </div>
                                </div>
                                <div class="form-group col-md-offset-1 col-md-10 col-md-offset-1">
                                    <label  class="col-md-4 form-control-label reglab"><?php echo $this->lang->line('inr_text'); ?> </label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control"  name="ind_currency" onblur="currencyconvertion(this.value)" placeholder="Enter the amount INR" required >
                                        <label class="error" for="ind_currency" style="display: none;"></label>
                                    </div>
                                    <div id="loading" style="display: none; margin-left: 100px; margin-bottom: -28px;">
                                        <img class="pull-right" src="<?php echo base_url(); ?>assets/images/ajax-loader.gif"/>
                                    </div>
                                </div>
                                <div class="form-group col-md-offset-1 col-md-10 col-md-offset-1">
                                    <label  class="col-md-4 form-control-label reglab"> <?php echo $this->lang->line('usd_text'); ?> </label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control"  id="usd" name="usd_currency" placeholder="" readonly required>
                                        <label class="error" for="usd_currency" style="display: none;"></label>
                                    </div>
                                </div>
                                <div class="form-group col-md-offset-2 col-md-8 col-md-offset-2">
                                    <button name="step1" class="btn btn-default subbtn pull-right" id="btn" type="submit"><?php echo $this->lang->line('register_profile_for_submit'); ?></button>
                                </div>
                            </form>
                        </div>	
                        <div class="clearfix"></div>
                    </div>
                </div>

                <input type="hidden" id="baseurl" value="<?php echo base_url(); ?>"/>
                <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>





                <div class="clearfix"></div>

            </div>  

        </div><!--row end-->

        <div class="clearfix"> </div>
    </div>
    <script>

                                            function paymentChanges(reqdata) {
                                                if (reqdata == 1) {
                                                    $("#paycheck_1").prop("checked", true);
                                                    $("#paycheck_2").prop("checked", false);
                                                    $("#paycheck_3").prop("checked", false);
                                                    $('#make_1').show();
                                                    $('#make_2').hide();
                                                    $('#make_3').hide();
                                                    $('#payment_1').addClass('selected');
                                                    $('#payment_2').removeClass('selected');
                                                    $('#payment_3').removeClass('selected');
                                                }

                                                if (reqdata == 2) {
                                                    $("#paycheck_2").prop("checked", true);
                                                    $("#paycheck_1").prop("checked", false);
                                                    $("#paycheck_3").prop("checked", false);
                                                    $('#make_2').show();
                                                    $('#make_1').hide();
                                                    $('#make_3').hide();
                                                    $('#payment_2').addClass('selected');
                                                    $('#payment_1').removeClass('selected');
                                                    $('#payment_3').removeClass('selected');
                                                }

                                                if (reqdata == 3) {
                                                    $("#paycheck_3").prop("checked", true);
                                                    $("#paycheck_2").prop("checked", false);
                                                    $("#paycheck_1").prop("checked", false);
                                                    $('#make_3').show();
                                                    $('#make_2').hide();
                                                    $('#make_1').hide();
                                                    $('#payment_3').addClass('selected');
                                                    $('#payment_2').removeClass('selected');
                                                    $('#payment_1').removeClass('selected');
                                                }

                                            }


    </script>

    <script>
        $(document).ready(function () {
            $("#currency").validate({
                rules: {
                    ind_currency: {
                        required: true,
                        number: true
                    },
                    month: {
                        required: true,
                        number: true
                    }
                }
            });
        });
        function currencyconvertion(reqData) {
            var intRegex = /[0-9 -()+]+$/;
            if (!reqData.match(intRegex)) {
                $('#usd').val('');
                return false
            }
            var baseurl = $('#baseurl').val();
            $('#loading').show();
            $('#btn').hide();
            $.ajax({
                type: "post",
                dataType: "text",
                url: baseurl + 'index.php/adminpayment/currencyconvertion?price=' + reqData,
                success: function (request) {
                    $('#usd').val(request);
                    $('#loading').hide();
                    $('#btn').show();
                },
                error: function (data) {
                    // location.reload();
                },
            });

            return false;

        }
    </script>
