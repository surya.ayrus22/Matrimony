
<div id="page-wrapper">
    <div class="graphs bgimage" style="min-height:600px;">
        <content-top>
            <div class="content-top clearfix">
                <h1 class="al-title"><?php echo $this->lang->line('payment'); ?></h1>
                <ul class="breadcrumb al-breadcrumb">
                    <li><a href="<?php echo base_url() . 'index.php/admin/dashboard'; ?>"><?php echo $this->lang->line('dashboard'); ?></a></li>
                    <li><a href="<?php echo base_url() . 'index.php/adminpayment/userdetails?type=13'; ?>"><?php echo $this->lang->line('view_payment'); ?></a></li>
                    <li class=""><?php echo $this->lang->line('payment'); ?></li>
                </ul>
            </div>
        </content-top>

        <div class="col-md-offset-2 col-md-8 col-md-offset-2 ">		

            <!--col-md-4-->
            <div class="">
                <ul style="padding-left: 0 !important;">
                    <li class="selfservice_tab_active_new"><?php echo $this->lang->line('self_service_plans'); ?></li>
                </ul>
                <?php if (!empty($membership)) { ?>		
                    <ul class="membership_tabs" style="padding-left: 0 !important;">
                        <?php foreach ($membership as $val) { ?>
                                            <!--<li>Premium Plus <span class="normal">(<span class="bold">+</span>)</span></li>-->
                            <li><?php echo $val['month'] ?> <?php echo $this->lang->line('months_text'); ?></li>
                        <?php } ?>
                        <div class="clearfix"></div>
                    </ul>
                    <div class=" cart_shadow">
                        <div class="display_table">
                            <ul class="membership_tabs_inner" style="padding-left: 0 !important;">
                                <?php foreach ($membership as $value) { ?>
                                    <li class="gc pointer" id ="payment_<?php echo $value['id']; ?>" onclick="paymentChanges('<?php echo $value['id']; ?>')">
                                        <form action="<?php echo base_url() . 'index.php/adminpayment/buy'; ?>" method="post">
                                            <input class="pointer" type="radio" value="<?php echo $value['id']; ?>" id="paycheck_<?php echo $value['id']; ?>" name="productcode">  
                                            <label for="ssp_gplus" class="cursor">
                                                <span class="font_18 green"><?php echo $value['name']; ?> <span class="plus_dark_grey"></span>  </span>
                                            </label>
                                            <div class="js_price_detail_block">
                                                <div class="spacer_5"></div>
                                                <span class="gray_price_new"><img height="10" width="6" title="Rs." alt="Rs." src=""> <?php echo $value['default_currency']; ?> </span>
                                                <div class="spacer_5"></div>
                                                <div>
                                                    <span class="membership_price_new"><i class="fa fa-inr"></i><?php echo $value['ind_currency']; ?></span>
                                                </div>
                                                <div>
                                                    <span class="membership_price_new"><i class="fa fa-usd"></i><?php echo $value['usd_currency']; ?></span>
                                                </div>
                                            </div>
                                            <input type="hidden" name="uid" value="<?php echo $userguid; ?>">
                                            <div  id="make_<?php echo $value['id']; ?>" style="display:none;" class="photo_view"><a onclick="$(this).closest('form').submit()">Make Payment</a></div>
                                        </form>
                                    </li>
                                <?php } ?>		
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <?php
                } else {
                    echo '<h4>' . $this->lang->line('no_data') . '</h4>';
                }
                ?>
                <div class="clearfix"></div>

            </div>  

        </div><!--row end-->

        <div class="clearfix"> </div>
    </div>
    <script>

        function paymentChanges(reqdata) {
            if (reqdata == 1) {
                $("#paycheck_1").prop("checked", true);
                $("#paycheck_2").prop("checked", false);
                $("#paycheck_3").prop("checked", false);
                $('#make_1').show();
                $('#make_2').hide();
                $('#make_3').hide();
                $('#payment_1').addClass('selected');
                $('#payment_2').removeClass('selected');
                $('#payment_3').removeClass('selected');
            }

            if (reqdata == 2) {
                $("#paycheck_2").prop("checked", true);
                $("#paycheck_1").prop("checked", false);
                $("#paycheck_3").prop("checked", false);
                $('#make_2').show();
                $('#make_1').hide();
                $('#make_3').hide();
                $('#payment_2').addClass('selected');
                $('#payment_1').removeClass('selected');
                $('#payment_3').removeClass('selected');
            }

            if (reqdata == 3) {
                $("#paycheck_3").prop("checked", true);
                $("#paycheck_2").prop("checked", false);
                $("#paycheck_1").prop("checked", false);
                $('#make_3').show();
                $('#make_2').hide();
                $('#make_1').hide();
                $('#payment_3').addClass('selected');
                $('#payment_2').removeClass('selected');
                $('#payment_1').removeClass('selected');
            }

        }


    </script>
