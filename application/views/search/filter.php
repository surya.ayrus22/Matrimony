
<div class="grid_3">
    <div class="container">

        <div class="breadcrumb1">
            <ul>
                <a href="<?php echo base_url(); ?>index.php/home/index"><i class="fa fa-home home_1"></i></a>
                <span class="divider">&nbsp;|&nbsp;</span>
                <li class="current-page"><?php echo $this->lang->line('search_text') . ' ' . $this->lang->line('profiles_text'); ?> </li>
            </ul>
        </div>


        <div class="col-md-3 col_5">
            <ul class="menu">
                <li class="item1"><h3 class="m_2"><?php echo $this->lang->line('search_title_1'); ?></h3>
                    <ul class="cute">

                        <li class="subitem1">
                            <form method="post"<?php echo base_url() . 'index.php/search/filtered'; ?>">
                                <input type="hidden" name="week" value="week">
                                <input type="hidden" name="caste" value="<?php echo (!empty($caste) ? $caste : ''); ?>">
                                <a onclick="$(this).closest('form').submit()"><?php echo sprintf($this->lang->line('search_with_text'), $this->lang->line('week_text')); ?> ( <?php echo $counts['week']; ?>  )</a>
                            </form>
                        </li>
                        <li class="subitem2">
                            <form method="post"<?php echo base_url() . 'index.php/search/filtered'; ?>">
                                <input type="hidden" name="month" value="month">
                                <input type="hidden" name="caste" value="<?php echo (!empty($caste) ? $caste : ''); ?>">
                                <a onclick="$(this).closest('form').submit()"><?php echo sprintf($this->lang->line('search_with_text'), $this->lang->line('month_text')); ?> ( <?php echo $counts['month']; ?> ) </a>
                            </form>
                        </li>
                    </ul>
                </li>
                <!--<li class="item1"><h3 class="m_2">Profile type</h3>
                  <ul class="cute">
                        <li class="subitem1">
                                <form method="post"<?php echo base_url() . 'index.php/search/filtered'; ?>">
                                        <input type="hidden" name="photo" value="photo">
                                        <input type="hidden" name="caste" value="<?php echo (!empty($caste) ? $caste : ''); ?>">
                                        <a onclick="$(this).closest('form').submit()">with Photo </a>
                                </form>
                        </li>
                  </ul>
                </li>-->
                <li class="item1"><h3 class="m_2"><?php echo $this->lang->line('register_profile_for_marriagestatus'); ?></h3>
                    <ul class="cute">
                        <li class="subitem1">
                            <form method="post"<?php echo base_url() . 'index.php/search/filtered'; ?>">
                                <input type="hidden" name="maritalstatus" value="1">
                                <input type="hidden" name="caste" value="<?php echo (!empty($caste) ? $caste : ''); ?>">
                                <a onclick="$(this).closest('form').submit()"><?php echo $this->lang->line('register_profile_for_marriagestatus_1'); ?>  ( <?php echo $counts['marital1']; ?> ) </a>
                            </form>
                        </li>
                        <li class="subitem1">
                            <form method="post"<?php echo base_url() . 'index.php/search/filtered'; ?>">
                                <input type="hidden" name="maritalstatus" value="2">
                                <input type="hidden" name="caste" value="<?php echo (!empty($caste) ? $caste : ''); ?>">
                                <a onclick="$(this).closest('form').submit()"><?php echo $this->lang->line('register_profile_for_marriagestatus_2'); ?>   ( <?php echo $counts['marital2']; ?> ) </a>
                            </form>

                        </li>
                        <li class="subitem1">
                            <form method="post"<?php echo base_url() . 'index.php/search/filtered'; ?>">
                                <input type="hidden" name="maritalstatus" value="3">
                                <input type="hidden" name="caste" value="<?php echo (!empty($caste) ? $caste : ''); ?>">
                                <a onclick="$(this).closest('form').submit()"><?php echo $this->lang->line('register_profile_for_marriagestatus_3'); ?>   (<?php echo $counts['marital3']; ?>)</a>
                            </form>
                        </li>
                        <li class="subitem1">
                            <form method="post"<?php echo base_url() . 'index.php/search/filtered'; ?>">
                                <input type="hidden" name="maritalstatus" value="4">
                                <input type="hidden" name="caste" value="<?php echo (!empty($caste) ? $caste : ''); ?>">
                                <a onclick="$(this).closest('form').submit()"><?php echo $this->lang->line('register_profile_for_marriagestatus_4'); ?>   ( <?php echo $counts['marital4']; ?> )</a>
                            </form>
                        </li>
                    </ul>
                </li>
                <!--<li class="item1"><h3 class="m_2">Mother Tongue</h3>
                  <ul class="cute">
                        <li class="subitem1">
                                <form method="post"<?php echo base_url() . 'index.php/search/filtered'; ?>">
                                        <input type="hidden" name="created" value="myself">
                                        <input type="hidden" name="caste" value="<?php echo (!empty($caste) ? $caste : ''); ?>">
                                        <a onclick="$(this).closest('form').submit()">Tamil </a>
                                </form>
                        </li>
                  </ul>
                </li>-->
                <li class="item1"><h3 class="m_2"><?php echo $this->lang->line('search_education_title'); ?></h3>
                    <ul class="cute">
                        <li class="subitem1">
                            <form method="post"<?php echo base_url() . 'index.php/search/filtered'; ?>">
                                <input type="hidden" name="education" value="1">
                                <input type="hidden" name="caste" value="<?php echo (!empty($caste) ? $caste : ''); ?>">
                                <a onclick="$(this).closest('form').submit()"><?php echo $this->lang->line('search_phd_text'); ?>( <?php echo $counts['edu1']; ?> )			</a>
                            </form>
                        </li>
                        <li class="subitem1">
                            <form method="post"<?php echo base_url() . 'index.php/search/filtered'; ?>">
                                <input type="hidden" name="education" value="2">
                                <input type="hidden" name="caste" value="<?php echo (!empty($caste) ? $caste : ''); ?>">
                                <a onclick="$(this).closest('form').submit()"><?php echo $this->lang->line('search_post_graduate_text'); ?> ( <?php echo $counts['edu2']; ?> )			</a>
                            </form>
                        </li>

                        <li class="subitem1">
                            <form method="post"<?php echo base_url() . 'index.php/search/filtered'; ?>">
                                <input type="hidden" name="education" value="3">
                                <input type="hidden" name="caste" value="<?php echo (!empty($caste) ? $caste : ''); ?>">
                                <a onclick="$(this).closest('form').submit()"><?php echo $this->lang->line('search_under_graduate_text'); ?> ( <?php echo $counts['edu3']; ?> )			</a>
                            </form>
                        </li>
                        <li class="subitem1">
                            <form method="post"<?php echo base_url() . 'index.php/search/filtered'; ?>">
                                <input type="hidden" name="education" value="4">
                                <input type="hidden" name="caste" value="<?php echo (!empty($caste) ? $caste : ''); ?>">
                                <a onclick="$(this).closest('form').submit()"><?php echo $this->lang->line('search_diploma_text'); ?> ( <?php echo $counts['edu4']; ?> )			</a>
                            </form>
                        </li>
                        <li class="subitem1">
                            <form method="post"<?php echo base_url() . 'index.php/search/filtered'; ?>">
                                <input type="hidden" name="education" value="5">
                                <input type="hidden" name="caste" value="<?php echo (!empty($caste) ? $caste : ''); ?>">
                                <a onclick="$(this).closest('form').submit()"><?php echo $this->lang->line('register_profile_for_highesteducation'); ?>( <?php echo $counts['edu5']; ?> )			</a>
                            </form>
                        </li>
                    </ul>
                </li>
                <li class="item1"><h3 class="m_2"><?php echo $this->lang->line('register_profile_for_occupation'); ?></h3>
                    <ul class="cute">
                        <li class="subitem1">
                            <form method="post"<?php echo base_url() . 'index.php/search/filtered'; ?>">
                                <input type="hidden" name="employeed" value="1">
                                <input type="hidden" name="caste" value="<?php echo (!empty($caste) ? $caste : ''); ?>">
                                <a onclick="$(this).closest('form').submit()"><?php echo $this->lang->line('register_profile_for_employeed_1'); ?> ( <?php echo $counts['emp_in1']; ?> )				</a>
                            </form>
                        </li>
                        <li class="subitem1">
                            <form method="post"<?php echo base_url() . 'index.php/search/filtered'; ?>">
                                <input type="hidden" name="employeed" value="2">
                                <input type="hidden" name="caste" value="<?php echo (!empty($caste) ? $caste : ''); ?>">
                                <a onclick="$(this).closest('form').submit()"><?php echo $this->lang->line('register_profile_for_employeed_2'); ?>	( <?php echo $counts['emp_in2']; ?> ) 					</a>
                            </form>
                        </li>
                        <li class="subitem1">
                            <form method="post"<?php echo base_url() . 'index.php/search/filtered'; ?>">
                                <input type="hidden" name="employeed" value="3">
                                <input type="hidden" name="caste" value="<?php echo (!empty($caste) ? $caste : ''); ?>">
                                <a onclick="$(this).closest('form').submit()"><?php echo $this->lang->line('register_profile_for_employeed_3'); ?> ( <?php echo $counts['emp_in3']; ?> ) 					</a>
                            </form>
                        </li>
                        <li class="subitem1">
                            <form method="post"<?php echo base_url() . 'index.php/search/filtered'; ?>">
                                <input type="hidden" name="employeed" value="4">
                                <input type="hidden" name="caste" value="<?php echo (!empty($caste) ? $caste : ''); ?>">
                                <a onclick="$(this).closest('form').submit()"><?php echo $this->lang->line('register_profile_for_employeed_4'); ?> ( <?php echo $counts['emp_in4']; ?> ) 					</a>
                            </form>
                        </li>
                    </ul>
                </li>
                <li class="item1"><h3 class="m_2"><?php echo $this->lang->line('register_profile_for_physicalstatus'); ?></h3>
                    <ul class="cute">
                        <li class="subitem1">
                            <form method="post"<?php echo base_url() . 'index.php/search/filtered'; ?>">
                                <input type="hidden" name="physicalstatus" value="1">
                                <input type="hidden" name="caste" value="<?php echo (!empty($caste) ? $caste : ''); ?>">
                                <a onclick="$(this).closest('form').submit()"><?php echo $this->lang->line('register_profile_for_physicalstatus_1'); ?> ( <?php echo $counts['physical1']; ?> ) 					</a>
                            </form>
                        </li>
                        <li class="subitem1">
                            <form method="post"<?php echo base_url() . 'index.php/search/filtered'; ?>">
                                <input type="hidden" name="physicalstatus" value="2">
                                <input type="hidden" name="caste" value="<?php echo (!empty($caste) ? $caste : ''); ?>">
                                <a onclick="$(this).closest('form').submit()"><?php echo $this->lang->line('register_profile_for_physicalstatus_2'); ?> ( <?php echo $counts['physical2']; ?> )  						</a>
                            </form>
                        </li>
                    </ul>
                </li>
                <li class="item1"><h3 class="m_2"><?php echo $this->lang->line('search_eatting_habit'); ?></h3>
                    <ul class="cute">
                        <li class="subitem1">
                            <form method="post"<?php echo base_url() . 'index.php/search/filtered'; ?>">
                                <input type="hidden" name="food" value="1">
                                <input type="hidden" name="caste" value="<?php echo (!empty($caste) ? $caste : ''); ?>">
                                <a onclick="$(this).closest('form').submit()"><?php echo $this->lang->line('register_profile_for_food_1'); ?> ( <?php echo $counts['food1']; ?> ) 						</a>
                            </form>
                        </li>
                        <li class="subitem1">
                            <form method="post"<?php echo base_url() . 'index.php/search/filtered'; ?>">
                                <input type="hidden" name="food" value="2">
                                <input type="hidden" name="caste" value="<?php echo (!empty($caste) ? $caste : ''); ?>">
                                <a onclick="$(this).closest('form').submit()"><?php echo $this->lang->line('register_profile_for_food_2'); ?> ( <?php echo $counts['food2']; ?> ) 						</a>
                            </form>
                        </li>
                        <li class="subitem1">
                            <form method="post"<?php echo base_url() . 'index.php/search/filtered'; ?>">
                                <input type="hidden" name="food" value="3">
                                <input type="hidden" name="caste" value="<?php echo (!empty($caste) ? $caste : ''); ?>">
                                <a onclick="$(this).closest('form').submit()"><?php echo $this->lang->line('register_profile_for_food_3'); ?> ( <?php echo $counts['food3']; ?> ) 						</a>
                            </form>
                        </li>
                    </ul>
                </li>
                <li class="item1"><h3 class="m_2"><?php echo $this->lang->line('register_profile_for_smoking'); ?></h3>
                    <ul class="cute">
                        <li class="subitem1">
                            <form method="post"<?php echo base_url() . 'index.php/search/filtered'; ?>">
                                <input type="hidden" name="smoking" value="1">
                                <input type="hidden" name="caste" value="<?php echo (!empty($caste) ? $caste : ''); ?>">
                                <a onclick="$(this).closest('form').submit()"><?php echo $this->lang->line('register_profile_for_smoking'); ?> ( <?php echo $counts['smoking1']; ?> ) 						</a>
                            </form>
                        </li>
                        <li class="subitem1">
                            <form method="post"<?php echo base_url() . 'index.php/search/filtered'; ?>">
                                <input type="hidden" name="smoking" value="2">
                                <input type="hidden" name="caste" value="<?php echo (!empty($caste) ? $caste : ''); ?>">
                                <a onclick="$(this).closest('form').submit()"><?php echo $this->lang->line('search_non_smoking'); ?>( <?php echo $counts['smoking2']; ?> ) 						</a>
                            </form>
                        </li>
                        <li class="subitem1">
                            <form method="post"<?php echo base_url() . 'index.php/search/filtered'; ?>">
                                <input type="hidden" name="smoking" value="3">
                                <input type="hidden" name="caste" value="<?php echo (!empty($caste) ? $caste : ''); ?>">
                                <a onclick="$(this).closest('form').submit()"><?php echo $this->lang->line('register_profile_for_smoking_3'); ?> ( <?php echo $counts['smoking3']; ?> ) 						</a>
                            </form>
                        </li>
                    </ul>
                </li>
                <li class="item1"><h3 class="m_2"><?php echo $this->lang->line('register_profile_for_drinking'); ?></h3>
                    <ul class="cute">
                        <li class="subitem1">
                            <form method="post"<?php echo base_url() . 'index.php/search/filtered'; ?>">
                                <input type="hidden" name="drinking" value="1">
                                <input type="hidden" name="caste" value="<?php echo (!empty($caste) ? $caste : ''); ?>">
                                <a onclick="$(this).closest('form').submit()"><?php echo $this->lang->line('register_profile_for_drinking'); ?>  ( <?php echo $counts['drinking1']; ?> ) 						</a>
                            </form>
                        </li>
                        <li class="subitem1">
                            <form method="post"<?php echo base_url() . 'index.php/search/filtered'; ?>">
                                <input type="hidden" name="drinking" value="2">
                                <input type="hidden" name="caste" value="<?php echo (!empty($caste) ? $caste : ''); ?>">
                                <a onclick="$(this).closest('form').submit()"><?php echo $this->lang->line('search_never_drinking'); ?> ( <?php echo $counts['drinking2']; ?> ) 						</a>
                            </form>
                        </li>
                        <li class="subitem1">
                            <form method="post"<?php echo base_url() . 'index.php/search/filtered'; ?>">
                                <input type="hidden" name="drinking" value="3">
                                <input type="hidden" name="caste" value="<?php echo (!empty($caste) ? $caste : ''); ?>">
                                <a onclick="$(this).closest('form').submit()"><?php echo $this->lang->line('register_profile_for_drinking_3'); ?> ( <?php echo $counts['drinking3']; ?> ) 						</a>
                            </form>
                        </li>
                    </ul>
                </li>
                <li class="item1"><h3 class="m_2"><?php echo sprintf($this->lang->line('search_profile_create'), $this->lang->line('by_text')); ?></h3>
                    <ul class="cute">
                        <li class="subitem1">
                            <form method="post"<?php echo base_url() . 'index.php/search/filtered'; ?>">
                                <input type="hidden" name="created" value="myself">
                                <input type="hidden" name="caste" value="<?php echo (!empty($caste) ? $caste : ''); ?>">
                                <a onclick="$(this).closest('form').submit()"><?php echo $this->lang->line('search_self'); ?> (<?php echo $counts['self']; ?>) </a>
                            </form>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
        <div class="col-md-9 members_box2" id="paging_container3">
            <h3>Search Profiles</h3> 
            <?php if (!empty($searchInfo)) { ?>
                <div class="alt_page_navigation pull-right" ></div>  
                <div class="clearfix"> </div>                     
                <div class="row_1 " >                      
                    <div id='loader' style='display:none'>
                        <img src="<?php echo base_url(); ?>assets/images/loading.gif"/>
                    </div>  
                    <?php
                    $i = 0;
                    foreach ($searchInfo as $v) {
                        ?>
                        <ul class="profile_item alt_content profile_items_view" id="week">
                            <li class="col-sm-4 " >
                                <div> 
                                    <ul>
                                        <li class="profile_item-img">
                                            <img src="<?php echo base_url() . 'assets/upload_images/' . $v['image']; ?>" width="75" height="75" class="img-responsive img_profile" alt=""/>
                                        </li>
                                        <li class="profile_item-desc">
                                            <h4><?php echo constant('MEMBERID'); ?><?php echo $v['userId']; ?></h4>
                                            <p><?php echo constant("GENDER_" . strtoupper($v['gender'])) . '|' . $v['age'] . 'years </br>' . $v['height'] . 'cms | ' . $v['caste_name']; ?> </p>
                                            <h5><a href="<?php echo base_url(); ?>index.php/profile/view?pid=<?php echo $v['userGuid']; ?>"><?php echo $this->lang->line('text_view_profile'); ?></a></h5>
                                        </li>
                                    </ul>
                                </div>		  
                            </li>
                        </ul>
                        <?php
                        $i++;
                    }
                    ?>
                </div>
                <?php
            } else {
                echo '<h4 style="text-align:center;">' . $this->lang->line("no_search_text") . '</h4>';
            }
            ?>
        </div>
        <div class="clearfix"> </div>
    </div>





    <script>
        $(document).load(function () {
            pagination();
        });

        $(document).ready(function () {
            pagination();
        });
        function pagination() {

            $('#paging_container3').pajinate({
                items_per_page: 15,
                item_container_id: '.alt_content',
                nav_panel_id: '.alt_page_navigation',
                nav_label_first: '<<',
                nav_label_last: '>>',
                nav_label_prev: '<',
                nav_label_next: '>'

            });
        }
        function request(reqdata, param) {
            if (reqdata == 0 || $.trim(reqdata) == '') {
                return false;
            }

            var baseurl = $('#baseurl').val();
            $('#loader').show();

            $.ajax({
                type: "post",
                dataType: "html",
                url: baseurl + 'index.php/search/request?' + param + '=' + reqdata,
                success: function (data) {
                    pagination()
                    $('#week').html(data);
                    $('#loader').hide();
                },
                error: function (data) {
                    $('#loader').hide();
                },
            });

            return false;
        }
        ;



    </script>
    <script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/pagination/js/jquery.pajinate.js"></script>

</div>

