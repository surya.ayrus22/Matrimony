
<style>
    .border1{
        border-color: #ddd;
        border-style: solid;
        border-width: 1px;
        margin-top:3%;
    }
    .nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus{
        background:#FFA417;
        color:#fff;
    }

</style>

<script>

    $(document).ready(function () {
        $(".js-example-basic-single").select2();

        $("#caste").select2({
            data: <?php echo $caste; ?>
        });

        $("#country").select2({
            data: <?php echo $country; ?>
        })

        $("#state").select2({
            data: <?php echo $state; ?>
        })
        $("#city").select2({
            data: <?php echo $city; ?>
        })
        $("#education").select2({
            data: <?php echo $education; ?>
        })
    });

</script>

<div class="container">

    <?php if ($offer == 2) { ?>
        <div class="col-md-12 " style="margin-top:20px;">
            <div class="col-md-offset-2 col-md-8 col-md-offset-2 sendleft" >
                <span class="leftpaytxt pull_left" style="color:#fff;"><?php echo $this->lang->line("payment_text"); ?> </span>
                <span class="btn btn-warning pull_right">
                    <a href="<?php echo base_url(); ?>index.php/payment" style="color:#fff;"><?php echo $this->lang->line("payment"); ?></a>
                </span>
            </div>
        </div>
    <?php } ?> 

    <div class="col-md-8 border1">
        <ul class="nav nav-tabs" style=" margin-top: 25px;">
            <li class="active"><a href="#tab_a" data-toggle="tab"><?php echo $this->lang->line('regular_text') . ' ' . $this->lang->line('search_text'); ?></a></li>
            <li class=""><a href="#tab_b" data-toggle="tab"><?php echo $this->lang->line('key_text') . ' ' . $this->lang->line('search_text'); ?></a></li>

        </ul>
        <div class="tab-content ">
            <div class="tab-pane active" id="tab_a" style="  margin-top: 7%;">
                <form action="<?php echo base_url(); ?>index.php/search/filtered" method="post">    
                    <div class="form-group row">
                        <label  class="col-sm-4 form-control-label"> <?php echo $this->lang->line("search_profile_for_age"); ?> </label>
                        <div class="col-sm-8">
                            <label class="col-sm-2 form-control-label"><?php echo $this->lang->line('from_text'); ?></label>
                            <div class="col-md-3">
                                <select class="form-control js-example-basic-single" id="age_from" name="age_from">
                                    <option value="">From</option>
                                    <option value="21">21</option>
                                    <option value="22">22</option>
                                    <option value="23">23</option>
                                    <option value="24">24</option>
                                    <option value="25">25</option>
                                    <option value="26">26</option>
                                    <option value="27">27</option>
                                    <option value="28">28</option>
                                    <option value="29">29</option>
                                    <option value="30">30</option>
                                    <option value="31">31</option>
                                    <option value="32">32</option>
                                    <option value="33">33</option>
                                    <option value="34">34</option>
                                    <option value="35">35</option>
                                    <option value="36">36</option>
                                    <option value="37">37</option>
                                    <option value="38">38</option>
                                    <option value="39">39</option>
                                    <option value="40">40</option>
                                    <option value="41">41</option>
                                    <option value="42">42</option>
                                    <option value="43">43</option>
                                    <option value="44">44</option>
                                    <option value="45">45</option>
                                    <option value="46">46</option>
                                    <option value="47">47</option>
                                    <option value="48">48</option>
                                    <option value="49">49</option>
                                    <option value="50">50</option>
                                    <option value="51">51</option>
                                    <option value="52">52</option>
                                    <option value="53">53</option>
                                    <option value="54">54</option>
                                    <option value="55">55</option>
                                    <option value="56">56</option>
                                    <option value="57">57</option>
                                    <option value="58">58</option>
                                    <option value="59">59</option>
                                    <option value="60">60</option>
                                    <option value="61">61</option>
                                    <option value="62">62</option>
                                    <option value="63">63</option>
                                    <option value="64">64</option>
                                    <option value="65">65</option>
                                    <option value="66">66</option>
                                    <option value="67">67</option>
                                    <option value="68">68</option>
                                    <option value="69">69</option>
                                    <option value="70">70</option>
                                </select>
                                <label class="error" for="age_from"></label>
                            </div>
                            <label class="col-sm-2 form-control-label"><?php echo $this->lang->line('to_text'); ?></label>
                            <div class="col-md-3">
                                <select class="form-control js-example-basic-single" id="age_to" name="age_to" >
                                    <option value="">To</option>
                                    <option value="25">25</option>
                                    <option value="26">26</option>
                                    <option value="27">27</option>
                                    <option value="28">28</option>
                                    <option value="29">29</option>
                                    <option value="30">30</option>
                                    <option value="31">31</option>
                                    <option value="32">32</option>
                                    <option value="33">33</option>
                                    <option value="34">34</option>
                                    <option value="35">35</option>
                                    <option value="36">36</option>
                                    <option value="37">37</option>
                                    <option value="38">38</option>
                                    <option value="39">39</option>
                                    <option value="40">40</option>
                                    <option value="41">41</option>
                                    <option value="42">42</option>
                                    <option value="43">43</option>
                                    <option value="44">44</option>
                                    <option value="45">45</option>
                                    <option value="46">46</option>
                                    <option value="47">47</option>
                                    <option value="48">48</option>
                                    <option value="49">49</option>
                                    <option value="50">50</option>
                                    <option value="51">51</option>
                                    <option value="52">52</option>
                                    <option value="53">53</option>
                                    <option value="54">54</option>
                                    <option value="55">55</option>
                                    <option value="56">56</option>
                                    <option value="57">57</option>
                                    <option value="58">58</option>
                                    <option value="59">59</option>
                                    <option value="60">60</option>
                                    <option value="61">61</option>
                                    <option value="62">62</option>
                                    <option value="63">63</option>
                                    <option value="64">64</option>
                                    <option value="65">65</option>
                                    <option value="66">66</option>
                                    <option value="67">67</option>
                                    <option value="68">68</option>
                                    <option value="69">69</option>
                                    <option value="70">70</option>
                                </select>  
                            </div>
                            <label class="error" for="age_to"></label>  
                        </div>
                    </div> 
                    <div class="form-group row">
                        <label for="weight" class="col-sm-4 form-control-label"><?php echo $this->lang->line("register_profile_for_height"); ?> </label>  
                        <div class="col-sm-8">
                            <select class="form-control js-example-basic-single" name="height" id="height" >
                                <option value=""> -- Height -- </option>
                                <option value="145" label="4' 9&quot; (145 cm)">4' 9" (145 cm)</option>
                                <option value="147" label="4' 10&quot; (147 cm)">4' 10" (147 cm)</option>
                                <option value="150" label="4' 11&quot; (150 cm)">4' 11" (150 cm)</option>
                                <option value="152" label="5'  0&quot; (152 cm)">5'  0" (152 cm)</option>
                                <option value="155" label="5' 1&quot; (155 cm)">5' 1" (155 cm)</option>
                                <option value="157" label="5' 2&quot; (157 cm)">5' 2" (157 cm)</option>
                                <option value="160" label="5' 3&quot; (160 cm)">5' 3" (160 cm)</option>
                                <option value="162" label="5' 4&quot; (162 cm)">5' 4" (162 cm)</option>
                                <option value="165" label="5' 5&quot; (165 cm)">5' 5" (165 cm)</option>
                                <option value="167" label="5' 6&quot; (167 cm)">5' 6" (167 cm)</option>
                                <option value="170" label="5' 7&quot; (170 cm)">5' 7" (170 cm)</option>
                                <option value="173" label="5' 8&quot; (173 cm)">5' 8" (173 cm)</option>
                                <option value="175" label="5' 9&quot; (175 cm)">5' 9" (175 cm)</option>
                                <option value="178" label="5' 10&quot; (178 cm)">5' 10" (178 cm)</option>
                                <option value="180" label="5' 11&quot; (180 cm)">5' 11" (180 cm)</option>
                                <option value="183" label="6' 0&quot; (183 cm)">6' 0" (183 cm)</option>
                                <option value="185" label="6' 1&quot; (185 cm)">6' 1" (185 cm)</option>
                                <option value="188" label="6' 2&quot; (188 cm)">6' 2" (188 cm)</option>
                                <option value="190" label="6' 3&quot; (190 cm)">6' 3" (190 cm)</option>
                                <option value="193" label="6' 4&quot; (193 cm)">6' 4" (193 cm)</option>
                                <option value="21">Above the Height </option>
                            </select> 
                            <label class="error" for="height"></label>        
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="marialstatus" class="col-sm-4 form-control-label"><?php echo $this->lang->line("register_profile_for_marriagestatus"); ?> </label>
                        <div class="col-sm-8">
                            <div class="col-md-3">
                                <label class="radio-inline reglabrad">
                                    <input type="radio" name="maritalstatus" id="maritalstatus" value="1"><?php echo $this->lang->line("register_profile_for_marriagestatus_1"); ?>
                                </label>
                            </div>
                            <div class="col-md-3">
                                <label class="radio-inline reglabrad">
                                    <input type="radio" name="maritalstatus" id="watch-me" value="2"> <?php echo $this->lang->line("register_profile_for_marriagestatus_2"); ?>	
                                </label>
                            </div>
                            <div class="col-md-3">
                                <label class="radio-inline reglabrad">
                                    <input type="radio" name="maritalstatus" id="watch-me" value="3"> <?php echo $this->lang->line("register_profile_for_marriagestatus_3"); ?> 
                                </label>
                            </div>
                            <div class="col-md-3">
                                <label class="radio-inline reglabrad">
                                    <input type="radio" name="maritalstatus" id="watch-me" value="4"> <?php echo $this->lang->line("register_profile_for_marriagestatus_4"); ?>
                                </label>
                            </div>
                        </div>
                    </div> 

                    <!--div class="form-group row" id="show-me" style="display:none">
                      <label for="children" class="col-sm-4 form-control-label">No.of Children</label>  
                      <div class="col-sm-8">
                      <select class="form-control js-example-basic-single" id="profile" name="profile">
                          <option value="0"> -- Childrens -- </option>
                          <option value="1">None</option>
                          <option value="2">1</option>
                          <option value="3">2</option>
                          <option value="4">3</option>
                          <option value="5">4 and above</option>
                        </select>         
                      </div>
                    </div-->

                    <div class="form-group row" >
                        <label for="country" class="col-sm-4 form-control-label"><?php echo $this->lang->line("register_profile_for_country"); ?></label>  
                        <div class="col-sm-8">
                            <select class="js-example-data-array" id="country" name="country" onchange="stateInfo(this.value)" >
                                <option value="0"> -- Select Country -- </option>
                            </select>         
                        </div>
                    </div>

                    <div class="form-group row" >
                        <label for="state" class="col-sm-4 form-control-label"><?php echo $this->lang->line("register_profile_for_state"); ?></label>  
                        <div class="col-sm-8">
                            <select class="form-control js-example-basic-single" id="state" name="state" onchange="cityInfo(this.value)">
                                <option value="0"> -- Select State -- </option>
                            </select>         
                        </div>
                    </div>

                    <div class="form-group row" >
                        <label for="city" class="col-sm-4 form-control-label"><?php echo $this->lang->line("register_profile_for_city"); ?> </label>  
                        <div class="col-sm-8">
                            <select class="form-control js-example-basic-single"  id="city" name="city">
                                <option value="0"> -- Select City -- </option>
                            </select>         
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="caste" class="col-sm-4 form-control-label reglab"><?php echo $this->lang->line("register_profile_for_caste"); ?> </label>     
                        <div class="col-sm-8">
                            <select class="form-control js-example-data-array" id="caste" name="caste">
                                <option value=""> -- Select Caste -- </option>
                            </select>
                        </div> 
                    </div>

                    <div class="form-group row">
                        <label for="edu" class="col-sm-4 form-control-label"><?php echo $this->lang->line("register_profile_for_highesteducation"); ?> </label>  
                        <div class="col-sm-8">
                            <select class="form-control js-example-data-array" name="education" id ="education">
                                <option value=""> -- Select education -- </option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="empin" class="col-sm-4 form-control-label"><?php echo $this->lang->line("register_profile_for_employeed"); ?></label>
                        <div class="col-sm-8">
                            <div class="col-md-3">
                                <label class="radio-inline reglabrad">
                                    <input type="radio" name="emp_in"  value="1" > <?php echo $this->lang->line("register_profile_for_employeed_1"); ?>
                                </label>
                            </div>
                            <div class="col-md-3">
                                <label class="radio-inline reglabrad">
                                    <input type="radio" name="emp_in"  value="2" > <?php echo $this->lang->line("register_profile_for_employeed_2"); ?>
                                </label>
                            </div>
                            <div class="col-md-3">
                                <label class="radio-inline reglabrad">
                                    <input type="radio" name="emp_in"  value="3" > <?php echo $this->lang->line("register_profile_for_employeed_3"); ?> 	
                                </label>
                            </div>
                            <div class="col-md-3">
                                <label class="radio-inline reglabrad">
                                    <input type="radio" name="emp_in"  value="4" > <?php echo $this->lang->line("register_profile_for_employeed_4"); ?>
                                </label>
                            </div>
                            <label class="error" for="emp_in"></label>
                        </div>
                    </div> 

                    <div class="form-group row">
                        <label for="star" class="col-sm-4 form-control-label"><?php echo $this->lang->line("register_profile_for_salary"); ?> </label>
                        <div class="col-sm-8">
                            <select class="form-control js-example-basic-single" name="salary" id="salary">
                                <option value=""> -- select salary -- </option>
                                <option value="1">100000 or less</option>
                                <option value="2">200000 to 300000</option>
                                <option value="3">300000 to 400000</option>
                                <option value="4">400000 to 500000</option>
                                <option value="5">500000 to 600000</option>
                                <option value="6">600000 to 700000</option>
                                <option value="7">700000 to 800000</option>
                                <option value="8">800000 to 900000</option>
                                <option value="9">900000 Above</option>
                                <option value="10">any</option>
                            </select>   
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-offset-4 col-sm-8">
                            <button style="float:right;"type="submit" id="search_btn"  class="btn btn-success subbtn"><?php echo $this->lang->line("register_profile_for_submit"); ?></button>
                        </div>
                    </div>
                </form>
            </div>

            <div class="tab-pane" id="tab_b">
                <form id="key_search" action="<?php echo base_url(); ?>index.php/search/filtered" method="post" style="margin-top: 3%;">    
                    <div class="search_text" style="margin-bottom:3%;"><span ><?php echo $this->lang->line("search_profile_for_keysearch_text"); ?></span></div>
                    <div class="form-group row" style=" margin-top:1%;">
                        <div class="col-sm-8">
                            <input type="text" class="form-control" name="keyword" id ="key_text" placeholder="Enter the search keyword" required>
                            <input type="hidden" class="form-control" name="keySearch">
                        </div>
                        <div  class="col-sm-2">
                            <button style="float:right;" type="submit" class="btn btn-success subbtn"><?php echo $this->lang->line("register_profile_for_submit"); ?></button>
                        </div>
                        <div class="col-sm-8 sear-id form-control-label reglab "><?php echo $this->lang->line('success_example_text'); ?></div>
                    </div>
                </form>
            </div>

        </div><!-- tab content -->

    </div><!-- col-md-8 end -->
    <div class="col-md-1" style="width:2%;"></div>
    <!--col-md -3-->
    <div class="col-md-3 ">
        <div class="per_img">
            <div class="txt-search">
                <label class=" form-control-label reglab sear-id"><?php echo $this->lang->line('search_text') . ' ' . $this->lang->line('by_text') . ' ' . $this->lang->line('profile_id'); ?></label>
                <form action="<?php echo base_url(); ?>index.php/search/filtered" method="post">
                    <div class="form-group row">
                        <div class="col-sm-1">
                            <label class="form-control-label reglab sear-id"><?php echo constant('MEMBERID'); ?></label>
                        </div>
                        <div class="col-sm-8 searchid" >
                            <input title="Enter number Ex:( M54 then just give 54 )only" type="number" min="1" value=""  required placeholder="<?php echo $this->lang->line('enter_text'); ?>" name="memberid"  class="form-control">
                        </div>
                        <div style="padding-left: 1px;" class="col-sm-2">
                            <button class="btn btn-success set_save" type="submit"><?php echo $this->lang->line('go_text'); ?>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>	

    </div>
    <!--col-md-3-->
    <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
    <script>
                                $('#search_btn').click(function () {
                                    var to = $('#age_to').val();
                                    var from = $('#age_from').val();
                                    if (to == '' && from != '') {
                                        alert('<?php echo $this->lang->line("please_select_age_to"); ?>');
                                        return false;
                                    }

                                    if (to != '' && from == '') {
                                        alert('<?php echo $this->lang->line("please_select_age_from"); ?>');
                                        return false;
                                    }
                                });
                                function stateInfo(reqData) {
                                    if (reqData == 0) {
                                        return false
                                    }
                                    $("#city").html('');
                                    $("#state").html('');
                                    var baseurl = $('#baseurl').val();
                                    $.ajax({
                                        type: "post",
                                        dataType: "json",
                                        url: baseurl + 'index.php/common/statelist/' + reqData,
                                        success: function (request) {
                                            if (request.status == 0) {
                                                $("#text-state").html('<input type="text" class="form-control" name="state_text" id="state" placeholder=" Enthe the state">');
                                                $("#text-city").html('<input type="text" class="form-control" name="city_text" id="city" placeholder=" Enthe the city">');
                                            } else {
                                                $("#state").select2({
                                                    data: request
                                                });
                                            }
                                        },
                                        error: function (data) {

                                        },
                                    });

                                    return false;
                                }

                                function cityInfo(reqData) {
                                    if (reqData == 0) {
                                        return false
                                    }
                                    $("#city").html('');
                                    var baseurl = $('#baseurl').val();
                                    $.ajax({
                                        type: "post",
                                        dataType: "json",
                                        url: baseurl + 'index.php/common/citylist/' + reqData,
                                        success: function (request) {
                                            if (request.status == 0) {
                                                $("#text-city").html('<input type="text" class="form-control" name="city_text" id="city" placeholder=" Enthe the city">');
                                            } else {
                                                $("#city").select2({
                                                    data: request
                                                });
                                            }
                                        },
                                        error: function (data) {

                                        },
                                    });

                                    return false;
                                }

                                $(document).ready(function () {
                                    $("#key_search").validate();

                                    $("#key_text").rules("add", {
                                        required: true,
                                        messages: {
                                            required: "Please enter the keywords",
                                        }
                                    });

                                });
    </script>
</div><!-- end of container -->
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="<?php echo base_url() . 'assets/css/select2/select2.min.css' ?>">

<!-- Latest compiled and minified JavaScript -->
<script src="<?php echo base_url() . 'assets/js/select2/select2.min.js' ?>"></script>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<!--script src="Bootstrap%203%20Navs%20Tabs%20Pills_files/jquery.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<!--script src="Bootstrap%203%20Navs%20Tabs%20Pills_files/bootstrap.js"></script-->


