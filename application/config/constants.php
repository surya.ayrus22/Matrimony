  <?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE')  OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code

define('ADMIN_ROLE_ID','49a2a2aec19547dc988730e6817e99b5');
define('MEMBER_ROLE_ID','ac6c82d4b84d4007aac815c3e089a883');

define ('DATE_TIME_FORMAT', 'Y-m-d H:i:s');
define ('DATE_FORMAT', 'Y-m-d');
define ('MARITAL_STATUS_1', 'UnMarried');
define ('MARITAL_STATUS_2', 'Widowed');
define ('MARITAL_STATUS_3', 'Divorced');
define ('MARITAL_STATUS_4', 'Awaiting divorce');

define ('BODY_TYPE_1', 'Slim');
define ('BODY_TYPE_2', 'Average');
define ('BODY_TYPE_3', 'Athletic');
define ('BODY_TYPE_4', 'Heavy');

define ('COMPLEXION_TYPE_1', 'Very Fair');
define ('COMPLEXION_TYPE_2', 'Fair');
define ('COMPLEXION_TYPE_3', 'Wheatish');
define ('COMPLEXION_TYPE_4', 'Dark ');

define ('PHYSICAL_STATUS_1', 'Normal');
define ('PHYSICAL_STATUS_2', 'Physically challenged');

define ('FAMILY_STATUS_1', 'Middle class');
define ('FAMILY_STATUS_2', 'Upper middle class');
define ('FAMILY_STATUS_3', 'Rich');
define ('FAMILY_STATUS_4', 'Affluent');

define ('FAMILY_TYPE_1', 'Joint');
define ('FAMILY_TYPE_2', 'Nuclear');

define ('FAMILY_VALUES_1', 'Orthodox');
define ('FAMILY_VALUES_2', 'Traditional');
define ('FAMILY_VALUES_3', 'Moderate');
define ('FAMILY_VALUES_4', 'Liberal');

define ('EMPLOYEED_IN_1', 'Government');
define ('EMPLOYEED_IN_2', 'Self Employeed');
define ('EMPLOYEED_IN_3', 'Businees');
define ('EMPLOYEED_IN_4', 'Private');

define ('DRINK_1', 'Yes');
define ('DRINK_2', 'No');
define ('DRINK_3', 'Drinks Socially');

define ('SMOKE_1', 'Yes');
define ('SMOKE_2', 'No');
define ('SMOKE_3', 'Occasionally');

define ('FOOD_TYPE_1', 'Vegetarian');
define ('FOOD_TYPE_2', 'Non-Vegetarian');
define ('FOOD_TYPE_3', 'Eggetarian');

define ('DHOSAM_TYPE_1', 'Yes');
define ('DHOSAM_TYPE_2', 'No');
define ('DHOSAM_TYPE_3',"Don't know");

define ('REQUEST_TYPE_1', 'Accept');
define ('REQUEST_TYPE_2', 'Ignore');
define ('REQUEST_TYPE_3', 'Send');
define ('REQUEST_TYPE_4', 'Interest');
define ('REQUEST_TYPE_5', 'Not Interested');
define ('REQUEST_TYPE_6', 'Block');
define ('REQUEST_TYPE_7', 'Shortlist');
define ('REQUEST_TYPE_8', 'Arranged');
define ('REQUEST_TYPE_DEFAULT', 'Reminder');
define ('REQUEST_TYPE_DEFAULT_1', 'View Suitable Matches');

define ('REQUEST_ACCEPT', '1');
define ('REQUEST_IGNORE', '2');
define ('REQUEST_SEND', '3');
define ('REQUEST_INTEREST', '4');
define ('REQUEST_NOT_INETREST', '5');
define ('REQUEST_BLOCK', '6');
define ('REQUEST_SHORTLIST', '7');
define ('REQUEST_ARRANGED', '8');
define ('REQUEST_IMAGE', '9');

define ('TEXT_COMPLETED', 'Completed');
define ('GENDER_MALE', 'M');
define ('GENDER_FEMALE', 'F');
define ('GENDER_M', 'Male');
define ('GENDER_F', 'Female');
define ('MEMBERID', 'M');
define ('TEXT_PARENT', 'Parent');
define ('TEXT_SON', 'son');
define ('TEXT_DAUGHTER', 'daughter');

define ('TYPE_1','1'); /* Overall Users */
define ('TYPE_2','2'); /* Male Users Only */
define ('TYPE_3','3'); /* Female Users Only */
define ('TYPE_4','4'); /* Overall Premium Members */
define ('TYPE_5','5'); /* Active Users Only */
define ('TYPE_6','6'); /* Deactive Users */
define ('TYPE_7','7'); /* Deleted Users */
define ('TYPE_8','8'); /* Platinum Users */
define ('TYPE_9','9'); /* Premium Users */
define ('TYPE_10','10'); /* Gold Users */
define ('TYPE_11','11'); /* Profile Approved */
define ('TYPE_12','12'); /* Success profile */
define ('TYPE_13','13'); /* Payment */
define ('TYPE_14','14'); /* Currency Convertion */
define ('TYPE_15','15'); /* Incomplete User Registration */

/** message enable **/
define('MESSAGE_ENABLE', '0'); /** message enable ON/OFF **/
define('MAIL_ENABLE', '1'); /** message enable ON/OFF **/

/** image name constant **/
define('MALE_IMAGE', 'male.png'); 
define('FEMALE_IMAGE', 'female.jpeg');  
define('DEFAULT_IMAGE', 'default.png'); 











