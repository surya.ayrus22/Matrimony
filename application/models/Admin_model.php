<?php

Class Admin_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function getMonthWiseUserCount($userGuid = '', $email = '', $gender = '', $limits = '', $roleGuid = MEMBER_ROLE_ID) {
        $this->db->select('	YEAR(user.created) AS YEAR,
								SUM(MONTH(user.created) = 1) AS January ,
								SUM(MONTH(user.created) = 2) AS February ,
								SUM(MONTH(user.created) = 3) AS March ,
								SUM(MONTH(user.created) = 4) AS April,
								SUM(MONTH(user.created) = 5) AS May ,
								SUM(MONTH(user.created) = 6) AS June ,
								SUM(MONTH(user.created) = 7) AS July ,
								SUM(MONTH(user.created) = 8) AS Augest,
								SUM(MONTH(user.created) = 9) AS September ,
								SUM(MONTH(user.created) = 10) AS October ,
								SUM(MONTH(user.created) = 11) AS November ,
								SUM(MONTH(user.created) = 12) AS December')
                ->join('user_role AS role', 'role.user_guid = user.guid')
                ->join('user_profile AS profile', 'profile.user_guid = user.guid')
                ->join('user_other_details AS edu', 'edu.user_guid = user.guid')
                ->join('user_religion_info AS reg', 'reg.user_guid = user.guid')
                ->join('user_food_habbits AS habbit', 'habbit.user_guid = user.guid', 'left')
                ->join('user_family_details AS family', 'family.user_guid = user.guid')
                ->where_in('role.role_guid', $roleGuid);

        if ($userGuid) {
            $this->db->where_in('user.guid', $userGuid);
        }
        if ($email) {
            $this->db->where_in('user.email', $email);
        }
        if ($gender) {
            $this->db->where_in('profile.gender', $gender);
        }
        if ($limits) {
            $this->db->limit($limits);
        }
        $this->db->where('user.created >= NOW() - INTERVAL 12 MONTH');
        $this->db->group_by('1');


        $this->db->where('user.deleted', 0);
        $query = $this->db->get('user as user');
        return $query->result_array();
    }

    public function userRegistrationStep1($requestData) {
        $result = '';
        if (isset($requestData['step1']) && !empty($requestData['username'])) {
            $uname = $requestData['username'];
            $gender = $requestData['gender'];
            $dob = $requestData['year'] . '-' . $requestData['month'] . '-' . $requestData['date'];
            $religion = $requestData['religion'];
            $tongue = $requestData['mother_tongue'];
            $email = $requestData['email'];
            $pass = $requestData['password'];
            $mobile = $requestData['mobile'];
            $profilecreated = $requestData['profile_created'];

            $insertUser = $insertProfile = $insertReligionInfo = $insertReligionInfo = 0;
            $userExists = $this->User_model->userDetails($email);
            if (empty($userExists)) {
                $this->db->trans_begin();
                $userGuid = $this->Utils->getGuid();
                $created = date(DATE_TIME_FORMAT);
                $insertUserData = array('email' => $email,
                    'password' => md5($pass),
                    'username' => $uname,
                    'created' => $created,
                    'created_by' => $userGuid,
                    'last_updated' => $created,
                    'last_updated_by' => $userGuid,
                    'guid' => $userGuid,
                );
                $insertUser = $this->User_model->insertUser($insertUserData);
                if ($insertUser) {
                    $profileGuid = $this->Utils->getGuid();
                    $insertProfileData = array(
                        'user_guid' => $userGuid,
                        'profile_created' => $profilecreated,
                        'mobile' => $mobile,
                        'dob' => $dob,
                        'age' => date_diff(date_create($dob), date_create('today'))->y,
                        'gender' => $gender,
                        'created' => $created,
                        'created_by' => $userGuid,
                        'last_updated' => $created,
                        'last_updated_by' => $userGuid,
                        'guid' => $profileGuid,
                    );

                    $insertProfile = $this->User_model->insertUserProfile($insertProfileData);

                    if (!empty($insertProfile)) {

                        $roleGuid = $this->Utils->getGuid();
                        $insertRoleData = array(
                            'user_guid' => $userGuid,
                            'role_guid' => MEMBER_ROLE_ID,
                            'created' => $created,
                            'created_by' => $userGuid,
                            'last_updated' => $created,
                            'last_updated_by' => $userGuid,
                            'guid' => $roleGuid,
                        );

                        $insertuserRole = $this->User_model->insertUserRole($insertRoleData);
                        if (!empty($insertuserRole)) {

                            $religionGuid = $this->Utils->getGuid();
                            $insertReligionData = array(
                                'user_guid' => $userGuid,
                                'religion' => $religion,
                                'mother_tongue' => $tongue,
                                'created' => $created,
                                'created_by' => $userGuid,
                                'last_updated' => $created,
                                'last_updated_by' => $userGuid,
                                'guid' => $religionGuid,
                            );

                            $insertReligionInfo = $this->User_model->insertReligionInfo($insertReligionData);
                        }
                    }
                }

                if (empty($insertUser) || empty($insertProfile) || empty($insertReligionInfo) || empty($insertReligionInfo)) {
                    $this->db->trans_rollback();
                    $result['message'] = 'Registration failed please try afer some time';
                    $result['url'] = 'index.php/admin/useradd';
                } else {
                    $this->db->trans_commit();
                    $result['message'] = "Success";
                    $result['url'] = 'index.php/admin/userinfo?uid=' . $userGuid;
                }
            } else {
                $result['message'] = 'User Already exists with same email';
                $result['url'] = 'index.php/admin/useradd';
            }
        }
        return $result;
    }

    public function userRegistrationStep2($requestData) {
        echo "123";
        $result = '';
        if (empty($requestData)) {
            return $result;
        }

        $userGuid = $requestData['uid'];
        $userProfileInfo = $this->User_model->userProfileDetails($userGuid);
        if (!empty($userProfileInfo)) {
            $this->db->trans_begin();
            $created = date(DATE_TIME_FORMAT);
            if (isset($requestData['state_text']) && isset($requestData['city_text'])) {

                $stateInfo = array('name' => $requestData['state_text'], 'country_id' => $requestData['country']);
                $stateId = $this->User_model->insertStateInfo($stateInfo);
                $requestData['state'] = $stateId;

                $cityInfo = array('name' => $requestData['city_text'], 'state_id' => $stateId);
                $cityId = $this->User_model->insertCityInfo($cityInfo);
                $requestData['city'] = $cityId;
            } elseif (!isset($requestData['state_text']) && isset($requestData['city_text'])) {

                $cityInfo = array('name' => $requestData['city_text'], 'state_id' => $requestData['state']);
                $cityId = $this->User_model->insertCityInfo($cityInfo);
                $requestData['city'] = $cityId;
            }

            if (isset($requestData['caste_text']) && !empty($requestData['caste_text'])) {
                $casteInfo = array('name' => $requestData['caste_text'], 'created' => date(DATE_TIME_FORMAT));
                $casteId = $this->User_model->insertCasteInfo($casteInfo);
                $requestData['caste'] = $casteId;
            }

            $userProfile = array('martial_status' => (isset($requestData['maritalstatus']) ? $requestData['maritalstatus'] : ''),
                'childs' => (isset($requestData['child']) ? $requestData['child'] : ''),
                'physical_status' => (isset($requestData['physicalstatus']) ? $requestData['physicalstatus'] : ''),
                'height' => (isset($requestData['height']) ? $requestData['height'] : ''),
                'bodytype' => (isset($requestData['bodytype']) ? $requestData['bodytype'] : ''),
                'complexion' => (isset($requestData['complexion']) ? $requestData['complexion'] : ''),
                'weight' => (isset($requestData['weight']) ? $requestData['weight'] : ''),
                'country' => (isset($requestData['country']) ? $requestData['country'] : ''),
                'state' => (isset($requestData['state']) ? $requestData['state'] : ''),
                'city' => (isset($requestData['city']) ? $requestData['city'] : ''),
                'last_updated' => $created,
                'last_updated_by' => $userGuid,
                'user_guid' => $userGuid
            );

            $updateProfile = $this->User_model->updateUserProfile($userGuid, $userProfile);

            if ($updateProfile) {
                $religionInfo = array('caste' => (isset($requestData['caste']) ? $requestData['caste'] : ''),
                    'sub_caste' => (isset($requestData['subcaste']) ? $requestData['subcaste'] : ''),
                    'gothram' => (isset($requestData['gothram']) ? $requestData['gothram'] : ''),
                    'dhosam' => (isset($requestData['dhosam']) ? $requestData['dhosam'] : ''),
                    'dhosam_type' => (isset($requestData['dhosam_type']) ? $requestData['dhosam_type'] : ''),
                    'star' => (isset($requestData['star']) ? $requestData['star'] : ''),
                    'raasi' => (isset($requestData['raasi']) ? $requestData['raasi'] : ''),
                    'last_updated' => $created,
                    'last_updated_by' => $userGuid,
                    'user_guid' => $userGuid
                );

                $religionExists = $this->User_model->getUserReligionInfo($userGuid);
                if ($religionExists[0]['guid']) {
                    $insertReligion = $this->User_model->updateReligionInfo($religionExists[0]['guid'], $religionInfo);
                } else {
                    $religionInfo['created'] = $created;
                    $religionInfo['created_by'] = $userGuid;
                    $religionInfo['guid'] = $this->Utils->getGuid();
                    $insertReligion = $this->User_model->insertReligionInfo($religionInfo);
                }

                $userFamily = array('father_status' => (isset($requestData['father_status']) ? $requestData['father_status'] : ''),
                    'mother_status' => (isset($requestData['mother_status']) ? $requestData['mother_status'] : ''),
                    'family_type' => (isset($requestData['ftype']) ? $requestData['ftype'] : ''),
                    'family_values' => (isset($requestData['fvalues']) ? $requestData['fvalues'] : ''),
                    'family_status' => (isset($requestData['fstatus']) ? $requestData['fstatus'] : ''),
                    'brother' => (isset($requestData['brother']) ? $requestData['brother'] : ''),
                    'sister' => (isset($requestData['sister']) ? $requestData['sister'] : ''),
                    'summary' => (isset($requestData['summary']) ? $requestData['summary'] : ''),
                    'last_updated' => $created,
                    'last_updated_by' => $userGuid,
                    'user_guid' => $userGuid
                );

                $familyExists = $this->User_model->getUserFamilyInfo($userGuid);
                if ($familyExists) {
                    $insertFamily = $this->User_model->updateFamilyInfo($userGuid, $userFamily);
                } else {
                    $userFamily['created'] = $created;
                    $userFamily['created_by'] = $userGuid;
                    $userFamily['guid'] = $this->Utils->getGuid();
                    $insertFamily = $this->User_model->insertFamilyInfo($userFamily);
                }

                $userHabbits = array('food' => (isset($requestData['food']) ? $requestData['food'] : ''),
                    'drink' => (isset($requestData['smoking']) ? $requestData['smoking'] : ''),
                    'smoke' => (isset($requestData['drinking']) ? $requestData['drinking'] : ''),
                    'last_updated' => $created,
                    'last_updated_by' => $userGuid,
                    'user_guid' => $userGuid
                );

                $habbitsExists = $this->User_model->getUserHabbitsInfo($userGuid);
                if ($habbitsExists) {
                    $insertHabbits = $this->User_model->updateHabbitsInfo($userGuid, $userHabbits);
                } else {
                    $userHabbits['created'] = $created;
                    $userHabbits['created_by'] = $userGuid;
                    $userHabbits['guid'] = $this->Utils->getGuid();
                    $insertHabbits = $this->User_model->insertHabbitsInfo($userHabbits);
                }

                $userOthers = array('user_guid' => $userGuid,
                    'education' => (isset($requestData['education']) ? $requestData['education'] : ''),
                    'qualification' => (isset($requestData['qualification']) ? $requestData['qualification'] : ''),
                    'occupation' => (isset($requestData['occupation']) ? $requestData['occupation'] : ''),
                    'employeed_in' => (isset($requestData['emp_in']) ? $requestData['emp_in'] : ''),
                    'income' => (isset($requestData['income']) ? $requestData['income'] : ''),
                    'salary' => (isset($requestData['salary']) ? $requestData['salary'] : ''),
                    'last_updated' => $created,
                    'last_updated_by' => $userGuid
                );

                $otherExists = $this->User_model->getUserOtherInfo($userGuid);
                if ($otherExists) {
                    $insertOther = $this->User_model->updateOtherInfo($userGuid, $userOthers);
                } else {
                    $userOthers['created'] = $created;
                    $userOthers['created_by'] = $userGuid;
                    $userOthers['guid'] = $this->Utils->getGuid();
                    $insertOther = $this->User_model->insertOtherInfo($userOthers);
                }
            }
            if (empty($updateProfile) || empty($insertReligion) || empty($insertFamily) || empty($insertHabbits) || empty($insertOther)) {
                $this->db->trans_rollback();
                $result['message'] = 'Registration failed please try afer some time';
                $result['url'] = 'index.php/admin/userinfo?uid=' . $userGuid;
            } else {
                $this->db->trans_commit();
                $result['message'] = "Success";
                $result['url'] = 'index.php/admin/userphoto?uid=' . $userGuid;
            }
        } else {
            $result['message'] = "Profile details not fount";
            $result['url'] = 'index.php/admin/useradd';
        }
        return $result;
    }

    public function userRegistrationStep3($requestData) {
        $result = '';
        if (empty($requestData)) {
            return $result;
        }

        $userGuid = $requestData['uid'];

        if (isset($requestData['state_text']) && isset($requestData['city_text'])) {

            $stateInfo = array('name' => $requestData['state_text'], 'country_id' => $requestData['country']);
            $stateId = $this->User_model->insertStateInfo($stateInfo);
            $requestData['state'] = $stateId;

            $cityInfo = array('name' => $requestData['city_text'], 'state_id' => $stateId);
            $cityId = $this->User_model->insertCityInfo($cityInfo);
            $requestData['city'] = $cityId;
        } elseif (!isset($requestData['state_text']) && isset($requestData['city_text'])) {

            $cityInfo = array('name' => $requestData['city_text'], 'state_id' => $requestData['state']);
            $cityId = $this->User_model->insertCityInfo($cityInfo);
            $requestData['city'] = $cityId;
        }

        $partnerInfo = array('age_from' => $requestData['age_from'],
            'age_to' => $requestData['age_to'],
            'maritalstatus' => $requestData['maritalstatus'],
            'height' => $requestData['height'],
            'physicalstatus' => $requestData['physicalstatus'],
            'food' => isset($requestData['food']) ? $requestData['food'] : 0,
            'smoking' => isset($requestData['smoking']) ? $requestData['smoking'] : 0,
            'drinking' => isset($requestData['drinking']) ? $requestData['drinking'] : 0,
            'caste' => $requestData['caste'],
            'dhosam' => isset($requestData['dhosam']) ? $requestData['dhosam'] : 0,
            'dhosam_type' => isset($requestData['dhosam_type']) ? $requestData['dhosam_type'] : '',
            'star' => isset($requestData['star']) ? $requestData['star'] : '',
            'country' => isset($requestData['country']) ? $requestData['country'] : 0,
            'state' => isset($requestData['state']) ? $requestData['state'] : 0,
            'city' => isset($requestData['city']) ? $requestData['city'] : 0,
            'education' => $requestData['education'],
            'qualification' => $requestData['qualification'],
            'occupation' => $requestData['occupation'],
            'employeed_in' => isset($requestData['emp_in']) ? $requestData['emp_in'] : 0,
            'salary' => isset($requestData['salary']) ? $requestData['salary'] : ''
        );
        $created = date(DATE_TIME_FORMAT);
        $insertPartnerInfo = array('user_guid' => $userGuid,
            'preference_info' => json_encode($partnerInfo),
            'last_updated' => $created,
            'last_updated_by' => $userGuid
        );
        $partnerExists = $this->User_model->getUserPartnerInfo($userGuid);

        if ($partnerExists[0]['guid']) {
            $insert = $this->User_model->updatePartnerInfo($partnerExists[0]['guid'], $insertPartnerInfo);
        } else {
            $insertPartnerInfo['created'] = $created;
            $insertPartnerInfo['created_by'] = $userGuid;
            $insertPartnerInfo['guid'] = $this->Utils->getGuid();
            $insert = $this->User_model->insertPartnerInfo($insertPartnerInfo);
        }
        if ($insert) {
            $result['message'] = 'Successfully added';
            $result['url'] = 'index.php/admin/dashboard?uid=' . $userGuid;
        } else {
            $result['message'] = 'Failed to add please try afer some time';
            $result['url'] = 'home/admin/userpartner?uid=' . $userGuid;
        }
        return $result;
    }

    /**
     * get not verified image based on user list
     * @todo using admin only
     */
    function getNotVerifyUserImageInfo() {

        $this->db->select('user_guid');

        $this->db->where('verify', 0);
        $this->db->where('deleted', 0);
        $this->db->group_by('user_guid');
        $query = $this->db->get('user_image_info');
        return $query->result_array();
    }
    
    /* Get Title*/
    function getTitle( $type ){
	        switch ($type) {
            case TYPE_1 : {
                    $data = $this->lang->line('users') . ' ' . $this->lang->line('detail_text');
                    break;
                }
            case TYPE_2 : {
                    $data = constant('GENDER_M') . ' ' . $this->lang->line('users');
                    break;
                }
            case TYPE_3 : {
                    $data = constant('GENDER_F') . ' ' . $this->lang->line('users');
                    break;
                }
            case TYPE_4 : {
                    $data = $this->lang->line('premium') . ' ' . $this->lang->line('users');
                    break;
                }
            case TYPE_5 : {
                    $data = $this->lang->line('active') . ' ' . $this->lang->line('users');
                    break;
                }
            case TYPE_6 : {
                    $data = $this->lang->line('deactive') . ' ' . $this->lang->line('users');
                    break;
                }
            case TYPE_7 : {
                    $data = $this->lang->line('delete') . ' ' . $this->lang->line('users');
                    break;
                }
            case TYPE_8 : {
                    $data = $this->lang->line('platinum_member_text');
                    break;
                }
            case TYPE_9 : {
                    $data = $this->lang->line('diamond_member_text');
                    break;
                }
            case TYPE_10 : {
                    $data = $this->lang->line('gold_member_text');
                    break;
                }
            case TYPE_11 : {
                    $data = $this->lang->line('profile_approved');
                    break;
                }
            case TYPE_12 : {
                    $data = $this->lang->line('success_story_title');
                    break;
                }
            case TYPE_13 : {
                    $data = $this->lang->line('unpaid-users');
                    break;
                }
            case TYPE_14 : {
                    $data = $this->lang->line('verify_image');
                    break;
                }
            case TYPE_15 : {
                    $data = 'Incomplete' . ' ' . $this->lang->line('users');
                    break;
                }
            Default: {
                    $data = $this->lang->line('users') . ' ' . $this->lang->line('detail_text');
                }
        }	
        return $data;
	}

}
