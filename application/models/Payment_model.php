<?php

Class Payment_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function insertTransaction($data) {
        $result = 0;
        if (empty($data)) {
            return $result;
        }

        $result = $this->db->insert('payment_details', $data);
        return $result;
    }

    function getPaymentMembershipInfo($userId = '') {
        $this->db->select('*');

        if ($userId) {
            $this->db->where('user_id', $userId);
        }
        $query = $this->db->get('payment_details');
        return $query->result_array();
        //echo $this->db->last_query();exit;
    }

}

?>
