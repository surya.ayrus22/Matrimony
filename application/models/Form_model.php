<?php

Class Form_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function casteDetails($id = '') {
        $this->db->select('id, name');

        if ($id) {
            $this->db->where('id', $id);
        }
        $query = $this->db->get('caste_info');
        return $query->result_array();
    }

    function countryDetails($cid = '') {
        $this->db->select('id, name');

        if ($cid) {
            $this->db->where('id', $cid);
        }
        $query = $this->db->get('countries');
        return $query->result_array();
    }

    function stateDetails($cid = '', $id = '') {
        $this->db->select('id, name');
        if ($cid) {
            $this->db->where('country_id', $cid);
        }
        if ($id) {
            $this->db->where('id', $id);
        }
        $query = $this->db->get('states');
        return $query->result_array();
    }

    function cityDetails($cid = '', $id = '') {
        $this->db->select('id, name');
        if ($cid) {
            $this->db->where('state_id', $cid);
        }
        if ($id) {
            $this->db->where('id', $id);
        }
        $query = $this->db->get('cities');
        return $query->result_array();
    }

    function educationInfo($id = '') {
        $this->db->select('id, name');

        if ($id) {
            $this->db->where('id', $id);
        }
        $query = $this->db->get('eduction_info');
        return $query->result_array();
    }

    function insertMembershipInfo($data) {
        $result = 0;
        if (empty($data)) {
            return $result;
        }

        $result = $this->db->insert('membership_info', $data);
        return $result;
    }

    function updateMembershipInfo($id, $data) {
        $result = 0;
        if (empty($id) || empty($data)) {
            return $result;
        }
        $this->db->where('id', $id);
        $result = $this->db->update('membership_info', $data);
        return $result;
    }

    function getMembershipInfo($id = '') {
        $this->db->select('id, name, month, ind_currency, usd_currency, default_currency');

        if ($id) {
            $this->db->where('id', $id);
        }
        $query = $this->db->get('membership_info');
        return $query->result_array();
    }

    function insertFeedbackInfo($data) {
        $result = 0;
        if (empty($data)) {
            return $result;
        }

        $result = $this->db->insert('feedback', $data);
        return $result;
    }

}

?>
