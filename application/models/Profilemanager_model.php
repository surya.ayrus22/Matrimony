<?php

Class Profilemanager_model extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->model('User_model', '', TRUE);
        $this->load->model('Form_model', '', TRUE);
        $this->load->model('Payment_model', '', TRUE);
        $this->load->model('Utils', '', TRUE);
        $this->load->library('session');
    }

    public function profileEditData($profile, $userGuid = '') {
        $data = array();
        if (empty($profile)) {
            return $data;
        }

        $data = $profile;

        $created = (!empty($profile['profile_created']) ? $profile['profile_created'] : '');
        $data['profile_created'] = $created;
        if ($created == TEXT_SON || $created == TEXT_DAUGHTER) {
            $data['profile_created'] = TEXT_PARENT;
        }

        if (!empty($profile['caste'])) {
            $casteDetails = $this->Form_model->casteDetails($profile['caste']);
            if (!empty($casteDetails[0]['name'])) {
                $data['caste_name'] = $casteDetails[0]['name'];
            }
        }
        if (!empty($profile['education'])) {
            $educationDetails = $this->Form_model->educationInfo($profile['education']);
            if (!empty($educationDetails[0]['name'])) {
                $data['edu_name'] = $educationDetails[0]['name'];
            }
        }
        if (!empty($profile['country'])) {
            $countryDetails = $this->Form_model->countryDetails($profile['country']);
            if (!empty($countryDetails[0]['name'])) {
                $data['country_name'] = $countryDetails[0]['name'];
            }
        }
        if (!empty($profile['state'])) {
            $stateDetails = $this->Form_model->stateDetails('', $profile['state']);
            if (!empty($stateDetails[0]['name'])) {
                $data['state_name'] = $stateDetails[0]['name'];
            }
        }
        if (!empty($profile['city'])) {
            $cityDetails = $this->Form_model->cityDetails('', $profile['city']);
            if (!empty($cityDetails[0]['name'])) {
                $data['city_name'] = $cityDetails[0]['name'];
            }
        }
        if (!empty($userGuid)) {
            $shortList = $this->User_model->ProfileRequestInfo($userGuid, $profile['userGuid'], '', REQUEST_SHORTLIST);
            if (!empty($shortList[0]['guid'])) {
                $data['shortlist'] = 1;
            }
        }

        if (!empty($profile['salary'])) {
            $salary = $profile['salary'];
            if ($salary == 1) {
                $data['salary_view'] = '100000 or less';
            } elseif ($salary == 9) {
                $data['salary_view'] = '900000 and above';
            } elseif ($salary == 10) {
                $data['salary_view'] = 'Any';
            } else {
                $data['salary_view'] = $profile['salary'] . '00000 To ' . ($profile['salary'] + 1) . '00000 Laks';
            }
        }
        $data1 = $this->Profilemanager_model->userRequestViewData($profile['userGuid']);
        $data = array_merge($data, $data1);
        return $data;
    }

    public function prefrenceEditData($profile) {
        $data = array();
        if (empty($profile)) {
            return $data;
        }
        $data = $profile;

        $created = (!empty($profile['profile_created']) ? $profile['profile_created'] : '');
        $data['profile_created'] = $created;
        if ($created == TEXT_SON || $created == TEXT_DAUGHTER) {
            $data['profile_created'] = TEXT_PARENT;
        }

        if (!empty($profile['caste'])) {
            $casteDetails = $this->Form_model->casteDetails($profile['caste']);
            if (!empty($casteDetails[0]['name'])) {
                $data['caste_name'] = $casteDetails[0]['name'];
            }
        }
        if (!empty($profile['education'])) {
            $educationDetails = $this->Form_model->educationInfo($profile['education']);
            if (!empty($educationDetails[0]['name'])) {
                $data['edu_name'] = $educationDetails[0]['name'];
            }
        }
        $data['country_name'] = 'Any';
        if (!empty($profile['country'])) {
            $countryDetails = $this->Form_model->countryDetails($profile['country']);
            if (!empty($countryDetails[0]['name'])) {
                $data['country_name'] = $countryDetails[0]['name'];
            }
        }
        $data['state_name'] = 'Any';
        if (!empty($profile['state'])) {
            $stateDetails = $this->Form_model->stateDetails('', $profile['state']);
            if (!empty($stateDetails[0]['name'])) {
                $data['state_name'] = $stateDetails[0]['name'];
            }
        }
        $data['city_name'] = 'Any';
        if (!empty($profile['city'])) {
            $cityDetails = $this->Form_model->cityDetails('', $profile['city']);
            if (!empty($cityDetails[0]['name'])) {
                $data['city_name'] = $cityDetails[0]['name'];
            }
        }
        if (!empty($profile['salary'])) {
            $salary = $profile['salary'];
            if ($salary == 1) {
                $data['salary_view'] = '100000 or less';
            } elseif ($salary == 9) {
                $data['salary_view'] = '900000 and above';
            } elseif ($salary == 10) {
                $data['salary_view'] = 'Any';
            } else {
                $data['salary_view'] = $profile['salary'] . '00000 To ' . ($profile['salary'] + 1) . '00000 Laks';
            }
        }
        return $data;
    }

    public function userRequestViewData($reqGuid) {
        $data = array();
        if (empty($reqGuid)) {
            return $data;
        }

        $userGuid = $this->session->userdata['userguid'];
        $data = array();
        /* $requestFrom = $this->User_model->ProfileRequestInfo($userGuid, $reqGuid, '', REQUEST_INTEREST);
          if (!empty( $requestFrom )){
          foreach ( $requestFrom as $request) {
          $data['interest'] = REQUEST_INTEREST;
          $data['interest_name'] = REQUEST_TYPE_DEFAULT;
          }
          }


          $notIntFrom = $this->User_model->ProfileRequestInfo($userGuid, $reqGuid, '', REQUEST_NOT_INETREST);
          if (!empty( $requestFrom )){
          foreach ( $requestFrom as $request) {
          $data['interest'] = REQUEST_ACCEPT;
          $data['interest_name'] = REQUEST_TYPE_1;
          }
          }

          $acceptFrom = $this->User_model->ProfileRequestInfo($userGuid, $reqGuid, '', REQUEST_ACCEPT);
          if (!empty( $acceptFrom )){
          foreach ( $acceptFrom as $accept) {
          $data['interest'] = REQUEST_ARRANGED;
          $data['interest_name'] = REQUEST_TYPE_8;
          }
          } */

        $requestTo = $this->User_model->ProfileRequestInfo($reqGuid, $userGuid, '', REQUEST_INTEREST);
        if (!empty($requestTo)) {
            $data['interest'] = REQUEST_ACCEPT;
            $data['interest_name'] = REQUEST_TYPE_1;
        } else {
            $requestFrom = $this->User_model->ProfileRequestInfo($userGuid, $reqGuid, '', REQUEST_INTEREST);
            if (!empty($requestFrom)) {
                $data['interest'] = REQUEST_INTEREST;
                $data['interest_name'] = REQUEST_TYPE_DEFAULT;
            }
        }

        $acceptTo = $this->User_model->ProfileRequestInfo($reqGuid, $userGuid, '', REQUEST_ACCEPT);
        if (!empty($acceptTo)) {
            $data['interest'] = REQUEST_ARRANGED;
            $data['interest_name'] = REQUEST_TYPE_8;
        } else {
            $acceptFrom = $this->User_model->ProfileRequestInfo($userGuid, $reqGuid, '', REQUEST_ACCEPT);
            if (!empty($acceptFrom)) {
                $data['interest'] = REQUEST_ARRANGED;
                $data['interest_name'] = REQUEST_TYPE_8;
            }
        }

        $notIntTo = $this->User_model->ProfileRequestInfo($reqGuid, $userGuid, '', REQUEST_NOT_INETREST);
        if (!empty($notIntTo)) {
            $data['interest'] = REQUEST_NOT_INETREST;
            $data['interest_name'] = REQUEST_TYPE_DEFAULT_1;
        } else {
            $notIntFrom = $this->User_model->ProfileRequestInfo($userGuid, $reqGuid, '', REQUEST_NOT_INETREST);
            if (!empty($notIntFrom)) {
                $data['interest'] = REQUEST_NOT_INETREST;
                $data['interest_name'] = REQUEST_TYPE_DEFAULT_1;
            }
        }

        return $data;
    }

    /**
     * insert or update ProfileRequestInfo table records 
     * if inactive records already available then active that account otherwise insert the record
     * @param unknown $from
     * @param unknown $to
     * @param unknown $activityId
     */
    public function insertOrUpdateProfileRequestInfo($from, $to, $activityId) {
        $result = 0;
        if (empty($from) || empty($to) || empty($activityId)) {
            return $result;
        }
        /** from and to user already arranged then don't insert and update* */
//  	$arranged = $this->User_model->ProfileRequestDetails( $from,  $to, REQUEST_ARRANGED );
//  	if ( empty( $arranged )) {
        if ($activityId != REQUEST_SHORTLIST && $activityId != REQUEST_SEND) {
            $id = array(REQUEST_INTEREST, REQUEST_ACCEPT, REQUEST_ARRANGED, REQUEST_NOT_INETREST);
            $existsId = $this->User_model->ProfileRequestInfo($from, $to, '', $id);
            if ($existsId) {
                $guidList = array();
                foreach ($existsId as $val) {
                    $guidList[] = $val['guid'];
                }

                if (!empty($guidList)) {
                    $updateId = array('deleted' => 1, 'last_updated' => date(DATE_TIME_FORMAT), 'last_updated_by' => $from);
                    $resultId = $this->User_model->updateProfileRequestInfo($guidList, $updateId);
                }
            }
        }
        $exists = $this->User_model->ProfileRequestDetails($from, $to, $activityId);
        if (!empty($exists[0]['guid'])) {
            $update = array('deleted' => 0, 'activity' => $activityId, 'last_updated' => date(DATE_TIME_FORMAT), 'last_updated_by' => $from);
            $result = $this->User_model->updateProfileRequestInfo($exists[0]['guid'], $update);
        } else {
            $insert = array('request_from' => $from,
                'request_to' => $to,
                'activity' => $activityId,
                'last_updated' => date(DATE_TIME_FORMAT),
                'last_updated_by' => $from,
                'created' => date(DATE_TIME_FORMAT),
                'created_by' => $from,
                'guid' => $this->Utils->getGuid()
            );

            $result = $this->User_model->insertProfileRequestInfo($insert);
        }
//  	}else{
//  		$result = 1;
//  	}
        return $result;
    }

    public function getSendMessageAndUserDetails($sessionUserGuid, $userListData, $conversation = 1) {
        $data = array();
        if (empty($sessionUserGuid) || empty($userListData)) {
            return $data;
        }

        $result1 = $this->User_model->getUserinformation($userListData);

        if (!empty($result1)) {
            foreach ($result1 as $value) {
                $data[$value['userGuid']] = $this->Profilemanager_model->userRequestViewData($value['userGuid']);
                $data[$value['userGuid']]['userId'] = $value['userId'];
                $data[$value['userGuid']]['userGuid'] = $value['userGuid'];
                $data[$value['userGuid']]['username'] = $value['username'];
                $data[$value['userGuid']]['age'] = $value['age'];
                $data[$value['userGuid']]['height'] = $value['height'];
                $data[$value['userGuid']]['gender'] = $value['gender'];
                $data[$value['userGuid']]['religion'] = $value['religion'];
                $data[$value['userGuid']]['education'] = $value['education'];
                $data[$value['userGuid']]['qualification'] = $value['qualification'];
                $data[$value['userGuid']]['occupation'] = $value['occupation'];

                $data[$value['userGuid']]['log_in'] = 'Not Login';
                $educationDetails = $this->Form_model->educationInfo($value['education']);
                if (!empty($educationDetails[0]['name'])) {
                    $data[$value['userGuid']]['edu_name'] = $educationDetails[0]['name'];
                }

                $loginActivity = $this->User_model->getLoginActivityInfo($value['userGuid'], 1);
                if (!empty($loginActivity[0]['client_date'])) {
                    $data[$value['userGuid']]['log_in'] = $this->User_model->relativeTime(strtotime($loginActivity[0]['client_date']));
                }

                /** get active  and verified image  if not active then get default image */
                $getUserImagesInfo = $this->getActiveProfileImage($value['userGuid'], $active = 1, $verified = 1);
                $data[$value['userGuid']]['image'] = $getUserImagesInfo['image'];
                $data[$value['userGuid']]['image_request'] = $getUserImagesInfo['image_request'];

                $activity = $this->User_model->ProfileRequestInfo($sessionUserGuid, $value['userGuid'], '', '', '', $conversation);
                if (!empty($activity)) {
                    foreach ($activity as $val1) {
                        if (!empty($val1['activity'])) {
                            if ($val1['activity'] != REQUEST_SHORTLIST && $val1['activity'] != REQUEST_IGNORE && $val1['activity'] != REQUEST_BLOCK) {
                                $data[$value['userGuid']]['activity'][] = (!empty($val1['activity']) ? $val1['activity'] : '');
                                $data[$value['userGuid']]['date'][] = (!empty($val1['last_updated']) ? date('d-M-Y H:i:s', strtotime($val1['last_updated'])) : '');
                            }
                        }
                    }
                }

                $activityAccept = $this->User_model->ProfileRequestInfo($sessionUserGuid, $value['userGuid'], '', REQUEST_ACCEPT, '', $conversation);
                if (!empty($activityAccept)) {
                    foreach ($activityAccept as $valAccept) {
                        $data[$value['userGuid']]['accept'] = (!empty($valAccept['activity']) ? $valAccept['activity'] : '');
                        $data[$value['userGuid']]['date_accept'] = (!empty($valAccept['last_updated']) ? date('d-M-Y H:i:s', strtotime($valAccept['last_updated'])) : '');
                    }
                }

                $activityNotInterest = $this->User_model->ProfileRequestInfo($sessionUserGuid, $value['userGuid'], '', REQUEST_NOT_INETREST, '', $conversation);
                if (!empty($activityNotInterest)) {
                    foreach ($activityNotInterest as $valNotInterest) {
                        $data[$value['userGuid']]['notinterest'] = (!empty($valNotInterest['activity']) ? $valNotInterest['activity'] : '');
                        $data[$value['userGuid']]['date_notinterest'] = (!empty($valNotInterest['last_updated']) ? date('d-M-Y H:i:s', strtotime($valNotInterest['last_updated'])) : '');
                    }
                }

                $activityReplied = $this->User_model->ProfileRequestInfo($sessionUserGuid, $value['userGuid'], '', REQUEST_SEND, '', $conversation);
                if (!empty($activityReplied)) {
                    foreach ($activityReplied as $valReplied) {
                        $data[$value['userGuid']]['replied'] = (!empty($valReplied['activity']) ? $valReplied['activity'] : '');
                        $data[$value['userGuid']]['date_replied'] = (!empty($valReplied['last_updated']) ? date('d-M-Y H:i:s', strtotime($valReplied['last_updated'])) : '');
                    }
                }
            }
        }
        return $data;
    }

    public function getSendActivityDetails($sessionUserGuid, $userListData, $from_msg = '') {

        $data = array();
        if (empty($sessionUserGuid) || empty($userListData)) {
            return $data;
        }

        $result = $this->getSendMessageAndUserDetails($sessionUserGuid, $userListData, $from_msg);

        if (!empty($result)) {
            $i = 0;
            foreach ($result as $value) {
                if (isset($value['accept']) && !empty($value['accept'])) {
                    $data['accept'][$i] = $value;
                }

                if (isset($value['notinterest']) && !empty($value['notinterest'])) {
                    $data['notinterest'][$i] = $value;
                }

                if (isset($value['replied']) && !empty($value['replied'])) {
                    $data['replied'][$i] = $value;
                    $conversation = $this->getConversationMessageDetails($sessionUserGuid, $value['userGuid'], $from_msg);
                    if (!empty($conversation)) {
                        foreach ($conversation as $conv) {
                            if (!empty($conv['count'])) {
                                $data['replied'][$i]['count'] = $conv['count'];
                            }
                        }
                    }
                }
                $i++;
            }
        }
        return $data;
    }

    public function getInboxMessageAndUserDetails($sessionUserGuid, $userListData, $conversation = '') {
        $data = array();
        if (empty($sessionUserGuid) || empty($userListData)) {
            return $data;
        }
        $gender = GENDER_MALE;
        if (strtoupper($this->session->userdata['gender']) == GENDER_MALE) {
            $gender = GENDER_FEMALE;
        }
        $result1 = $this->User_model->getUserinformation($userListData);

        if (!empty($result1)) {
            foreach ($result1 as $value) {
                $data[$value['userGuid']] = $this->Profilemanager_model->userRequestViewData($value['userGuid']);
                $data[$value['userGuid']]['userId'] = $value['userId'];
                $data[$value['userGuid']]['userGuid'] = $value['userGuid'];
                $data[$value['userGuid']]['username'] = $value['username'];
                $data[$value['userGuid']]['age'] = $value['age'];
                $data[$value['userGuid']]['height'] = $value['height'];
                $data[$value['userGuid']]['gender'] = $value['gender'];
                $data[$value['userGuid']]['religion'] = $value['religion'];
                $data[$value['userGuid']]['education'] = $value['education'];
                $data[$value['userGuid']]['qualification'] = $value['qualification'];
                $data[$value['userGuid']]['occupation'] = $value['occupation'];

                $data[$value['userGuid']]['log_in'] = 'Not Login';
                $educationDetails = $this->Form_model->educationInfo($value['education']);
                if (!empty($educationDetails[0]['name'])) {
                    $data[$value['userGuid']]['edu_name'] = $educationDetails[0]['name'];
                }

                $loginActivity = $this->User_model->getLoginActivityInfo($value['userGuid'], 1);
                if (!empty($loginActivity[0]['client_date'])) {
                    $data[$value['userGuid']]['log_in'] = $this->User_model->relativeTime(strtotime($loginActivity[0]['client_date']));
                }

                /** get active  and verified image  if not active then get default image */
                $getUserImagesInfo = $this->getActiveProfileImage($value['userGuid'], $active = 1, $verified = 1);
                $data[$value['userGuid']]['image'] = $getUserImagesInfo['image'];
                $data[$value['userGuid']]['image_request'] = $getUserImagesInfo['image_request'];

                $activity = $this->User_model->ProfileRequestInfo($value['userGuid'], $sessionUserGuid, '', '', '', '', $conversation);
                if (!empty($activity)) {
                    foreach ($activity as $val1) {
                        if (!empty($val1['activity'])) {
                            if ($val1['activity'] != REQUEST_SHORTLIST && $val1['activity'] != REQUEST_IGNORE && $val1['activity'] != REQUEST_BLOCK) {
                                $data[$value['userGuid']]['activity'][] = (!empty($val1['activity']) ? $val1['activity'] : '');
                                $data[$value['userGuid']]['date'][] = (!empty($val1['last_updated']) ? date('d-M-Y H:i:s', strtotime($val1['last_updated'])) : '');
                            }
                        }
                    }
                }

                $activityAccept = $this->User_model->ProfileRequestInfo($value['userGuid'], $sessionUserGuid, '', REQUEST_ACCEPT, '', '', $conversation);
                if (!empty($activityAccept)) {
                    foreach ($activityAccept as $valAccept) {
                        $data[$value['userGuid']]['accept'] = (!empty($valAccept['activity']) ? $valAccept['activity'] : '');
                        $data[$value['userGuid']]['date_accept'] = (!empty($valAccept['last_updated']) ? date('d-M-Y H:i:s', strtotime($valAccept['last_updated'])) : '');
                    }
                }

                $activityNotInterest = $this->User_model->ProfileRequestInfo($value['userGuid'], $sessionUserGuid, '', REQUEST_NOT_INETREST, '', '', $conversation);
                if (!empty($activityNotInterest)) {
                    foreach ($activityNotInterest as $valNotInterest) {
                        $data[$value['userGuid']]['notinterest'] = (!empty($valNotInterest['activity']) ? $valNotInterest['activity'] : '');
                        $data[$value['userGuid']]['date_notinterest'] = (!empty($valNotInterest['last_updated']) ? date('d-M-Y H:i:s', strtotime($valNotInterest['last_updated'])) : '');
                    }
                }

                $activityReplied = $this->User_model->ProfileRequestInfo($value['userGuid'], $sessionUserGuid, '', REQUEST_SEND, '', '', $conversation);
                if (!empty($activityReplied)) {
                    foreach ($activityReplied as $valReplied) {
                        $data[$value['userGuid']]['replied'] = (!empty($valReplied['activity']) ? $valReplied['activity'] : '');
                        $data[$value['userGuid']]['date_replied'] = (!empty($valReplied['last_updated']) ? date('d-M-Y H:i:s', strtotime($valReplied['last_updated'])) : '');
                    }
                }
            }
        }
        return $data;
    }

    public function getInboxActivityDetails($sessionUserGuid, $userListData, $to_msg = '') {

        $data = array();
        if (empty($sessionUserGuid) || empty($userListData)) {
            return $data;
        }

        $result = $this->getInboxMessageAndUserDetails($sessionUserGuid, $userListData, $to_msg);
        if (!empty($result)) {
            $i = 0;
            foreach ($result as $value) {
                if (isset($value['accept']) && !empty($value['accept'])) {
                    $data['accept'][$i] = $value;
                }

                if (isset($value['notinterest']) && !empty($value['notinterest'])) {
                    $data['notinterest'][$i] = $value;
                }

                if (isset($value['replied']) && !empty($value['replied'])) {
                    $data['replied'][$i] = $value;
                    $conversation = $this->getConversationMessageDetails($value['userGuid'], $sessionUserGuid, '', $to_msg);
                    if (!empty($conversation)) {
                        foreach ($conversation as $conv) {
                            if (!empty($conv['count'])) {
                                $data['replied'][$i]['count'] = $conv['count'];
                            }
                        }
                    }
                }
                $i++;
            }
        }
        return $data;
    }

    public function getConversationMessageDetails($from, $to, $from_msg = '', $to_msg = '') {
        $data = array();
        if (empty($from) || empty($to)) {
            return $data;
        }

        $activity = $this->User_model->ProfileRequestInfo($from, $to, '', '', '', $from_msg, $to_msg);
        if (!empty($activity)) {
            $i = 0;
            foreach ($activity as $val2) {
                if (!empty($val2['activity'])) {
                    if ($val2['activity'] != REQUEST_SHORTLIST && $val2['activity'] != REQUEST_IGNORE && $val2['activity'] != REQUEST_BLOCK) {
                        if ($val2['activity'] == REQUEST_SEND) {
                            $userConversation = $this->Mail->userMailConversationInfo($from, $to, '', $from_msg, $to_msg);
                            if (!empty($userConversation)) {
                                $data[$i]['message'] = $userConversation;
                                $data[$i]['count'] = count($userConversation);
                            }
                        }
                        $data[$i]['activity'] = (!empty($val2['activity']) ? $val2['activity'] : '');
                        $data[$i]['date'] = (!empty($val2['last_updated']) ? date('d-M-Y H:i:s', strtotime($val2['last_updated'])) : '');
                        $data[$i]['from'] = $from;
                        $i++;
                    }
                }
            }
        }

        return $data;
    }

    public function getManagedUserInformation($userid = '') {
        $data = array();
        /* if ( empty( $userid)){
          return $data;
          } */

        $userInfo = $this->User_model->getUserinformation($userid);
        if (!empty($userInfo)) {
            foreach ($userInfo as $val) {
                $data[$val['userGuid']] = $this->Profilemanager_model->userRequestViewData($val['userGuid']);
                $data[$val['userGuid']]['userId'] = $val['userId'];
                $data[$val['userGuid']]['userGuid'] = $val['userGuid'];
                $data[$val['userGuid']]['email'] = $val['email'];
                $data[$val['userGuid']]['username'] = $val['username'];
                $data[$val['userGuid']]['mobile'] = $val['mobile'];
                $data[$val['userGuid']]['age'] = $val['age'];
                $data[$val['userGuid']]['height'] = $val['height'];
                $data[$val['userGuid']]['gender'] = $val['gender'];
                $data[$val['userGuid']]['religion'] = $val['religion'];
                $data[$val['userGuid']]['education'] = $val['education'];
                $data[$val['userGuid']]['qualification'] = $val['qualification'];
                $data[$val['userGuid']]['occupation'] = $val['occupation'];
                $data[$val['userGuid']]['log_in'] = 'Not Login';

                $educationDetails = $this->Form_model->educationInfo($val['education']);
                if (!empty($educationDetails[0]['name'])) {
                    $data[$val['userGuid']]['edu_name'] = $educationDetails[0]['name'];
                }

                $loginActivity = $this->User_model->getLoginActivityInfo($val['userGuid'], 1);
                if (!empty($loginActivity[0]['client_date'])) {
                    $data[$val['userGuid']]['log_in'] = $this->User_model->relativeTime(strtotime($loginActivity[0]['client_date']));
                }

                if (!empty($val['country'])) {
                    $countryDetails = $this->Form_model->countryDetails($val['country']);
                    if (!empty($countryDetails[0]['name'])) {
                        $data[$val['userGuid']]['country_name'] = $countryDetails[0]['name'];
                    }
                }
                if (!empty($val['state'])) {
                    $stateDetails = $this->Form_model->stateDetails('', $val['state']);
                    if (!empty($stateDetails[0]['name'])) {
                        $data[$val['userGuid']]['state_name'] = $stateDetails[0]['name'];
                    }
                }
                if (!empty($val['city'])) {
                    $cityDetails = $this->Form_model->cityDetails('', $val['city']);
                    if (!empty($cityDetails[0]['name'])) {
                        $data[$val['userGuid']]['city_name'] = $cityDetails[0]['name'];
                    }
                }
                /** get active  and verified image  if not active then get default image */
                $getUserImagesInfo = $this->getActiveProfileImage($val['userGuid'], $active = 1, $verified = 1);
                $data[$val['userGuid']]['image'] = $getUserImagesInfo['image'];
                $data[$val['userGuid']]['image_request'] = $getUserImagesInfo['image_request'];
            }
        }
        return $data;
    }

    public function getUserSendConversationInformation($sessionUserGuid, $userid, $mailconversation = '', $from_msg = '') {
        $data = array();
        if (empty($sessionUserGuid) || empty($userid)) {
            return $data;
        }

        $userInfo = $this->getManagedUserInformation($userid);

        if (!empty($mailconversation)) {
            if (!empty($userInfo)) {
                $i = 0;
                foreach ($userInfo as $key => $value) {
                    $data['conversation'][$i] = $value;
                    $userConversation = $this->Mail->userMailConversationInfo($sessionUserGuid, $userid, '', $from_msg);

                    if (!empty($userConversation)) {
                        $data['conversation'][$i]['message'] = $userConversation;
                        $data['conversation'][$i]['count'] = count($userConversation);
                    }
                    $i++;
                }
            }
        } else {
            if (!empty($userInfo)) {
                $i = 0;
                foreach ($userInfo as $key => $value) {
                    $data['conversation'][$i] = $value;
                    $msg = $this->getConversationMessageDetails($sessionUserGuid, $userid, $from_msg);

                    if (!empty($msg)) {
                        $data['conversation'][$i]['text_list'] = $msg;
                    }
                    $i++;
                }
            }
        }
        return $data;
    }

    public function getUserInboxConversationInformation($sessionUserGuid, $userid, $mailconversation = '', $to_msg = '') {
        $data = array();
        if (empty($sessionUserGuid) || empty($userid)) {
            return $data;
        }
        $userInfo = $this->getManagedUserInformation($userid);

        if (!empty($mailconversation)) {
            if (!empty($userInfo)) {
                $i = 0;
                foreach ($userInfo as $key => $value) {
                    $data['conversation'][$i] = $value;
                    $userConversation = $this->Mail->userMailConversationInfo($userid, $sessionUserGuid, '', '', $to_msg);
                    if (!empty($userConversation)) {
                        $data['conversation'][$i]['message'] = $userConversation;
                        $data['conversation'][$i]['count'] = count($userConversation);
                    }
                    $i++;
                }
            }
        } else {
            if (!empty($userInfo)) {
                $i = 0;
                foreach ($userInfo as $key => $value) {
                    $data['conversation'][$i] = $value;
                    $msg = $this->getConversationMessageDetails($userid, $sessionUserGuid, '', $to_msg);
                    if (!empty($msg)) {
                        $data['conversation'][$i]['text_list'] = $msg;
                    }
                    $i++;
                }
            }
        }
        return $data;
    }

    /**
     * Get user active Image - only one image
     * @param string $userGuid
     * @return array
     */
    public function getActiveProfileImage($userGuid, $active = 1, $verified = '') {
        $data['image'] = DEFAULT_IMAGE;
        $data['image_request'] = 1;
        if (empty($userGuid)) {
            return $data;
        }

        /** get active image */
        $getUserImagesInfo = $this->User_model->getUserImageInfo($userGuid, '', $active, '', $verified);

        /** get user information */
        $userdata = $this->User_model->getUserinformation($userGuid);
        $image = '';
        if (!empty($getUserImagesInfo[0]['image'])) {
            if (file_exists(dirname($_SERVER["SCRIPT_FILENAME"]) . "/assets/upload_images/" . $getUserImagesInfo[0]['image'])) {
                $image = $getUserImagesInfo[0]['image'];
            }
        }

        /** validate user gender if male then default image male elseif female then default image female otherwise default image showing */
        if (!empty($userdata['0']['gender'])) {
            $profileImage = DEFAULT_IMAGE;
            if (strtoupper($userdata['0']['gender']) == GENDER_MALE) {
                $profileImage = MALE_IMAGE;
            } elseif (strtoupper($userdata['0']['gender']) == GENDER_FEMALE) {
                $profileImage = FEMALE_IMAGE;
            }
            $data['image'] = $profileImage;
        }

        if (!empty($image)) {
            $data['image'] = $image;
            $data['image_request'] = 0;
        }

        return $data;
    }

    public function getUserInformation($userGuid = '', $gender = '', $limits = '') {
        $userdata = $this->User_model->getUserinformation($userGuid, '', $gender, $limits = '');
        $this->data = array();
        if (!empty($userdata)) {
            $j = 0;
            foreach ($userdata as $key => $matches) { //echo "<pre>"; print_r($userdata);exit;
                $this->data[$j]['userId'] = $matches['userId'];
                $this->data[$j]['username'] = $matches['username'];
                $this->data[$j]['userGuid'] = $matches['userGuid'];
                $this->data[$j]['email'] = $matches['email'];
                $this->data[$j]['gender'] = $matches['gender'];
                $this->data[$j]['age'] = $matches['age'];
                $this->data[$j]['height'] = $matches['height'];
                $this->data[$j]['qualification'] = $matches['qualification'];
                //$this->data[$j]['created']	= date('d-M-Y H:i:s', strtotime($matches['created']));
                $this->data[$j]['religion'] = $matches['religion'];
                $this->data[$j]['martialstatus'] = (!empty($matches['martial_status']) ? constant("MARITAL_STATUS_" . $matches['martial_status']) : '');
                $this->data[$j]['profile_created'] = $matches['profile_created'];

                $created = (!empty($matches['profile_created']) ? $matches['profile_created'] : '');
                $this->data[$j]['profile_created'] = $created;
                if ($created == TEXT_SON || $created == TEXT_DAUGHTER) {
                    $this->data[$j]['profile_created'] = TEXT_PARENT;
                }

                $casteDetail = $this->Form_model->casteDetails($matches['caste']);
                if (!empty($casteDetail[0]['name'])) {
                    $this->data[$j]['caste_name'] = $casteDetail[0]['name'];
                }

                $educationDetails = $this->Form_model->educationInfo($matches['education']);
                if (!empty($educationDetails[0]['name'])) {
                    $this->data[$j]['edu_name'] = $educationDetails[0]['name'];
                }

                /** get active  and verified image  if not active then get default image */
                $getUserImagesInfo = $this->getActiveProfileImage($matches['userGuid'], $active = 1, $verified = 1);
                $this->data[$j]['image'] = $getUserImagesInfo['image'];
                $this->data[$j]['image_request'] = $getUserImagesInfo['image_request'];

                $j++;
            }
        }

        return $this->data;
    }

}

?>
