<?php

Class Prefrence_model extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->model('User_model', '', TRUE);
    }

    /**
     * @todo need to change this method dynamically use getUserinformationData function in User_model
     * @param unknown $prefrenceData
     * @param unknown $gender
     * @param string $notUserGuid
     * @param string $userGuidList
     * @param string $limits
     */
    function getUserPrefrenceDetails($prefrenceData, $gender, $notUserGuid = '', $userGuidList = '', $limits = '', $userRoleguid = MEMBER_ROLE_ID) {
        $result = array();
        if (empty($prefrenceData) || empty($gender)) {
            return $result;
        }

        $ageFrom = (!empty($prefrenceData['age_from']) ? $prefrenceData['age_from'] : '');
        $ageTo = (!empty($prefrenceData['age_to']) ? $prefrenceData['age_to'] : '');
        $maritalstatus = (!empty($prefrenceData['maritalstatus']) ? $prefrenceData['maritalstatus'] : '');
        $height = (!empty($prefrenceData['height']) ? $prefrenceData['height'] : '');
        $physicalstatus = (!empty($prefrenceData['physicalstatus']) ? $prefrenceData['physicalstatus'] : '');
        $food = (!empty($prefrenceData['food']) ? $prefrenceData['food'] : '');
        $smoking = (!empty($prefrenceData['smoking']) ? $prefrenceData['smoking'] : '');
        $drinking = (!empty($prefrenceData['drinking']) ? $prefrenceData['drinking'] : '');
// 	$dhosam	 		= (!empty( $prefrenceData['dhosam'] ) ? $prefrenceData['dhosam'] :'');
// 	$star	 		= (!empty( $prefrenceData['star'] ) ? $prefrenceData['star'] :'');
        $country = (!empty($prefrenceData['country']) ? $prefrenceData['country'] : '');
        $state = (!empty($prefrenceData['state']) ? $prefrenceData['state'] : '');
        $city = (!empty($prefrenceData['city']) ? $prefrenceData['city'] : '');
        $education = (!empty($prefrenceData['education']) ? $prefrenceData['education'] : '');
// 	$qualification	= (!empty( $prefrenceData['qualification'] ) ? $prefrenceData['qualification'] :'');
// 	$occupation		= (!empty( $prefrenceData['occupation'] ) ? $prefrenceData['occupation'] :'');
// 	$salary			= (!empty( $prefrenceData['salary'] ) ? $prefrenceData['salary'] :'');
        $created = (!empty($prefrenceData['last_updated']) ? $prefrenceData['last_updated'] : '');
        $week = (!empty($prefrenceData['week']) ? $prefrenceData['week'] : '');
        $month = (!empty($prefrenceData['month']) ? $prefrenceData['month'] : '');
        $emp_in = (!empty($prefrenceData['employeed']) ? $prefrenceData['employeed'] : '');
        $userId = (!empty($prefrenceData['memberid']) ? $prefrenceData['memberid'] : '');
        $profileCreated = (!empty($prefrenceData['created']) ? $prefrenceData['created'] : '');

        $caste = 0;
        if (!empty($prefrenceData['caste']) && $prefrenceData['caste'] != 48 && $prefrenceData['caste'] != 49) {
            $caste = $prefrenceData['caste'];
        }

        /** get user information query */
        $query = $this->User_model->getUserinformationData($userGuidList, $userRoleguid, $notUserGuid);

        $query1 = '';

        if ($userId) {
            $query1.= ' AND user.id=' . $userId;
        }
        if ($profileCreated) {
            $query1.= ' AND profile.profile_created="' . $profileCreated . '"';
        }
        if (!empty($ageFrom) && !empty($ageTo)) {
            $query1.= ' AND profile.age BETWEEN "' . $ageFrom . '" AND "' . $ageTo . '"';
        }
        if ($maritalstatus) {
            $query1.= ' AND profile.martial_status="' . $maritalstatus . '"';
        }
        if (!empty($gender)) {
            $query1.= ' AND profile.gender="' . $gender . '"';
        }
        if ($gender == 'F') {
            if ($height) {
                $query1.= ' AND profile.height <=' . $height;
            }
        } else {
            if ($height) {
                $query1.= ' AND profile.height >=' . $height;
            }
        }
        if ($physicalstatus) {
            $query1.= ' AND profile.physical_status=' . $physicalstatus;
        }
        if ($country) {
            $query1.= ' AND profile.country = ' . $country;
        }
        if ($state) {
            $query1.= ' AND profile.state = ' . $state;
        }
        if ($city) {
            $query1.= ' AND profile.city = ' . $city;
        }
        if ($food) {
            $query1.= ' AND habbit.food = ' . $food;
        }
        if ($drinking) {
            $query1.= ' AND habbit.drink = ' . $drinking;
        }
        if ($smoking) {
            $query1.= ' AND habbit.smoke =' . $smoking;
        }
        if ($caste) {
            $query1.= ' AND reg.caste =' . $caste;
        }
        if ($education) {
            $query1.= ' AND edu.education =' . $education;
        }
        if ($month) {
            $query1.= ' AND profile.last_updated BETWEEN DATE_SUB( CURDATE() , INTERVAL 1 MONTH ) AND CURDATE()';
        }
        if ($week) {
            $query1.= ' AND profile.last_updated BETWEEN DATE_SUB( CURDATE() , INTERVAL 1 WEEK ) AND CURDATE()';
        }
        if ($emp_in) {
            $query1.= ' AND edu.employeed_in =' . $emp_in;
        }

        if ($created) {
            $query1.= ' AND profile.last_updated LIKE "%' . $created . '%"';
        }

        /* if( $dhosam ){
          $query1.= ' AND reg.dhosam='. $dhosam;
          }
          if( $star ){
          $query1.= ' AND reg.star ='. $star;
          }
          if( $qualification ){
          $query1.= ' AND edu.qualification='. $qualification;
          }
          if( $occupation ){
          $query1.= ' AND edu.occupation = '. $occupation;
          }
          if( $salary ){
          $query1.= ' AND edu.salary >='. $salary;
          } */

        $query1.= ' ORDER BY profile.last_updated DESC';

        if ($limits) {
            $query1.= ' LIMIT ' . $limits;
        }

        $res = $this->db->query($query . $query1);
        //echo $this->db->last_query();exit;

        /* 	if( $userId ){
          $this -> db ->where('user.id', $userId);
          }
          if( $profileCreated ){
          $this -> db ->where('profile.profile_created', $profileCreated);
          }
          if( !empty( $ageFrom ) &&  !empty( $ageTo ) ){
          $this -> db ->where('profile.age BETWEEN "'. $ageFrom. '" and "'. $ageTo.'"');
          }
          if( $maritalstatus ){
          $this -> db ->where('profile.martial_status', $maritalstatus);
          }
          if( $gender ){
          $this -> db ->where('profile.gender', $gender);
          }
          if ( $gender == 'F') {
          if( $height ){
          $this -> db ->where('profile.height <=', $height);
          }
          }else{
          if( $height ){
          $this -> db ->where('profile.height>=', $height);
          }
          }
          if( $physicalstatus ){
          $this -> db ->where('profile.physical_status', $physicalstatus);
          }
          if( $country ){
          $this -> db ->where('profile.country', $country);
          }
          if( $state ){
          $this -> db ->where('profile.state', $state);
          }
          if( $city ){
          $this -> db ->where('profile.city', $city);
          }
          if( $food ){
          $this -> db ->where('habbit.food', $food);
          }
          if( $drinking ){
          $this -> db ->where('habbit.drink', $drinking);
          }
          if( $smoking ){
          $this -> db ->where('habbit.smoke', $smoking);
          }
          if( $caste ){
          $this -> db ->where('reg.caste', $caste);
          }
          if( $education ){
          $this -> db ->where('edu.education', $education);
          }
          if( $week ){
          $this -> db ->where('profile.last_updated BETWEEN DATE_SUB( CURDATE() , INTERVAL 1 WEEK ) AND CURDATE()');
          }
          if( $month ){
          $this -> db ->where('profile.last_updated BETWEEN DATE_SUB( CURDATE() , INTERVAL 1 MONTH ) AND CURDATE()');
          }

          // 	if( $dhosam ){
          // 		$this -> db ->where('reg.dhosam', $dhosam);
          // 	}
          // 	if( $star ){
          // 		$this -> db ->where('reg.star', $star);
          // 	}
          // 	if( $qualification ){
          // 		$this -> db ->where('edu.qualification', $qualification);
          // 	}
          // 	if( $occupation ){
          // 		$this -> db ->where('edu.occupation', $occupation);
          // 	}
          if( $emp_in ){
          $this -> db ->where('edu.employeed_in', $emp_in);
          }
          // 	if( $salary ){
          // 		$this -> db ->where('edu.salary >=', $salary);
          // 	}

          if( $notUserGuid ){
          $this -> db ->where_not_in('user.guid ', $notUserGuid);
          }

          if( $userGuidList ){
          $this -> db ->where_in('user.guid ', $userGuidList);
          }

          if( $created ){
          $this -> db ->like('profile.last_updated', $created);
          }

          if( $limits ){
          $this -> db ->limit($limits);
          }

          $this -> db ->order_by('profile.last_updated', "DESC");
          $this -> db ->where('user.deleted', 0);
          $query = $this -> db -> get('user '); */
// 	echo $this->db->last_query();exit;
        return $res->result_array();
    }

    /**
     * this function used for count list
     * @todo need to change this method dynamically use getUserinformationData function in User_model
     * @param unknown $prefrenceData
     * @param unknown $gender
     * @param string $notUserGuid
     * @param string $casteData
     */
    function getUserSearchDetails($prefrenceData, $gender, $notUserGuid = '', $casteData = '', $userRoleguid = MEMBER_ROLE_ID) {
        $result = array();
        if (empty($prefrenceData) || empty($gender)) {
            return $result;
        }

        $ageFrom = (!empty($prefrenceData['age_from']) ? $prefrenceData['age_from'] : '');
        $ageTo = (!empty($prefrenceData['age_to']) ? $prefrenceData['age_to'] : '');
        $maritalstatus = (!empty($prefrenceData['maritalstatus']) ? $prefrenceData['maritalstatus'] : '');
        $physicalstatus = (!empty($prefrenceData['physicalstatus']) ? $prefrenceData['physicalstatus'] : '');
        $food = (!empty($prefrenceData['food']) ? $prefrenceData['food'] : '');
        $smoking = (!empty($prefrenceData['smoking']) ? $prefrenceData['smoking'] : '');
        $drinking = (!empty($prefrenceData['drinking']) ? $prefrenceData['drinking'] : '');
        $country = (!empty($prefrenceData['country']) ? $prefrenceData['country'] : '');
        $state = (!empty($prefrenceData['state']) ? $prefrenceData['state'] : '');
        $city = (!empty($prefrenceData['city']) ? $prefrenceData['city'] : '');
        $education = (!empty($prefrenceData['education']) ? $prefrenceData['education'] : '');
        $salary = (!empty($prefrenceData['salary']) ? $prefrenceData['salary'] : '');
        $week = (!empty($prefrenceData['week']) ? $prefrenceData['week'] : '');
        $month = (!empty($prefrenceData['month']) ? $prefrenceData['month'] : '');
        $emp_in = (!empty($prefrenceData['employeed_in']) ? $prefrenceData['employeed_in'] : '');
        $profileCreated = (!empty($prefrenceData['created']) ? $prefrenceData['created'] : '');

        $caste = 0;
        if (!empty($casteData) && $casteData != 48 && $casteData != 49) {
            $caste = $casteData;
        }
        /* $res = $this->db->query($query.$query1);

          $this -> db -> select('user.guid AS userGuid')
          ->join('user_role AS role', 'role.user_guid = user.guid')
          ->join('user_profile AS profile', 'profile.user_guid = user.guid')
          ->join('user_other_details AS edu', 'edu.user_guid = user.guid')
          ->join('user_religion_info AS reg', 'reg.user_guid = user.guid')
          ->join('user_food_habbits AS habbit', 'habbit.user_guid = user.guid','left')
          ->where_in('role.role_guid', MEMBER_ROLE_ID);//need to change hard code

          if( !empty( $ageFrom ) &&  !empty( $ageTo ) ){
          $this -> db ->where('profile.age BETWEEN "'. $ageFrom. '" and "'. $ageTo.'"');
          }
          if( $profileCreated ){
          $this -> db ->where('profile.profile_created', $profileCreated);
          }
          if( $maritalstatus ){
          $this -> db ->where('profile.martial_status', $maritalstatus);
          }
          if( $physicalstatus ){
          $this -> db ->where('profile.physical_status', $physicalstatus);
          }
          if( $gender ){
          $this -> db ->where('profile.gender', $gender);
          }
          if( $country ){
          $this -> db ->where('profile.country', $country);
          }
          if( $state ){
          $this -> db ->where('profile.state', $state);
          }
          if( $city ){
          $this -> db ->where('profile.city', $city);
          }
          if( $food ){
          $this -> db ->where('habbit.food', $food);
          }
          if( $drinking ){
          $this -> db ->where('habbit.drink', $drinking);
          }
          if( $smoking ){
          $this -> db ->where('habbit.smoke', $smoking);
          }
          if( $caste ){
          $this -> db ->where('reg.caste', $caste);
          }
          if( $education ){
          $this -> db ->where('edu.education', $education);
          }
          if( $emp_in ){
          $this -> db ->where('edu.employeed_in', $emp_in);
          }
          if( $week ){
          $this -> db ->where('profile.last_updated BETWEEN DATE_SUB( CURDATE() , INTERVAL 1 WEEK ) AND CURDATE()');
          }
          if( $month ){
          $this -> db ->where('profile.last_updated BETWEEN DATE_SUB( CURDATE() , INTERVAL 1 MONTH ) AND CURDATE()');
          }
          if( $salary ){
          $this -> db ->where('edu.salary >=', $salary);
          }

          if( $notUserGuid ){
          $this -> db ->where_not_in('user.guid ', $notUserGuid);
          }

          $this -> db ->where('user.deleted', 0);
          $this -> db ->group_by('user.guid');
          $query = $this -> db -> get('user '); */
        /** get user information query */
        $userquery = $this->User_model->getUserinformationData('', $userRoleguid, $notUserGuid);

        $query1 = '';

        if (!empty($gender)) {
            $query1.= ' AND profile.gender="' . $gender . '"';
        }

        if (!empty($ageFrom) && !empty($ageTo)) {
            $query1.= ' AND profile.age BETWEEN "' . $ageFrom . '" AND "' . $ageTo . '"';
        }
        if ($maritalstatus) {
            $query1.= ' AND profile.martial_status="' . $maritalstatus . '"';
        }
        if ($physicalstatus) {
            $query1.= ' AND profile.physical_status=' . $physicalstatus;
        }
        if ($food) {
            $query1.= ' AND habbit.food = ' . $food;
        }
        if ($smoking) {
            $query1.= ' AND habbit.smoke =' . $smoking;
        }
        if ($drinking) {
            $query1.= ' AND habbit.drink = ' . $drinking;
        }
        if ($country) {
            $query1.= ' AND profile.country = ' . $country;
        }
        if ($state) {
            $query1.= ' AND profile.state = ' . $state;
        }
        if ($city) {
            $query1.= ' AND profile.city = ' . $city;
        }
        if ($education) {
            $query1.= ' AND edu.education =' . $education;
        }
        if ($salary) {
            $query1.= ' AND edu.salary >=' . $salary;
        }
        if ($week) {
            $query1.= ' AND profile.last_updated BETWEEN DATE_SUB( CURDATE() , INTERVAL 1 WEEK ) AND CURDATE()';
        }
        if ($month) {
            $query1.= ' AND profile.last_updated BETWEEN DATE_SUB( CURDATE() , INTERVAL 1 MONTH ) AND CURDATE()';
        }
        if ($emp_in) {
            $query1.= ' AND edu.employeed_in =' . $emp_in;
        }
        if ($profileCreated) {
            $query1.= ' AND profile.profile_created="' . $profileCreated . '"';
        }
        if ($caste) {
            $query1.= ' AND reg.caste =' . $caste;
        }

        /* if( $limits ){
          $query1.= ' AND LIMIT'.$limits;
          }
          if( $dhosam ){
          $query1.= ' AND reg.dhosam='. $dhosam;
          }
          if( $star ){
          $query1.= ' AND reg.star ='. $star;
          }
          if( $qualification ){
          $query1.= ' AND edu.qualification='. $qualification;
          }
          if( $occupation ){
          $query1.= ' AND edu.occupation = '. $occupation;
          }
         */

        $query1.= ' group BY user.guid';
        $query = $this->db->query($userquery . $query1);

// 		echo $this->db->last_query();exit;
        return $query->result_array();
    }

    public function getCountList($gender, $notUserGuid = '', $caste = '') {
        if (empty($gender)) {
            return 0;
        }

        $week['week'] = 1;
        $month['month'] = 1;
        /** maritial status values */
        $marital1['maritalstatus'] = 1;
        $marital2['maritalstatus'] = 2;
        $marital3['maritalstatus'] = 3;
        $marital4['maritalstatus'] = 4;
        /** physical status values */
        $physical1['physicalstatus'] = 1;
        $physical2['physicalstatus'] = 2;
        /** employeed_in values */
        $emp1['employeed_in'] = 1;
        $emp2['employeed_in'] = 2;
        $emp3['employeed_in'] = 3;
        $emp4['employeed_in'] = 4;
        /** education values */
        $edu1['education'] = 1;
        $edu2['education'] = 2;
        $edu3['education'] = 3;
        $edu4['education'] = 4;
        $edu5['education'] = 5;
        /** food values */
        $food1['food'] = 1;
        $food2['food'] = 2;
        $food3['food'] = 3;
        /** smoking values */
        $smoking1['smoking'] = 1;
        $smoking2['smoking'] = 2;
        $smoking3['smoking'] = 3;
        /** drinking values */
        $drinking1['drinking'] = 1;
        $drinking2['drinking'] = 2;
        $drinking3['drinking'] = 3;

        $country['country'] = 101;
        $self['created'] = 'myself';
        //$salary['salary'] 	= 1;

        /** get count list by given data */
        $data['week'] = count($this->getUserSearchDetails($week, $gender, $notUserGuid, $caste));
        $data['month'] = count($this->getUserSearchDetails($month, $gender, $notUserGuid, $caste));
        $data['marital1'] = count($this->getUserSearchDetails($marital1, $gender, $notUserGuid, $caste));
        $data['marital2'] = count($this->getUserSearchDetails($marital2, $gender, $notUserGuid, $caste));
        $data['marital3'] = count($this->getUserSearchDetails($marital3, $gender, $notUserGuid, $caste));
        $data['marital4'] = count($this->getUserSearchDetails($marital4, $gender, $notUserGuid, $caste));
        $data['physical1'] = count($this->getUserSearchDetails($physical1, $gender, $notUserGuid, $caste));
        $data['physical2'] = count($this->getUserSearchDetails($physical2, $gender, $notUserGuid, $caste));
        $data['emp_in1'] = count($this->getUserSearchDetails($emp1, $gender, $notUserGuid, $caste));
        $data['emp_in2'] = count($this->getUserSearchDetails($emp2, $gender, $notUserGuid, $caste));
        $data['emp_in3'] = count($this->getUserSearchDetails($emp3, $gender, $notUserGuid, $caste));
        $data['emp_in4'] = count($this->getUserSearchDetails($emp4, $gender, $notUserGuid, $caste));
        $data['edu1'] = count($this->getUserSearchDetails($edu1, $gender, $notUserGuid, $caste));
        $data['edu2'] = count($this->getUserSearchDetails($edu2, $gender, $notUserGuid, $caste));
        $data['edu3'] = count($this->getUserSearchDetails($edu3, $gender, $notUserGuid, $caste));
        $data['edu4'] = count($this->getUserSearchDetails($edu4, $gender, $notUserGuid, $caste));
        $data['edu5'] = count($this->getUserSearchDetails($edu5, $gender, $notUserGuid, $caste));
        $data['food1'] = count($this->getUserSearchDetails($food1, $gender, $notUserGuid, $caste));
        $data['food2'] = count($this->getUserSearchDetails($food2, $gender, $notUserGuid, $caste));
        $data['food3'] = count($this->getUserSearchDetails($food3, $gender, $notUserGuid, $caste));
        $data['smoking1'] = count($this->getUserSearchDetails($smoking1, $gender, $notUserGuid, $caste));
        $data['smoking2'] = count($this->getUserSearchDetails($smoking2, $gender, $notUserGuid, $caste));
        $data['smoking3'] = count($this->getUserSearchDetails($smoking3, $gender, $notUserGuid, $caste));
        $data['drinking1'] = count($this->getUserSearchDetails($drinking1, $gender, $notUserGuid, $caste));
        $data['drinking2'] = count($this->getUserSearchDetails($drinking2, $gender, $notUserGuid, $caste));
        $data['drinking3'] = count($this->getUserSearchDetails($drinking3, $gender, $notUserGuid, $caste));
// 		$data['country']	= count($this->getUserSearchDetails($country, $gender, $notUserGuid, $caste ));
// 		$data['salary'] 	= count($this->getUserSearchDetails($salary, $gender, $notUserGuid, $caste ));
        $data['self'] = count($this->getUserSearchDetails($self, $gender, $notUserGuid, $caste));
        return $data;
    }

    /**
     * @todo need to change this method dynamically use getUserinformationData function in User_model
     * @param unknown $keyword
     * @param unknown $gender
     * @param string $notUserGuid
     * @param string $userGuidList
     */
    function getUserKeywordSearchDetails($keyword, $gender, $notUserGuid = '', $userGuidList = '', $userRoleguid = MEMBER_ROLE_ID) {
        $result = array();
        if (empty($keyword) || empty($gender)) {
            return $result;
        }

        /*
          $this -> db -> select('user.guid AS userGuid, user.username, user.email, user.id as userId, user.deleted')
          ->select( 'profile.profile_created, profile.mobile, profile.dob, profile.age,profile.martial_status, profile.childs, profile.gender, profile.physical_status, profile.bodytype, profile.complexion, profile.height, profile.weight, profile.country, profile.state, profile.city, profile.last_updated')
          ->select( 'edu.education, edu.qualification, edu.occupation,edu.employeed_in, edu.income, edu.salary')
          ->select( 'reg.caste, reg.sub_caste, reg.religion,reg.mother_tongue, reg.gothram, reg.dhosam, reg.dhosam_type, reg.star, reg.raasi ')
          ->select( 'habbit.food, habbit.drink, habbit.smoke')
          ->select( 'family.summary')
          ->join('user_role AS role', 'role.user_guid = user.guid')
          ->join('user_profile AS profile', 'profile.user_guid = user.guid')
          ->join('user_other_details AS edu', 'edu.user_guid = user.guid')
          ->join('user_religion_info AS reg', 'reg.user_guid = user.guid')
          ->join('user_food_habbits AS habbit', 'habbit.user_guid = user.guid','left')
          ->join('user_family_details AS family', 'family.user_guid = user.guid','left')
          ->where_in('role.role_guid', MEMBER_ROLE_ID);// need to change hard code

          if( $gender ){
          $this -> db ->where('profile.gender', $gender);
          }
          if( $userId ){
          $this -> db ->like('user.id', $userId);
          }
          if( $profileCreated ){
          $this -> db ->or_like('profile.profile_created', $profileCreated);
          }
          if( !empty( $ageFrom ) ){
          $this -> db ->or_like('profile.age', $ageFrom);
          }
          if( $qualification ){
          $this -> db ->or_like('edu.qualification', $qualification);
          }
          if( $occupation ){
          $this -> db ->or_like('edu.occupation', $occupation);
          }

          $this -> db ->order_by('profile.last_updated', "DESC");
          $this -> db ->where('user.deleted', 0);
          $query = $this -> db -> get('user '); */

        /** get user information query */
        $userquery = $this->User_model->getUserinformationData($userGuidList, $userRoleguid, $notUserGuid);

        $query1 = '';

        if (!empty($gender)) {
            $query1.= ' AND profile.gender="' . $gender . '"';
        }
        $query1.= ' AND ( user.id LIKE "%' . $keyword . '%"';
        $query1.= ' OR profile.profile_created LIKE "%' . $keyword . '%"';
        $query1.= ' OR profile.age LIKE "%' . $keyword . '%"';
        $query1.= ' OR edu.qualification LIKE "%' . $keyword . '%"';
        $query1.= ' OR edu.occupation LIKE "%' . $keyword . '%" )';

        $query1.= ' ORDER BY profile.last_updated DESC';
        $query = $this->db->query($userquery . $query1);
// 		echo $this->db->last_query();exit;
        return $query->result_array();
    }

}

?>
