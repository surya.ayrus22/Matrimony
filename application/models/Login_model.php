<?php

Class Login_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function loginValidation( $options, $roleguid) {
        /** Variable creation */
        $userDetail = array();

        /** Input validation */
        if (empty($options['password']) || empty($options['username']))
            return $userDetail;

        $password = md5($options['password']);

        /** Get user detail information based on email id  * */
        $userDetail = $this->getLoginUserCredentialDetail($options, $roleguid);

        /** Check Password are match * */
        if (!empty($userDetail) && $password) {
            if ($userDetail['0']['password'] != $password) {
                $userDetail = array();
            }
        }
        return $userDetail;
    }

    function getLoginUserCredentialDetail($options, $roleguid) {
        $result = array();
        $username = (!empty($options['username'])) ? $options['username'] : '';
        if (empty($username)) {
            return $result;
        }
        $this->db->select('user.guid AS userGuid, user.password, user.username, user.status, user.email, profile.mobile, profile.photo, profile.gender, role.name AS roleName,role.guid AS userRoleGuid')
                ->join('user_profile AS profile', 'profile.user_guid = user.guid')
                ->join('user_role as ur', 'ur.user_guid = user.guid')
                ->join('roles AS role', 'role.guid = ur.role_guid')
                ->where('user.email', $username)
                ->where('ur.role_guid', $roleguid);
        $query = $this->db->get('user');
        return $query->result_array();
    }

    function loginRedirection($userRoleGuid) {
        /** Input validation  */
        if (empty($userRoleGuid)) {
            return false;
        }

        /** variable creation  */
        $path = '';

        switch ($userRoleGuid) {

            case MEMBER_ROLE_ID: {
                    $path = base_url() . 'index.php/matches/view/';
                    return $path;
                    break;
                }
            default: {
                    $path = 'index.php/logout';
                    return $path;
                    break;
                }
        }
    }

    function loginActivityEntry($userGuid, $logId = 1, $deviceType = 'Cloud') {
        $result = 0;
        if (empty($userGuid)) {
            return $result;
        }

        $loginDate = date(DATE_TIME_FORMAT);
        $activitydata = array(
            'activity_log_id' => $logId,
            'user_guid' => $userGuid,
            'client_date' => $loginDate,
            'activity_data1' => 'Login',
            'activity_data2' => 'Login',
            'active' => 1,
            'activity_comment' => 'Logged in Successfully',
            'device_type' => $deviceType,
            'created' => $loginDate,
            'created_by' => $userGuid,
        );
        $data = array('active' => 0);
        $this->db->where('user_guid', $userGuid);
        $result = $this->db->update('activity_log', $data);

        $insert = $this->db->insert('activity_log', $activitydata);
        return $insert;
    }

}

?>
