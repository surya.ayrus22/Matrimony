<?php
/* about us*/
$lang['aboutus_page_step1_text']						= 'Mr.R.Somasundarampillai, the founder Chairman of Madurai Somaeswar Matrimony is fondly identified as the President of Tamilnadu Saiva Vellalar Sangam and Anaithu Vellalar Federation. ';
$lang['aboutus_page_step2_title']						= 'Founder Achievements';
$lang['aboutus_page_step2_title1']						= '1.President';
$lang['aboutus_page_step2_title1_text']					= 'Tourist Taxi Owner’s Association';
$lang['aboutus_page_step2_title2']						= '2.President';
$lang['aboutus_page_step2_title2_text']					= 'Chinna chokkikulam & Periya Chokkikulam Madurai,Welfare Association.';
$lang['aboutus_page_step2_title3']						= '3.Hon. President ';
$lang['aboutus_page_step2_title3_text']					= 'Madurai Automobiles Consultant & Association';
$lang['aboutus_page_step2_title4']						= '4.Executive Member  ';
$lang['aboutus_page_step2_title4_text']					= 'Tamilnadu Chamber of Commerce &Industry, Madurai &Chennai';
$lang['aboutus_page_step2_title5']						= '5.Member  ';
$lang['aboutus_page_step2_title5_text']					= 'Cosmopolitan club, Union Club Madurai';
$lang['aboutus_page_step2_title6']						= '6. Area Commander Home Guards-In Madurai city   ';
$lang['aboutus_page_step2_title6_text']					= '1997 to 2004 Somu Travels Tourist Taxi operators,';
$lang['aboutus_page_step2_title7']						= '7. Proprietor  ';
$lang['aboutus_page_step2_title7_text']					= 'Somu Car’s Second Hand Car Dealer (for the past 30yrs) Dist-Pro- Lion’s Club 324 B-3(International)';
$lang['aboutus_page_step3_text']						= 'We provide Matrimonial, Matchmaking and Marriage Services exclusively for Hindus. Presently, we are proud to state that, we are entering into the service for marriage alliance. We offer FREE Matrimonial Profile Registration and FREE Profile listing. Bride / Groom search is also offered FREE. In our site, we provide matrimonial profile listing, horoscopes and profile photographs. Profile Contact details and horoscopes are provided only to paid users. Thereby, your privacy is 100% assured. However, any user may contact you, if you are a paid user. Madurai Somaeswar matrimony.com is a Prestigious Service from Somu Travels Tourist Taxi Operators, Madurai, Tamilnadu, India being operated from the Temple City (Madurai) of South India. Madurai Somaeswar matrimony.com blends the holiness of traditionally arranged marriages, with the interactivity of Internet technology. ';
/*faq page */
$lang['faq_page_step1_title_q1']						= 'How do I change my email address or password?';
$lang['faq_page_step1_title_a1']						= 'You can change your email address on the "edit your profile" page. Here you can change some other information including search preferences, address and other preferences about your daily routines, habits, hobbies etc. After youve made your changes, click "Save" at the bottom of the page to save.';
$lang['faq_page_step1_title_q2']						= 'How does "My Matches" works?';
$lang['faq_page_step1_title_a2']						= 'My Matches works according to the basic profile and "search preferences" at the bottom of the "edit profile" page.';
$lang['faq_page_step1_title_q3']						= 'Do people actually meet on your site? Do they ever get married?';
$lang['faq_page_step1_title_a3']						= 'Thousands of people meet on our site daily and go on to relationships. Weve also had hundreds of marriages across many borders. To see for yourself, check the site for real stories of real connections.';
$lang['faq_page_step1_title_q4']						= 'When someone emails me, where does the email go and how do I respond?';
$lang['faq_page_step1_title_a4']						= 'When a member sends you a note, it goes straight into your onsite message Inbox. We then send an email to your personal email address to let you know that its there. To read the note, simply login to the site and go to your Inbox. If you decide to write back, simply click "reply", write your own note and send away. Your note will go straight into the members onsite Inbox, and well let them know its there with an email to their personal email address. All communications with other members stay onsite so that you never have to give out any personal information until you feel completely ready. Then when you are, you can exchange phone numbers and even meet in person.';
$lang['faq_page_step1_title_q5']						= 'How do I edit my profile? How do I hide my profile?';
$lang['faq_page_step1_title_a5']						= 'To update your profile, click "Edit Profile" in Members Panel menu. Be sure to save your changes at the bottom of the screen when youre done. Try to put your best foot forward with at least one photo and snappy, detailed essays about your interests. The photo is key since most people feel more comfortable writing to members they can see. Try to stay positive in your essays and let the real you shine. To hide your profile, select "No" for the option "Other members can see when Im online?" on the "Edit Profile" page . Keep in mind, however, that members who are most active on the site keep their profile visible in all places because it ups their chances of connecting with people. And you never know, someone special may be looking for you and never get to find you if your profile is hidden.';
$lang['faq_page_step1_title_q6']						= 'When I suspend my membership, does it end my subscription? Can I put my subscription on hold?';
$lang['faq_page_step1_title_a6']						= 'No. Once you purchase a subscription, it will remain active for the remainder of your initial term.';
/*contact us*/
$lang['contact_title']									='OUR ADDRESS';
$lang['contact_name']									='Mr.R.Somasundarampillai';
$lang['contact_address']								='Alagappan Flats';
$lang['contact_address_1']								='#43, Hakeem Ajmal khan Road,';
$lang['contact_address_2']								='Chinna Chokkikulam, ';
$lang['contact_address_3']								='Madurai - 625 002, ';
$lang['contact_address_4']								='Tamil Nadu,India. ';
$lang['contact_mobile']									='Ph: +91  452 - 2530246, 2523123, 2530412 ';
$lang['contact_photo']									='Cell:+91-96552-22970 ';

/* Register page home*/
$lang['register_profile_for_title1']					= 'Register FREE!';
$lang['register_profile_for_profile1']					= 'Profile for';
$lang['register_profile_for_name']						= 'Name';
$lang['register_profile_for_male']						='Male';
$lang['register_profile_for_female']					='Female';
$lang['register_profile_for_gender']					= 'Gender';
$lang['register_profile_for_dob']						= 'Date of birth';
$lang['register_profile_for_religion']					= 'Religion';
$lang['register_profile_for_tongue']					= 'Mother Tongue';
$lang['register_profile_for_email']						= 'Email ';
$lang['register_profile_for_password']					= 'Password ';
$lang['register_profile_for_mobile']					= 'Mobile Number ';
$lang['register_profile_for_reg_now']					= 'Register Now ';
$lang['home_page_step2_title']							= 'Find the Special Some One';
$lang['home_page_groom_text']							='Groom';
$lang['home_page_bride_text']							='Bride';
$lang['home_page_find_matches']							='Find Matches';
$lang['home_page_step3_title']							= 'Guest Messages';

/* register step1*/
 $lang['register_profile_for_process1']					= 'Register';
 $lang['register_profile_for_process2']					= 'Personal Information';
 $lang['register_profile_for_process3']					= 'Photo add';
 $lang['register_profile_for_process4']					= 'Partner perference';
 $lang['register_profile_for_process5']					= 'Completed';
 $lang['register_profile_for_marriagestatus']			= 'Marital Status';
 $lang['register_profile_for_marriagestatus_1']			= 'Never Married ';
 $lang['register_profile_for_marriagestatus_2']			= 'Widowed ';
 $lang['register_profile_for_marriagestatus_3']			= 'Divorced ';
 $lang['register_profile_for_marriagestatus_4']			= 'Awaiting divorce';
 $lang['register_profile_for_caste']					= 'Caste';
 $lang['register_profile_for_subcaste']					= 'Sub Caste';
 $lang['register_profile_for_gothram']					= 'Gothram';
 $lang['register_profile_for_location_title1']			= 'Location Details';
 $lang['register_profile_for_country']					= 'Country living in ';
 $lang['register_profile_for_state']					= 'State living in';
 $lang['register_profile_for_city']						= 'City living in ';
 $lang['register_profile_for_physical_title1']			= 'Physical Attributes';
 $lang['register_profile_for_height']					= 'Height';
 $lang['register_profile_for_weight']					= 'Weight';
 $lang['register_profile_for_bodytype']					= 'Body type '; 
 $lang['register_profile_for_bodytype_1']				= 'Slim'; 
 $lang['register_profile_for_bodytype_2']				= 'Average'; 
 $lang['register_profile_for_bodytype_3']				= 'Athletic'; 
 $lang['register_profile_for_bodytype_4']				= 'Heavy '; 
 $lang['register_profile_for_complexion']				= 'Complexion';
 $lang['register_profile_for_complexion_1']				= 'Very Fair ';
 $lang['register_profile_for_complexion_2']				= 'Fair ';
 $lang['register_profile_for_complexion_3']				= 'Wheatish ';
 $lang['register_profile_for_complexion_4']				= 'Dark ';
 $lang['register_profile_for_physicalstatus']			= 'Physical Status';
 $lang['register_profile_for_physicalstatus_1']			= 'Normal ';
 $lang['register_profile_for_physicalstatus_2']			= 'Physically challenged ';
 $lang['register_profile_for_education_title1']			= 'Education & Occupation';
 $lang['register_profile_for_highesteducation']			= 'Highest Education';
 $lang['register_profile_for_educationqualification']	= 'Education Qualification ';
 $lang['register_profile_for_occupation']				= 'Occupation  ';
 $lang['register_profile_for_employeed']				= 'Employeed in  ';
 $lang['register_profile_for_employeed_1']				= 'Government ';
 $lang['register_profile_for_employeed_2']				= 'Self Employeed   ';
 $lang['register_profile_for_employeed_3']				= 'Businees   ';
 $lang['register_profile_for_employeed_4']				= 'Private  ';
 $lang['register_profile_for_salary']					= 'Annual Salary ';
 $lang['register_profile_for_habbit_title1']			= 'Habbits';
 $lang['register_profile_for_food']						= 'Food  ';
 $lang['register_profile_for_food_1']					= 'Vegetarian   ';
 $lang['register_profile_for_food_2']					= 'Non-Vegetarian   ';
 $lang['register_profile_for_food_3']					= 'Eggetarian   ';
 $lang['register_profile_for_yes']						= 'yes  ';
 $lang['register_profile_for_no']						= 'No  ';
 $lang['register_profile_for_smoking']					= 'Smoking  ';
 $lang['register_profile_for_smoking_3']				= 'Occasionally ';
 $lang['register_profile_for_drinking']					= 'Drinking  ';
 $lang['register_profile_for_drinking_3']				= 'Drinks Socially   ';
 $lang['register_profile_for_astrological_title1']		= 'Astrological Info';
 $lang['register_profile_for_dhosam']					= 'Have a Dhosam?  ';
 $lang['register_profile_for_dhosam_3']					= 'Dont know ';
 $lang['register_profile_for_star']						= 'Star  ';
 $lang['register_profile_for_raasi']					= 'Raasi  ';
 $lang['register_profile_for_family_title1']			= 'Family Details';
 $lang['register_profile_for_familystatus']				= 'Family Status   ';
 $lang['register_profile_for_familystatus_1']			= 'Middle class   ';
 $lang['register_profile_for_familystatus_2']			= 'Upper middle class    ';
 $lang['register_profile_for_familystatus_3']			= 'Rich   ';
 $lang['register_profile_for_familystatus_4']			= 'Affluent   ';
 $lang['register_profile_for_familytype']				= 'Family Type   ';
 $lang['register_profile_for_familytype_1']				= 'Joint   ';
 $lang['register_profile_for_familytype_2']				= 'Nuclear   ';
 $lang['register_profile_for_familyvalue']				= 'Family Values  ';
 $lang['register_profile_for_familyvalue_1']			= 'Orthodox  ';
 $lang['register_profile_for_familyvalue_2']			= 'Traditional  ';
 $lang['register_profile_for_familyvalue_3']			= 'Moderate   ';
 $lang['register_profile_for_familyvalue_4']			= 'Liberal   ';
 $lang['register_profile_for_something_title1']			= 'Something about you';
 $lang['register_profile_for_submit']					= 'Submit';
/*photo add */
 $lang['register_profile_for_next']						= 'Next';
/*partner*/
 $lang['register_profile_for_age']						= 'Preferred Age ';
 $lang['search_profile_for_age']						= 'Age ';
$lang['qualification_text']								='Qualification';
/*search page*/
 $lang['search_profile_for_keysearch_text']				= 'Find profiles based on keywords. If youre looking for very specific results ';
/*payment option */
 $lang['payment_text']									= 'Your account going to expired please join premium member ';
 $lang['payment']										='Payment';
 /* page */
  $lang['inbox_text']									='Inbox';
 $lang['send_text']										='Send';
 $lang['view_all']										='View all';
 $lang['profile_text']									='Profile %s';
 $lang['profile_id']									='ID';
 $lang['by_text']										='by';
 $lang['for_text']										='for';
 $lang['to_text']										='To %s';
 $lang['month_text']									='Month';
 $lang['week_text']										='Week';
 $lang['last_text']										='Last %s';
 $lang['more_text']										='More %s';
 $lang['search_education_title']						='Education';
 $lang['location_text']									='Location';
 $lang['close_text']									='Close';
 $lang['edit_text']										='Edit';
 $lang['save_text']										='Save';
 $lang['from_text']										='From';
 $lang['to_text']										='To';

 /* Inbox */
 $lang['all_new']										=	'All & New';
 $lang['moreconversation']								=	'more conversations';

/* Convertation */
$lang['text_other_converstation']						=	'Other Conversations with this member';
$lang['text_modal_to_send_mail']						=	'To Send Mail';
$lang['conversation_not']								='Conversation not found';
$lang['received']										='Received On';
$lang['conversation_no']								='No Conversation';
$lang['home_page_service']								='Exclutive Matchmaking service for the matrimony';
$lang['home_page_contact']								='Contact Us';
$lang['mr_text']										='Mr';
$lang['mrs_text']										='Mrs';
$lang['forgot_your_password']							='Forgot your password?';
$lang['reset_password']									='Reset Password';
$lang['forget_password']								='Forgot Password? ';
$lang['footer_text']									='© 2016 Matrimonial . All Rights Reserved  | Design by';
$lang['kevell_text']									='KevellCorp';
$lang['payment_offer_text']								='Payment offer';
$lang['take_offer_text']								='Take the offer';
$lang['limited_time_text']								='Limited Time Offer!';
$lang['offer_content']									='take off on magical journey for love';
$lang['offer_text']										='Offer';
$lang['edit_profile']									=	'Edit Profile';
$lang['view_similar_profile']							='View Similar Profiles';
$lang['mamber_who_viewed_this_profile_also_viewed']		='Members who viewed this profile also viewed';
$lang['click_here']										='click here';
$lang['verify_image']									=	'Verify Image';

$lang['unpaid-users']	=	'Un Paid Users';


?>
