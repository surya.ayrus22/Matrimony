<?php
$lang['header_home']		= 'Home';
$lang['header_about']		= 'About us';
$lang['header_matches']		= 'Matches';
$lang['header_matches_1']	= 'New matches';
$lang['header_matches_2']	= 'Who Viewed my profile';
$lang['header_matches_3']	= 'Profile I Viewed ';
$lang['header_matches_4']	= ' Shortlisted Profile ';
$lang['header_matches_5']	= 'Profile I have Ignore';
$lang['header_matches_6']	= 'Mutual Matches';
$lang['header_message']		= 'Message';
$lang['header_message_1']	= 'Inbox';
$lang['header_message_2']	= 'Send';
$lang['header_search']		= 'Search';
$lang['header_upgrade']		= 'Upgrade';
$lang['header_payment']		= 'Payment';
$lang['header_contact']		= 'Contact Us';
$lang['header_fqas']		= 'FAQs';
$lang['edit_profile']		= 'Edit profile';
$lang['edit_photo']			= 'Edit Photo';
$lang['setting']			= 'Setting';
$lang['logout']				= 'Logout';

/** Admin **/
$lang['settings']			= 'Settings';
$lang['dashboard']			= 'Dashboard';
$lang['user_details'] 		= 'User Details';
$lang['useradd']			= 'User Add';
$lang['users']				= 'Users';
$lang['male_user']			= 'Male User';
$lang['female_user']		= 'Female User';
$lang['premium_user']		= 'Premium User';
$lang['active_user']		= 'Active User';
$lang['deactive_user']		= 'Deactive User';
$lang['deleted_user']		= 'Deleted User';
$lang['profile_admin']		= 'Profile'; 
$lang['profile_approved']	= 'Profile Approved';
$lang['success_profile']	= 'Success Profile';
$lang['premium_member']		= 'Premium Member';
$lang['platinum']			= 'Platinum';
$lang['premium']			= 'Premium';
$lang['gold']				= 'Gold';
$lang['curreny_convertion'] = 'Currency Convertion';
$lang['incomplete_user']	= 'Incomplete Users';
$lang['free_user']	= 'Free Users';







	
?>
